#pragma once

constexpr int MAX_ID_LEN = 16;
constexpr int MAX_CHAT_LEN = 255;


enum ENUMOP{ OP_RECV,OP_SEND,OP_ACCEPT };

enum C_STATUS{ ST_FREE, ST_ALLOC, ST_ACTIVE_LOBBY ,ST_ACTIVE_SEARCH, ST_ACTIVE_GAME, ST_ACTIVE_RESULT};

enum CH_ROLE {R_NORMAL, R_WITCH};


#define WORLD_WIDTH		128
#define WORLD_HEIGHT	128

#define SERVER_PORT 9000
// 월드 인포
// 오브젝트들, 만들고 지우고
// 업데이트 해주고
// 

//------------- C2S ---------------//
enum C2S {

 C2S_LOGIN,	
 C2S_MOVE,
 C2S_READY,
 C2S_RESULT,

	// C2S_MOVE
	// C2S_SKILL
	// C2S_JUMP
	// C2S_ATTACK
};

//------------- S2C ---------------//
enum S2C {

	S2C_LOGIN_OK,
	S2C_ENTER,
	S2C_LEAVE,
	S2C_INPUT,
	S2C_RESULT,

};



enum PlayerInput {
	D_UP,
	D_DOWN,
	D_LEFT,
	D_RIGHT,
	D_RUN,
	D_JUMP,
	D_SKILL1,
	D_SKILL2,
	D_ATTACK,

};



#pragma pack (push,1)

//---------sc------------//
struct SC_PacketLoginOk {
	char size;
	char type;
	int id;
	
};

struct SC_PacketInput {
	char size;
	char type;
	int id;
	short x, y;
};


struct SC_PacketReady {
	char size;
	char type;
	int id;
};

struct SC_PacketLobby {
	char size;
	char type;
	int id;
	bool is_ready;
};

struct SC_PacketResult {
	char size;
	char type;
	int id;

};


constexpr unsigned char O_PLAYER = 0;
constexpr unsigned char O_OBJECT = 1;


struct SC_PacketEnter {
	char size;
	char type;
	int id;
	char name[MAX_ID_LEN];
	char o_type;
	short x, y;
	char room;
	char role;
};


struct SC_PacketLeave {
	char size;
	char type;
	int id;
};


//-----------cs-----------//
struct CS_PacketLogin {
	char size;
	char type;
	char name[MAX_ID_LEN];
};


struct CS_PacketInput {
	char size;
	char type;
	char input;
};

struct CS_PacketReady {
	char size;
	char type;
	bool is_ready;
};




#pragma pack (pop)


void sendPacket(int user_id, void* p);
void sendLoginOkPacket(int user_id);
void sendMovePacket(int user_id, int mover);
void doMove(int user_id, int direction,C_STATUS flag);
void sendEnterPacket(int user_id, int o_id);
void enterGame(int user_id, char name[]);
void enterLobby(int user_id, char name[]);
void searchGame(int user_id, bool flag);
void processPakcet(int user_id, char* buf);
void initialzeClients();
void sendLeavePacket(int user_id, int o_id);
void disconnect(int user_id);
void recvPacketConstruct(int user_id, DWORD io_byte);
void workerThread();


void storeAndSend();
void updatedAndStore();