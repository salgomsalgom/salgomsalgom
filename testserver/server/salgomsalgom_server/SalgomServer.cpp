#include <iostream>
#include <WS2tcpip.h>
#include <MSWSock.h>
#include <vector>
#include <thread>
#include <mutex>
#include <chrono>
#include <unordered_set>
#pragma comment (lib, "WS2_32.lib")
#pragma comment (lib, "mswsock.lib")

using namespace std;

#include "Protocol.h"

constexpr auto MAX_PACKET_SIZE = 255;
constexpr auto MAX_BUFFER_SIZE = 1024;
constexpr auto MAX_USER = 10;
constexpr auto MAX_ROOM_USER = 3;
constexpr auto MAX_ROOM_NUMBER = 10;
constexpr auto VIEW_RADIUS = 6;

struct EXOVER {
	WSAOVERLAPPED	over;
	ENUMOP			op;
	char			io_buf[MAX_BUFFER_SIZE];
	union
	{
		WSABUF			wsabuf;
		SOCKET			c_socket;
	};
};

struct CLIENT {
	mutex m_cl;
	SOCKET m_s;
	int m_id;
	EXOVER m_recv_over;
	int m_prev_size;
	char m_packe_buf[MAX_PACKET_SIZE];
	C_STATUS m_status;
	char m_game_room;
	CH_ROLE m_role;

	short x, y;
	char m_name[MAX_ID_LEN + 1];

	unordered_set <int> m_view_list;
};

struct GAME_ROOM {
	int m_p_num;
	bool m_occupied;
	int m_clients_id[4];
};

GAME_ROOM g_GameRoom[MAX_ROOM_NUMBER];
CLIENT g_Clients[MAX_USER];
HANDLE g_iocp;
SOCKET g_ListenSocket;
chrono::system_clock::time_point g_ClockStart;
chrono::system_clock::time_point g_ClockEnd;


/*

존 처리 O

남은 일 
시야 처리랑 
모든 오브젝트 업데이트
맵 정보 가지고 있어야함


*/
void sendPacket(int user_id, void* p)
{

	//프레임 시간 여기서 계산
	char* buf = reinterpret_cast<char*> (p);
	CLIENT& u = g_Clients[user_id];
	EXOVER* exover = new EXOVER;
	exover->op = OP_SEND;
	ZeroMemory(&exover->over, sizeof(exover->over));
	exover->wsabuf.buf = exover->io_buf;
	exover->wsabuf.len = buf[0];
	memcpy(exover->io_buf, buf, buf[0]);
	Sleep(16);
	WSASend(u.m_s, &exover->wsabuf, 1, NULL, 0, &exover->over, NULL);
}

void sendLoginOkPacket(int user_id)
{
	SC_PacketLoginOk p;
	p.size = sizeof(p);
	p.type = S2C_LOGIN_OK;
	p.id = user_id;
	
	sendPacket(user_id, &p);
}


void sendMovePacket(int user_id, int mover)
{
	SC_PacketInput p;
	p.id = mover;
	p.size = sizeof(p);
	p.type = S2C_INPUT;
	p.x = g_Clients[mover].x;
	p.y = g_Clients[mover].y;

	g_Clients[user_id].m_cl.lock();
	g_Clients[user_id].m_view_list.insert(mover);
	g_Clients[user_id].m_cl.unlock();


	sendPacket(user_id, &p);
}

bool isNear(int a, int b)
{
	if (sqrt(pow(g_Clients[a].x - g_Clients[b].x, 2) + pow(g_Clients[a].y - g_Clients[b].y, 2)) > VIEW_RADIUS) return false;
	return true;
}

void doMove(int user_id, int direction, C_STATUS flag , int room_num)
{
	// 여기 시간 재고
	if (flag != ST_ACTIVE_GAME)return;
	CLIENT& u = g_Clients[user_id];
	int x = u.x;
	int y = u.y;
	switch (direction) {
	case D_UP:
		if (y > 0) y--;
		break;
	case D_DOWN:
		if (y < WORLD_HEIGHT - 1) y++;
		break;
	case D_LEFT:
		if (x > 0) x--;
		break;
	case D_RIGHT:
		if (x < WORLD_WIDTH - 1) x++;
		break;
	default:
		cout << "Unkown Direction\n";
		DebugBreak();
		exit(-1);
	}
	u.x = x;
	u.y = y;


	//시야처리
	g_Clients[user_id].m_cl.lock();
	unordered_set <int> old_view_list = g_Clients[user_id].m_view_list;
	g_Clients[user_id].m_cl.unlock();
	unordered_set <int> new_view_list;

	//Zone으로 나눔
	GAME_ROOM& grm = g_GameRoom[room_num];
	for (int i = 0; i < MAX_ROOM_USER; ++i) {
		if (grm.m_clients_id[i] == user_id)continue;
		if (false == isNear(grm.m_clients_id[i], user_id))continue;
		new_view_list.insert(grm.m_clients_id[i]);
	}

	
	for (auto& np : new_view_list) { 
		if (0 == old_view_list.count(np)) { // 새로 시야에 들어 왔을 때
			sendEnterPacket(user_id, np);
			
			g_Clients[user_id].m_cl.lock();
			if (0 == g_Clients[np].m_view_list.count(user_id)) {
				g_Clients[user_id].m_cl.unlock();
				sendEnterPacket(np, user_id);
			}
			else {
				g_Clients[user_id].m_cl.unlock();
				sendMovePacket(np, user_id);
			}
		
		}
		else { // 계속 시야에 있을 때
			g_Clients[np].m_cl.lock();
			if (0 != g_Clients[np].m_view_list.count(user_id)) {
				g_Clients[np].m_cl.unlock();
				sendMovePacket(np, user_id);

			}
			else {
				g_Clients[np].m_cl.unlock();
				sendEnterPacket(np, user_id);
			}
		}     
	}


	
	for (auto& old_p : old_view_list) { // 시야에서 벗어 났을 때
		if (0 == new_view_list.count(old_p)) {
			sendLeavePacket(user_id, old_p);
		}
		g_Clients[old_p].m_cl.lock();
		if (0 != g_Clients[old_p].m_view_list.count(user_id)) {
			g_Clients[old_p].m_cl.unlock();
			sendLeavePacket(old_p, user_id);
		}
		else {
			g_Clients[old_p].m_cl.unlock();
		}

	}


	//// 여기서 같은 존에게만 send
	//for (int i = 0; i < MAX_ROOM_USER; ++i) {
	//	sendMovePacket(grm.m_clients_id[i], user_id);
	//}
}

void sendEnterPacket(int user_id, int o_id)
{
	SC_PacketEnter p;
	p.id = o_id;
	p.size = sizeof(p);
	p.type = S2C_ENTER;
	p.x = g_Clients[o_id].x;
	p.y = g_Clients[o_id].y;
	strcpy_s(p.name, g_Clients[o_id].m_name);
	p.o_type = O_PLAYER;
	p.room = g_Clients[o_id].m_game_room;
	p.role = g_Clients[o_id].m_role;

	sendPacket(user_id, &p);
}

int g_ClientsCount;
void enterGame(int user_id, char name[])
{
	g_Clients[user_id].m_cl.lock();
	strcpy_s(g_Clients[user_id].m_name, name);
	g_Clients[user_id].m_name[MAX_ID_LEN] = NULL;

	for (int i = 0; i < MAX_ROOM_NUMBER; ++i) {
		if (!g_GameRoom[i].m_occupied) {
			for(int j=g_ClientsCount; j < MAX_ROOM_USER;++j)
				g_GameRoom[i].m_clients_id[j] = g_Clients[user_id].m_id;
			g_ClientsCount++;
			g_GameRoom[i].m_p_num++;
			g_Clients[user_id].m_game_room = i;
			if (g_GameRoom[i].m_p_num == MAX_ROOM_USER) {
				g_Clients[user_id].m_role = R_WITCH;
				g_GameRoom[i].m_occupied = true;
				g_ClientsCount = 0;
			}	//게임 끝날 때 플레이어와 같은 룸 번호 헤제
			break;
		}
	}

	sendEnterPacket(user_id, user_id);

	for (int i = 0; i < MAX_USER; ++i) {
		if (user_id == i)continue;
		g_Clients[i].m_cl.lock();
		if (g_Clients[i].m_status == ST_ACTIVE_GAME && g_Clients[i].m_game_room == g_Clients[user_id].m_game_room )
			if (user_id != i) {
				sendEnterPacket(user_id, i);
				sendEnterPacket(i, user_id);
			}
		g_Clients[i].m_cl.unlock();
	}
	g_Clients[user_id].m_status = ST_ACTIVE_GAME;
	g_Clients[user_id].m_cl.unlock();


}


void enterLobby(int user_id, char name[])
{
	g_Clients[user_id].m_cl.lock();
	strcpy_s(g_Clients[user_id].m_name, name);
	g_Clients[user_id].m_name[MAX_ID_LEN] = NULL;
	sendLoginOkPacket(user_id);


	g_Clients[user_id].m_status = ST_ACTIVE_LOBBY;
	g_Clients[user_id].m_cl.unlock();


}


void searchGame(int user_id, bool flag) {

	int ready_player[MAX_ROOM_USER]{};
	int ready_player_count{};
	if (flag) {
		g_Clients[user_id].m_cl.lock();
		g_Clients[user_id].m_status = ST_ACTIVE_SEARCH;
		g_Clients[user_id].m_cl.unlock();
	}
	else {
		g_Clients[user_id].m_cl.lock();
		g_Clients[user_id].m_status = ST_ACTIVE_LOBBY;
		g_Clients[user_id].m_cl.unlock();
		return;
	}
	for (auto& cl : g_Clients) {
		if (cl.m_status == ST_ACTIVE_SEARCH) {
			ready_player[ready_player_count] = cl.m_id;
			ready_player_count++;
		}

		if (ready_player_count == MAX_ROOM_USER) {  // 사람 수 다 차면 게임 시작
			for (int i = 0; i < ready_player_count; ++i) {
				for (auto& cl : g_Clients) {
					if (cl.m_id == ready_player[i]) {
						enterGame(cl.m_id, cl.m_name);
						break;
					}
				}
			}
			ready_player_count = 0;	
		}

	}
}

void processPakcet(int user_id, char* buf)
{
	switch (buf[1]) {
	case C2S_LOGIN: {
		CS_PacketLogin* packet = reinterpret_cast<CS_PacketLogin*>(buf);

		enterLobby(user_id, packet->name);

		break;
	}
	case C2S_MOVE: {
		CS_PacketInput* packet = reinterpret_cast<CS_PacketInput*>(buf);
		doMove(user_id, packet->input, g_Clients[user_id].m_status,g_Clients[user_id].m_game_room);

		break;
	}
	
	case C2S_READY:
	{
		CS_PacketReady* packet = reinterpret_cast<CS_PacketReady*>(buf);
		searchGame(user_id, packet->is_ready);

		break;
	}

	case C2S_RESULT:
	{
		SC_PacketResult* packet = reinterpret_cast<SC_PacketResult*>(buf);
		//endGame(user_id);
		break;
	}
	default:
		cout << "unknown packet type error\n";
		DebugBreak();
		exit(-1);
	}
}

void initialzeClients() {
	for (int i = 0; i < MAX_USER; ++i) {
		g_Clients[i].m_id = i;
		g_Clients[i].m_role = R_NORMAL;
		g_Clients[i].m_status = ST_FREE;
		g_Clients[i].m_game_room = -999;
	}
}

void sendLeavePacket(int user_id, int o_id)
{
	SC_PacketLeave p;
	p.id = o_id;
	p.size = sizeof(p);
	p.type = S2C_LEAVE;

	g_Clients[user_id].m_cl.lock();
	g_Clients[user_id].m_view_list.erase(o_id);
	g_Clients[user_id].m_cl.unlock();
	

	sendPacket(user_id, &p);
}

void disconnect(int user_id)
{
	sendLeavePacket(user_id, user_id);
	g_Clients[user_id].m_cl.lock();
	g_Clients[user_id].m_status = ST_ALLOC;
	closesocket(g_Clients[user_id].m_s);
	for (auto& cl : g_Clients) {
		if (cl.m_id == user_id) continue;
		//cl.m_cl.lock();
		if (ST_ACTIVE_GAME == g_Clients[cl.m_id].m_status)
			sendLeavePacket(cl.m_id, user_id);
		//cl.m_cl.unlock();
	}
	
	g_Clients[user_id].m_status = ST_FREE;	
	g_Clients[user_id].m_cl.unlock();
	cout << "disconnected\n";
}

void recvPacketConstruct(int user_id, DWORD io_byte)
{
	CLIENT& cu = g_Clients[user_id];
	EXOVER& r_o = cu.m_recv_over;

	int rest_byte = io_byte;
	char* p = r_o.io_buf;

	int packet_size = 0;
	if (0 != cu.m_prev_size) packet_size = cu.m_packe_buf[0];

	while (rest_byte > 0) {
		if (0 == packet_size) packet_size = *p;
		if (packet_size <= rest_byte + cu.m_prev_size) {
			memcpy(cu.m_packe_buf + cu.m_prev_size, p, packet_size - cu.m_prev_size);
			p += packet_size - cu.m_prev_size;
			rest_byte -= packet_size - cu.m_prev_size;
			packet_size = 0;
			processPakcet(user_id, cu.m_packe_buf);
			cu.m_prev_size = 0;

		}
		else {
			memcpy(cu.m_packe_buf + cu.m_prev_size, p, rest_byte);
			cu.m_prev_size += rest_byte;
			rest_byte = 0;
			p += rest_byte;
			}
	}
}




void workerThread() {

	while (true) {

		DWORD io_byte;
		ULONG_PTR key;
		WSAOVERLAPPED* over;
		GetQueuedCompletionStatus(g_iocp, &io_byte, &key, &over, INFINITE); //0.016 초 안에 안오면 리턴 update
		EXOVER* exover = reinterpret_cast<EXOVER*> (over);
		int user_id = static_cast<int>(key);
		CLIENT& cl = g_Clients[user_id];
		g_ClockStart = chrono::system_clock::now();


		switch (exover->op) {
		case OP_RECV: {
			if (0 == io_byte)
				disconnect(user_id);
			else {
				recvPacketConstruct(user_id, io_byte);
				ZeroMemory(&cl.m_recv_over.over, sizeof(cl.m_recv_over.over));
				DWORD flags = 0;
				cout << "recv\n";
				WSARecv(cl.m_s, &cl.m_recv_over.wsabuf, 1, NULL, &flags, &cl.m_recv_over.over, NULL);
			}
					break;
		}
		case OP_SEND:
			cout << "send\n";
			if (0 == io_byte)
				disconnect(user_id);
			delete exover;

			break;


		case OP_ACCEPT: {
			cout << "accept\n";
			int user_id = -1;
			for (int i = 0; i < MAX_USER; ++i) {
				lock_guard<mutex> gl{ g_Clients[i].m_cl };
				if (ST_FREE == g_Clients[i].m_status) {
					g_Clients[i].m_status = ST_ALLOC;
					user_id = i;

					break;
				}
			}

			SOCKET c_socket = exover->c_socket;
			if (-1 == user_id)
				closesocket(c_socket);
			else {

				CreateIoCompletionPort(reinterpret_cast<HANDLE>(c_socket), g_iocp, user_id, 0);
				CLIENT& nc = g_Clients[user_id];

				nc.m_prev_size = 0;
				nc.m_recv_over.op = OP_RECV;
				ZeroMemory(&nc.m_recv_over.over, sizeof(nc.m_recv_over.over));
				nc.m_recv_over.wsabuf.buf = nc.m_recv_over.io_buf;
				nc.m_recv_over.wsabuf.len = MAX_BUFFER_SIZE;
				nc.m_s = c_socket;
				nc.x = rand() % WORLD_WIDTH;
				nc.y = rand() % WORLD_HEIGHT;
				DWORD flags = 0;
				WSARecv(nc.m_s, &nc.m_recv_over.wsabuf, 1, NULL, &flags, &nc.m_recv_over.over, NULL);
			}
			c_socket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
			exover->c_socket = c_socket;
			ZeroMemory(&exover->over, sizeof(exover->over));
			AcceptEx(g_ListenSocket, c_socket, exover->io_buf, NULL, sizeof(sockaddr_in) + 16, sizeof(sockaddr_in) + 16, NULL, &exover->over);

		}
					  break;
		/*case OP_UPDATE:
			
			break;
		*/
		default:
			cout << "unknown OP\n";
		
		}
	}
}



int main()
{

	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 2), &WSAData);

	g_ListenSocket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);

	SOCKADDR_IN s_address;
	memset(&s_address, 0, sizeof(s_address));
	s_address.sin_family = AF_INET;
	s_address.sin_port = htons(SERVER_PORT);
	s_address.sin_addr.S_un.S_addr = htonl(INADDR_ANY);
	::bind(g_ListenSocket, reinterpret_cast<sockaddr*>(&s_address), sizeof(s_address));

	listen(g_ListenSocket, SOMAXCONN);

	g_iocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, 0);

	initialzeClients();

	CreateIoCompletionPort(reinterpret_cast<HANDLE> (g_ListenSocket), g_iocp, 999, 0);
	SOCKET c_socket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	EXOVER accept_over;
	ZeroMemory(&accept_over.over, sizeof(accept_over.over));
	accept_over.op = OP_ACCEPT;
	accept_over.c_socket = c_socket;
	AcceptEx(g_ListenSocket, c_socket, accept_over.io_buf, NULL, sizeof(sockaddr_in) + 16, sizeof(sockaddr_in) + 16, NULL, &accept_over.over);

	vector <thread> worker_threads;

	for (int i = 0; i < 4; ++i) worker_threads.emplace_back(workerThread);


	for (auto& th : worker_threads) th.join();
}