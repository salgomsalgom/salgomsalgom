#pragma once

namespace Math {

	class FloatVector2
	{
	public:
		FloatVector2() : x(0.f), y(0.f) { }
		FloatVector2(float x, float y) : x(x), y(y) { }
		
	public:
		bool operator==(const FloatVector2& rhs)
		{
			return (x == rhs.x) && (y == rhs.y);
		}

	public:
		float x;
		float y;
	};

	class FloatVector3
	{
	public:
		FloatVector3() : x(0.f), y(0.f), z(0.f) { }
		FloatVector3(float x, float y, float z) : x(x), y(y), z(z) { }

	public:
		bool operator==(const FloatVector3& rhs)
		{
			return (x == rhs.x) && (y == rhs.y) && (z == rhs.z);
		}

	public:
		float x;
		float y;
		float z;
	};

	class FloatVector4
	{
	public:
		float x;
		float y;
		float z;
		float w;
	};
}