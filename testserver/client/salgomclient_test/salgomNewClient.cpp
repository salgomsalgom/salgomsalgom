#include "../../../SalgomSalgom/SalgomSalgomGameServer/pch.h"
#include <Windows.h>
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
HINSTANCE g_hInst;
LPSTR lpszClass = "KeyDown";

constexpr auto BUF_SIZE = 1024;
using namespace std;


int g_Myid;

SOCKET g_Sock;
mutex g_Lock;

bool g_wKeyFlag;
bool g_aKeyFlag;
bool g_sKeyFlag;
bool g_dKeyFlag;
bool g_updatePacketRecv;
string g_output;


int recvn(SOCKET s, char* buf, int len, int flags) {
	int received;
	short* ptr = (short*)buf;
	int left = len;

	while (left > 0) {
		received = recv(s, (char*)ptr, left, flags);
		if (received == SOCKET_ERROR)
			return SOCKET_ERROR;
		else if (received == 0)
			break;
		else if (ptr[0] < len) {
			return ptr[0];
		}

		left -= received;
		ptr += received;
	}

	return (len - left);
}


void errorQuit(char* msg)
{
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL,
		WSAGetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPSTR)&lpMsgBuf, 0, NULL);
	MessageBox(NULL, (LPCTSTR)lpMsgBuf, msg, MB_ICONERROR);
	LocalFree(lpMsgBuf);
	exit(1);

}


void errorDisplay(char* msg) {
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, WSAGetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	printf("[%s] %s", msg, (char*)lpMsgBuf);
	LocalFree(lpMsgBuf);
}

void sendPacket(void* packet) {

	WSABUF send_wsabuf[1];
	DWORD flags = 0;
	DWORD send_bytes{};
	char buf[MAX_BUFFER_SIZE]{};
	char* p = reinterpret_cast<char*>(packet);
	send_wsabuf[0].buf = buf;
	send_wsabuf[0].len = p[0];
	memcpy(&buf, p, p[0]);
	WSASend(g_Sock, send_wsabuf, 1, &send_bytes, 0, 0, 0);
	if (send_bytes == SOCKET_ERROR) {
		//display_error("send", WSAGetLastError());
		return;
	}
}

void sendInputPacket(PlayerInput input)
{
	CS_PacketInput p;
	p.size = sizeof(p);
	p.type = C2S_INPUT;
	p.input = input;


	sendPacket(&p);
}

void sendReadyPacket(bool flag)
{
	CS_PacketReady p;
	p.size = sizeof(p);
	p.type = C2S_READY;
	p.is_ready = flag;

	sendPacket(&p);
}




void processPacket(char* ptr) { //패킷 넘겨주기 and 일처리 여기
	
	switch (ptr[2]) {

	
	case S2C_LOGIN_OK:
	{
		SC_PacketLoginOk* my_packet = reinterpret_cast<SC_PacketLoginOk*>(ptr);
		if (my_packet->size != sizeof(SC_PacketLoginOk))break;
		g_Myid = my_packet->id;	// 내 아이디 설정
		cout << "my id number: " << g_Myid << '\n';

	}
	break;

	case S2C_ENTER:
	{
		SC_PacketEnter* my_packet = reinterpret_cast<SC_PacketEnter*>(ptr);
		int id = my_packet->id;
		int room = my_packet->room;
		// 게임 그리기 
		// drawMap
		if (id == g_Myid) {
			system("cls");
			cout << id << "me";
		}
		else {
			cout << id;
		}


	}
	break;
	case S2C_LEAVE:
	{
		SC_PacketLeave* my_packet = reinterpret_cast<SC_PacketLeave*>(ptr);
		int other_id = my_packet->id;
		//if(other_id == g_myid) //나
		//else { //다른 사람

		//}
	}
	break;

	case S2C_RESULT:
	{

	}

	case S2C_UPDATE:
	{
		SC_PacketUpdate* my_packet = reinterpret_cast<SC_PacketUpdate*>(ptr);
		system("cls");
		//cout << "\x1B[2J\x1B[H";
		for (int i = 0; i < 5; ++i) {
			if (i == g_Myid)continue;
			cout << "other_P location: " << i << " x: " << my_packet->players[i].m_location.x << " y: " << my_packet->players[i].m_location.y << " z: " << my_packet->players[i].m_location.z << '\n';
		
		}

		cout << "My_location " << "x: " << my_packet->players[g_Myid].m_location.x << " y: " << my_packet->players[g_Myid].m_location.y << " z: " << my_packet->players[g_Myid].m_location.z << "\n";

		break;
	}


	default:
		cout << "unkonw packet\n";
	/*	DebugBreak();
		exit(-1);
	*/
	}
}

void processData(char* buf, DWORD io_byte)
{
	char* ptr = buf;
	static size_t in_packet_size = 0;
	static size_t saved_packet_size = 0;
	static char packet_buffer[BUF_SIZE];

	while (0 != io_byte) {
		if (0 == in_packet_size) in_packet_size = io_byte;
		if (io_byte + saved_packet_size >= in_packet_size) {
			memcpy(packet_buffer + saved_packet_size, ptr, in_packet_size - saved_packet_size);
			ptr += in_packet_size - saved_packet_size;
			io_byte -= in_packet_size - saved_packet_size;
			in_packet_size = 0;
			processPacket(packet_buffer);
			saved_packet_size = 0;
		}
		else {
			memcpy(packet_buffer + saved_packet_size, ptr, io_byte);
			saved_packet_size += io_byte;

			io_byte = 0;
		}
	}

}

void recvPacket() {
	
	while (true) {
		WSABUF recv_wsabuf[1];
		DWORD flags = 0;
		DWORD recv_bytes{};
		char buf[BUF_SIZE]{};
		recv_wsabuf[0].buf = buf;
		recv_wsabuf[0].len = BUF_SIZE;
		WSARecv(g_Sock, recv_wsabuf , 1, &recv_bytes, &flags, 0, 0);
		//int retval = recvn(g_Sock, (char*)&buf, sizeof(SC_PacketUpdate), 0);
		if (recv_bytes == SOCKET_ERROR) {
			errorDisplay("recv");
			return;
		}
		else if (recv_bytes != 0) {
			// 여기를 쓰레드로 다시 제작
			processData( buf, recv_bytes);
		}
	}
}


void init_Network() {
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	int retval;
	WSADATA wsa;
	WSAStartup(MAKEWORD(2, 2), &wsa);

	//socket()
	g_Sock = WSASocket(AF_INET, SOCK_STREAM, 0, 0, 0, WSA_FLAG_OVERLAPPED);
	if (g_Sock == INVALID_SOCKET) errorQuit("socket()");

	//connect()
	SOCKADDR_IN serveraddr;
	ZeroMemory(&serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	serveraddr.sin_port = htons(SERVER_PORT);
	retval = WSAConnect(g_Sock, (sockaddr*)&serveraddr,sizeof(serveraddr) ,0,0,0,0);
	//retval = connect(g_Sock, (SOCKADDR*)&serveraddr, sizeof(serveraddr));
	if (retval == SOCKET_ERROR) errorQuit("bind()");
	cout << "conneted\n";

	char name[MAX_ID_LEN] = "phil";
	CS_PacketLogin l_packet;
	l_packet.size = sizeof(l_packet);
	l_packet.type = C2S_LOGIN;
	strcpy_s(l_packet.name, name);
	sendPacket(&l_packet);
	char buf[BUF_SIZE];
	WSABUF recv_wsabufer[1];
	DWORD flag{ 0 };
	DWORD recv_bytes{};
	recv_wsabufer[0].buf = buf;
	recv_wsabufer[0].len = BUF_SIZE;
	WSARecv(g_Sock, recv_wsabufer, 1,&recv_bytes ,&flag, 0, 0);
	if (recv_bytes == SOCKET_ERROR) {
		errorDisplay("recv");
		return;
	}
	else if (recv_bytes != 0) {
		processData(buf, recv_bytes);
	}


}


int main void ()
{

}