#include "../../../../SalgomSalgom/SalgomSalgomGameServer/pch.h"



// SQLConnect_ref.cpp  
// compile with: odbc32.lib  
#include <windows.h>  
#include <sqlext.h>  
#include <iostream>

using namespace std;


OBSTACLE g_Obstacle[1];
CLIENT g_Clients[MAX_USER + 1];
Gem g_Gems[MAX_GEM_NUMBER];

SOCKET g_ListenSocket;
HANDLE g_iocp;

bool ready_user[1];


/************************************************************************
/* HandleDiagnosticRecord : display error/warning information
/*
/* Parameters:
/* hHandle ODBC handle
/* hType Type of handle (SQL_HANDLE_STMT, SQL_HANDLE_ENV, SQL_HANDLE_DBC)
/* RetCode Return code of failing command
/************************************************************************/
void HandleDiagnosticRecord(SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode)
{
	SQLSMALLINT iRec = 0;
	SQLINTEGER iError;
	WCHAR wszMessage[1000];
	WCHAR wszState[SQL_SQLSTATE_SIZE + 1];
	if (RetCode == SQL_INVALID_HANDLE) {
		fwprintf(stderr, L"Invalid handle!\n");
		return;
	}
	while (SQLGetDiagRec(hType, hHandle, ++iRec, wszState, &iError, wszMessage,
		(SQLSMALLINT)(sizeof(wszMessage) / sizeof(WCHAR)), (SQLSMALLINT*)NULL) == SQL_SUCCESS) {
		// Hide data truncated..
		if (wcsncmp(wszState, L"01004", 5)) {
			fwprintf(stderr, L"[%5.5s] %s (%d)\n", wszState, wszMessage, iError);
		}
	}
}

bool DB_Check(wchar_t ID[MAX_ID_LEN], wchar_t PW[MAX_PW_LEN]);

void sendPacket(int user_id, void* p, int size)
{
	CLIENT& u = g_Clients[user_id];
	EXOVER exover;
	exover.op = OP_SEND;
	ZeroMemory(&exover.over, sizeof(exover.over));
	exover.wsabuf.buf = exover.io_buf;
	exover.wsabuf.len = size;
	memcpy(exover.io_buf, p, size);
	WSASend(u.m_s, &exover.wsabuf, 1, NULL, 0, &exover.over, NULL);

}


void sendStartPacket(int user_id)
{
	SC_PacketStart p;
	p.size = sizeof(p);
	p.type = S2C_START;

	sendPacket(user_id,&p, p.size);
}

void sendLeavePacket(int user_id, int o_id)
{
	SC_PacketLeave p;
	p.id = o_id;
	p.size = sizeof(p);
	p.type = S2C_LEAVE;
	sendPacket(user_id, &p, p.size);
}

void sendPacketLoginOK(int user_id) {
	SC_PacketLoginOk p;
	p.size = sizeof(p);
	p.type = S2C_LOGIN_OK;
	p.id = user_id;
	sendPacket(user_id, &p, p.size);
}

void sendPacketLoginFail(int user_id)
{
	SC_PacketLoginFail p;
	p.size = sizeof(p);
	p.type = S2C_LOGIN_FAIL;
	p.id = user_id;
	sendPacket(user_id, &p, p.size);
}

void disconnect(int user_id)
{
	sendLeavePacket(user_id, user_id);
	g_Clients[user_id].m_cl.lock();
	g_Clients[user_id].m_status = ST_ALLOC;
	closesocket(g_Clients[user_id].m_s);
	for (auto& cl : g_Clients) {
		if (cl.m_id == user_id) continue;
		
		if (ST_ACTIVE_GAME == g_Clients[cl.m_id].m_status)
			sendLeavePacket(cl.m_id, user_id);
	}

	g_Clients[user_id].m_status = ST_FREE;
	g_Clients[user_id].m_cl.unlock();
	cout << "disconnected\n";
}


void processPakcet(int user_id, char* buf)
{
	CS_PacketInput* packet = reinterpret_cast<CS_PacketInput*>(buf);

	switch (packet->type) {
	case C2S_LOGIN:
	{
		CS_PacketLogin* packet = reinterpret_cast<CS_PacketLogin*>(buf);
		// 접속허락 후 아이디 보내기
		// DB 여기서 확인
	/*	bool login_success = DB_Check(packet->name,packet->password );
		if (login_success)sendPacketLoginOK(user_id);
		else sendPacketLoginFail(user_id);*/
		// else loginFale
		sendPacketLoginOK(user_id);
	}
	break;
	
	
	case C2S_READY:
	{
		CS_PacketReady* packet = reinterpret_cast<CS_PacketReady*>(buf);
		if (packet->is_ready == true) {
			g_Clients[user_id].m_ready = true;
			ready_user[user_id] = true;
		}
		else {
			g_Clients[user_id].m_ready = false;
			ready_user[user_id] = false;
		}

		bool game_start{ false };
		for (auto& rd : ready_user) {  // 다 레디이면
			if (rd == false) {
				game_start = false;
				break;
			}
			else {
				game_start = true;
			}
			
		}
		if (game_start) {


			for (int i = 0; i < MAX_USER; ++i)
				sendStartPacket(i);

			for (auto& cl : g_Clients)
				closesocket(cl.m_s);
			cout << "game start\n";


		}


	}
		break;

	case C2S_RESULT:
	{
		SC_PacketResult* packet = reinterpret_cast<SC_PacketResult*>(buf);

	}
		break;
	default:
		cout << "unknown packet type error\n";
	//	DebugBreak();
	//	exit(-1);
	}
}

void recvPacketConstruct(int user_id, DWORD io_byte)
{
	CLIENT& cu = g_Clients[user_id];
	EXOVER& r_o = cu.m_recv_over;

	int rest_byte = io_byte;
	char* p = r_o.io_buf;

	int packet_size = 0;
	if (0 != cu.m_prev_size) packet_size = cu.m_packe_buf[0];

	while (rest_byte > 0) {
		if (0 == packet_size) packet_size = *p;
		if (packet_size <= rest_byte + cu.m_prev_size) {
			memcpy(cu.m_packe_buf + cu.m_prev_size, p, packet_size - cu.m_prev_size);
			p += packet_size - cu.m_prev_size;
			rest_byte -= packet_size - cu.m_prev_size;
			packet_size = 0;
			processPakcet(user_id, cu.m_packe_buf);
			cu.m_prev_size = 0;

		}
		else {
			memcpy(cu.m_packe_buf + cu.m_prev_size, p, rest_byte);
			cu.m_prev_size += rest_byte;
			rest_byte = 0;
			p += rest_byte;
		}
	}
}


void workerThread() {

	while (true) {

		DWORD io_byte;
		ULONG_PTR key;
		WSAOVERLAPPED* over;
		GetQueuedCompletionStatus(g_iocp, &io_byte, &key, &over, INFINITE);
		EXOVER* exover = reinterpret_cast<EXOVER*> (over);
		int user_id = static_cast<int>(key);
		CLIENT& cl = g_Clients[user_id];


		switch (exover->op) {
		case OP_RECV: {
			if (0 == io_byte)
				disconnect(user_id);
			else {
				recvPacketConstruct(user_id, io_byte);
				ZeroMemory(&cl.m_recv_over.over, sizeof(cl.m_recv_over.over));
				DWORD flags = 0;
				WSARecv(cl.m_s, &cl.m_recv_over.wsabuf, 1, NULL, &flags, &cl.m_recv_over.over, NULL);
			}
			break;
		}

		case OP_SEND:
			if (0 == io_byte)
				disconnect(user_id);

			break;


		case OP_ACCEPT:
		{
			int user_id = -1;
			for (int i = 0; i < MAX_USER; ++i) {
				lock_guard<mutex> gl{ g_Clients[i].m_cl };
				if (ST_FREE == g_Clients[i].m_status) {
					g_Clients[i].m_status = ST_ALLOC;
					user_id = i;
					break;
				}
			}

			SOCKET c_socket = exover->c_socket;
			if (-1 == user_id)
				closesocket(c_socket);
			else {

				CreateIoCompletionPort(reinterpret_cast<HANDLE>(c_socket), g_iocp, user_id, 0);
				CLIENT& nc = g_Clients[user_id];
				nc.m_prev_size = 0;
				nc.m_recv_over.op = OP_RECV;
				ZeroMemory(&nc.m_recv_over.over, sizeof(nc.m_recv_over.over));
				nc.m_recv_over.wsabuf.buf = nc.m_recv_over.io_buf;
				nc.m_recv_over.wsabuf.len = MAX_BUFFER_SIZE;
				nc.m_s = c_socket;
				nc.m_location.x = -20;
				nc.m_location.y = 0;
				nc.m_location.z = 0;
				nc.m_rotation.x = 0;
				nc.m_rotation.y = 0;
				nc.m_rotation.z = 0;
				nc.m_status = ST_ACTIVE_LOBBY;
				nc.m_id = user_id;
				nc.m_rotation.x = 1;
				nc.m_role = R_NORMAL;

				//recvPacketConstruct(user_id, io_byte);
				//ZeroMemory(&cl.m_recv_over.over, sizeof(cl.m_recv_over.over));
				DWORD flags{};
				WSARecv(nc.m_s, &nc.m_recv_over.wsabuf, 1, NULL, &flags, &nc.m_recv_over.over, NULL);
			
			}
			c_socket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
			exover->c_socket = c_socket;
			ZeroMemory(&exover->over, sizeof(exover->over));
			AcceptEx(g_ListenSocket, c_socket, exover->io_buf, NULL, sizeof(sockaddr_in) + 16, sizeof(sockaddr_in) + 16, NULL, &exover->over);
		
		
		}
		break;
	
		default:
			break;
		}
	}
}


int main()
{
	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 2), &WSAData);

	g_ListenSocket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);

	SOCKADDR_IN s_address;
	memset(&s_address, 0, sizeof(s_address));
	s_address.sin_family = AF_INET;
	s_address.sin_port = htons(LOBBY_PORT);
	s_address.sin_addr.S_un.S_addr = htonl(INADDR_ANY);
	::bind(g_ListenSocket, reinterpret_cast<sockaddr*>(&s_address), sizeof(s_address));

	listen(g_ListenSocket, SOMAXCONN);


	g_iocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, 0);

	CreateIoCompletionPort(reinterpret_cast<HANDLE> (g_ListenSocket), g_iocp, 999, 0);
	SOCKET c_socket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	EXOVER accept_over;
	ZeroMemory(&accept_over.over, sizeof(accept_over.over));
	accept_over.op = OP_ACCEPT;
	accept_over.c_socket = c_socket;
	AcceptEx(g_ListenSocket, c_socket, accept_over.io_buf, NULL, sizeof(sockaddr_in) + 16, sizeof(sockaddr_in) + 16, NULL, &accept_over.over);

	wchar_t name[MAX_ID_LEN] = L"qwe";
	wchar_t password[MAX_PW_LEN] = L"asdf12";
	bool login_success = DB_Check(name, password);

	workerThread();

}

void DB_Store_playerinfo(wchar_t ID[MAX_ID_LEN], wchar_t PW[MAX_PW_LEN])
{

}

bool DB_Check(wchar_t ID[MAX_ID_LEN], wchar_t PW[MAX_PW_LEN]) { // bool로 바꾸고 다음에 성공하면 트루 아니면 false
	SQLHENV henv;
	SQLHDBC hdbc;
	SQLHSTMT hstmt = 0;
	SQLRETURN retcode;
	SQLWCHAR PlayerID[MAX_ID_LEN]{}, PlayerPW[MAX_PW_LEN]{};
	SQLLEN cbPlayerPW = 0, cbPlayerID = 0;
	setlocale(LC_ALL, "korean");
	std::wcout.imbue(std::locale("korean"));


	// Allocate environment handle  
	retcode = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &henv);

	// Set the ODBC version environment attribute  
	if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
		retcode = SQLSetEnvAttr(henv, SQL_ATTR_ODBC_VERSION, (SQLPOINTER*)SQL_OV_ODBC3, 0);

		// Allocate connection handle  
		if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
			retcode = SQLAllocHandle(SQL_HANDLE_DBC, henv, &hdbc);

			// Set login timeout to 5 seconds  
			if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
				SQLSetConnectAttr(hdbc, SQL_LOGIN_TIMEOUT, (SQLPOINTER)5, 0);

				// Connect to data source  
				retcode = SQLConnect(hdbc, (SQLWCHAR*)L"salgomsalgom", SQL_NTS, (SQLWCHAR*)NULL, 0, NULL, 0);

				// Allocate statement handle  
				if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
					retcode = SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hstmt);

					// 아이디에 따른 패스워드 찾기
					// 쿼리로 찾기
					wstring query;
					query.append(L"EXEC CheckPW ");
					query.append(ID);

					retcode = SQLExecDirect(hstmt, (SQLWCHAR*)query.c_str(), SQL_NTS);
					if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {

						// Bind columns 1, 2
						//retcode = SQLBindCol(hstmt, 3, SQL_WCHAR, &PlayerID, 20, &cbPlayerID);
						retcode = SQLBindCol(hstmt, 1, SQL_WCHAR, &PlayerPW, 20, &cbPlayerPW);

						// Fetch and print each row of data. On an error, display a message and exit.  
						retcode = SQLFetch(hstmt);
						if (retcode == SQL_ERROR || retcode == SQL_SUCCESS_WITH_INFO)
							HandleDiagnosticRecord(hstmt, SQL_HANDLE_STMT, retcode);
						if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
						{
							//strcmp(ID, (const char*)PlayerID);
							if (wcscmp(PW, PlayerPW) == 0 ) return true;
							else return false;
							//wcout << L"  Name: " << reinterpret_cast<wchar_t*>(PlayerID) << '\n';
						}


					}
					else {
						HandleDiagnosticRecord(hstmt, SQL_HANDLE_STMT, retcode);
						return false;
					}

					// Process data  
					if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
						SQLCancel(hstmt);
						SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
					}

					SQLDisconnect(hdbc);
				}

				SQLFreeHandle(SQL_HANDLE_DBC, hdbc);
			}
		}
		SQLFreeHandle(SQL_HANDLE_ENV, henv);
	}
	return false;
}