import bpy
import os
import struct
import mathutils
from math import radians

sce = bpy.context.scene
ob = bpy.context.object
key_indices=[]
myaction = bpy.context.object.animation_data.action
fcurves=myaction.fcurves
ani_list=['AWalking','Idle'] #animation name list
rotation='rotation_quaternion'
ani_index=0
bonenum=17
arma = bpy.data.objects["NORMAL.001"]

with open(os.path.splitext(bpy.data.filepath)[0] + "_NORMAL_NB.txt", 'w') as fs:
    arm_matrix= arma.matrix_world
    fs.write("%i\n"%bonenum)
        
    for pose_bone in ob.pose.bones:
        bone_p= pose_bone.bone
        print(bone_p.name)
        fs.write("%s\n"%bone_p.name)
        local = arma.data.bones[bone_p.name].matrix_local
        basis = arma.pose.bones[bone_p.name].matrix_basis
        
        if (bone_p.parent):
            print(bone_p.name , bone_p.parent.name)
            parent_local = arma.data.bones[bone_p.parent.name].matrix_local
            print("inverted: ",parent_local.inverted())
            
            resultmat = parent_local.inverted() @ local

            for i in range(0,4):
                for j in range(0,4):
                    fs.write("%.3f "%resultmat[i][j])
                fs.write("\n")
        else:
            #pelmat=local @ basis
            for i in range(0,4):
                for j in range(0,4):
                    fs.write("%.3f "%local[i][j])
                fs.write("\n")
    

    fs.write("%i\n"%len(ani_list))
    for idx in range(0,len(ani_list)):
        key_indices.clear()
        ob.animation_data.action= bpy.data.actions.get(ani_list[idx])
        myaction = ob.animation_data.action #get current action
        print("cation name:",myaction.name)
        fs.write("%s\n"%myaction.name)
        fcurves=myaction.fcurves #get current action fcurves
        for fcu in myaction.fcurves:
            for keyframe in fcu.keyframe_points:
                key_indices.append(int(keyframe.co[0]))
        set_key_indices = list(set(key_indices))
        set_key_indices.sort()
        print("keyframes:",set_key_indices)
        
        # number of keyframes
        fs.write("%s\n" % len(set_key_indices))

        for pbone in ob.pose.bones:  # enum bone
            bone = bpy.data.objects['NORMAL.001'].pose.bones[pbone.name]
            fs.write("%s\n" % pbone.name) #bone name
            
            for idx, kidx in enumerate(set_key_indices): # enum keyframe
                time = float(kidx/30.0)
                
                # Get Path from Bone
                loc_path = bone.path_from_id("location")
                rot_path = bone.path_from_id(rotation)
                scl_path = bone.path_from_id("scale")
                
                # Set Transform Data
                loc_ele = []
                rot_ele = []
                scl_ele = []
                
                for i in range(0, 3):
                    fc = fcurves.find(loc_path, index = i)
                    curr_frame = fc.keyframe_points[idx]
                    loc_ele.append(float(curr_frame.co[1]))
                    
                for i in range(0, 4):
                    fc = fcurves.find(rot_path, index = i)
                    curr_frame = fc.keyframe_points[idx]
                    rot_ele.append(float(curr_frame.co[1]))
                    
                for i in range(0, 3):
                    fc = fcurves.find(scl_path, index = i)
                    curr_frame = fc.keyframe_points[idx]
                    scl_ele.append(float(curr_frame.co[1]))
                
                # Out Data
                fs.write("%.3f\n" % time)  #key frame
                
                for ele in loc_ele:
                    fs.write("%.3f " % ele)
                fs.write("\n")
                
                fs.write("%.3f " % rot_ele[1])
                fs.write("%.3f " % rot_ele[2])
                fs.write("%.3f " % rot_ele[3])
                fs.write("%.3f " % rot_ele[0])
                fs.write("\n")
                
                for ele in scl_ele:
                    fs.write("%.3f " % ele)
                fs.write("\n")

    fs.close()
    
with open(os.path.splitext(bpy.data.filepath)[0] + "_NORMAL_B.txt", 'wb') as fs:
    bin_value=struct.pack('i',bonenum)
    fs.write(bin_value)#num of bones
    
    for pose_bone in ob.pose.bones:
        bone_p= pose_bone.bone
        print(bone_p.name)
        bin_value=struct.pack('10s',bone_p.name.encode())
        fs.write(bin_value) #num of aactions
        local = arma.data.bones[bone_p.name].matrix_local
        basis = arma.pose.bones[bone_p.name].matrix_basis
        
        if (bone_p.parent):
            print(bone_p.name , bone_p.parent.name)
            parent_local = arma.data.bones[bone_p.parent.name].matrix_local
            #print("inverted: ",parent_local.inverted())
            
            resultmat = parent_local.inverted() @ local
            #print("child_mat:\n" , local)
            #print("parent_mat:\n" , parent_local)
            #print("result_mat: \n", resultmat)
            
            for i in range(0,4):
                for j in range(0,4):
                    bin_value=struct.pack('f', resultmat[i][j])
                    fs.write(bin_value)
                bin_value=struct.pack('\n')
                fs.write(bin_value)
        else:
            for i in range(0,4):
                for j in range(0,4):
                    bin_value=struct.pack('f', local[i][j])
                    fs.write(bin_value)
                bin_value=struct.pack('\n')
                fs.write(bin_value)


    bin_value=struct.pack('i',len(ani_list))
    fs.write(bin_value)
    for idx in range(0,len(ani_list)):
        key_indices.clear()
        
        ob.animation_data.action= bpy.data.actions.get(ani_list[idx])
        myaction = ob.animation_data.action #get current action
        bin_value=struct.pack('10s',myaction.name.encode())
        fs.write(bin_value) #num of aactions
        fcurves=myaction.fcurves #get current action fcurves
        for fcu in myaction.fcurves:
            for keyframe in fcu.keyframe_points:
                key_indices.append(int(keyframe.co[0]))
        set_key_indices = list(set(key_indices))
        set_key_indices.sort()
        print("keyframes:",set_key_indices)
        
        # number of keyframes
        bin_value=struct.pack('i',len(set_key_indices))
        fs.write(bin_value)#num of bones

        for pbone in ob.pose.bones:  # enum bone
            bone = bpy.data.objects['NORMAL.001'].pose.bones[pbone.name]
            bin_value=struct.pack('10s',pbone.name.encode())
            fs.write(bin_value) #num of aactions
            
            for idx, kidx in enumerate(set_key_indices): # enum keyframe
                time = float(kidx/30.0)
                
                # Get Path from Bone
                loc_path = bone.path_from_id("location")
                rot_path = bone.path_from_id(rotation)
                scl_path = bone.path_from_id("scale")
                
                # Set Transform Data
                loc_ele = []
                rot_ele = []
                scl_ele = []
                
                for i in range(0, 3):
                    fc = fcurves.find(loc_path, index = i)
                    curr_frame = fc.keyframe_points[idx]
                    loc_ele.append(float(curr_frame.co[1]))
                    
                for i in range(0, 4):
                    fc = fcurves.find(rot_path, index = i)
                    curr_frame = fc.keyframe_points[idx]
                    rot_ele.append(float(curr_frame.co[1]))
                    
                for i in range(0, 3):
                    fc = fcurves.find(scl_path, index = i)
                    curr_frame = fc.keyframe_points[idx]
                    scl_ele.append(float(curr_frame.co[1]))
                
                # Out Data
                bin_value=struct.pack('f',time)
                fs.write(bin_value)
                
                for ele in loc_ele:
                    bin_value=struct.pack('f ',ele)
                    fs.write(bin_value)
                bin_value=struct.pack('\n')
                fs.write(bin_value)
            
                bin_value=struct.pack('f ',rot_ele[1])
                fs.write(bin_value)
                bin_value=struct.pack('f ',rot_ele[2])
                fs.write(bin_value)
                bin_value=struct.pack('f ',rot_ele[3])
                fs.write(bin_value)
                bin_value=struct.pack('f ',rot_ele[0])
                fs.write(bin_value)
                bin_value=struct.pack('\n')
                fs.write(bin_value)
                
                for ele in scl_ele:
                    bin_value=struct.pack('f ',ele)
                    fs.write(bin_value)
                    #fs.write("%.3f " % ele)
                bin_value=struct.pack('\n')
                fs.write(bin_value)

    fs.close()
