import bpy
import os
import struct
import mathutils
from math import radians

sce = bpy.context.scene
ob = bpy.context.object
key_indices=[]
bone_idx=0;
arma = bpy.data.objects["NORMAL.001"]
#empty_ob.matrix_world = arma.matrix_world * matrix_world(arma, bone_name )

with open(os.path.splitext(bpy.data.filepath)[0] + "matTest.txt", 'w') as fs:
    arm_matrix= arma.matrix_world
    print("arm_matrix: \n", arm_matrix)
    #
    #rest_b_mat=armature.data.bones[0].matrix_local
    
   
    #print("rest_b_mat:" , rest_b_mat)
    #print("bone[0]:", armature.data.bones[0].name)
    
    #??
    #SystemMatrix = mathutils.Matrix.Scale(-1, 4, mathutils.Vector((0, 0, 1))) * mathutils.Matrix.Rotation(radians(-90), 4, 'X')
    #print("sys_matrix: \n", SystemMatrix)
    
    for pose_bone in ob.pose.bones:
        bone_p= pose_bone.bone
        print(bone_p.name)
        
        #print('bon mat: %s'%(bone.matrix_local))
        local = arma.data.bones[bone_p.name].matrix_local
        basis = arma.pose.bones[bone_p.name].matrix_basis

        #local = armature_ob.data.bones[bone_name].matrix_local
        #basis = armature_ob.pose.bones[bone_name].matrix_basis
        #rest_b_mat=bone.matrix_local
        #basis_b_mat = arma.pose.bones[bone_name].matrix_basis
        #
        #local = arma.bone_d.matrix_local
        #basis = arma.bone_p.matrix_basis #o
        
        
        # print("basis_mat:\n" , basis)
        
        #local rest matrix relative to the bone's parent
        #l1 = armature.data.bones[1].matrix_local * l0.copy().inverted
        
        #       parent armature space matrix, translation, child bone rotation
        #bone.matrix_local == bone.parent.matrix_local * bone_translation * bone.matrix.to_4x4()
        
        if (bone_p.parent):
            print(bone_p.name , bone_p.parent.name)
            parent_local = arma.data.bones[bone_p.parent.name].matrix_local
            print("inverted: ",parent_local.inverted())
            
            resultmat = parent_local.inverted() @ local
            print("child_mat:\n" , local)
            print("parent_mat:\n" , parent_local)
            print("result_mat: \n", resultmat)
            
            for i in range(0,4):
                for j in range(0,4):
                    fs.write("%.3f "%resultmat[i][j])
                fs.write("\n")
            
            
            #bone_translation = Matrix.Translation(Vector((0, bone.parent.length, 0)) + bone.head)
        
        else: 
            print(local* basis)
            

            
fs.close()
