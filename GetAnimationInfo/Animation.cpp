#include <iostream>
#include <string>
#include <fstream>
#include <vector>
using namespace std;

#pragma once
struct float3 {
	float x;
	float y;
	float z;
};
struct float4 {
	float w;
	float x;
	float y;
	float z;
};

int main() {
	int* datalen;
	int stringlen = 10;//10s
	//Model m;
	string fileName = "anitestB.txt";
	ifstream in{ fileName, std::ios_base::binary };

	if (!in) {
		cout << " - 파일을 열 수 없습니다." << endl;
		return false;
	}

	if (in) {
		cout << "파일 읽기 성공" << endl;
		int temp;
		//int Bonenum;
		//int actionNum;
		int keyframenum;
		float time;
		float3 lo;
		float4 ro;
		float3 sca;

		in.read((char*)&temp, sizeof(int)); //bone number
		cout << temp << endl;

		in.read((char*)&temp, sizeof(int));//action number
		cout << temp << endl;
		//여기부터 반복문으로 처리 필요
		
		for (int i = 0; i < 3; i++) {
			//액션이름  받아 오기
			char *buffer = new char[stringlen];
			in.read(buffer, stringlen);
			cout << buffer << endl;
			delete[] buffer;
			buffer = nullptr;

			//키프레임 숫자
			in.read((char*)&keyframenum, sizeof(int));
			cout << keyframenum << endl;

			//j: 0~<본 개수(실험할것 있어서 5로 써둠) 
			for (int j = 0; j < 5; j++) {
				char *buffer1 = new char[stringlen];
				in.read(buffer1, stringlen);
				cout << buffer1 << endl;
				delete[] buffer1;
				buffer1 = nullptr;

				//k는 키프레임 만큼 돈다!
				for (int k = 0; k < keyframenum; k++) {

					in.read((char*)&time, sizeof(float)); //time
					cout << time << endl;

					in.read((char*)&lo, sizeof(float) * 3); //location
					cout << lo.x << endl;
					cout << lo.y << endl;
					cout << lo.z << endl;

					in.read((char*)&ro, sizeof(float) * 4); //rotation
					cout << ro.w << endl;
					cout << ro.x << endl;
					cout << ro.y << endl;
					cout << ro.z << endl;

					in.read((char*)&sca, sizeof(float) * 3); //scale
					cout << sca.x << endl;
					cout << sca.y << endl;
					cout << sca.z << endl;
				}

			}

		}

	}

}


