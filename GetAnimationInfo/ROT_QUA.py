#PLZ!!!!!!!!!!!!!CHECK inserted keyframe!!!!!!!!!!
import bpy
import os
import struct

sce = bpy.context.scene
ob = bpy.context.object
key_indices=[]
myaction = bpy.context.active_object.animation_data.action
fcurves = myaction.fcurves
tempkeys=[]

#---------------------------------------------------
AniName="Falldown"
AmaName="NORMAL.001"
  
with open(os.path.splitext(bpy.data.filepath)[0] + AniName+"_NB.txt", 'w') as fs:
    fs.write("%d\n"%len(ob.pose.bones))#num of bones
    #fs.write("%d\n"%len(bpy.data.actions)) #num of aactions
    fs.write("%d\n"%1)
    
    key_indices.clear()
    tempkeys.clear()
    action=bpy.data.actions[AniName]  # enum action
    print(type(action))
    fs.write("%s\n" % action.name)
    
    # Get Keyframe Points from Action
    for fcu in action.fcurves:
        for keyframe in fcu.keyframe_points:
            key_indices.append(int(keyframe.co[0]))
            
    set_key_indices = list(set(key_indices))
    set_key_indices.sort()
    
    # number of keyframes
    fs.write("%s\n" % len(set_key_indices))

    for pbone in ob.pose.bones:  # enum bone
        bone = bpy.data.objects[AmaName].pose.bones[pbone.name]
        fs.write("%s\n" % pbone.name) #bone name
        print(pbone.name)
        
        for idx, kidx in enumerate(set_key_indices): # enum keyframe
            time = float(kidx/30.0)
            
            # Get Path from Bone
            loc_path = bone.path_from_id("location")
            rot_path = bone.path_from_id("rotation_euler")
            scl_path = bone.path_from_id("scale")
            
            # Set Transform Data
            loc_ele = []
            rot_ele = []
            scl_ele = []
            
            for i in range(0, 3):
                fc = fcurves.find(loc_path, index = i)
                curr_frame = fc.keyframe_points[idx]
                loc_ele.append(float(curr_frame.co[1]))
                
            for i in range(0, 3):
                fc = fcurves.find(rot_path, index = i)
                curr_frame = fc.keyframe_points[idx]
                rot_ele.append(float(curr_frame.co[1]))
                
            for i in range(0, 3):
                fc = fcurves.find(scl_path, index = i)
                curr_frame = fc.keyframe_points[idx]
                scl_ele.append(float(curr_frame.co[1]))
            
            # Out Data
            fs.write("%.3f\n" % time)  #key frame
            
            for ele in loc_ele:
                fs.write("%.3f " % ele)
            fs.write("\n")
            
            for ele in rot_ele:
                fs.write("%.3f " % ele)
            fs.write("\n")
            
            for ele in scl_ele:
                fs.write("%.3f " % ele)
            fs.write("\n")

fs.close()
with open(os.path.splitext(bpy.data.filepath)[0] + AniName +"_B.txt", 'wb') as fs:
    #bin_value=struct.pack('10s',action.name.encode())
    bin_value=struct.pack('i',len(ob.pose.bones))
    fs.write(bin_value)
    bin_value=struct.pack('i',1)
    fs.write(bin_value)
    
    key_indices.clear()
    tempkeys.clear()
    action=bpy.data.actions[AniName]  # enum action

    for action in bpy.data.actions:     # enum action
        key_indices.clear()
        tempkeys.clear()
        bin_value=struct.pack('10s',action.name.encode())
        fs.write(bin_value)
        
        # Get Keyframe Points from Action
        for fcu in action.fcurves:
            for keyframe in fcu.keyframe_points:
                key_indices.append(int(keyframe.co[0]))
                
        set_key_indices = list(set(key_indices))
        set_key_indices.sort()
        
        # number of keyframes
        bin_value=struct.pack('i',len(set_key_indices))
        fs.write(bin_value)

        for pbone in ob.pose.bones:  # enum bone
            bone = bpy.data.objects[AmaName].pose.bones[pbone.name]
            bin_value=struct.pack('10s',pbone.name.encode())
            fs.write(bin_value) #bone name
            for idx, kidx in enumerate(set_key_indices): # enum keyframe
                time = float(kidx/30.0)
                
                # Get Path from Bone
                loc_path = bone.path_from_id("location")
                rot_path = bone.path_from_id("rotation_euler")
                scl_path = bone.path_from_id("scale")
                
                # Set Transform Data
                loc_ele = []
                rot_ele = []
                scl_ele = []
                
                for i in range(0, 3):
                    fc = fcurves.find(loc_path, index = i)
                    curr_frame = fc.keyframe_points[idx]
                    loc_ele.append(float(curr_frame.co[1]))
                    
                for i in range(0, 3):
                    fc = fcurves.find(rot_path, index = i)
                    curr_frame = fc.keyframe_points[idx]
                    rot_ele.append(float(curr_frame.co[1]))
                    
                for i in range(0, 3):
                    fc = fcurves.find(scl_path, index = i)
                    curr_frame = fc.keyframe_points[idx]
                    scl_ele.append(float(curr_frame.co[1]))
                
                # Out Data
                bin_value=struct.pack('f',time)
                fs.write(bin_value)  #key frame
                
                for ele in loc_ele:
                    bin_value=struct.pack('f',ele)
                    fs.write(bin_value)
                bin_value=struct.pack('\n')
                fs.write(bin_value)
                
                for ele in rot_ele:
                    bin_value=struct.pack('f',ele)
                    fs.write(bin_value)
                bin_value=struct.pack('\n')
                fs.write(bin_value)
                
                for ele in scl_ele:
                    bin_value=struct.pack('f',ele)
                    fs.write(bin_value)
                bin_value=struct.pack('\n')
                fs.write(bin_value)
            
        break
            
fs.close()
