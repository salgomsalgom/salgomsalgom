import bpy
import os
import struct

sce = bpy.context.scene
ob = bpy.context.object
key_indices=[]
print(type(key_indices))

with open(os.path.splitext(bpy.data.filepath)[0] + ".txt", 'wb') as fs:
    bin_value=struct.pack('i',len(ob.pose.bones))
    fs.write(bin_value)#num of bones
    bin_value=struct.pack('i',len(bpy.data.actions))
    fs.write(bin_value) #num of aactions
    
    for action in bpy.data.actions:
        key_indices.clear()
        bin_value=struct.pack('10s',action.name.encode())
        fs.write(bin_value)
        for fcu in action.fcurves:
            for keyframe in fcu.keyframe_points:
                key_indices.append(keyframe.co[0])
        set_key_indices=list(set(key_indices))
        set_key_indices.sort()
        bin_value=struct.pack('i',len(set_key_indices))
        fs.write(bin_value)

        for pbone in ob.pose.bones:
            bin_value=struct.pack('10s',pbone.name.encode())
            fs.write(bin_value)
            for kidx in set_key_indices:
                bin_value=struct.pack('f',kidx/60.0)
                fs.write(bin_value)
                loc=pbone.location
                bin_value=struct.pack('fff',loc[0],loc[1],loc[2])
                fs.write(bin_value)
                
                qua = pbone.rotation_quaternion        
                bin_value=struct.pack('ffff',qua[0],qua[1],qua[2],qua[3])
                fs.write(bin_value)
                
                sca=pbone.scale
                bin_value=struct.pack('fff',sca[0],sca[1],sca[2])
                fs.write(bin_value)
        
