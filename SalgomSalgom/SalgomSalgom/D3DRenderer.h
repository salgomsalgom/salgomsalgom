#pragma once
#include "Fence.h"

class Camera;
class CubeMesh;

using namespace Microsoft::WRL;

class D3DRenderer
{
public:
	void Initialize();
	void CreateDevice();
	void CreateCommandObject();
	void CreateSwapChain();
	void CreateDescriptorHeap();
	void CreateRenderTargetView();
	void CreateDepthStencilView();

	void SetFullScreen(bool bIsFullScreen, RECT clientRect);
	void Render();
	void SetFence();

	IDXGISwapChain3* GetSwapChain() { return m_SwapChain.Get(); }

private:
	void CreateGraphicRootSignature();
	void BuildShaders();
	void CreatePSO();
	void BuildObject();

private:
	void LogAdapters();
	void LogAdapterOutputs();

private:
	static const UINT m_SwapChainBuffersCount = 2;

	UINT m_RtvDescriptorSize = 0;
	UINT m_DsvDescriptorSize = 0;
	UINT m_SbvSrvDescriptorSize = 0;

	UINT m_SwapChainBufferIdx = 0;

private:
	// DXGI
	ComPtr<IDXGIFactory4>	m_Factory;
	ComPtr<IDXGISwapChain3>	m_SwapChain;

	// D3D12
	ComPtr<ID3D12Device>				m_Device;
	ComPtr<ID3D12CommandQueue>			m_CommandQueue;
	ComPtr<ID3D12CommandAllocator>		m_CommandAllocator;
	ComPtr<ID3D12GraphicsCommandList>	m_CommandList;

	ComPtr<ID3D12DescriptorHeap> m_RtvDescriptorHeap;
	ComPtr<ID3D12DescriptorHeap> m_DsvDescriptorHeap;
	ComPtr<ID3D12Resource>		m_RenderTargetBuffers[m_SwapChainBuffersCount];
	ComPtr<ID3D12Resource>		m_DepthStencilBuffer;

	ComPtr<ID3D12Fence>		m_Fence;
	UINT					m_FenceValue;
	HANDLE					m_FenceEvent;

private:
	ComPtr<ID3D12RootSignature> m_RootSignature;
	ComPtr<ID3D12PipelineState> m_PipelineState;

	// 쉐이더!
	ID3DBlob* m_VertexShaderBlob;
	ID3DBlob* m_PixelShaderBlob;

	int m_VertexCount;
	int m_IndexCount;


	// Mesh에 들어가야 할 정보들
	ComPtr<ID3D12Resource> VertexBufferGPU;
	ComPtr<ID3D12Resource> VertexBufferUploader;
	D3D12_VERTEX_BUFFER_VIEW m_TestBufferView;

	ComPtr<ID3D12Resource> IndexBufferGPU;
	ComPtr<ID3D12Resource> IndexBufferUploader;
	D3D12_INDEX_BUFFER_VIEW m_TestIndexBufferView;

private:
	Camera* m_Camera;
	CubeMesh* m_CubeMesh;
};
