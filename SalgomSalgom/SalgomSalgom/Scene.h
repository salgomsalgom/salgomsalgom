#pragma once
#include "../Packet.h"

using namespace DirectX;

struct StaticObjectInfo {
	XMFLOAT3 m_Position;
	XMFLOAT3 m_Rotation;
	XMFLOAT3 m_Scale;
	string m_Name;
};

struct CollisionBoxInfo
{
	DirectX::XMFLOAT3 m_Size;
};


enum class ChRole { NORMAL, WITCH };
enum class ChStatus { ALIVE, DEAD };

struct CharacterInfo
{
	int m_ID;
	ChRole m_Role;
	XMFLOAT3 m_Position;
	XMFLOAT3 m_Rotation;
	XMFLOAT3 m_DirVector;

	float m_RespawnTime;
	bool gem;
};

struct SkillInfo {
	bool m_Exist		= false;
	float m_CoolTime	= 0.f;
	float m_DurationTime = 0.f;
	XMFLOAT3 m_Position	= XMFLOAT3{ 0, 0, 0 };
	SkillStatus m_Stat	= SkillStatus::READY;
};

struct GemInfo
{
	MagicColor m_Color;
	XMFLOAT3 m_Position;
	XMFLOAT3 m_Rotation;
	bool m_Transparent;
	int m_Player_id;
};

struct WitchStoneInfo 
{
	MagicColor m_Color;
	bool m_Alive;
};

struct BattleData {
	char numPlayer;
	char soulCount;
	CharacterInfo  players[5];
	SkillInfo skill1[5];
	SkillInfo skill2[5];
	GemInfo gems[16];
	WitchStoneInfo witchStoneState[4];
};

struct UpdateData 
{
	CharacterInfo players[5];
	GemInfo gems[16];
	SkillInfo skill1[5];
	SkillInfo skill2[5];
};

struct AniEvent
{
	int id;
	A_STATUS stat;
};

// 맵 & 배경
void WriteID(int id);
void MoveMapData(int numObjects, StaticObjectInfo* objects);
void MoveCollisionBoxData(int numObjects, CollisionBoxInfo* boxs);

// 배틀 데이터
void InitBattleData(BattleData& data);
void WriteUpdateData(UpdateData& data);
void WriteWitchStoneInfo(WitchStoneInfo* wsInfo);
void WriteSoulCount(int count);

// update message
void UpdateWitchStone();
void UpdateAniEvent(int id, A_STATUS stat);
void UpdatePowerEvent(float skill, float speed);


//--------------------------------------------------------------
#include "Camera.h"

enum class PSOType;
class GraphicsObject;
class StaticObject;
class Sprite;
class SpriteFont;
class Text;
class InputSystem;
class Character;
class SpriteAnimation;
class Body;

enum class SceneKey {
	TITLE,
	LOGIN,
	LOBBY,
	BATTLE,
	NORMAL_WIN,
	WITCH_WIN,

	TEST,
};

class Scene 
{
public:
	explicit Scene(SceneKey key);
	virtual ~Scene();

	Scene(const Scene&) = delete;
	// Scene(Scene&&) = delete;

public:
	virtual void Initialize() = 0;
	virtual void Update(float frame_time) = 0;
	virtual void HandleInput(InputSystem& input) = 0;

	virtual void Destroy() = 0;

	GraphicsObject* AddObject(GraphicsObject* obj);
	GraphicsObject* ReaddObject(GraphicsObject* obj);
	void RemoveObject(GraphicsObject* obj);

public:
	const SceneKey	m_Key;
	Camera			m_Camera;
	std::map<PSOType, std::list<GraphicsObject*>> m_Objects;
	float m_SceneTime = 0.f;

};

class BattleScene : public Scene
{
public:
	explicit BattleScene(SceneKey key);
	virtual ~BattleScene();

public:
	void Initialize() override;
	void Update(float frame_time) override;
	void HandleInput(InputSystem& input) override;
	void Destroy() override;

public:
	int								m_ClientID;
	ChRole							m_Role;
	map<int, Character*>			m_Characters;
	vector<GraphicsObject*>			m_Gems;

	// witch stone
	map<MagicColor, GraphicsObject*>	m_WitchStones;
	map<MagicColor, Sprite*>			m_WitchStoneUIs;
	map<MagicColor, Sprite*>			m_WitchStoneBrokenUIs;
	bool m_WitchStoneAlive[4];

	Body* m_Hand;
	SpriteFont* m_Font;
	SpriteFont* m_EngFont;

	// Soul
	Text* m_SoulCount;

	// Skill Tests
	StaticObject* m_Sphere;
	bool m_RenderSphere;

private:
	Text* m_TextSpeed;
	Text* m_TextTimer;

private:
	float SKILL1_COOLTIME;
	float SKILL2_COOLTIME;
	
	Sprite* m_Skill1Filter;
	Sprite* m_Skill2Filter;

private:
	bool m_Death = false;
	bool m_FakeDeath = false;
	Sprite* m_RespawnLetter;
	Sprite* m_FakeDeathLetter;
	Sprite* m_RespawnBG;
	Sprite* m_RespawnFill;


private:
	float MOUSE_LBUTTON_COOLTIME;
	float m_LBtnCurrCool = 0.f;
	int m_MoveKeyDownCount = 0;
};
