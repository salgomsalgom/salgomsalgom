#pragma once
class ShaderVisibleDescriptorHeap
{
public:
	ShaderVisibleDescriptorHeap() : m_Heap() { }

	void Create(ID3D12Device* device, UINT count);

	void Allocate(D3D12_CPU_DESCRIPTOR_HANDLE* out_cpu_handle, D3D12_GPU_DESCRIPTOR_HANDLE* out_gpu_handle);

	ID3D12DescriptorHeap** GetAddressOf() { return m_Heap.GetAddressOf(); }

	ID3D12DescriptorHeap* operator->() { return m_Heap.Get(); }
	const ID3D12DescriptorHeap* operator->() const { return m_Heap.Get(); }

private:
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> m_Heap;
	D3D12_CPU_DESCRIPTOR_HANDLE m_CurrCPUDescriptorHandle;
	D3D12_GPU_DESCRIPTOR_HANDLE m_CurrGPUDescriptorHandle;
	UINT m_DescriptorSize;
	UINT m_FreeHandle;

};

