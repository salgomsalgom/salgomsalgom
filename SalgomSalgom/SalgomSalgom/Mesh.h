#pragma once
#include "GpuResource.h"
#include "UploadBuffer.h"

// 버텍스 버퍼, 인덱스 버퍼 등을 가진다.
struct D2bBuf;

class Mesh {
public:
	explicit Mesh(D3D12_PRIMITIVE_TOPOLOGY primitive_topology);
	explicit Mesh(UINT vb_size, UINT v_count, void* pV,
		UINT i_count, void* pI, ID3D12GraphicsCommandList* command_list);
	explicit Mesh(D2bBuf& buf);
	~Mesh() { }

public:
	void InitBuffers() { }

	D3D12_PRIMITIVE_TOPOLOGY GetPrimitiveTopology() {
		return m_PrimitiveTopology;
	}

public:
	UINT m_VertexCount;
	UINT m_IndexCount;

	GpuBuffer m_VertexBuffer;
	GpuBuffer m_IndexBuffer;

	// 언제 해제해도 되는 거냐
	UploadBuffer m_VertexUploadBuffer;
	UploadBuffer m_IndexUploadBuffer;

	D3D12_VERTEX_BUFFER_VIEW m_VertexBufferView;
	D3D12_INDEX_BUFFER_VIEW m_IndexBufferView;
	
	D3D12_PRIMITIVE_TOPOLOGY m_PrimitiveTopology;
};

class RectangleMesh : public Mesh
{
public:
	explicit RectangleMesh(float width, float hight);

	void InitBuffers(/*ID3D12GraphicsCommandList* command_list*/);
};

class CubeMesh : public Mesh {
public:
	explicit CubeMesh(float size);
};

class HeightMap;

class GridMesh : public Mesh
{
public:
	GridMesh(const DirectX::XMFLOAT3& scale, HeightMap* heightmap);
	~GridMesh();

	UINT GetIndexCount() { return m_IndexCount; }

private:
	HeightMap* m_HeightMap;
};
