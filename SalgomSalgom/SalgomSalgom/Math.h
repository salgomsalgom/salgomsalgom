#pragma once

namespace XMMath {
	using namespace DirectX;

	constexpr XMVECTOR XMVECTOR_ORIGIN{ 0.f, 0.f, 0.f, 0.f };

	inline XMFLOAT3 Add(const XMFLOAT3& v1, const XMFLOAT3& v2)
	{
		XMFLOAT3 ret;
		XMStoreFloat3(&ret, XMLoadFloat3(&v1) + XMLoadFloat3(&v2));
		return ret;
	}

	inline XMFLOAT3 Normalize(const XMFLOAT3& value)
	{
		XMFLOAT3 normal;
		XMStoreFloat3(&normal, XMVector3Normalize(XMLoadFloat3(&value)));
		return normal;
	}

	inline XMFLOAT3 ScalarProduct(const XMFLOAT3& v, float scalar, bool normalizing = true)
	{
		XMFLOAT3 ret;
		if (normalizing) XMStoreFloat3(&ret, XMVector3Normalize(XMLoadFloat3(&v)) * scalar);
		else XMStoreFloat3(&ret, XMLoadFloat3(&v) * scalar);
		return ret;
	}

	inline float DotProduct(const XMFLOAT3& v1, const XMFLOAT3& v2)
	{
		XMFLOAT3 ret;
		XMStoreFloat3(&ret, XMVector3Dot(XMLoadFloat3(&v1), XMLoadFloat3(&v2)));
		return ret.x;
	}

	inline XMFLOAT3 CrossProduct(const XMFLOAT3& v1, const XMFLOAT3& v2, bool normalizing = true)
	{
		XMFLOAT3 ret;
		if (normalizing) XMStoreFloat3(&ret, XMVector3Normalize(XMVector3Cross(XMLoadFloat3(&v1), XMLoadFloat3(&v2))));
		else XMStoreFloat3(&ret, XMVector3Cross(XMLoadFloat3(&v1), XMLoadFloat3(&v2)));
		return ret;
	}
}

