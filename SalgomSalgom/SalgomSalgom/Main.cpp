#include "pch.h"
#include "WindowApp.h"
#include "Game.h"
#include "D3DRenderer.h"

int APIENTRY _tWinMain
(HINSTANCE instance, HINSTANCE prevInstance, LPTSTR cmdLine, int cmdShow)
{
	GameCore::Game* game = new TestGame();	// 동적 할당을 받았으니 힙에 자리한다.
	return WindowApp::Run(instance, *game, TEXT("SalgomSalgom"));
}
