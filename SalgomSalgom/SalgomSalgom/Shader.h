#pragma once


enum class ShaderKey {
	SKYBOX_VS,
	SKYBOX_PS,
	TERRAIN_VS,
	TERRAIN_PS,
	STATICOBJECT_VS,
	STATICOBJECT_PS,
	CHARACTER_VS,
	CHARACTER_PS,
	BODY_VS,
	BODY_PS,
	SPRITE_VS,
	SPRITE_PS,

	TEST_VS,
	TEST_PS,
};

enum class ShaderType {
	VERTEX,
	PIXEL,
};

class Shader
{
public:
	Shader();
	explicit Shader(const wchar_t* filename, ShaderType type);
	~Shader();

	bool Compile();

	inline D3D12_SHADER_BYTECODE GetShaderBytecode()
	{
		return m_Bytecode;
	}

private:
	D3D12_SHADER_BYTECODE m_Bytecode;

	Microsoft::WRL::ComPtr<ID3DBlob> m_ShaderBlob;

	std::wstring m_FilePath;
	std::string m_EntryPointName;
	std::string m_ShaderTarget;
};

extern std::map<ShaderKey, Shader> g_Shaders;
void CompileShaders();
