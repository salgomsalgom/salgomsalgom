﻿#pragma once
#include "GraphicsCore.h"
#include "GpuResource.h"
#include "DescriptorHeap.h"

#include "GameData.h"

#include "Mesh.h"
#include "Texture.h"
#include "Animation.h"

using namespace DirectX;
enum class PSOType;

struct CB_ObjectInfo 
{
	XMFLOAT4X4 m_WorldTransform;
	XMFLOAT4X4 m_TexTransform;
};

struct CB_Sprite
{
	XMFLOAT4X4 m_WorldTransform;
	XMFLOAT4X4 m_TexTransform;
};

struct SkinnedInfo
{
	XMFLOAT4X4 m_BoneTransforms[96];
};

class GraphicsObject abstract
{
protected:
	GraphicsObject();

public:
	virtual ~GraphicsObject();

	virtual PSOType GetPSOType() = 0;

	virtual void SetPosition(const XMFLOAT3& pos);
	virtual void SetRotation(const XMFLOAT3& rot);
	virtual void SetScale(const XMFLOAT3& scl);
	virtual void SetScale(float scl);

	virtual void CreateConstantBuffer() = 0;
	virtual void UpdateConstantBuffer(ID3D12GraphicsCommandList* cl) = 0;

	inline void SetName(const string& name) { m_Name = name; }
	inline string GetName() { return m_Name; }

	inline Mesh* GetMesh() { return m_Mesh; }
	inline Texture* GetTexture() { return m_Texture; }

protected:
	string m_Name;

	XMFLOAT3 m_Position;
	XMFLOAT4 m_Rotation;
	XMFLOAT3 m_Scale;

	XMFLOAT4X4 m_WorldTransform;

	Mesh*		m_Mesh;
	Texture*	m_Texture;

public:
	// 쉐이더로 정보를 보내기 위한 변수들
	CB_ObjectInfo *m_ObjectInfo;
	GpuBuffer m_ObjectInfoCB;
	DescriptorHeap m_DescriptorHeap;

	bool m_Visible = true;
};

class SkyBox : public GraphicsObject {
public:
	SkyBox();
	~SkyBox();

public:
	PSOType GetPSOType() override;

	void SetLocation(const XMFLOAT3& location);

	void CreateConstantBuffer() override;
	void UpdateConstantBuffer(ID3D12GraphicsCommandList* cl) override;

	inline ID3D12DescriptorHeap** GetDescriptorHeapAddress() {
		return m_DescriptorHeap.GetAddressOf();
	}

	inline D3D12_GPU_DESCRIPTOR_HANDLE GetGpuDescriptorHandle() {
		return m_DescriptorHeap->GetGPUDescriptorHandleForHeapStart();
	}

private:

};

class Terrain : public GraphicsObject {
public:
	Terrain();
	~Terrain() { }

	PSOType GetPSOType() override;

	void CreateConstantBuffer() override { };
	void UpdateConstantBuffer(ID3D12GraphicsCommandList* cl) override { };

private:

};

class Character : public GraphicsObject 
{
public:
	Character(int id)
		: m_ID(id)
		, m_SkinnedInfo(nullptr)
		, m_Animation(nullptr)
	{
	}

	Character(int id, MeshKey mesh, TextureKey tex)
		: m_ID(id)
		, m_SkinnedInfo(nullptr)
	{
		m_Mesh = StaticGameData::g_Meshs[mesh];
		m_Texture = StaticGameData::g_Textures[tex];
		m_Animation = new Animation(*StaticGameData::g_Animations[mesh]);
	}

public:
	PSOType GetPSOType() override;

	void SetRotation(XMFLOAT3 rot) {
		// rot : 방향벡터
		rot.y = 0;

		XMVECTOR qua_rot = XMQuaternionRotationRollPitchYaw(0, XMConvertToRadians(rot.y), 0);
		XMStoreFloat4(&m_Rotation, qua_rot);
	}

	void SetRotation(XMFLOAT3 rot, bool turn) {
		rot.x += -XMMath::XM_PIDIV2;
		rot.y += XMMath::XM_PI;
		XMVECTOR qua_rot = XMQuaternionRotationRollPitchYaw(rot.x, rot.y, rot.z);
		XMStoreFloat4(&m_Rotation, qua_rot);
	}

	void CreateConstantBuffer() override;
	void UpdateConstantBuffer(ID3D12GraphicsCommandList* cl) override;

	inline void SetAnimation(const string& name) { 
		auto iter = m_Animation->animationClips.find(name);
		assert(m_Animation->animationClips.end() != iter);

		m_Animation->SetAnimation(name);
	};

	void PlayOnce(const string& name){ m_Animation->PlayOnce(name); }

	void Animate(float time);

public:
	GpuBuffer m_SkinnedInfoCB;

private:
	int m_ID;
	Animation* m_Animation;

	SkinnedInfo* m_SkinnedInfo;
};

class Body : public GraphicsObject
{
public:
	Body(MeshKey mesh, TextureKey tex)
		: m_SkinnedInfo(nullptr)
	{
		m_Mesh = StaticGameData::g_Meshs[mesh];
		m_Texture = StaticGameData::g_Textures[tex];
		m_Animation = StaticGameData::g_Animations[mesh];
	}

public:
	PSOType GetPSOType() override;

	void CreateConstantBuffer() override;
	void UpdateConstantBuffer(ID3D12GraphicsCommandList* cl) override;

	inline void SetAnimation(const string& name) {
		auto iter = m_Animation->animationClips.find(name);
		assert(m_Animation->animationClips.end() != iter);

		m_Animation->SetAnimation(name);
	};

	void PlayOnce(const string& name);

	void Animate(float time);

public:
	GpuBuffer m_SkinnedInfoCB;

private:
	Animation* m_Animation;
	SkinnedInfo* m_SkinnedInfo;

};


class StaticObject : public GraphicsObject 
{
public:
	explicit StaticObject(MeshKey mesh, TextureKey texture);
	~StaticObject() { }

public:
	PSOType GetPSOType() override;

	UINT GetIndexCount() {
		return m_Mesh->m_IndexCount;
	}

	const D3D12_VERTEX_BUFFER_VIEW* GetVertexBufferView() {
		return &(m_Mesh->m_VertexBufferView);
	}

	const D3D12_INDEX_BUFFER_VIEW* GetIndexBufferView() {
		return &(m_Mesh->m_IndexBufferView);
	}

	void CreateConstantBuffer() override;
	void UpdateConstantBuffer(ID3D12GraphicsCommandList* cl);

private:
};

class Sprite : public GraphicsObject
{
public:
	explicit Sprite(float width, float height, TextureKey texture);
	explicit Sprite(TextureKey texture, float pixel_x, float pixel_y);

public:
	bool InMousePointer(const POINT& pointer)
	{
		float half_w = static_cast<float>(m_Texture->GetWidth()) / 2.f;
		float half_h = static_cast<float>(m_Texture->GetHeight()) / 2.f;

		if (pointer.x < m_ClientPos.x - half_w) return false;
		if (pointer.y < m_ClientPos.y - half_h) return false;
		if (pointer.x > m_ClientPos.x + half_w) return false;
		if (pointer.y > m_ClientPos.y + half_h) return false;
		return true;
	}

	void SetPosition(float x, float y);

	void SetScale(float x, float y) 
	{
		m_Scale.x = m_OriginScale.x * x;
		m_Scale.y = m_OriginScale.y * y;
	}

public:
	PSOType GetPSOType() override;

	void CreateConstantBuffer() override;
	void UpdateConstantBuffer(ID3D12GraphicsCommandList* cl) override;

	void UpdateWorldTransform();
	void UpdateTextureTransform();

	void SetTexturePos(float x, float y);
	void SetTextureScl(float x, float y);

private:
	CB_Sprite* m_SpriteInfo;

	XMFLOAT2 m_TexPos;
	XMFLOAT2 m_TexScl;

	POINT m_ClientPos;
	XMFLOAT3 m_OriginScale;
};


#include "SpriteFont.h"
#include "SpriteAnimation.h"