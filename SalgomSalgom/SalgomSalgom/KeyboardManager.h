#pragma once

class InputSystem;


enum class KeyboardEvent : unsigned short {
	KEY_UP_0, KEY_DOWN_0,
	KEY_UP_1, KEY_DOWN_1,
	KEY_UP_2, KEY_DOWN_2,
	KEY_UP_3, KEY_DOWN_3,
	KEY_UP_4, KEY_DOWN_4,
	KEY_UP_5, KEY_DOWN_5,
	KEY_UP_6, KEY_DOWN_6,
	KEY_UP_7, KEY_DOWN_7,
	KEY_UP_8, KEY_DOWN_8,
	KEY_UP_9, KEY_DOWN_9,
	KEY_UP_F1, KEY_DOWN_F1,
	KEY_UP_F2, KEY_DOWN_F2,
	KEY_UP_F3, KEY_DOWN_F3,
	KEY_UP_F4, KEY_DOWN_F4,
	KEY_UP_F5, KEY_DOWN_F5,
	KEY_UP_F6, KEY_DOWN_F6,
	KEY_UP_F7, KEY_DOWN_F7,
	KEY_UP_F8, KEY_DOWN_F8,
	KEY_UP_F9, KEY_DOWN_F9,
	KEY_UP_F10, KEY_DOWN_F10,
	KEY_UP_F11, KEY_DOWN_F11,
	KEY_UP_F12, KEY_DOWN_F12,
	KEY_UP_A, KEY_DOWN_A,
	KEY_UP_B, KEY_DOWN_B,
	KEY_UP_C, KEY_DOWN_C,
	KEY_UP_D, KEY_DOWN_D,
	KEY_UP_E, KEY_DOWN_E,
	KEY_UP_F, KEY_DOWN_F,
	KEY_UP_G, KEY_DOWN_G,
	KEY_UP_H, KEY_DOWN_H,
	KEY_UP_I, KEY_DOWN_I,
	KEY_UP_J, KEY_DOWN_J,
	KEY_UP_K, KEY_DOWN_K,
	KEY_UP_L, KEY_DOWN_L,
	KEY_UP_M, KEY_DOWN_M,
	KEY_UP_N, KEY_DOWN_N,
	KEY_UP_O, KEY_DOWN_O,
	KEY_UP_P, KEY_DOWN_P,
	KEY_UP_Q, KEY_DOWN_Q,
	KEY_UP_R, KEY_DOWN_R,
	KEY_UP_S, KEY_DOWN_S,
	KEY_UP_T, KEY_DOWN_T,
	KEY_UP_U, KEY_DOWN_U,
	KEY_UP_V, KEY_DOWN_V,
	KEY_UP_W, KEY_DOWN_W,
	KEY_UP_X, KEY_DOWN_X,
	KEY_UP_Y, KEY_DOWN_Y,
	KEY_UP_Z, KEY_DOWN_Z,
	KEY_UP_SPACE, KEY_DOWN_SPACE,
	KEY_UP_ENTER, KEY_DOWN_ENTER,

	NONE,
};

typedef unsigned short VKCODE;

class KeyboardManager {
public:
	KeyboardManager() { }
	~KeyboardManager() { }

public:
	static void Initialize();
	static void PushWindowKeyDownEvent(VKCODE vkcode);
	static void PushWindowKeyUpEvent(VKCODE vkcode);
	static KeyboardEvent GetEvent();

public:
	static void Update();

private:
	static constexpr int m_KeyCount = 10 + 12 + 26 + 2;
	static constexpr int m_Code[m_KeyCount] = {
		/* 숫자 */
		0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39,
		VK_F1, VK_F2, VK_F3, VK_F4, VK_F5, VK_F6, VK_F7, VK_F8, VK_F9, VK_F10, VK_F11, VK_F12,
		/* 알파벳 */
		0x41, 0x42,	0x43, 0x44,	0x45, 0x46,	0x47, 0x48,	0x49, 0x4A,
		0x4B, 0x4C, 0x4D, 0x4E, 0x4F, 0x50, 0x51, 0x52, 0x53, 0x54,
		0x55, 0x56, 0x57, 0x58, 0x59, 0x5A,
		/* 특수 키 */
		VK_SPACE, VK_RETURN,
	};

private:
	static bool m_KeyState[m_KeyCount];

	static std::queue<KeyboardEvent> m_EventQueue;
};
