#pragma once
#include "SystemNode.h"

class SystemCommunicator;
 
class TimerSystem : SystemNode
{
	static constexpr float LOCK_FPS = 60.f;
	static constexpr float LOCK_SPF = 1.f / LOCK_FPS;
	static constexpr int FPS_SAMPLE_COUNT = 60;

	static bool s_Instantiated;

public:
	explicit TimerSystem(SystemCommunicator* const communicator);
	~TimerSystem();

public:
	void Initialize() override;

	void Tick();
	float GetElapsedTime();
	float GetBaseTime();
	float GetFrameRate();

private:
	inline void UpdateFrameRate(float elapsed_time);

private:
	float m_TimeScale;

	__int64 m_EntryCount;
	__int64 m_CurrCount;
	__int64 m_LastCount;

	float m_ElapsedTime;

public:
	// for calculate frame rate
	std::array<float, FPS_SAMPLE_COUNT> m_TimeSample;
	int m_SampleIdx;
	float m_CurrFrameRate;
	float m_FrameRate;
};
