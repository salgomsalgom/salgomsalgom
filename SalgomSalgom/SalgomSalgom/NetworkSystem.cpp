#include "pch.h"
#include "NetworkSystem.h"
#include "InputSystem.h"

#include "Game.h"
#include "WindowApp.h"
#include "Scene.h"
#include "LobbyScene.h"

using namespace std;

bool NetworkSystem::s_Instantiated = false;


std::thread g_RecvThread{ NetworkSystem::RecvPacket };

int			g_Myid;
SOCKET		g_Sock;
bool		g_BufferFlag;

unsigned long SERVER_ADDR = inet_addr("127.0.0.1");

void processData(char* buf, DWORD io_byte);
void sendPacket(void* packet);
void mapDataProcess(char* buf, DWORD io_byte);
void processPacket(char* ptr);

void display_error(const char* msg, int error)
{
	WCHAR* lpMsgBuf;
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	yhl::dout << msg;
	yhl::dout << L"에러 " << lpMsgBuf << yhl::fls;
	exit(-1);
	LocalFree(lpMsgBuf);
}

NetworkSystem::NetworkSystem(SystemCommunicator* const communicator) :
	SystemNode(communicator)
{
	assert(!s_Instantiated);
	s_Instantiated = true;
}

NetworkSystem::~NetworkSystem()
{
}

void sendPacket(void* packet) 
{
	WSABUF send_wsabuf[1];
	DWORD flags = 0;
	DWORD send_bytes{};
	char buf[MAX_BUFFER_SIZE]{};
	char* p = reinterpret_cast<char*>(packet);
	send_wsabuf[0].buf = buf;
	send_wsabuf[0].len = p[0];
	memcpy(&buf, p, p[0]);
	WSASend(g_Sock, send_wsabuf, 1, &send_bytes, 0, 0, 0);
	if (send_bytes == SOCKET_ERROR) {
		display_error("send", WSAGetLastError());
		return;
	}
}

void NetworkSystem::SendInputPacket(PlayerInput input)
{
	CS_PacketInput p;
	p.size = sizeof(p);
	p.type = C2S_INPUT;
	p.input = input;
	sendPacket(&p);
}

void NetworkSystem::SendMousePacket(float rotate_x, float rotate_y)
{
	CS_PacketMouse p;
	p.size = sizeof(p);
	p.type = C2S_MOUSE_MOVE;
	p.delta.x = rotate_x;
	p.delta.y = rotate_y;

	sendPacket(&p);
}

void NetworkSystem::SendRedayPacket(bool ready)
{
	CS_PacketReady p;
	p.size = sizeof(p);
	p.type = C2S_READY;
	p.is_ready = ready;
	sendPacket(&p);
}

void NetworkSystem::SendDataRequestPacket() {
	CS_PacketDataRequest p;
	p.size = sizeof(p);
	p.type = C2S_DATA_REQUEST;
	
	sendPacket(&p);
}

void NetworkSystem::SendAttackPacket() {
	CS_PacketAttack p;
	p.size = sizeof(p);
	p.type = C2S_WITCH_ATTACK;
	
	sendPacket(&p);
}

void NetworkSystem::SendGemPickUp() {
	CS_PacketPickGem p;
	p.size = sizeof(p);
	p.type = C2S_GEM_PICKUP;
	
	sendPacket(&p);
}

void NetworkSystem::SendGemThrow(){
	CS_PacketThrowGem p;
	p.size = sizeof(p);
	p.type = C2S_GEM_THROW;
	sendPacket(&p);
}

void NetworkSystem::SendRenderRequest() {
	CS_PacketRenderRequest p;
	p.size = sizeof(p);
	p.type = C2S_RENDER_START_REQUEST;

	sendPacket(&p);
}

void NetworkSystem::SendUseSkill2() {
	CS_PacketSkill2 p;
	p.size = sizeof(CS_PacketSkill2);
	p.type = C2S_SKILL2;
	sendPacket(&p);
}

void NetworkSystem::Send_L_Login(const char* name, const char* pw) {
	
	CS_L_PacketLogin p;
	p.size = sizeof(CS_L_PacketLogin);
	p.type = C2S_L_LOGIN;
	strcpy_s(p.name, name);
	strcpy_s(p.password, pw);
	
	sendPacket(&p);
}

void NetworkSystem::Send_L_Join(const char* name, const char* pw) {
	CS_L_PacketJoin p;
	p.size = sizeof(CS_L_PacketJoin);
	p.type = C2S_L_JOININ;
	strcpy_s(p.name, name);
	strcpy_s(p.password, pw);
	sendPacket(&p);
}

void mapDataProcess(char* buf, DWORD io_byte) {
	char* pos = buf;
	static int* ptr{};
	int in_packet_size = 0;
	static int remain_packet_size = 0;
	static char packet_buffer[WORLD_BUFFER];
	int rest_byte = io_byte + remain_packet_size;

	if (remain_packet_size == 0) {
		ptr = reinterpret_cast<int*>(pos);
	}

	while (rest_byte > 0) {
		if (0 == in_packet_size) in_packet_size = ptr[0];
		if (rest_byte + remain_packet_size >= in_packet_size) {
			memcpy(packet_buffer + remain_packet_size, pos, in_packet_size - remain_packet_size);
			pos += in_packet_size- remain_packet_size;
			ptr = reinterpret_cast<int*>(pos);
			rest_byte -= in_packet_size;
			in_packet_size = 0;
			processPacket(packet_buffer);
			remain_packet_size = 0;
			
		}
		else {
			memcpy(packet_buffer + remain_packet_size, pos, rest_byte);
			remain_packet_size += rest_byte;
			ptr = reinterpret_cast<int*>(packet_buffer);
			rest_byte = 0;
		}
	}
}


void NetworkSystem::RecvPacket() {

	while (true) {
		WSABUF recv_wsabuf[1];
		DWORD flags = 0;
		DWORD recv_bytes{};
		char* buf = new char[WORLD_BUFFER];
		recv_wsabuf[0].buf = buf;
		recv_wsabuf[0].len = WORLD_BUFFER;
		WSARecv(g_Sock, recv_wsabuf, 1, &recv_bytes, &flags, 0, 0);

		if (recv_bytes == SOCKET_ERROR) {
			display_error("recv", WSAGetLastError());
			return;
		}
		else if (recv_bytes != 0) mapDataProcess(buf, recv_bytes);

		delete[] buf;
	}
}

void processPacket(char* ptr) { //패킷 넘겨주기 and 일처리 여기

	SC_PacketLoginOk* my_packet = reinterpret_cast<SC_PacketLoginOk*>(ptr);

	switch (my_packet->type) {
	case S2C_LOGIN_OK:
	{
		// 배틀씬으로 가라는 것
		SC_PacketLoginOk* my_packet = reinterpret_cast<SC_PacketLoginOk*>(ptr);
		if (my_packet->size != sizeof(SC_PacketLoginOk))break;
		g_Myid = my_packet->id;	// 내 아이디 설정
		NetworkSystem::SendDataRequestPacket();		// S2C_MAP_DATA 요청
		WriteID(g_Myid);	
		break;
	}
	case S2C_MAP_DATA:
	{
		SC_PacketStart* my_packet = reinterpret_cast<SC_PacketStart*>(ptr);
		int num_objects = my_packet->number_of_objects;

		// 일부로 메모리 해제 안함, Scene에서 객체 모두 생성 후 메모리 해제해줌
		StaticObjectInfo* static_objects = new StaticObjectInfo[num_objects];
		CollisionBoxInfo* boxs = new CollisionBoxInfo[num_objects];

		for (int i = 0; i < num_objects; ++i) {
			static_objects[i].m_Position.x = my_packet->objects[i].m_position.x;
			static_objects[i].m_Position.y = my_packet->objects[i].m_position.y;
			static_objects[i].m_Position.z = my_packet->objects[i].m_position.z;

			auto degree_rot = my_packet->objects[i].m_rotation;
			float rot_x = DirectX::XMConvertToRadians(degree_rot.x);
			float rot_y = DirectX::XMConvertToRadians(degree_rot.y);
			float rot_z = DirectX::XMConvertToRadians(degree_rot.z);
			static_objects[i].m_Rotation = DirectX::XMFLOAT3(rot_x, rot_y, rot_z);

			static_objects[i].m_Scale.x = my_packet->objects[i].m_scale.x;
			static_objects[i].m_Scale.y = my_packet->objects[i].m_scale.y;
			static_objects[i].m_Scale.z = my_packet->objects[i].m_scale.z;

			// debug
			boxs[i].m_Size.x = my_packet->objects[i].m_cbox.x;
			boxs[i].m_Size.y = my_packet->objects[i].m_cbox.y;
			boxs[i].m_Size.z = my_packet->objects[i].m_cbox.z;

			static_objects[i].m_Name = my_packet->objects[i].name;
		}

		MoveMapData(num_objects, static_objects);
		MoveCollisionBoxData(num_objects, boxs);
		 
		NetworkSystem::SendRenderRequest();	// S2C_RENDERSTART 요청
		break;
	}
	case S2C_RENDERSTART:
	{
		SC_PacketRenderStart* my_packet = reinterpret_cast<SC_PacketRenderStart*>(ptr);

		BattleData battle_data;
		battle_data.numPlayer = my_packet->number_of_player;
		battle_data.soulCount = my_packet->soul_left;

		// Player
		for (int i = 0; i < battle_data.numPlayer; ++i) {
			auto& player = my_packet->players[i];

			battle_data.players[i].m_ID = player.m_id;
			battle_data.players[i].m_Role = (player.m_role == CH_ROLE::R_NORMAL) ? ChRole::NORMAL : ChRole::WITCH;
			memcpy(&(battle_data.players[i].m_Position), &(player.m_location), sizeof(XMFLOAT3));
			memcpy(&(battle_data.players[i].m_Rotation), &(player.m_rotation), sizeof(XMFLOAT3));
			memcpy(&(battle_data.players[i].m_Rotation), &(player.m_dirVector), sizeof(XMFLOAT3));
			// 방향벡터?

			// skill 초기화는 struct 내에서 함
		}

		// Gem
		for (int i = 0; i < 16; ++i) {
			auto& server_gem = my_packet->gems[i];
			auto& gem = battle_data.gems[i];

			gem.m_Color = server_gem.m_color;
			memcpy(&(gem.m_Position), &(server_gem.m_location), sizeof(XMFLOAT3));
			
			gem.m_Transparent = server_gem.transparency;
		}

		// WitchStone
		for (int i = 0; i < 4; ++i) {
			battle_data.witchStoneState[i].m_Alive = my_packet->witchStoneState[i].m_alive;
			battle_data.witchStoneState[i].m_Color = my_packet->witchStoneState[i].m_color;
		}

		// 애니메이션 스테이터스도 있음
		InitBattleData(battle_data);

		GameCore::PushSystemMessage(GameCore::SystemMessage::BATTLE_START);

		break;
	}
	case S2C_LEAVE:
	{
		SC_PacketLeave* my_packet = reinterpret_cast<SC_PacketLeave*>(ptr);
		int other_id = my_packet->id;
		break;
	}
	case S2C_UPDATE:
	{
		SC_PacketUpdate* my_packet = reinterpret_cast<SC_PacketUpdate*>(ptr);

		UpdateData update_data;
		
		for (int i = 0; i < GAME_START_NUMBER; ++i) {
			auto& p_info = my_packet->players[i];
			auto& info = update_data.players[i];

			info.m_ID = p_info.m_id;
			memcpy(&info.m_Position, &p_info.m_location, sizeof(XMFLOAT3));
			memcpy(&info.m_Rotation, &p_info.m_rotation, sizeof(XMFLOAT3));
			memcpy(&info.m_DirVector, &p_info.m_dirVector, sizeof(XMFLOAT3));
			info.m_Role = (CH_ROLE::R_WITCH == p_info.m_role) ? ChRole::WITCH : ChRole::NORMAL;
			info.m_RespawnTime = p_info.m_respawn_time;
			info.gem = my_packet->players[i].gem_picked;

			// 스킬
			auto& s1 = my_packet->skill_1[i];
			update_data.skill1[i].m_Exist = s1.m_exist;
			update_data.skill1[i].m_CoolTime = s1.coolTime;
			update_data.skill1[i].m_DurationTime = s1.duration;
			memcpy(&(update_data.skill1[i].m_Position), &(s1.m_location), sizeof(XMFLOAT3));
			update_data.skill1[i].m_Stat = s1.m_Stat;

			auto& s2 = my_packet->skill_2[i];
			update_data.skill2[i].m_Exist = s2.m_exist;
			update_data.skill2[i].m_CoolTime = s2.coolTime;
			update_data.skill2[i].m_DurationTime = s2.duration;
			memcpy(&(update_data.skill2[i].m_Position), &(s2.m_location), sizeof(XMFLOAT3));
			update_data.skill2[i].m_Stat = s2.m_Stat;
		}

		// 젬 방향 업데이트도 해야함
		for (int i = 0; i < 16; ++i) {
			auto& server_gem = my_packet->gems[i];
			auto& gem = update_data.gems[i];

			gem.m_Color = server_gem.m_color;
			memcpy(&(gem.m_Position), &(server_gem.m_location), sizeof(XMFLOAT3));
			memcpy(&(gem.m_Rotation), &(server_gem.m_rot), sizeof(XMFLOAT3));
			gem.m_Player_id = server_gem.player_id;

			// 투명 관련
			gem.m_Transparent = server_gem.transparency;
		}
		
		WriteUpdateData(update_data);
		break;
	}

	case S2C_ANIMATION_CHANGE:
	{
		SC_PacketAnimationChange* my_packet = reinterpret_cast<SC_PacketAnimationChange*>(ptr);
		auto id = my_packet->id;
		auto stat = my_packet->animation_status;

		UpdateAniEvent(id, stat);
		break;
	}

	case S2C_SOULCHANGE:
	{
		SC_PacketSoulChagne* my_packet = reinterpret_cast<SC_PacketSoulChagne*>(ptr);
		WriteSoulCount(my_packet->soul_left);
		break;
	}

	case S2C_WITCHSTONE_ST_CHANGE:
	{
		SC_PakcetWitchStone_ST_Change* my_packet = reinterpret_cast<SC_PakcetWitchStone_ST_Change*>(ptr);

		WitchStoneInfo info[4];
		for (int i = 0; i < 4; ++i)
		{
			auto& server_ws = my_packet->witchStoneState[i];
			info[i].m_Color = server_ws.m_color;
			info[i].m_Alive = server_ws.m_alive;
		}

		WriteWitchStoneInfo(info);
		UpdateWitchStone();
		break;
	}

	case S2C_CHARACTER_POWER_CHANGE:
	{
		SC_Packet_CH_PowerChange* my_packet = reinterpret_cast<SC_Packet_CH_PowerChange*>(ptr);
		UpdatePowerEvent(my_packet->skill_power, my_packet->speed_power);
		break;
	}

	case S2C_RESULT:
	{
		SOCKADDR_IN serveraddr;
		int retval{};
		closesocket(g_Sock);	// 로비 서버의 연결을 먼저 끊어준다.
		g_Sock = WSASocket(AF_INET, SOCK_STREAM, 0, 0, 0, WSA_FLAG_OVERLAPPED);
		ZeroMemory(&serveraddr, sizeof(serveraddr));
		serveraddr.sin_family = AF_INET;
		serveraddr.sin_addr.s_addr = SERVER_ADDR;

		serveraddr.sin_port = htons(LOBBY_PORT);
		retval = WSAConnect(g_Sock, (sockaddr*)&serveraddr, sizeof(serveraddr), 0, 0, 0, 0);

		// 씬 전환
		SC_PacketResult* my_packet = reinterpret_cast<SC_PacketResult*>(ptr);
		auto msg = (my_packet->win == R_NORMAL) ? GameCore::SystemMessage::NORMAL_WIN : GameCore::SystemMessage::WITCH_WIN;
		GameCore::PushSystemMessage(msg);
		break;
	}
	// --------------------------Lobby---------------------------//
	case S2C_L_JOIN_OK:
	{
		SC_L_PacketJoinOK* my_packet = reinterpret_cast<SC_L_PacketJoinOK*>(ptr);
		GameCore::PushSystemMessage(GameCore::SystemMessage::JOIN_IN_SUCCESS);

		break;
	}
	case S2C_L_JOIN_FAIL:
	{
		SC_L_PacketJoinFail* my_packet = reinterpret_cast<SC_L_PacketJoinFail*>(ptr);

		// 아이디가 겹침 다른거

		break;
	}

	case S2C_L_LOGIN_OK:
	{
		SC_L_PacketLoginOk* my_packet = reinterpret_cast<SC_L_PacketLoginOk*>(ptr);
		WriteUserName(my_packet->name);
		GameCore::PushSystemMessage(GameCore::SystemMessage::LOBBY_START);
		break;
	}

	case  S2C_L_LOGIN_FAIL:
	{
		SC_L_PacketLoginFail* my_packet = reinterpret_cast<SC_L_PacketLoginFail*>(ptr);

		// 실패 // 재입력해서 보내기 // 여기서 계속 루프

		break;
	}

	case S2C_START: {
		SOCKADDR_IN serveraddr;
		int retval{};
		closesocket(g_Sock);	// 로비 서버의 연결을 먼저 끊어준다.
		g_Sock = WSASocket(AF_INET, SOCK_STREAM, 0, 0, 0, WSA_FLAG_OVERLAPPED);
		ZeroMemory(&serveraddr, sizeof(serveraddr));
		serveraddr.sin_family = AF_INET;
		serveraddr.sin_addr.s_addr = SERVER_ADDR;

		serveraddr.sin_port = htons(SERVER_PORT);
		retval = WSAConnect(g_Sock, (sockaddr*)&serveraddr, sizeof(serveraddr), 0, 0, 0, 0);

		char name[MAX_ID_LEN] = " ";

		CS_PacketLogin l_packet;
		l_packet.size = sizeof(l_packet);
		l_packet.type = C2S_LOGIN;
		strcpy_s(l_packet.name, name);
		sendPacket(&l_packet);
		break;
	}


	default:
		yhl::dout << "unknown packet" << yhl::fls;
		break;
	}
}


void NetworkSystem::Initialize()
{
	yhl::dout << "Initialized Network System." << yhl::fls;
	int retval;
	WSADATA wsa;
	WSAStartup(MAKEWORD(2, 2), &wsa);

	//socket()
	g_Sock = WSASocket(AF_INET, SOCK_STREAM, 0, 0, 0, WSA_FLAG_OVERLAPPED);
	if (g_Sock == INVALID_SOCKET) display_error("socket()",WSAGetLastError());

	// 서버 아이피 읽어오기
	ifstream in{ "server_ip.txt" };
	if (in) {
		string str_server_addr;
		in >> str_server_addr;

		SERVER_ADDR = inet_addr(str_server_addr.c_str());
	}

	//connect()
	SOCKADDR_IN serveraddr;

	ZeroMemory(&serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = SERVER_ADDR;

	serveraddr.sin_port = htons(LOBBY_PORT);
	retval = WSAConnect(g_Sock, (sockaddr*)&serveraddr, sizeof(serveraddr), 0, 0, 0, 0);
	if (retval == SOCKET_ERROR) display_error("bind()",WSAGetLastError());
	yhl::dout << "conneted" << yhl::fls;

}

void NetworkSystem::Update()
{
	// Scene의 객체에 데이터를 업데이트 한다.
}
