#pragma once
#include "SystemCommunicator.h"

class Scene;
enum class SceneKey;

namespace GameCore {

	enum class SystemMessage
	{
		JOIN_IN_SUCCESS,

		BATTLE_START,
		LOBBY_START,
		NORMAL_WIN,
		WITCH_WIN,
	};

	void PushSystemMessage(SystemMessage msg);


	class Game abstract
	{
	public:
		static SystemCommunicator s_Systems;

	protected:
		std::vector<Scene*>		m_Scenes;
		Scene*					m_CurrScene;

		// std::vector<Scene*>		m_SceneStack;

	public:
		Game() { }		// ��� �ʱ�ȭ
		virtual ~Game() { }

		virtual void StartUp() = 0;
		virtual void CleanUp() = 0;

		virtual void Update(float frame_time) = 0;
		virtual void HandleInput() = 0;

		Scene* GetCurrScene() { return m_CurrScene; }

		void ChangeScene(SceneKey key);
	};

	// flatform interface
	void Initialize(Game& game);

	void Update(Game& game);
	
	void Terminate(Game& game);
}

class TestGame : public GameCore::Game {
public:
	TestGame() { }
	virtual ~TestGame() { }

	void StartUp() override;		// �� ����
	void CleanUp() override;		// �� �Ҹ�

	void Update(float frame_time) override;				// ���� �� ������Ʈ
	void HandleInput() override;		// input ó��
};
