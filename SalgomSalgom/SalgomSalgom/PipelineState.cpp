#include "pch.h"
#include "PipelineState.h"

#include "GraphicsCore.h"
using namespace Graphics;

#include "Shader.h"
#include "Vertex.h"

#pragma region WrappedStruct
struct WRAPPED_RASTERIZER_DESC : public D3D12_RASTERIZER_DESC {
	WRAPPED_RASTERIZER_DESC() 
	{
		ZeroMemory(this, sizeof(D3D12_RASTERIZER_DESC));
	}

	explicit WRAPPED_RASTERIZER_DESC(
		D3D12_FILL_MODE fill_mode,
		D3D12_CULL_MODE cull_mode,
		BOOL front_counter_clockwise,
		INT depth_bias,
		FLOAT depth_bias_clamp,
		FLOAT slope_scaled_depth_bias,
		BOOL depth_clip_enable,
		BOOL multisample_enable,
		BOOL antialiased_line_enable,
		UINT forced_sample_count,
		D3D12_CONSERVATIVE_RASTERIZATION_MODE conservative_raster
	)
	{
		FillMode = fill_mode;
		CullMode = cull_mode;
		FrontCounterClockwise = front_counter_clockwise;
		DepthBias = depth_bias;
		DepthBiasClamp = depth_bias_clamp;
		SlopeScaledDepthBias = slope_scaled_depth_bias;
		DepthClipEnable = depth_clip_enable;
		MultisampleEnable = multisample_enable;
		AntialiasedLineEnable = antialiased_line_enable;
		ForcedSampleCount = forced_sample_count;
		ConservativeRaster = conservative_raster;
	}
};

struct WRAPPED_SAMPLE_DESC : public DXGI_SAMPLE_DESC
{
	WRAPPED_SAMPLE_DESC()
	{
		ZeroMemory(this, sizeof(DXGI_SAMPLE_DESC));
	}

	explicit WRAPPED_SAMPLE_DESC(UINT count, UINT quality)
	{
		Count = count;
		Quality = quality;
	}
};

struct WRAPPED_DEPTH_STENCILOP_DESC : D3D12_DEPTH_STENCILOP_DESC
{
	WRAPPED_DEPTH_STENCILOP_DESC() 
	{
		StencilFailOp		= D3D12_STENCIL_OP_KEEP;
		StencilDepthFailOp	= D3D12_STENCIL_OP_KEEP;
		StencilPassOp		= D3D12_STENCIL_OP_KEEP;
		StencilFunc			= D3D12_COMPARISON_FUNC_NEVER;
	}
	
	explicit WRAPPED_DEPTH_STENCILOP_DESC(
		D3D12_STENCIL_OP failOp, 
		D3D12_STENCIL_OP depthFailOp,
		D3D12_STENCIL_OP passOp,
		D3D12_COMPARISON_FUNC comparisonFunc) 
	{
		StencilFailOp		= failOp;
		StencilDepthFailOp	= depthFailOp;
		StencilPassOp		= passOp;
		StencilFunc			= comparisonFunc;
	}
};

struct WRAPPED_DEPTH_STENCIL_DESC : D3D12_DEPTH_STENCIL_DESC
{
	WRAPPED_DEPTH_STENCIL_DESC() {
		ZeroMemory(this, sizeof(WRAPPED_DEPTH_STENCIL_DESC));
	}

	explicit WRAPPED_DEPTH_STENCIL_DESC(
		BOOL depthEnable, D3D12_DEPTH_WRITE_MASK depthWriteMask, D3D12_COMPARISON_FUNC depthFunc,
		BOOL stencilEnable, UINT8 stencilReadMask, UINT8 stencilWriteMask,
		const WRAPPED_DEPTH_STENCILOP_DESC& frontFace, const WRAPPED_DEPTH_STENCILOP_DESC& backFace)
	{
		DepthEnable = depthEnable;
		DepthWriteMask = depthWriteMask;
		DepthFunc = depthFunc;
		StencilEnable = stencilEnable;
		StencilReadMask = stencilReadMask;
		StencilWriteMask = stencilWriteMask;
		FrontFace = frontFace;
		BackFace = backFace;
	}
};
#pragma endregion WrappedStruct

GraphicsPSO::GraphicsPSO(const PSOType type) :
	m_Type(type),
	m_PipelineState()
{
}


void GraphicsPSO::Create(ID3D12Device* device, ID3D12RootSignature* rootSig)
{
	D3D12_GRAPHICS_PIPELINE_STATE_DESC pso_desc;
	ZeroMemory(&pso_desc, sizeof(pso_desc));		// 이거 안 해주면 터짐

	switch (m_Type) {
	case PSOType::SKYBOX: {
		D3D12_BLEND_DESC blend_desc;
		ZeroMemory(&blend_desc, sizeof(blend_desc));
		blend_desc.AlphaToCoverageEnable = FALSE;
		blend_desc.IndependentBlendEnable = FALSE;
		blend_desc.RenderTarget[0].BlendEnable = FALSE;
		blend_desc.RenderTarget[0].LogicOpEnable = FALSE;
		blend_desc.RenderTarget[0].SrcBlend = D3D12_BLEND_ONE;
		blend_desc.RenderTarget[0].DestBlend = D3D12_BLEND_ZERO;
		blend_desc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
		blend_desc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
		blend_desc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
		blend_desc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
		blend_desc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
		blend_desc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;

		D3D12_DEPTH_STENCIL_DESC depth_stencil_desc = WRAPPED_DEPTH_STENCIL_DESC(
				FALSE, D3D12_DEPTH_WRITE_MASK_ZERO, D3D12_COMPARISON_FUNC_NEVER,
				FALSE, static_cast<UINT8>(0x00), static_cast<UINT8>(0x00), 
				WRAPPED_DEPTH_STENCILOP_DESC(), WRAPPED_DEPTH_STENCILOP_DESC()
		);

		pso_desc.pRootSignature = rootSig;
		pso_desc.VS = g_Shaders[ShaderKey::SKYBOX_VS].GetShaderBytecode();
		pso_desc.PS = g_Shaders[ShaderKey::SKYBOX_PS].GetShaderBytecode();
		pso_desc.RasterizerState = WRAPPED_RASTERIZER_DESC(D3D12_FILL_MODE_SOLID, D3D12_CULL_MODE_BACK, FALSE, 0, 0.f, 0.f, TRUE, FALSE, FALSE, 0, D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF);
		pso_desc.BlendState = blend_desc;
		pso_desc.DepthStencilState = depth_stencil_desc;
		pso_desc.InputLayout = PosTex::GetInputLayoutDesc();
		pso_desc.SampleMask = UINT_MAX;
		pso_desc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		pso_desc.NumRenderTargets = 1;
		pso_desc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
		pso_desc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
		pso_desc.SampleDesc = WRAPPED_SAMPLE_DESC(1, 0);
		break;
	}
	case PSOType::TERRAIN: {
		D3D12_BLEND_DESC blend_desc;
		ZeroMemory(&blend_desc, sizeof(blend_desc));
		blend_desc.AlphaToCoverageEnable = FALSE;
		blend_desc.IndependentBlendEnable = FALSE;
		blend_desc.RenderTarget[0].BlendEnable = FALSE;
		blend_desc.RenderTarget[0].LogicOpEnable = FALSE;
		blend_desc.RenderTarget[0].SrcBlend = D3D12_BLEND_ONE;
		blend_desc.RenderTarget[0].DestBlend = D3D12_BLEND_ZERO;
		blend_desc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
		blend_desc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
		blend_desc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
		blend_desc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
		blend_desc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
		blend_desc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;

		D3D12_DEPTH_STENCIL_DESC depth_stencil_desc = WRAPPED_DEPTH_STENCIL_DESC(
			TRUE, D3D12_DEPTH_WRITE_MASK_ALL, D3D12_COMPARISON_FUNC_LESS,
			FALSE, static_cast<UINT8>(0x00), static_cast<UINT8>(0x00),
			WRAPPED_DEPTH_STENCILOP_DESC(), WRAPPED_DEPTH_STENCILOP_DESC()
		);

		pso_desc.pRootSignature = rootSig;
		pso_desc.VS = g_Shaders[ShaderKey::TERRAIN_VS].GetShaderBytecode();
		pso_desc.PS = g_Shaders[ShaderKey::TERRAIN_PS].GetShaderBytecode();
		pso_desc.RasterizerState = WRAPPED_RASTERIZER_DESC(
			D3D12_FILL_MODE_SOLID, D3D12_CULL_MODE_BACK, TRUE, 0, 0.f, 0.f, 
			TRUE, FALSE, FALSE, 0, D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF);
		pso_desc.BlendState = blend_desc;
		pso_desc.DepthStencilState = depth_stencil_desc;
		pso_desc.InputLayout = TerrainVertex::GetInputLayoutDesc();
		pso_desc.SampleMask = UINT_MAX;
		pso_desc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		pso_desc.NumRenderTargets = 1;
		pso_desc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
		pso_desc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
		pso_desc.SampleDesc = WRAPPED_SAMPLE_DESC(1, 0);
		break;
	}
	case PSOType::STATIC_OBJECT : {
		D3D12_BLEND_DESC blend_desc;
		ZeroMemory(&blend_desc, sizeof(blend_desc));
		blend_desc.AlphaToCoverageEnable					= FALSE;
		blend_desc.IndependentBlendEnable					= FALSE;
		blend_desc.RenderTarget[0].BlendEnable				= TRUE;
		blend_desc.RenderTarget[0].LogicOpEnable			= FALSE;
		blend_desc.RenderTarget[0].SrcBlend					= D3D12_BLEND_SRC_ALPHA;
		blend_desc.RenderTarget[0].DestBlend				= D3D12_BLEND_INV_SRC_ALPHA;
		blend_desc.RenderTarget[0].BlendOp					= D3D12_BLEND_OP_ADD;
		blend_desc.RenderTarget[0].SrcBlendAlpha			= D3D12_BLEND_SRC_ALPHA;
		blend_desc.RenderTarget[0].DestBlendAlpha			= D3D12_BLEND_INV_SRC_ALPHA;
		blend_desc.RenderTarget[0].BlendOpAlpha				= D3D12_BLEND_OP_ADD;
		blend_desc.RenderTarget[0].LogicOp					= D3D12_LOGIC_OP_NOOP;
		blend_desc.RenderTarget[0].RenderTargetWriteMask	= D3D12_COLOR_WRITE_ENABLE_ALL;

		D3D12_DEPTH_STENCIL_DESC depth_stencil_desc = WRAPPED_DEPTH_STENCIL_DESC(
			TRUE, D3D12_DEPTH_WRITE_MASK_ALL, D3D12_COMPARISON_FUNC_LESS,
			FALSE, static_cast<UINT8>(0x00), static_cast<UINT8>(0x00),
			WRAPPED_DEPTH_STENCILOP_DESC(), WRAPPED_DEPTH_STENCILOP_DESC()
		);

		pso_desc.pRootSignature = rootSig;
		pso_desc.VS = g_Shaders[ShaderKey::STATICOBJECT_VS].GetShaderBytecode();
		pso_desc.PS = g_Shaders[ShaderKey::STATICOBJECT_PS].GetShaderBytecode();
		pso_desc.RasterizerState = WRAPPED_RASTERIZER_DESC(
			D3D12_FILL_MODE_SOLID, D3D12_CULL_MODE_NONE, FALSE, 0, 0.f, 0.f, 
			TRUE, FALSE, FALSE, 0, D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF);
		pso_desc.BlendState = blend_desc;
		pso_desc.DepthStencilState = depth_stencil_desc;
		pso_desc.InputLayout = StaticVertex::GetInputLayoutDesc();
		pso_desc.SampleMask = UINT_MAX;
		pso_desc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		pso_desc.NumRenderTargets = 1;
		pso_desc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
		pso_desc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
		pso_desc.SampleDesc = WRAPPED_SAMPLE_DESC(1, 0);
		break;
	}
	case PSOType::COLLISION_BOX: {		// 사용 X
		D3D12_BLEND_DESC blend_desc;
		ZeroMemory(&blend_desc, sizeof(blend_desc));
		blend_desc.AlphaToCoverageEnable = FALSE;
		blend_desc.IndependentBlendEnable = FALSE;
		blend_desc.RenderTarget[0].BlendEnable = TRUE;
		blend_desc.RenderTarget[0].LogicOpEnable = FALSE;
		blend_desc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
		blend_desc.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
		blend_desc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
		blend_desc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_SRC_ALPHA;
		blend_desc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_INV_SRC_ALPHA;
		blend_desc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
		blend_desc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
		blend_desc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;

		D3D12_DEPTH_STENCIL_DESC depth_stencil_desc = WRAPPED_DEPTH_STENCIL_DESC(
			TRUE, D3D12_DEPTH_WRITE_MASK_ALL, D3D12_COMPARISON_FUNC_LESS,
			FALSE, static_cast<UINT8>(0x00), static_cast<UINT8>(0x00),
			WRAPPED_DEPTH_STENCILOP_DESC(), WRAPPED_DEPTH_STENCILOP_DESC()
		);

		pso_desc.pRootSignature = rootSig;
		pso_desc.VS = g_Shaders[ShaderKey::STATICOBJECT_VS].GetShaderBytecode();
		pso_desc.PS = g_Shaders[ShaderKey::STATICOBJECT_PS].GetShaderBytecode();
		pso_desc.RasterizerState = WRAPPED_RASTERIZER_DESC(
			D3D12_FILL_MODE_SOLID, D3D12_CULL_MODE_BACK, FALSE, 0, 0.f, 0.f,
			TRUE, FALSE, FALSE, 0, D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF);
		pso_desc.BlendState = blend_desc;
		pso_desc.DepthStencilState = depth_stencil_desc;
		pso_desc.InputLayout = StaticVertex::GetInputLayoutDesc();
		pso_desc.SampleMask = UINT_MAX;
		pso_desc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		pso_desc.NumRenderTargets = 1;
		pso_desc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
		pso_desc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
		pso_desc.SampleDesc = WRAPPED_SAMPLE_DESC(1, 0);
		break;
	}
	case PSOType::TEST: {
		D3D12_BLEND_DESC blend_desc;
		ZeroMemory(&blend_desc, sizeof(blend_desc));
		blend_desc.AlphaToCoverageEnable = FALSE;
		blend_desc.IndependentBlendEnable = FALSE;
		blend_desc.RenderTarget[0].BlendEnable = FALSE;
		blend_desc.RenderTarget[0].LogicOpEnable = FALSE;
		blend_desc.RenderTarget[0].SrcBlend = D3D12_BLEND_ONE;
		blend_desc.RenderTarget[0].DestBlend = D3D12_BLEND_ZERO;
		blend_desc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
		blend_desc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
		blend_desc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
		blend_desc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
		blend_desc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
		blend_desc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;

		D3D12_DEPTH_STENCIL_DESC depth_stencil_desc = WRAPPED_DEPTH_STENCIL_DESC(
			TRUE, D3D12_DEPTH_WRITE_MASK_ALL, D3D12_COMPARISON_FUNC_LESS,
			FALSE, static_cast<UINT8>(0x00), static_cast<UINT8>(0x00),
			WRAPPED_DEPTH_STENCILOP_DESC(), WRAPPED_DEPTH_STENCILOP_DESC()
		);

		D3D12_INPUT_ELEMENT_DESC input_ele[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
			{ "COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		};

		D3D12_INPUT_LAYOUT_DESC input_layout_desc;
		input_layout_desc.NumElements = 2;
		input_layout_desc.pInputElementDescs = input_ele;

		pso_desc.pRootSignature = rootSig;
		pso_desc.VS = g_Shaders[ShaderKey::TEST_VS].GetShaderBytecode();
		pso_desc.PS = g_Shaders[ShaderKey::TEST_PS].GetShaderBytecode();
		pso_desc.RasterizerState = WRAPPED_RASTERIZER_DESC(
			D3D12_FILL_MODE_SOLID, D3D12_CULL_MODE_BACK, FALSE, 0, 0.f, 0.f,
			TRUE, FALSE, FALSE, 0, D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF);
		pso_desc.BlendState = blend_desc;
		pso_desc.DepthStencilState = depth_stencil_desc;
		pso_desc.InputLayout = input_layout_desc;
		pso_desc.SampleMask = UINT_MAX;
		pso_desc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		pso_desc.NumRenderTargets = 1;
		pso_desc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
		pso_desc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
		pso_desc.SampleDesc = WRAPPED_SAMPLE_DESC(1, 0);
		break;
	}
	case PSOType::CHARACTER: {
		D3D12_BLEND_DESC blend_desc;
		ZeroMemory(&blend_desc, sizeof(blend_desc));
		blend_desc.AlphaToCoverageEnable = FALSE;
		blend_desc.IndependentBlendEnable = FALSE;
		blend_desc.RenderTarget[0].BlendEnable = FALSE;
		blend_desc.RenderTarget[0].LogicOpEnable = FALSE;
		blend_desc.RenderTarget[0].SrcBlend = D3D12_BLEND_ONE;
		blend_desc.RenderTarget[0].DestBlend = D3D12_BLEND_ZERO;
		blend_desc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
		blend_desc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
		blend_desc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
		blend_desc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
		blend_desc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
		blend_desc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;

		D3D12_DEPTH_STENCIL_DESC depth_stencil_desc = WRAPPED_DEPTH_STENCIL_DESC(
			TRUE, D3D12_DEPTH_WRITE_MASK_ALL, D3D12_COMPARISON_FUNC_LESS,
			FALSE, static_cast<UINT8>(0x00), static_cast<UINT8>(0x00),
			WRAPPED_DEPTH_STENCILOP_DESC(), WRAPPED_DEPTH_STENCILOP_DESC()
		);

		pso_desc.pRootSignature = rootSig;
		pso_desc.VS = g_Shaders[ShaderKey::CHARACTER_VS].GetShaderBytecode();
		pso_desc.PS = g_Shaders[ShaderKey::CHARACTER_PS].GetShaderBytecode();
		pso_desc.RasterizerState = WRAPPED_RASTERIZER_DESC(
			D3D12_FILL_MODE_SOLID, D3D12_CULL_MODE_BACK, FALSE, 0, 0.f, 0.f,
			TRUE, FALSE, FALSE, 0, D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF);
		pso_desc.BlendState = blend_desc;
		pso_desc.DepthStencilState = depth_stencil_desc;
		pso_desc.InputLayout = SkinnedVertex::GetInputLayoutDesc();
		pso_desc.SampleMask = UINT_MAX;
		pso_desc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		pso_desc.NumRenderTargets = 1;
		pso_desc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
		pso_desc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
		pso_desc.SampleDesc = WRAPPED_SAMPLE_DESC(1, 0);
		break;
	}
	case PSOType::BODY: {
		D3D12_BLEND_DESC blend_desc;
		ZeroMemory(&blend_desc, sizeof(blend_desc));
		blend_desc.AlphaToCoverageEnable					= FALSE;
		blend_desc.IndependentBlendEnable					= FALSE;
		blend_desc.RenderTarget[0].BlendEnable				= FALSE;
		blend_desc.RenderTarget[0].LogicOpEnable			= FALSE;
		blend_desc.RenderTarget[0].SrcBlend					= D3D12_BLEND_SRC_ALPHA;
		blend_desc.RenderTarget[0].DestBlend				= D3D12_BLEND_INV_SRC_ALPHA;
		blend_desc.RenderTarget[0].BlendOp					= D3D12_BLEND_OP_ADD;
		blend_desc.RenderTarget[0].SrcBlendAlpha			= D3D12_BLEND_SRC_ALPHA;
		blend_desc.RenderTarget[0].DestBlendAlpha			= D3D12_BLEND_INV_SRC_ALPHA;
		blend_desc.RenderTarget[0].BlendOpAlpha				= D3D12_BLEND_OP_ADD;
		blend_desc.RenderTarget[0].LogicOp					= D3D12_LOGIC_OP_NOOP;
		blend_desc.RenderTarget[0].RenderTargetWriteMask	= D3D12_COLOR_WRITE_ENABLE_ALL;

		D3D12_DEPTH_STENCIL_DESC depth_stencil_desc = WRAPPED_DEPTH_STENCIL_DESC(
			TRUE, D3D12_DEPTH_WRITE_MASK_ALL, D3D12_COMPARISON_FUNC_LESS,
			FALSE, static_cast<UINT8>(0x00), static_cast<UINT8>(0x00),
			WRAPPED_DEPTH_STENCILOP_DESC(), WRAPPED_DEPTH_STENCILOP_DESC()
		);

		pso_desc.pRootSignature = rootSig;
		pso_desc.VS = g_Shaders[ShaderKey::BODY_VS].GetShaderBytecode();
		pso_desc.PS = g_Shaders[ShaderKey::BODY_PS].GetShaderBytecode();
		pso_desc.RasterizerState = WRAPPED_RASTERIZER_DESC(
			D3D12_FILL_MODE_SOLID, D3D12_CULL_MODE_BACK, FALSE, 0, 0.f, 0.f,
			TRUE, FALSE, FALSE, 0, D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF);
		pso_desc.BlendState = blend_desc;
		pso_desc.DepthStencilState = depth_stencil_desc;
		pso_desc.InputLayout = SkinnedVertex::GetInputLayoutDesc();
		pso_desc.SampleMask = UINT_MAX;
		pso_desc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		pso_desc.NumRenderTargets = 1;
		pso_desc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
		pso_desc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
		pso_desc.SampleDesc = WRAPPED_SAMPLE_DESC(1, 0);
		break;
	}
	case PSOType::SPRITE: {
		D3D12_BLEND_DESC blend_desc;
		ZeroMemory(&blend_desc, sizeof(blend_desc));
		blend_desc.AlphaToCoverageEnable					= FALSE;
		blend_desc.IndependentBlendEnable					= FALSE;
		blend_desc.RenderTarget[0].BlendEnable				= TRUE;
		blend_desc.RenderTarget[0].LogicOpEnable			= FALSE;
		blend_desc.RenderTarget[0].SrcBlend					= D3D12_BLEND_SRC_ALPHA;
		blend_desc.RenderTarget[0].DestBlend				= D3D12_BLEND_INV_SRC_ALPHA;
		blend_desc.RenderTarget[0].BlendOp					= D3D12_BLEND_OP_ADD;
		blend_desc.RenderTarget[0].SrcBlendAlpha			= D3D12_BLEND_SRC_ALPHA;
		blend_desc.RenderTarget[0].DestBlendAlpha			= D3D12_BLEND_INV_SRC_ALPHA;
		blend_desc.RenderTarget[0].BlendOpAlpha				= D3D12_BLEND_OP_ADD;
		blend_desc.RenderTarget[0].LogicOp					= D3D12_LOGIC_OP_NOOP;
		blend_desc.RenderTarget[0].RenderTargetWriteMask	= D3D12_COLOR_WRITE_ENABLE_ALL;

		D3D12_DEPTH_STENCIL_DESC depth_stencil_desc = WRAPPED_DEPTH_STENCIL_DESC(
			FALSE, D3D12_DEPTH_WRITE_MASK_ALL, D3D12_COMPARISON_FUNC_LESS,
			FALSE, static_cast<UINT8>(0x00), static_cast<UINT8>(0x00),
			WRAPPED_DEPTH_STENCILOP_DESC(), WRAPPED_DEPTH_STENCILOP_DESC()
		);

		pso_desc.pRootSignature = rootSig;
		pso_desc.VS = g_Shaders[ShaderKey::SPRITE_VS].GetShaderBytecode();
		pso_desc.PS = g_Shaders[ShaderKey::SPRITE_PS].GetShaderBytecode();
		pso_desc.RasterizerState = WRAPPED_RASTERIZER_DESC(
			D3D12_FILL_MODE_SOLID, D3D12_CULL_MODE_BACK, FALSE, 0, 0.f, 0.f,
			TRUE, FALSE, FALSE, 0, D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF);
		pso_desc.BlendState = blend_desc;
		pso_desc.DepthStencilState = depth_stencil_desc;
		pso_desc.InputLayout = PosTex::GetInputLayoutDesc();
		pso_desc.SampleMask = UINT_MAX;
		pso_desc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		pso_desc.NumRenderTargets = 1;
		pso_desc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
		pso_desc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
		pso_desc.SampleDesc = WRAPPED_SAMPLE_DESC(1, 0);
		break;
	}
	default:
		assert(false);
	}

	HRESULT rtv = device->CreateGraphicsPipelineState(
		&pso_desc,
		IID_PPV_ARGS(m_PipelineState.GetAddressOf())
	);

	if (S_OK != rtv) {
		yhl::dout << "PSOType::" << static_cast<int>(m_Type) << " Create Fail." << yhl::fls;
		while (true);
	}
}
