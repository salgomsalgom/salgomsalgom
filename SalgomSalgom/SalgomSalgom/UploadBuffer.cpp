#include "pch.h"
#include "UploadBuffer.h"

#include "GraphicsCore.h"

using namespace Graphics;


UploadBuffer::UploadBuffer()
{
}

UploadBuffer::UploadBuffer(UINT64 bytes)
{
	HeapProperties heap_properties{ D3D12_HEAP_TYPE_UPLOAD };
	ResourceDesc resource_desc{ (UINT64) bytes};

	GraphicsCore::GetDevice()->CreateCommittedResource(
		heap_properties.Get(),
		D3D12_HEAP_FLAG_NONE,
		resource_desc.Get(),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(m_Resource.GetAddressOf())
	);
}

void UploadBuffer::Create(UINT64 bytes)
{
	HeapProperties heap_properties{ D3D12_HEAP_TYPE_UPLOAD };
	ResourceDesc resource_desc{ (UINT64)bytes };

	GraphicsCore::GetDevice()->CreateCommittedResource(
		heap_properties.Get(),
		D3D12_HEAP_FLAG_NONE,
		resource_desc.Get(),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(m_Resource.GetAddressOf())
	);
}

void UploadBuffer::CopyData(void* src)
{
	UINT64 size = m_Resource->GetDesc().Width;

	// 업로드 버퍼에 데이터 복사
	UINT8* data_begin = reinterpret_cast<UINT8*>(GetCPUPointer());
	memcpy(data_begin, src, size);
	m_Resource->Unmap(NULL, NULL);
}

UploadBuffer::~UploadBuffer()
{
}

