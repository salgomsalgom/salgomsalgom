#pragma once
#include "SystemNode.h"

enum class SystemMassage {
	NONE,
};

class SystemCommunicator 
{
public:
	explicit SystemCommunicator() { }
	~SystemCommunicator() { }

public:
	void Initialize();
	void RegisterSystem(SystemNode* node);
	void RemoveSystem(SystemNode* node);
	void SendSystemMessage(const SystemMassage message);

private:
	std::list<SystemNode*>		m_SystemList;
	std::queue<SystemMassage>	m_MessageQueue;
};
