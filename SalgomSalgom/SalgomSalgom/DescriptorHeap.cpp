#include "pch.h"
#include "DescriptorHeap.h"
#include "GraphicsCore.h"

using namespace Graphics;

DescriptorHeap::DescriptorHeap() :
    m_DescriptorHeap(),
    m_Handle(),
    m_DescriptorSize(0)
{
}

DescriptorHeap::DescriptorHeap(DescriptorHeapType type, UINT count) :
    m_DescriptorHeap()
{
    Create(type, count);
}

DescriptorHeap::~DescriptorHeap()
{
}

void DescriptorHeap::Create(DescriptorHeapType type, UINT count)
{
    auto device = GraphicsCore::GetDevice();

    if (nullptr == device) {
        yhl::dout << "DescriptorHeap::Create - Device is nullptr." << yhl::fls;
        return;
    }

    D3D12_DESCRIPTOR_HEAP_DESC heap_desc;

    switch (type) {
    case DescriptorHeapType::CBV_SRV_UAV:
        heap_desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
        heap_desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
        break;
    case DescriptorHeapType::SAMPLER:
        // m_HeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER;
        // m_HeapDesc.NumDescriptors = count;
        // m_HeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
        // m_HeapDesc.NodeMask = 0;
        break;
    case DescriptorHeapType::RTV:
        heap_desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
        heap_desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
        break;
    case DescriptorHeapType::DSV:
        heap_desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
        heap_desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
        break;
    }
    heap_desc.NumDescriptors = count;
    heap_desc.NodeMask = 0;


    HRESULT ret = device->CreateDescriptorHeap(&heap_desc, IID_PPV_ARGS(m_DescriptorHeap.GetAddressOf()));

    if (S_OK != ret) {
        HRESULT reason = device->GetDeviceRemovedReason();
        yhl::dout << reason << yhl::fls;
    }

    m_Handle = m_DescriptorHeap->GetCPUDescriptorHandleForHeapStart();

    m_DescriptorSize = device->GetDescriptorHandleIncrementSize(heap_desc.Type);
}

D3D12_CPU_DESCRIPTOR_HANDLE DescriptorHeap::GetHandle(int index)
{
    D3D12_CPU_DESCRIPTOR_HANDLE handle;
    handle.ptr = m_Handle.ptr + (m_DescriptorSize * index);

   
    return handle;
}
