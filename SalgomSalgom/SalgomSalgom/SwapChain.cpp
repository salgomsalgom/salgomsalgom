#include "pch.h"
#include "SwapChain.h"

#include "WindowApp.h"

SwapChain::SwapChain()
{
}

SwapChain::~SwapChain()
{
}

void SwapChain::Initailize(MinimumFactory* const factory, ID3D12CommandQueue* const commandQueue)
{
	if (nullptr == factory) return;
	if (nullptr == commandQueue) return;

	DXGI_SWAP_CHAIN_DESC1 desc{ };
	desc.Width = WindowApp::GetResolutionWidth();
	desc.Height = WindowApp::GetResolutionHeight();
	desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	desc.SampleDesc.Count = 1; // (m_MSAA4xEnable) ? 4 : 1;
	desc.SampleDesc.Quality = 0; // (m_MSAA4xEnable) ? m_MSAA4xQualityLevels - 1 : 0;
	desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	desc.BufferCount = 2;
	desc.Scaling = DXGI_SCALING_NONE;
	desc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	desc.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;
	desc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	DXGI_SWAP_CHAIN_FULLSCREEN_DESC fullscreen_desc{ };
	fullscreen_desc.RefreshRate.Numerator = 60;
	fullscreen_desc.RefreshRate.Denominator = 1;
	fullscreen_desc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	fullscreen_desc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	fullscreen_desc.Windowed = TRUE;

	factory->CreateSwapChainForHwnd(
		commandQueue,
		WindowApp::GetHwnd(),
		&desc,
		&fullscreen_desc,
		(IDXGIOutput*) nullptr,
		(IDXGISwapChain1**)m_SwapChain.GetAddressOf()
	);

	yhl::dout << "Initialized SwapChain." << yhl::fls;
}

void SwapChain::Present()
{
	if (nullptr == m_SwapChain.Get())
	{
		yhl::dout << "Swap Chain is nullptr." << yhl::fls;
		return;
	}

	// Presents a rendered image to the user.
	m_SwapChain->Present(0, 0);
}
