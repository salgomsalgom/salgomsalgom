#include "pch.h"
#include "Camera.h"
#include "WindowApp.h"

#include "GraphicsCore.h"


Camera::Camera()
{
	m_EyePosition = XMFLOAT3(0.f, 0.f, 0.f);
	m_FocusPosition = XMFLOAT3(0.f, 0.f, 1.f);
	m_UpDirection = XMFLOAT3(0.f, 1.f, 0.f);

	SetViewportAndScissorRect();
}

void Camera::CreateConstantBuffer()
{
	m_DescriptorHeap.Create(DescriptorHeapType::CBV_SRV_UAV, 1);

	UINT64 bytes = static_cast<UINT64>((sizeof(m_CBCameraInfo) + 255) & ~255);
	m_ConstantBuffer.Create(bytes, true);

	D3D12_CONSTANT_BUFFER_VIEW_DESC cbv_desc;
	cbv_desc.BufferLocation = m_ConstantBuffer->GetGPUVirtualAddress();
	cbv_desc.SizeInBytes = static_cast<int>(bytes);

	Graphics::GraphicsCore::GetDevice()->CreateConstantBufferView(&cbv_desc, m_DescriptorHeap.GetHandle());

	auto rtv = m_ConstantBuffer->Map(0, NULL, reinterpret_cast<void**>(&m_CBCameraInfo));

	SetViewMatrix();
	SetProjectionMatrix();
}

void Camera::SetPosition(const XMFLOAT3& position)
{
	m_EyePosition = position;
}

void Camera::SetDirection(const XMFLOAT3& dir)
{
	m_DirectionVector = dir;
}

void Camera::SetViewMatrix()
{
	auto right = XMMath::CrossProduct(XMFLOAT3(0, 1, 0), m_DirectionVector);
	m_FocusPosition = XMMath::Add(m_EyePosition, m_DirectionVector);
	m_UpDirection = XMMath::CrossProduct(m_DirectionVector, right);

	XMMATRIX view = XMMatrixLookAtLH(XMLoadFloat3(&m_EyePosition), XMLoadFloat3(&m_FocusPosition), XMLoadFloat3(&m_UpDirection));
	XMStoreFloat4x4(&(m_CBCameraInfo->view), XMMatrixTranspose(view));
}

void Camera::SetProjectionMatrix()
{
	XMMATRIX projection =
		XMMatrixPerspectiveFovLH(XMConvertToRadians(45.f), m_AspectRatio, 0.1f, 1000.f);

	XMStoreFloat4x4(&(m_CBCameraInfo->projection), XMMatrixTranspose(projection));

	// XMMatrixPerspective
	// 투영 행렬을 얻을 수 있는 6개의 함수가 있다.
	// XMMatrixPerspectiveLH();
	// XMMatrixPerspectiveRH();
	// XMMatrixPerspectiveFovLH();
	// XMMatrixPerspectiveFovRH();
	// XMMatrixPerspectiveOffCenterLH();
	// XMMatrixPerspectiveOffCenterRH();
}

void Camera::UpdateShaderVariables(ID3D12GraphicsCommandList* cmdList)
{
	SetViewMatrix();
	m_CBCameraInfo->eye_pos = m_EyePosition;
	cmdList->SetGraphicsRootConstantBufferView(0, m_ConstantBuffer->GetGPUVirtualAddress());
}

D3D12_VIEWPORT* Camera::GetViewport()
{
	return &m_Viewport;
}

D3D12_RECT* Camera::GetScissorRect()
{
	return &m_ScissorRect;
}

void Camera::SetViewportAndScissorRect()
{
	RECT client_rect = WindowApp::GetClientRect();

	int width = client_rect.right - client_rect.left;
	int height = client_rect.bottom - client_rect.top;

	m_Viewport.Width = (FLOAT)width;
	m_Viewport.Height = (FLOAT)height;
	m_Viewport.TopLeftX = 0;
	m_Viewport.TopLeftY = 0;
	m_Viewport.MaxDepth = 1;
	m_Viewport.MinDepth = 0;

	m_ScissorRect.left = 0;
	m_ScissorRect.right = (LONG)width;
	m_ScissorRect.top = 0;
	m_ScissorRect.bottom = (LONG)height;
}
