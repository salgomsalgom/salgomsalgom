#pragma once
#include "Scene.h"

void WriteUserName(char* name);

class LobbyScene : public Scene
{
public:
	explicit LobbyScene(SceneKey key);
	virtual ~LobbyScene();

public:
	void Initialize() override;
	void Update(float frame_time) override;
	void HandleInput(InputSystem& input) override;
	void Destroy() override;

public:
	Text* m_Test;
	vector<SpriteAnimation*> m_LobbyObjs; //�����,��4��,�Ź� (�κ�)

	bool Ready = false;
	bool m_ReadyDown = false;
	bool m_QuitDown = false;
	bool m_ReadyButtonMouseIn = false;
	bool m_QuitButtonMouseIn = false;
	Sprite* m_ReadyButton;
	Sprite* m_QuitButton;
	Sprite* m_SearchQuitButton;
	SpriteAnimation* m_LoadingIcon;
};
