#pragma once

#include "ShaderVisibleDescriptorHeap.h"
using namespace Microsoft::WRL;

class SwapChain;
class CommandListManager;
class DescriptorHeap;
class DescriptorAllocator;
class RenderTargetBuffer;
class DepthBuffer;
class GraphicsPSO;
enum class PSOType;

class Scene;

namespace Graphics {

	// for DX Rendering
	class GraphicsCore {
	private:
		explicit GraphicsCore() { }
		~GraphicsCore() { }

	public:
		static void Initialize();
		static void ShutDown();

		static void Resize();
		static void ToggleFullScreen();
		static void Present(Scene* scene);

		static ID3D12Device* GetDevice() { return s_Device.Get(); }

		static void AllocateSRVDescriptor
		(D3D12_CPU_DESCRIPTOR_HANDLE* cpu_handle, D3D12_GPU_DESCRIPTOR_HANDLE* gpu_handle) {
			return s_BindingHeap.Allocate(cpu_handle, gpu_handle);
		}

		// test
		static ID3D12GraphicsCommandList* GetCommandList();
		static void ExcuteCommandList();

	private:
		static void CreateDeviceForHardware();
		static void BuildPSO();

	private:
		static ComPtr<IDXGIFactory4>	s_Factory;
		static SwapChain				s_SwapChain;

		static ComPtr<ID3D12Device>		s_Device;
		static CommandListManager		s_CommandListManager;

		static DescriptorHeap				s_RtvDescriptorHeap;
		static DescriptorHeap				s_DsvDescriptorHeap;
		static DescriptorAllocator			s_DescriptorAllocator;
		static ShaderVisibleDescriptorHeap	s_BindingHeap;

		static constexpr unsigned short s_SwapChainBufferCount = 2;
		static ComPtr<ID3D12Resource>	s_DisplayPlanes[s_SwapChainBufferCount];
		static int						s_DisplayPleneIdx;
		static DepthBuffer				s_DepthStencilBuffer;

		static ComPtr<ID3D12RootSignature> s_GraphicsRS;
		static map<PSOType, GraphicsPSO*> s_GraphicsPSOs;

	};
} 
