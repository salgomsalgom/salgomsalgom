#pragma once

struct HeapProperties {
	explicit HeapProperties(D3D12_HEAP_TYPE type) {
		m_HeapProperties.Type = type;

		// 아래는 건드릴 필요 없다.
		m_HeapProperties.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
		m_HeapProperties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
		m_HeapProperties.CreationNodeMask = 1;
		m_HeapProperties.VisibleNodeMask = 1;
	}

	const D3D12_HEAP_PROPERTIES* Get() const {
		return &m_HeapProperties;
	}

	D3D12_HEAP_PROPERTIES* operator->() {
		return &m_HeapProperties;
	}

	const D3D12_HEAP_PROPERTIES* operator->() const {
		return &m_HeapProperties;
	}

private:
	D3D12_HEAP_PROPERTIES m_HeapProperties;
};

struct ResourceDesc {
	explicit ResourceDesc(const UINT64& resourceSize) {
		m_ResourceDesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
		m_ResourceDesc.Alignment = 0;
		m_ResourceDesc.Width = resourceSize;
		m_ResourceDesc.Height = 1;
		m_ResourceDesc.DepthOrArraySize = 1;
		m_ResourceDesc.MipLevels = 1;
		m_ResourceDesc.Format = DXGI_FORMAT_UNKNOWN;
		m_ResourceDesc.SampleDesc.Count = 1;
		m_ResourceDesc.SampleDesc.Quality = 0;
		m_ResourceDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
		m_ResourceDesc.Flags = D3D12_RESOURCE_FLAG_NONE;
	}

	const D3D12_RESOURCE_DESC* Get() const {
		return &m_ResourceDesc;
	}

	D3D12_RESOURCE_DESC* operator->() {
		return &m_ResourceDesc;
	}

	const D3D12_RESOURCE_DESC* operator->() const {
		return &m_ResourceDesc;
	}

private:
	D3D12_RESOURCE_DESC m_ResourceDesc;
};


class GpuResource
{
public:
	explicit GpuResource();
	virtual ~GpuResource();

public:
	ID3D12Resource* operator->() { return m_Resource.Get(); }
	const ID3D12Resource* operator->() const { return m_Resource.Get(); }

	ID3D12Resource* Get() { return m_Resource.Get(); }

protected:
	Microsoft::WRL::ComPtr<ID3D12Resource> m_Resource;
};

// vertex buffer, index buffer, constant buffer모두 이걸로 퉁치자
class UploadBuffer;
class GpuBuffer : public GpuResource
{
public:
	GpuBuffer() { };
	~GpuBuffer() { };

	void Create(UINT64 bytes);
	void Create(UINT64 bytes, bool cb);
	
	void CopyData(UploadBuffer& uploader);
	void TransitionResourceState(D3D12_RESOURCE_STATES state);

private:
	inline UINT64 GetSize() { return m_Resource->GetDesc().Width; }
};
