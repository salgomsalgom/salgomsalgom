#pragma once
#include "SystemNode.h"
#include "KeyboardManager.h"
#include "MouseManager.h"

class SystemCommunicator;

class InputSystem : SystemNode
{
public:
	explicit InputSystem(SystemCommunicator* const communicator);
	virtual ~InputSystem() { }

public:
	void Initialize() override;
	void Update();

	static void SetWindowFocus(bool value);

private:
	static bool s_Instantiated;
	static bool s_WindowFocus;
};
