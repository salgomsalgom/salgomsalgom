#pragma once

#include "GraphicsObject.h"

struct SpriteAnimationInfo
{
	int end_idx;
	int num_r;//row
	int num_c;//column
	float width;
	float height;
	float speed;
};


class SpriteAnimation : public GraphicsObject
{
public:
	explicit SpriteAnimation(TextureKey key, float width, float height);
	//explicit SpriteAnimation(TextureKey key, float pixel_x, float pixel_y);

public:
	virtual PSOType GetPSOType() override;

	virtual void CreateConstantBuffer() override;
	virtual void UpdateConstantBuffer(ID3D12GraphicsCommandList* cl) override;

	void SetTexturePos(float x, float y);
	void SetTextureScl(float x, float y);

	void UpdateIndex();


private:
	int m_CurrentIdx;
	int m_NumOfUpdatedFrame;
	XMFLOAT2 m_TexPos;
	XMFLOAT2 m_TexScl;
	SpriteAnimationInfo m_AniInfo;
	
};

class Scene;

