#pragma once
#include <Windows.h>
#include <atlconv.h>
#include <sstream>
#include <mutex>
using namespace std;

namespace yhl {

	class EndDelim { };

	class DebugOut {
		stringstream ss;
		mutex lock;

	public:
		DebugOut() { }

		DebugOut& operator<<(EndDelim) {
			lock_guard<mutex> L{ lock };
			ss << "\n";
			OutputDebugStringA(ss.str().c_str());
			ss.str("");			// 이 때 버퍼를 비워준다.
			return *this;
		}

		DebugOut& operator<< (int val) {
			lock_guard<mutex> L{ lock };
			ss << val;
			return *this;
		}

		DebugOut& operator<< (unsigned int val) {
			lock_guard<mutex> L{ lock };
			ss << val;
			return *this;
		}

		DebugOut& operator<< (short val) {
			lock_guard<mutex> L{ lock };
			ss << val;
			return *this;
		}

		DebugOut& operator<< (unsigned short val) {
			lock_guard<mutex> L{ lock };
			ss << val;
			return *this;
		}

		DebugOut& operator<< (long val) {
			lock_guard<mutex> L{ lock };
			ss << val;
			return *this;
		}

		DebugOut& operator<< (unsigned long val) {
			lock_guard<mutex> L{ lock };
			ss << val;
			return *this;
		}

		DebugOut& operator<< (long long val) {
			lock_guard<mutex> L{ lock };
			ss << val;
			return *this;
		}

		DebugOut& operator<< (unsigned long long val) {
			lock_guard<mutex> L{ lock };
			ss << val;
			return *this;
		}

		DebugOut& operator<< (float val) {
			lock_guard<mutex> L{ lock };
			ss << val;
			return *this;
		}

		DebugOut& operator<< (double val) {
			lock_guard<mutex> L{ lock };
			ss << val;
			return *this;
		}

		DebugOut& operator<< (long double val) {
			lock_guard<mutex> L{ lock };
			ss << val;
			return *this;
		}

		DebugOut& operator<< (bool val) {
			lock_guard<mutex> L{ lock };
			if (val)	ss << "true";
			else		ss << "false";
			return *this;
		}

		DebugOut& operator<< (const char* val) {
			lock_guard<mutex> L{ lock };
			ss << val;
			return *this;
		}

		DebugOut& operator<< (const string& val) {
			lock_guard<mutex> L{ lock };
			ss << val;
			return *this;
		}

		DebugOut& operator<< (const wchar_t* val) {
			lock_guard<mutex> L{ lock };
			USES_CONVERSION;
			string str{ W2A(val) };

			ss << str;
			return *this;
		}

		DebugOut& operator<< (const wstring& val) {
			lock_guard<mutex> L{ lock };
			USES_CONVERSION;
			string str{ W2A(val.c_str()) };

			ss << str;
			return *this;
		}

	};

	extern EndDelim fls;
	extern DebugOut dout;
}
