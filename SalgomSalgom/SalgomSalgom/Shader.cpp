#include "pch.h"
#include "Shader.h"

constexpr wchar_t g_ShaderFileDirectory[] = L"./Shaders/";

std::map<ShaderKey, Shader> g_Shaders;

void CompileShaders() {
	g_Shaders.try_emplace(ShaderKey::SKYBOX_VS, L"SkyBox.hlsl", ShaderType::VERTEX);
	g_Shaders.try_emplace(ShaderKey::SKYBOX_PS, L"SkyBox.hlsl", ShaderType::PIXEL);
	g_Shaders.try_emplace(ShaderKey::TERRAIN_VS, L"Terrain.hlsl", ShaderType::VERTEX);
	g_Shaders.try_emplace(ShaderKey::TERRAIN_PS, L"Terrain.hlsl", ShaderType::PIXEL);

	g_Shaders.try_emplace(ShaderKey::TEST_VS, L"TestShader.hlsl", ShaderType::VERTEX);
	g_Shaders.try_emplace(ShaderKey::TEST_PS, L"TestShader.hlsl", ShaderType::PIXEL);

	g_Shaders.try_emplace(ShaderKey::CHARACTER_VS, L"Character.hlsl", ShaderType::VERTEX);
	g_Shaders.try_emplace(ShaderKey::CHARACTER_PS, L"Character.hlsl", ShaderType::PIXEL);

	g_Shaders.try_emplace(ShaderKey::STATICOBJECT_VS, L"StaticObject.hlsl", ShaderType::VERTEX);
	g_Shaders.try_emplace(ShaderKey::STATICOBJECT_PS, L"StaticObject.hlsl", ShaderType::PIXEL);

	g_Shaders.try_emplace(ShaderKey::SPRITE_VS, L"Sprite.hlsl", ShaderType::VERTEX);
	g_Shaders.try_emplace(ShaderKey::SPRITE_PS, L"Sprite.hlsl", ShaderType::PIXEL);

	g_Shaders.try_emplace(ShaderKey::BODY_VS, L"Body.hlsl", ShaderType::VERTEX);
	g_Shaders.try_emplace(ShaderKey::BODY_PS, L"Body.hlsl", ShaderType::PIXEL);

	for (auto& [key, shader] : g_Shaders)
	{
		shader.Compile();
	}
}


Shader::Shader() :
	m_ShaderBlob(),
	m_FilePath(),
	m_EntryPointName(),
	m_ShaderTarget()
{
	ZeroMemory(&m_Bytecode, sizeof(m_Bytecode));
}

// 쉐이더와 연결되는 PSO가 필요한데, 하나의 쉐이더 함수가 다른 종류의 PSO를 사용할 수 있고,
Shader::Shader(const wchar_t* filename, ShaderType type) :
	m_ShaderBlob(nullptr),
	m_FilePath(g_ShaderFileDirectory)
{
	ZeroMemory(&m_Bytecode, sizeof(m_Bytecode));

	m_FilePath.append(filename);

	switch (type)
	{
	case ShaderType::VERTEX:
		m_EntryPointName = "VSMain";
		m_ShaderTarget = "vs_5_1";
		break;
	case ShaderType::PIXEL:
		m_EntryPointName = "PSMain";
		m_ShaderTarget = "ps_5_1";
		break;
	}
}

Shader::~Shader()
{
}

bool Shader::Compile()
{
	UINT compile_flags = 0;
	ID3DBlob* error_msg{ nullptr };

#if defined(_DEBUG)
	compile_flags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

	// MS 문서를 보니 권장하지 않는 방법 같음. 그치만 쓴다.
	HRESULT rtv = ::D3DCompileFromFile(
		(LPCWSTR)m_FilePath.c_str(),
		NULL, NULL,
		m_EntryPointName.c_str(),
		m_ShaderTarget.c_str(),
		compile_flags,
		0,
		m_ShaderBlob.GetAddressOf(),
		&error_msg
	);

	if (S_OK != rtv) {
		yhl::dout << "쉐이더 컴파일 실패" << yhl::fls;
	}

	if (error_msg) {
		yhl::dout << (char*)error_msg->GetBufferPointer() << yhl::fls;
		return false;
	}

	// BYTECODE 생성
	m_Bytecode.BytecodeLength = m_ShaderBlob->GetBufferSize();
	m_Bytecode.pShaderBytecode = m_ShaderBlob->GetBufferPointer();

	return true;
}
