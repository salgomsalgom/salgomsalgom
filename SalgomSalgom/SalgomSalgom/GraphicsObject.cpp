#include "pch.h"
#include "GraphicsObject.h"
#include "Mesh.h"
#include "Texture.h"

#include "PipelineState.h"

#include "WindowApp.h"

GraphicsObject::GraphicsObject() 
	: m_Name()
	, m_Position()
	, m_Rotation(0.f, 0.f, 0.f, 1.f)
	, m_Scale(1, 1, 1)
	, m_WorldTransform()
	, m_Mesh(nullptr)
	, m_Texture(nullptr)
	, m_ObjectInfo(nullptr)
{
}

GraphicsObject::~GraphicsObject()
{
}

void GraphicsObject::SetPosition(const XMFLOAT3& pos)
{
	m_Position = pos;
}

void GraphicsObject::SetRotation(const XMFLOAT3& rot)
{
	XMVECTOR qua_rot = XMQuaternionRotationRollPitchYaw(rot.x, rot.y, rot.z);
	XMStoreFloat4(&m_Rotation, qua_rot);
}

void GraphicsObject::SetScale(const XMFLOAT3& scl)
{
	m_Scale = scl;
}

void GraphicsObject::SetScale(float scl)
{
	m_Scale.x = scl;
	m_Scale.y = scl;
	m_Scale.z = scl;
}


SkyBox::SkyBox() :
	GraphicsObject()
{
	m_Mesh = StaticGameData::g_Meshs[MeshKey::SKYBOX];
	m_Texture = StaticGameData::g_Textures[TextureKey::SKYBOX_SIDE];
}

SkyBox::~SkyBox()
{
}

PSOType SkyBox::GetPSOType() { return PSOType::SKYBOX; };

void SkyBox::SetLocation(const XMFLOAT3& location)
{
	m_Position = location;
}

void SkyBox::CreateConstantBuffer()
{
	m_DescriptorHeap.Create(DescriptorHeapType::CBV_SRV_UAV, 1);

	// 상수 버퍼는 256byte의 배수여야 함
	UINT64 bytes = static_cast<UINT64>((sizeof(CB_ObjectInfo) + 255) & ~255);

	m_ObjectInfoCB.Create(bytes, true);

	D3D12_CONSTANT_BUFFER_VIEW_DESC cbv_desc;
	cbv_desc.BufferLocation = m_ObjectInfoCB->GetGPUVirtualAddress();
	cbv_desc.SizeInBytes = static_cast<int>(bytes);

	Graphics::GraphicsCore::GetDevice()->CreateConstantBufferView(&cbv_desc, m_DescriptorHeap.GetHandle());		// 힙과 버퍼를 묶어 뷰를 생성

	auto rtv = m_ObjectInfoCB->Map(0, NULL, reinterpret_cast<void**>(&m_ObjectInfo));
}

void SkyBox::UpdateConstantBuffer(ID3D12GraphicsCommandList* cl)
{
	XMMATRIX world_mtx = XMMatrixTranslationFromVector(XMLoadFloat3(&m_Position));
	XMStoreFloat4x4(&(m_ObjectInfo->m_WorldTransform), XMMatrixTranspose(world_mtx));

	cl->SetGraphicsRootConstantBufferView(2, m_ObjectInfoCB->GetGPUVirtualAddress());
}


Terrain::Terrain() {
	m_Mesh = StaticGameData::g_Meshs[MeshKey::TERN];
	m_Texture = StaticGameData::g_Textures[TextureKey::TERN];
}

PSOType Terrain::GetPSOType() { return PSOType::TERRAIN; }


PSOType Character::GetPSOType() { return PSOType::CHARACTER; }

void Character::Animate(float time)
{
	m_Animation->GetFinalTransforms(time, m_SkinnedInfo->m_BoneTransforms);
}

void Character::CreateConstantBuffer()
{
	auto device = Graphics::GraphicsCore::GetDevice();

	// Desriptor가 2개 들어가는 힙 생성
	m_DescriptorHeap.Create(DescriptorHeapType::CBV_SRV_UAV, 2);
	D3D12_CONSTANT_BUFFER_VIEW_DESC cbv_desc;
	UINT64 bytes;

	// 객체 위치
	bytes = static_cast<UINT64>((sizeof(CB_ObjectInfo) + 255) & ~255);
	m_ObjectInfoCB.Create(bytes, true);

	cbv_desc.BufferLocation = m_ObjectInfoCB->GetGPUVirtualAddress();
	cbv_desc.SizeInBytes = static_cast<UINT>(bytes);

	device->CreateConstantBufferView(&cbv_desc, m_DescriptorHeap.GetHandle(0));
	m_ObjectInfoCB->Map(0, NULL, reinterpret_cast<void**>(&m_ObjectInfo));

	// 애니 정보
	bytes = static_cast<UINT64>((sizeof(SkinnedInfo) + 255) & ~255);
	m_SkinnedInfoCB.Create(bytes, true);

	cbv_desc.BufferLocation = m_SkinnedInfoCB->GetGPUVirtualAddress();
	cbv_desc.SizeInBytes = static_cast<int>(bytes);

	device->CreateConstantBufferView(&cbv_desc, m_DescriptorHeap.GetHandle(1));
	m_SkinnedInfoCB->Map(0, NULL, reinterpret_cast<void**>(&m_SkinnedInfo));
}

void Character::UpdateConstantBuffer(ID3D12GraphicsCommandList* cl) {
	XMVECTOR pos = XMLoadFloat3(&m_Position);
	XMVECTOR rot = XMLoadFloat4(&m_Rotation);
	XMVECTOR scl = XMLoadFloat3(&m_Scale);

	XMFLOAT3 f3_ori{ 0.f, 0.f, 0.f };
	XMVECTOR origin = XMLoadFloat3(&f3_ori);

	XMMATRIX world_mtx = XMMatrixAffineTransformation(scl, origin, rot, pos);

	XMStoreFloat4x4(&(m_ObjectInfo->m_WorldTransform), XMMatrixTranspose(world_mtx));

	cl->SetGraphicsRootConstantBufferView(2, m_ObjectInfoCB->GetGPUVirtualAddress());
	cl->SetGraphicsRootConstantBufferView(3, m_SkinnedInfoCB->GetGPUVirtualAddress());
}


PSOType Body::GetPSOType() { return PSOType::BODY; }

void Body::CreateConstantBuffer()
{
	auto device = Graphics::GraphicsCore::GetDevice();

	// Desriptor가 2개 들어가는 힙 생성
	m_DescriptorHeap.Create(DescriptorHeapType::CBV_SRV_UAV, 2);
	D3D12_CONSTANT_BUFFER_VIEW_DESC cbv_desc;
	UINT64 bytes;

	// 객체 위치
	bytes = static_cast<UINT64>((sizeof(CB_ObjectInfo) + 255) & ~255);
	m_ObjectInfoCB.Create(bytes, true);

	cbv_desc.BufferLocation = m_ObjectInfoCB->GetGPUVirtualAddress();
	cbv_desc.SizeInBytes = static_cast<UINT>(bytes);

	device->CreateConstantBufferView(&cbv_desc, m_DescriptorHeap.GetHandle(0));
	m_ObjectInfoCB->Map(0, NULL, reinterpret_cast<void**>(&m_ObjectInfo));

	// 애니 정보
	bytes = static_cast<UINT64>((sizeof(SkinnedInfo) + 255) & ~255);
	m_SkinnedInfoCB.Create(bytes, true);

	cbv_desc.BufferLocation = m_SkinnedInfoCB->GetGPUVirtualAddress();
	cbv_desc.SizeInBytes = static_cast<int>(bytes);

	device->CreateConstantBufferView(&cbv_desc, m_DescriptorHeap.GetHandle(1));
	m_SkinnedInfoCB->Map(0, NULL, reinterpret_cast<void**>(&m_SkinnedInfo));
}

void Body::UpdateConstantBuffer(ID3D12GraphicsCommandList* cl)
{
	XMVECTOR pos = XMLoadFloat3(&m_Position);
	XMVECTOR rot = XMLoadFloat4(&m_Rotation);
	XMVECTOR scl = XMLoadFloat3(&m_Scale);

	XMFLOAT3 f3_ori{ 0.f, 0.f, 0.f };
	XMVECTOR origin = XMLoadFloat3(&f3_ori);

	XMMATRIX world_mtx = XMMatrixAffineTransformation(scl, origin, rot, pos);

	XMStoreFloat4x4(&(m_ObjectInfo->m_WorldTransform), XMMatrixTranspose(world_mtx));

	cl->SetGraphicsRootConstantBufferView(2, m_ObjectInfoCB->GetGPUVirtualAddress());
	cl->SetGraphicsRootConstantBufferView(3, m_SkinnedInfoCB->GetGPUVirtualAddress());
}

void Body::PlayOnce(const string& name)
{
	m_Animation->PlayOnce(name);
}


void Body::Animate(float time)
{
	m_Animation->GetFinalTransforms(time, m_SkinnedInfo->m_BoneTransforms);
}


StaticObject::StaticObject(MeshKey mesh, TextureKey texture)
{
	Mesh* m = StaticGameData::g_Meshs[mesh];
	Texture* t = StaticGameData::g_Textures[texture];

	if (nullptr == m || nullptr == t)
		while (true);

	m_Mesh = m;
	m_Texture = t;
}

PSOType StaticObject::GetPSOType() { return PSOType::STATIC_OBJECT; };


void StaticObject::CreateConstantBuffer()
{
	m_DescriptorHeap.Create(DescriptorHeapType::CBV_SRV_UAV, 1);

	// 상수 버퍼는 256byte의 배수여야 함
	UINT64 bytes = static_cast<UINT64>((sizeof(CB_ObjectInfo) + 255) & ~255);

	m_ObjectInfoCB.Create(bytes, true);

	D3D12_CONSTANT_BUFFER_VIEW_DESC cbv_desc;
	cbv_desc.BufferLocation = m_ObjectInfoCB->GetGPUVirtualAddress();
	cbv_desc.SizeInBytes = static_cast<int>(bytes);

	Graphics::GraphicsCore::GetDevice()->CreateConstantBufferView(&cbv_desc, m_DescriptorHeap.GetHandle());		// 힙과 버퍼를 묶어 뷰를 생성

	auto rtv = m_ObjectInfoCB->Map(0, NULL, reinterpret_cast<void**>(&m_ObjectInfo));
}

void StaticObject::UpdateConstantBuffer(ID3D12GraphicsCommandList* cl)
{
	XMVECTOR pos = XMLoadFloat3(&m_Position);
	XMVECTOR rot = XMLoadFloat4(&m_Rotation);
	XMVECTOR scl = XMLoadFloat3(&m_Scale);

	XMFLOAT3 f3_ori{ 0.f, 0.f, 0.f };
	XMVECTOR origin = XMLoadFloat3(&f3_ori);

	XMMATRIX world_mtx = XMMatrixAffineTransformation(scl, origin, rot, pos);

	XMStoreFloat4x4(&(m_ObjectInfo->m_WorldTransform), XMMatrixTranspose(world_mtx));
	cl->SetGraphicsRootConstantBufferView(2, m_ObjectInfoCB->GetGPUVirtualAddress());
}


Sprite::Sprite(float width, float height, TextureKey texture)
	: GraphicsObject()
	, m_SpriteInfo(nullptr)
	, m_TexPos()
	, m_TexScl(1, 1)
{
	m_Mesh = StaticGameData::g_Meshs[MeshKey::RECTANGLE];
	m_Texture = StaticGameData::g_Textures[texture];


	auto wr = WindowApp::GetClientRect();
	float wnd_w = static_cast<float>(wr.right - wr.left);
	float wnd_h = static_cast<float>(wr.bottom - wr.top);

	float scl_x = (width / wnd_w) * WINDOW_MODE_RATE;
	float scl_y = (height / wnd_h) * WINDOW_MODE_RATE;
	m_Scale = XMFLOAT3(scl_x, scl_y, 0);
}



Sprite::Sprite(TextureKey texture, float pixel_x, float pixel_y)
	: GraphicsObject()
	, m_TexPos()
	, m_TexScl(1.f, 1.f)
{
	m_Mesh = StaticGameData::g_Meshs[MeshKey::RECTANGLE];
	m_Texture = StaticGameData::g_Textures[texture];

	auto w = static_cast<float>(m_Texture->GetWidth());
	auto h = static_cast<float>(m_Texture->GetHeight());

	auto wr = WindowApp::GetClientRect();
	float wnd_w = static_cast<float>(wr.right - wr.left);
	float wnd_h = static_cast<float>(wr.bottom - wr.top);

	float scl_x = (w / wnd_w) * WINDOW_MODE_RATE;
	float scl_y = (h / wnd_h) * WINDOW_MODE_RATE;
	m_Scale = m_OriginScale = XMFLOAT3(scl_x, scl_y, 0);

	m_ClientPos.x = pixel_x;
	m_ClientPos.y = pixel_y;

	float pos_x = (pixel_x * 2.f / wnd_w * WINDOW_MODE_RATE) - 1.f;
	float pos_y = (pixel_y * 2.f / wnd_h * WINDOW_MODE_RATE) - 1.f;
	m_Position = XMFLOAT3(pos_x, -pos_y, 0.f);
}

void Sprite::SetPosition(float x, float y)
{
	auto wr = WindowApp::GetClientRect();
	float wnd_w = static_cast<float>(wr.right - wr.left);
	float wnd_h = static_cast<float>(wr.bottom - wr.top);

	float pos_x = (x * 2.f / wnd_w * WINDOW_MODE_RATE) - 1.f;
	float pos_y = (y * 2.f / wnd_h * WINDOW_MODE_RATE) - 1.f;
	m_Position = XMFLOAT3(pos_x, -pos_y, 0.f);
}


PSOType Sprite::GetPSOType() { return PSOType::SPRITE; }

void Sprite::CreateConstantBuffer() 
{
	m_DescriptorHeap.Create(DescriptorHeapType::CBV_SRV_UAV, 1);

	// 상수 버퍼는 256byte의 배수여야 함
	UINT64 bytes = static_cast<UINT64>((sizeof(CB_ObjectInfo) + 255) & ~255);

	m_ObjectInfoCB.Create(bytes, true);

	D3D12_CONSTANT_BUFFER_VIEW_DESC cbv_desc;
	cbv_desc.BufferLocation = m_ObjectInfoCB->GetGPUVirtualAddress();
	cbv_desc.SizeInBytes = static_cast<int>(bytes);

	Graphics::GraphicsCore::GetDevice()->CreateConstantBufferView(&cbv_desc, m_DescriptorHeap.GetHandle());		// 힙과 버퍼를 묶어 뷰를 생성

	auto rtv = m_ObjectInfoCB->Map(0, NULL, reinterpret_cast<void**>(&m_ObjectInfo));

}

void Sprite::UpdateConstantBuffer(ID3D12GraphicsCommandList* cl)
{
	UpdateWorldTransform();
	UpdateTextureTransform();

	cl->SetGraphicsRootConstantBufferView(2, m_ObjectInfoCB->GetGPUVirtualAddress());
}

void Sprite::UpdateWorldTransform()
{
	XMVECTOR pos = XMLoadFloat3(&m_Position);
	XMVECTOR rot = XMLoadFloat4(&m_Rotation);
	XMVECTOR scl = XMLoadFloat3(&m_Scale);
	XMMATRIX world_mtx = XMMatrixAffineTransformation(scl, XMMath::XMVECTOR_ORIGIN, rot, pos);
	XMStoreFloat4x4(&(m_ObjectInfo->m_WorldTransform), XMMatrixTranspose(world_mtx));
}

void Sprite::UpdateTextureTransform()
{
	XMVECTOR pos = XMLoadFloat2(&m_TexPos);
	XMVECTOR scl = XMLoadFloat2(&m_TexScl);
	XMVECTOR rot{ 0.f, 0.f, 0.f, 1.f };
	XMMATRIX tex_mtx = XMMatrixAffineTransformation(scl, XMMath::XMVECTOR_ORIGIN, rot, pos);
	XMStoreFloat4x4(&(m_ObjectInfo->m_TexTransform), XMMatrixTranspose(tex_mtx));
}

void Sprite::SetTexturePos(float x, float y)
{
	m_TexPos.x = x;
	m_TexPos.y = y;
}

void Sprite::SetTextureScl(float x, float y)
{
	m_TexScl.x = x;
	m_TexScl.y = y;
}
