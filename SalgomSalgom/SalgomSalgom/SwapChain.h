#pragma once

using namespace Microsoft::WRL;

typedef IDXGIFactory2 MinimumFactory;

constexpr int SWAP_CHAIN_BURRER_COUNT = 2;

class SwapChain
{
public:
	SwapChain();
	~SwapChain();

public:
	void Initailize(MinimumFactory* const factory, ID3D12CommandQueue* const commandQueue);
	void Present();

public:
	IDXGISwapChain3* operator->() {
		return m_SwapChain.Get();
	}

	const IDXGISwapChain3* operator->() const {
		return m_SwapChain.Get();
	}

private:
	ComPtr<IDXGISwapChain3> m_SwapChain;

};

