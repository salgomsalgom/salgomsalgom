#pragma once
#include "DescriptorHeap.h"
#include "GpuResource.h"

using namespace DirectX;

// 상수 버퍼를 위한 구조체
struct CB_CameraInfo {
	XMFLOAT4X4	view;
	XMFLOAT4X4	projection;
	XMFLOAT3	eye_pos;
};

class Camera
{
	const float m_AspectRatio = 1920.f / 1080.f;	// 1.77...
public:
	Camera();

private:
	XMFLOAT3 m_EyePosition;
	XMFLOAT3 m_FocusPosition;
	XMFLOAT3 m_UpDirection;

	XMFLOAT3 m_DirectionVector = XMFLOAT3(1, 0, 0);

	D3D12_VIEWPORT	m_Viewport;
	D3D12_RECT		m_ScissorRect;

	DescriptorHeap  m_DescriptorHeap;
	GpuBuffer		m_ConstantBuffer;
	CB_CameraInfo*  m_CBCameraInfo;

public:
	void CreateConstantBuffer();

	void SetPosition(const XMFLOAT3& position);
	inline XMFLOAT3 GetPosition() { return m_EyePosition; }
	void SetDirection(const XMFLOAT3& dir);

	void UpdateShaderVariables(ID3D12GraphicsCommandList* cmdList);

	D3D12_VIEWPORT*		GetViewport();
	D3D12_RECT*			GetScissorRect();

	void SetViewportAndScissorRect();

private:
	void SetViewMatrix();
	void SetProjectionMatrix();

};
