#pragma once
#include "fmod.hpp"

#define SOUND_NUM 10

#pragma comment(lib, "FMOD/lib/fmod64_vc.lib")

using namespace FMOD;

constexpr int LOGIN_SOUND = 0;
constexpr int LOBBY_SOUND = 1;
constexpr int BATTLE_SOUND = 2;
constexpr int NORMAL_WIN_SOUND = 3;
constexpr int WITCH_WIN_SOUND = 4;
constexpr int ATTACK_SOUND = 5;
constexpr int WALKING_SOUND = 6;
constexpr int RESPAWN_SOUND = 7;
constexpr int SHIELD_SOUND = 8;
constexpr int MAGICSTICK_SOUND = 9;


class CSound
{
public:
	CSound();
	virtual ~CSound();
	System * pSystem;
	Sound * pSound[SOUND_NUM];
	Channel * pChannel[SOUND_NUM];

	void Add_sound();
	void Play(int n);
};

extern CSound g_Sound;
