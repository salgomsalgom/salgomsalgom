#pragma once
struct Vertex;
struct Bone;
struct Keyframe;

enum class VertexType : char { STATIC, SKINNED };

struct AniInfo
{
	char			name[10];
	unsigned short	num_keys;
};

struct D2bBuf
{
	using Index = unsigned short;
public:
	D2bBuf();
	~D2bBuf();

	void LoadD2b(const std::string& filename);
	int GetVertexBytes();

private:
	void AllocBuf();
	void ResetBuf();

public:
#pragma pack(push, 1)
	struct Header {
		VertexType v_type;
		bool rigged;

		int v_count;
		int i_count;
		int b_count;
		int ani_count;
		int key_count;
	};
#pragma pack(pop)

	Header header;

	// geo info
	Vertex* v_buf;
	Index* i_buf;

	// ani info
	Bone* b_buf;
	AniInfo* ani_buf;
	Keyframe* key_buf;
};
