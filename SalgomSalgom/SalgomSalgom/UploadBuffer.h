#pragma once
#include "GpuResource.h"

class UploadBuffer : public GpuResource
{
public:
	UploadBuffer();
	explicit UploadBuffer(UINT64 bytes);
	~UploadBuffer();

public:
	void Create(UINT64 bytes);

	void* GetCPUPointer() {
		D3D12_RANGE read_range = { 0, 0 };
		void* data_begin = nullptr;

		m_Resource->Map(NULL, &read_range, &data_begin);
		return data_begin;
	}

	void CopyData(void* src);

private:
	
};
