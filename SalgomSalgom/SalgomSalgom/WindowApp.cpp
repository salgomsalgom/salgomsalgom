#include "pch.h"
#include "WindowApp.h"

#include "Game.h"
#include "InputSystem.h"
#include "LoginScene.h"


// static 변수 초기화
HWND WindowApp::m_Wnd = NULL;
BOOL WindowApp::m_bFullScreen = false;
RECT WindowApp::m_ClientRect{ };


int WindowApp::Run(HINSTANCE instance, GameCore::Game& game, const TCHAR* className)
{
    // Register class
    WNDCLASSEX wcex;
    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = instance;
    wcex.hIcon = LoadIcon(instance, IDI_APPLICATION);
    wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszMenuName = nullptr;
    wcex.lpszClassName = className;
    wcex.hIconSm = LoadIcon(instance, IDI_APPLICATION);
    RegisterClassEx(&wcex);

    // 윈도우 모드로 시작
    m_bFullScreen = false;
    m_ClientRect = WINDOW_MODE_RECT;

    RECT wnd_rect = m_ClientRect;
    ::AdjustWindowRect(&wnd_rect, m_WindowStyle, FALSE);

    // Create window
    m_Wnd = CreateWindow(
        className,
        className,
        m_WindowStyle,
        0/*CW_USEDEFAULT*/,
        0/*CW_USEDEFAULT*/,
        wnd_rect.right - wnd_rect.left,
        wnd_rect.bottom - wnd_rect.top,
        nullptr,        // We have no parent window.
        nullptr,        // We aren't using menus.
        instance,
        nullptr
    );

    GameCore::Initialize(game);

    ShowWindow(m_Wnd, SW_SHOWDEFAULT);
    DebugOutClientSize();

    // Message loop
    MSG msg = {};
    while (true)
    {
        if (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT) break;
            ::TranslateMessage(&msg);
            ::DispatchMessage(&msg);
        }
        else
        {
            GameCore::Update(game);
        }
    }

    return static_cast<int>(msg.wParam);
}


void WindowApp::ToggleFullScreenWindow()
{
    m_bFullScreen = !m_bFullScreen;
    m_ClientRect = (m_bFullScreen ? FULLSCREEN_MODE_RECT : WINDOW_MODE_RECT);
    // m_Renderer->SetFullScreen(m_bFullScreen, m_ClientRect);
    DebugOutClientSize();
}

void WindowApp::ToggleFullScreenWindow(IDXGISwapChain3* swapChain)
{
    // 나는 무엇을 위해 이 함수를 작성하였을까..
    // 이렇게 윈도우 화면을 전환해주는 것이랑,
    // SwapChain에서 FullScreenState값만 바꿔서 전환해주는 것이랑
    // 무슨 차이가 있을까?

    if (m_bFullScreen) {
        SetWindowLong(m_Wnd, GWL_STYLE, m_WindowStyle);

        SetWindowPos(
            m_Wnd,
            HWND_NOTOPMOST,
            0,
            0,
            500,
            500,
            SWP_FRAMECHANGED | SWP_NOACTIVATE);

        ShowWindow(m_Wnd, SW_NORMAL);
    }
    else {
        SetWindowLong(m_Wnd, GWL_STYLE, m_WindowStyle
            & ~(WS_CAPTION | WS_MAXIMIZEBOX | WS_MINIMIZEBOX | WS_SYSMENU | WS_THICKFRAME));

        RECT fullscreenWindowRect;

        if (swapChain) {
            Microsoft::WRL::ComPtr<IDXGIOutput> pOutput;
            if (S_OK == swapChain->GetContainingOutput(&pOutput)) {
                DXGI_OUTPUT_DESC output_desc;
                pOutput->GetDesc(&output_desc);
                fullscreenWindowRect = output_desc.DesktopCoordinates;
                SetWindowPos(
                    m_Wnd,
                    HWND_TOPMOST,
                    fullscreenWindowRect.left,
                    fullscreenWindowRect.top,
                    fullscreenWindowRect.right,
                    fullscreenWindowRect.bottom,
                    SWP_FRAMECHANGED | SWP_NOACTIVATE);
            }
        }

    }

    ShowWindow(m_Wnd, SW_NORMAL);
    m_bFullScreen = !m_bFullScreen;
}

LRESULT WindowApp::WndProc
(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{    
    switch (message)
    {
    case WM_SIZE: 
        break;
    case WM_MOVE:
        yhl::dout << "wm_move" << yhl::fls;
        break;
    case WM_SETFOCUS:
        InputSystem::SetWindowFocus(true);
        yhl::dout << "wm_setfocus" << yhl::fls;
        break;
    case WM_KILLFOCUS:
        InputSystem::SetWindowFocus(false);
        yhl::dout << "wm_killfocus" << yhl::fls;
        break;
    case WM_KEYDOWN:
        // 시스템 인풋
        switch (wParam) {
        case VK_F1:
            MouseManager::ChangeMouseMode(MouseMode::DEFAULT_MODE);
            break;
        case VK_F2:
            MouseManager::ChangeMouseMode(MouseMode::CAMERACONTROL_MODE);
            break;
        default:
            break;
        }
        break;
    case WM_KEYUP:
        break;
    case WM_LBUTTONDOWN:
    case WM_RBUTTONDOWN:
    case WM_LBUTTONUP:
    case WM_RBUTTONUP:
    {
        MouseManager::PushWindowInput(message, wParam, lParam);
        break;
    }
    case WM_CHAR:
    {
        char ch = static_cast<char>(wParam);
        // 로그인 씬일때만 Push 하고싶다.
        PushChar(ch);
        
        break;
    }
    case WM_DESTROY: {
        PostQuitMessage(0);
        break;
    }
    default: 
        return DefWindowProc(hWnd, message, wParam, lParam);
    }

    return 0;
}

void WindowApp::DebugOutClientSize()
{
    RECT client;
    ::GetClientRect(m_Wnd, &client);
    yhl::dout << "width: " << client.right - client.left << ", height: " << client.bottom - client.top << yhl::fls;
}
