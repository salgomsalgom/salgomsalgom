#pragma once
#include "Scene.h"

class NormalWinScene : public Scene
{
public:
	explicit NormalWinScene(SceneKey key) : Scene(key) { }
	virtual ~NormalWinScene() { }

public:
	void Initialize() override;
	void Update(float frame_time) override;
	void HandleInput(InputSystem& input) override;
	void Destroy() override;

private:
	vector<SpriteAnimation*> m_ResultObjs;

};

class WitchWinScene : public Scene
{
public:
	explicit WitchWinScene(SceneKey key) : Scene(key) { }
	virtual ~WitchWinScene() { }

public:
	void Initialize() override;
	void Update(float frame_time) override;
	void HandleInput(InputSystem& input) override;
	void Destroy() override;

private:
	vector<SpriteAnimation*> m_ResultObjs;

};
