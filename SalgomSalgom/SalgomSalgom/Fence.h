#pragma once

using namespace Microsoft::WRL;

class Fence
{
public:
	Fence();
	~Fence();

	void Initialize(ID3D12Device* const device);
	void Wait(ID3D12CommandQueue* const commandQueue);

private:
	ComPtr<ID3D12Fence>		m_Fence;
	UINT64					m_FenceValue;
	HANDLE					m_FenceEvent;

};
