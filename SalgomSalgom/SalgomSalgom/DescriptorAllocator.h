#pragma once
class DescriptorAllocator
{
public:
	DescriptorAllocator() = delete;
	explicit DescriptorAllocator(D3D12_DESCRIPTOR_HEAP_TYPE type) : m_Type(type) ,m_CurrHeap(nullptr) { }

	D3D12_CPU_DESCRIPTOR_HANDLE Allocate(UINT count);

	UINT GetHeapCount();

private:
	static ID3D12DescriptorHeap* RequestNewHeap(D3D12_DESCRIPTOR_HEAP_TYPE Type);

private:
	D3D12_DESCRIPTOR_HEAP_TYPE	m_Type;
	ID3D12DescriptorHeap		*m_CurrHeap;
	D3D12_CPU_DESCRIPTOR_HANDLE m_CurrHandle;
	uint32_t					m_DescriptorSize;
	uint32_t					m_RemainingFreeHandles;
};
