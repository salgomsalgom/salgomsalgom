#pragma once
#include "GpuResource.h"

class Texture : public GpuResource
{
	bool LoadTextureFromFile(const wchar_t* filename);

public:
	explicit Texture(const wchar_t* filename);

	D3D12_GPU_DESCRIPTOR_HANDLE GetGPUDescriptorHandle() {
		return m_GPUDescriptorHandle;
	}

	D3D12_CPU_DESCRIPTOR_HANDLE GetCPUDescriptorHandle() {
		return m_CPUDescriptorHandle;
	}

	D3D12_GPU_VIRTUAL_ADDRESS GetGPUVirtualAddress()
	{
		return m_Resource->GetGPUVirtualAddress();
	}

	UINT64 GetWidth() { return m_Resource->GetDesc().Width; }
	UINT64 GetHeight() { return m_Resource->GetDesc().Height; }

	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> m_DescriptorHeap;

private:
	bool m_Valid;
	D3D12_CPU_DESCRIPTOR_HANDLE m_CPUDescriptorHandle;
	D3D12_GPU_DESCRIPTOR_HANDLE m_GPUDescriptorHandle;
};

