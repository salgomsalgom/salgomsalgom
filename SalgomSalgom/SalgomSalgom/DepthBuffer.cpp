#include "pch.h"
#include "DepthBuffer.h"

#include "WindowApp.h"
#include "GraphicsCore.h"

using namespace Graphics;

DepthBuffer::DepthBuffer() :
	GpuResource()
{
	
}

void DepthBuffer::Create()
{
	auto device = GraphicsCore::GetDevice();

	HeapProperties heap_properties{ D3D12_HEAP_TYPE_DEFAULT };

	// 이건 사실 버퍼 아니라 텍스처
	D3D12_RESOURCE_DESC resource_desc = { };
	resource_desc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	resource_desc.Alignment = 0;
	resource_desc.Width = WindowApp::GetResolutionWidth();
	resource_desc.Height = WindowApp::GetResolutionWidth();
	resource_desc.DepthOrArraySize = 1;
	resource_desc.MipLevels = 1;
	resource_desc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	resource_desc.SampleDesc.Count = 1;// (m_MSAA4xEnable) ? 4 : 1;
	resource_desc.SampleDesc.Quality = 0;// (m_MSAA4xEnable) ? (m_MSAA4xQualityLevels - 1) : 0;
	resource_desc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	resource_desc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

	D3D12_CLEAR_VALUE clear_value{ };
	clear_value.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	clear_value.DepthStencil.Depth = 1.0f;
	clear_value.DepthStencil.Stencil = 0;

	device->CreateCommittedResource(
		heap_properties.Get(),
		D3D12_HEAP_FLAG_NONE,
		&resource_desc,
		D3D12_RESOURCE_STATE_DEPTH_WRITE,
		&clear_value,
		IID_PPV_ARGS(m_Resource.GetAddressOf())
	);
}
