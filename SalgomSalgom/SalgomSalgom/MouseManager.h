#pragma once

#define GET_X_LPARAM(lp)		((int)(short)LOWORD(lp))
#define GET_Y_LPARAM(lp)		((int)(short)HIWORD(lp))

struct WindowInput {
	explicit WindowInput(UINT message, WPARAM wParam, LPARAM lParam)
		: message(message), wParam(wParam), lParam(lParam) { }
	
	UINT message;
	WPARAM wParam;
	LPARAM lParam;
};



enum class MouseMode: char {
	DEFAULT_MODE,
	CAMERACONTROL_MODE,
};

typedef POINT ClientPoint;
typedef POINT ScreenPoint;

struct MouseEvent {
	UINT message;
	ClientPoint p;
};

class MouseManager
{
private:
	MouseManager() { }
	~MouseManager() { }

public:
	static void Initialize();

	static void PushWindowInput(UINT message, WPARAM wParam, LPARAM lParam);

	static void Update();

	static void ChangeMouseMode(const MouseMode mode);

	inline static POINTF GetDelta() { return s_Delta; }
	inline static POINT GetMousePos() { return s_LastMousePos; }
	static bool PopMouseInput(UINT& message, int& x, int& y);


private:
	static void UpdateDefault();
	static void UpdateCameraControl();

private:
	static std::queue<WindowInput> m_MouseInput;

	static MouseMode m_Mode;
	static void (*m_UpdateMouseMode)( void );

	static POINTF s_Delta;
	static ClientPoint s_LastMousePos;

};
