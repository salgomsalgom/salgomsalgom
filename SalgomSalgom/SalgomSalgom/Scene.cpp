#include "pch.h"
#include "Scene.h"

#include "Math.h"
#include "NetworkSystem.h"
#include "InputSystem.h"
#include "GraphicsObject.h"

#include "Sound.h"

int	g_MyID;
int g_NumObjects;
StaticObjectInfo* g_StaticObjectInfo;
CollisionBoxInfo* g_BoxInfo;

void WriteID(int id)
{
	g_MyID = id;
}

void MoveMapData(int numObjects, StaticObjectInfo* objects)
{
	g_NumObjects = numObjects;
	g_StaticObjectInfo = objects;
}

void MoveCollisionBoxData(int numObjects, CollisionBoxInfo* boxs)
{
	g_BoxInfo = boxs;
}

//==================================================================================================
BattleData g_BattleData;
mutex g_BattleDataLock;


// Write Method
void InitBattleData(BattleData& data)
{
	g_BattleDataLock.lock();
	g_BattleData = data;
	g_BattleDataLock.unlock();
}

void WriteUpdateData(UpdateData& data)
{
	g_BattleDataLock.lock();
	memcpy(g_BattleData.players, data.players, sizeof(CharacterInfo) * g_BattleData.numPlayer);
	memcpy(g_BattleData.gems, data.gems, sizeof(GemInfo) * 16);
	memcpy(g_BattleData.skill1, data.skill1, sizeof(SkillInfo) * g_BattleData.numPlayer);
	memcpy(g_BattleData.skill2, data.skill2, sizeof(SkillInfo) * g_BattleData.numPlayer);
	g_BattleDataLock.unlock();
}

void WriteWitchStoneInfo(WitchStoneInfo* wsInfo)
{
	g_BattleDataLock.lock();
	memcpy(g_BattleData.witchStoneState, wsInfo, sizeof(WitchStoneInfo) * 4);
	g_BattleDataLock.unlock();
}

void WriteSoulCount(int count)
{
	g_BattleDataLock.lock();
	g_BattleData.soulCount = count;
	g_BattleDataLock.unlock();
}

// Read Method
static void ReadBattleData(BattleData& data)
{
	g_BattleDataLock.lock();
	data = g_BattleData;
	g_BattleDataLock.unlock();
}

static void ReadCharacterInfo(int id, CharacterInfo& info)
{
	g_BattleDataLock.lock();
	info = g_BattleData.players[id];
	g_BattleDataLock.unlock();
}

// Update Message Queue
queue<int> g_WitchStoneUpdateQueue;
mutex g_WitchStoneUpdateLock;

void UpdateWitchStone()
{
	g_WitchStoneUpdateLock.lock();
	g_WitchStoneUpdateQueue.push(0);
	g_WitchStoneUpdateLock.unlock();
}

static void GetWitchStoneUpdateMessage()
{
	g_WitchStoneUpdateLock.lock();
	g_WitchStoneUpdateQueue.pop();
	g_WitchStoneUpdateLock.unlock();
}

queue<AniEvent> g_AniUpdateQueue;
mutex g_AniUpdateLock;
unordered_map<A_STATUS, string> ANI_STRING;

void UpdateAniEvent(int id, A_STATUS stat)
{
	g_AniUpdateLock.lock();
	g_AniUpdateQueue.push(AniEvent(id, stat));
	g_AniUpdateLock.unlock();
}

static AniEvent GetAniEventUpdateMessage()
{
	g_AniUpdateLock.lock();
	AniEvent e = g_AniUpdateQueue.front();
	g_AniUpdateQueue.pop();
	g_AniUpdateLock.unlock();
	return e;
}

queue<POINTF> g_PowerUpdateQueue;
mutex g_PowerUpdateLock;

void UpdatePowerEvent(float skill, float speed)
{
	g_PowerUpdateLock.lock();
	g_PowerUpdateQueue.push(POINTF(skill, speed));
	g_PowerUpdateLock.unlock();
}

static POINTF GetPowerEventUpdateMessage()
{
	g_PowerUpdateLock.lock();
	POINTF e = g_PowerUpdateQueue.front();
	g_PowerUpdateQueue.pop();
	g_PowerUpdateLock.unlock();
	return e;
}

// object
static void BuildObjectKeyMap(map <string, pair<MeshKey, TextureKey>>& dic)
{
	dic.emplace("PKN1", make_pair(MeshKey::PKN1, TextureKey::PKN1_PKN2));
	dic.emplace("PKN2", make_pair(MeshKey::PKN2_PKN3, TextureKey::PKN1_PKN2));
	dic.emplace("PKN3", make_pair(MeshKey::PKN2_PKN3, TextureKey::PKN3));
	dic.emplace("HFPK", make_pair(MeshKey::HFPK, TextureKey::HFPK));
	dic.emplace("ONIO", make_pair(MeshKey::ONIO, TextureKey::ONIO));
	dic.emplace("TREE", make_pair(MeshKey::TREE, TextureKey::TREE));
	dic.emplace("MSH1", make_pair(MeshKey::MSH1, TextureKey::MSH1));
	dic.emplace("MSH2", make_pair(MeshKey::MSH2, TextureKey::MSH2));
	dic.emplace("ROK1", make_pair(MeshKey::ROK1, TextureKey::ROK1));
	dic.emplace("ROK2", make_pair(MeshKey::ROK2, TextureKey::ROK2));
	dic.emplace("LTRE", make_pair(MeshKey::LTRE, TextureKey::LTRE));
	dic.emplace("SNAL", make_pair(MeshKey::SNAL, TextureKey::SNAL));
	dic.emplace("CATS", make_pair(MeshKey::CATS, TextureKey::CATS));
	dic.emplace("SOFA", make_pair(MeshKey::SOFA, TextureKey::SOFA));
	dic.emplace("CHAR", make_pair(MeshKey::CHAR, TextureKey::TX97));
	dic.emplace("BPOT", make_pair(MeshKey::BPOT, TextureKey::BPOT));
	dic.emplace("STL1", make_pair(MeshKey::STL1, TextureKey::STL1_STL2));
	dic.emplace("STL2", make_pair(MeshKey::STL2, TextureKey::STL1_STL2));
	dic.emplace("ARCH", make_pair(MeshKey::ARCH, TextureKey::ARCH));
	dic.emplace("BASN", make_pair(MeshKey::BASN, TextureKey::BASN));
	dic.emplace("BSF1", make_pair(MeshKey::BSF1, TextureKey::BSF1));
	dic.emplace("BSF2", make_pair(MeshKey::BSF2, TextureKey::BSF2));
	dic.emplace("FBU1", make_pair(MeshKey::FBU1_FBU2, TextureKey::FBU1));
	dic.emplace("FBU2", make_pair(MeshKey::FBU1_FBU2, TextureKey::FBU2));
	dic.emplace("MOPP", make_pair(MeshKey::MOPP, TextureKey::MOPP));
	dic.emplace("INLI", make_pair(MeshKey::INLI, TextureKey::INLI));
	dic.emplace("ROT1", make_pair(MeshKey::ROT1, TextureKey::ROOT));
	dic.emplace("ROT2", make_pair(MeshKey::ROT2, TextureKey::ROOT));
	dic.emplace("ROT3", make_pair(MeshKey::ROT3, TextureKey::ROOT));
	dic.emplace("ROT4", make_pair(MeshKey::ROT4, TextureKey::ROOT));
	dic.emplace("CUSN", make_pair(MeshKey::CUSN, TextureKey::TX97));
	dic.emplace("SIGN", make_pair(MeshKey::SIGN, TextureKey::SIGN));
	dic.emplace("SPMP", make_pair(MeshKey::SPMP, TextureKey::SPMP));
	dic.emplace("LEV1", make_pair(MeshKey::LEV1, TextureKey::LEV1));
	dic.emplace("LEV2", make_pair(MeshKey::LEV2, TextureKey::LEV2));
	dic.emplace("DRW1", make_pair(MeshKey::DRW1, TextureKey::DRW1));
	dic.emplace("DRW2", make_pair(MeshKey::DRW2, TextureKey::DRW2));
	dic.emplace("DESK", make_pair(MeshKey::DESK, TextureKey::DESK));
	dic.emplace("LEAV", make_pair(MeshKey::LEAV, TextureKey::LEAV));
	dic.emplace("GEMB", make_pair(MeshKey::MGEM, TextureKey::GEMB));
	dic.emplace("GEMW", make_pair(MeshKey::MGEM, TextureKey::GEMW));
	dic.emplace("GEMR", make_pair(MeshKey::MGEM, TextureKey::GEMR));
	dic.emplace("GEMP", make_pair(MeshKey::MGEM, TextureKey::GEMP));
	dic.emplace("MSTB", make_pair(MeshKey::MGST, TextureKey::MSTB));
	dic.emplace("MSTP", make_pair(MeshKey::MGST, TextureKey::MSTP));
	dic.emplace("MSTW", make_pair(MeshKey::MGST, TextureKey::MSTW));
	dic.emplace("MSTR", make_pair(MeshKey::MGST, TextureKey::MSTR));
	dic.emplace("FENC", make_pair(MeshKey::FENC, TextureKey::FENC));
	dic.emplace("MPED", make_pair(MeshKey::MPED, TextureKey::MPED));
	dic.emplace("BENC", make_pair(MeshKey::BENC, TextureKey::BENC));
	dic.emplace("PDW1", make_pair(MeshKey::PDW1, TextureKey::TX64));
	dic.emplace("PDW2", make_pair(MeshKey::PDW2, TextureKey::TX64));
	dic.emplace("NOST", make_pair(MeshKey::NOST, TextureKey::NMGD));
	dic.emplace("WIST", make_pair(MeshKey::WIST, TextureKey::WICH));
	dic.emplace("SCS1", make_pair(MeshKey::SCS1, TextureKey::SCS1));
	dic.emplace("SCS2", make_pair(MeshKey::SCS2, TextureKey::SCS2));
	dic.emplace("LEV3", make_pair(MeshKey::LEV2, TextureKey::LEV2));
	dic.emplace("LEV4", make_pair(MeshKey::LEV1, TextureKey::LEV1));
}
static void BuildAniNameMap() {
	// 공통
	ANI_STRING[A_ST_IDLE]			= "Idle_NW";
	ANI_STRING[A_ST_MOVE_LEFT]		= "SWalk_L";
	ANI_STRING[A_ST_MOVE_RIGHT]		= "SWalk_R";
	ANI_STRING[A_ST_MOVE_FORWAD]	= "Run_NW";
	ANI_STRING[A_ST_MOVE_BACKWARD]	= "WalkBack";

	// 노말
	ANI_STRING[A_ST_DEAD]			= "Falldown";
	ANI_STRING[A_ST_FAKE_DEATH]		= "Falldown";
	ANI_STRING[A_ST_GEM_THROW]		= "Throwing";
	ANI_STRING[A_ST_GEM_PICK]		= "Picking";
	ANI_STRING[A_ST_DASH]			= "Run_NW";

	// 마녀
	ANI_STRING[A_ST_ATTACK]			= "Attack";
	ANI_STRING[A_ST_GEM_INVISIBLE]	= "Invisible";
	ANI_STRING[A_ST_HALT_1]			= "Halt01";
	ANI_STRING[A_ST_HALT_2]			= "Halt02";

}
//-------------------------------------------------------------------------------

Scene::Scene(SceneKey key) 
	: m_Key(key)
{
	m_Camera.CreateConstantBuffer();
}

Scene::~Scene() {

}


GraphicsObject* Scene::AddObject(GraphicsObject* obj)
{
	auto pso = obj->GetPSOType();
	auto iter = m_Objects.find(pso);

	if (m_Objects.end() == iter)
		m_Objects.emplace(pso, std::list<GraphicsObject*>({ obj }));
	else iter->second.push_back(obj);

	obj->CreateConstantBuffer();

	return obj;
}

GraphicsObject* Scene::ReaddObject(GraphicsObject* obj)
{
	auto pso = obj->GetPSOType();
	auto iter = m_Objects.find(pso);

	if (m_Objects.end() == iter)
		m_Objects.emplace(pso, std::list<GraphicsObject*>({ obj }));
	else iter->second.push_back(obj);

	return obj;
}


void Scene::RemoveObject(GraphicsObject* obj) 
{
	auto pso_type = obj->GetPSOType();
	
	for (auto o : m_Objects[pso_type])
	{
		if (obj == o) {
			m_Objects[pso_type].remove(o);
			break;
		}
	}
}

BattleScene::BattleScene(SceneKey key) : Scene(key)
{
	BuildAniNameMap();
}

BattleScene::~BattleScene()
{

}

void BattleScene::Initialize()

{
	// snd.pSystem->release();			system release
	// snd.pSystem->close();			system close
	// snd.pChannel[0]->stop();

	MouseManager::ChangeMouseMode(MouseMode::CAMERACONTROL_MODE);

	AddObject(new SkyBox());
	AddObject(new Terrain());

	map <string, pair<MeshKey, TextureKey>> ObjectNameKeyDic;
	BuildObjectKeyMap(ObjectNameKeyDic);

	for (int i = 0; i < g_NumObjects; ++i) {
		auto& info = g_StaticObjectInfo[i];

		// boxc 추가
		// auto& boxc_info = g_BoxInfo[i];
		// GraphicsObject* boxc = new StaticObject(MeshKey::CUBE, TextureKey::TX97);
		// boxc->SetPosition(info.m_Position);
		// boxc->SetRotation(info.m_Rotation);
		// boxc->SetScale(boxc_info.m_Size);
		// AddObject(boxc);

		auto iter = ObjectNameKeyDic.find(info.m_Name);
		
		if (ObjectNameKeyDic.end() == iter) {
			yhl::dout << "Static Object Create Fail - " << info.m_Name << yhl::fls;
			continue;
		}

		GraphicsObject* obj = new StaticObject(iter->second.first, iter->second.second);

		obj->SetPosition(info.m_Position);
		obj->SetRotation(info.m_Rotation);
		obj->SetScale(info.m_Scale);
		obj->SetName(info.m_Name);

		// 메쉬가 젬이면 벡터에 추가해 관리한다.
		if (iter->second.first == MeshKey::MGEM) {
			// for debug
			// obj->SetScale(XMFLOAT3(1, 1, 1));

			m_Gems.push_back(obj);
		}

		// 메쉬가 위치스톤이면 벡터에 추가해 관리한다.
		if (iter->second.first == MeshKey::MGST) {
			TextureKey tex = iter->second.second;

			MagicColor color;
			switch (tex) {
			case TextureKey::MSTR:
				color = MagicColor::R;
				break;
			case TextureKey::MSTB:
				color = MagicColor::B;
				break;
			case TextureKey::MSTP:
				color = MagicColor::P;
				break;
			case TextureKey::MSTW:
				color = MagicColor::W;
				break;
			default:
				assert(false);
				break;
			}

			m_WitchStones.emplace(color, obj);
		}

		AddObject(obj);
	}

	// 씬에 추가하고 객체 정보 삭제해준다.
	delete[] g_StaticObjectInfo;
	delete[] g_BoxInfo;

	//==============================================================
	BattleData battle_data;
	ReadBattleData(battle_data);

	// 플레이어	
	m_ClientID = g_MyID;
	m_Role = battle_data.players[m_ClientID].m_Role;

	// 손 추가
	if (ChRole::WITCH == m_Role) {
		m_Hand = new Body(MeshKey::WC_HAND, TextureKey::WICH);
	}
	else {
		m_Hand = new Body(MeshKey::NM_HAND, TextureKey::TXNM);
	}

	m_Hand->SetAnimation("Idle_NW");
	m_Hand->SetPosition(XMFLOAT3(0.f, -0.13f, 0.25f));
	m_Hand->SetScale(0.25f);
	AddObject(m_Hand);


	// 플레이어 이름
	for (int i = 0; i < battle_data.numPlayer;++i)
	{
		if (m_ClientID == i) continue;

		CharacterInfo& pc_info = battle_data.players[i];

		Character* obj;
		if (pc_info.m_Role == ChRole::WITCH)
			obj = new Character(pc_info.m_ID, MeshKey::WICH, TextureKey::WICH);
		else
			obj = new Character(pc_info.m_ID, MeshKey::NMGD, TextureKey::NMGD);

		obj->SetAnimation("Idle_NW"s);
		obj->SetPosition(pc_info.m_Position);
		obj->SetRotation(pc_info.m_Rotation);

		AddObject(obj);
		m_Characters.emplace(pc_info.m_ID, obj);
	}

	// 위치스톤
	for (int i = 0; i < 4; ++i) m_WitchStoneAlive[i] = true;
	
	AddObject(new Sprite(TextureKey::BATTLE_UI_WS_LEFT, 240, 900));
	AddObject(new Sprite(TextureKey::BATTLE_UI_WS_RIGHT, 1680, 900));

	m_WitchStoneUIs[MagicColor::R] = new Sprite(TextureKey::BATTLE_UI_WS_RED, 90, 900);
	m_WitchStoneUIs[MagicColor::B] = new Sprite(TextureKey::BATTLE_UI_WS_BLUE, 1830, 900);
	m_WitchStoneUIs[MagicColor::W] = new Sprite(TextureKey::BATTLE_UI_WS_WHITE, 270, 960);
	m_WitchStoneUIs[MagicColor::P] = new Sprite(TextureKey::BATTLE_UI_WS_PURPLE, 1650, 960);
	for (auto& [color, ui] : m_WitchStoneUIs) AddObject(ui);

	m_WitchStoneBrokenUIs[MagicColor::R] = new Sprite(TextureKey::BATTLE_UI_WS_RED_BROKEN, 90, 900);
	m_WitchStoneBrokenUIs[MagicColor::B] = new Sprite(TextureKey::BATTLE_UI_WS_BLUE_BROKEN, 1830, 900);
	m_WitchStoneBrokenUIs[MagicColor::W] = new Sprite(TextureKey::BATTLE_UI_WS_WHITE_BROKEN, 270, 960);
	m_WitchStoneBrokenUIs[MagicColor::P] = new Sprite(TextureKey::BATTLE_UI_WS_PURPLE_BROKEN, 1650, 960);

	// 소울 UI
	AddObject(new Sprite(TextureKey::BATTLE_UI_SOUL_ICON, 1620, 126));
	string soul_count = to_string(static_cast<int>(battle_data.soulCount));
	m_SoulCount = new Text(this, TextureKey::BATTLE_UI_SOUL_FONT, 1740, 126, soul_count);

	// 능력치 증감 UI
	AddObject(new Sprite(TextureKey::BATTLE_UI_CHARSKILL_BG, 216, 348));
	AddObject(new Sprite(TextureKey::BATTLE_UI_CHARSKILL_BG, 216, 504));
	AddObject(new Sprite(TextureKey::BATTLE_UI_TIMER_ICON, 96, 348));
	AddObject(new Sprite(TextureKey::BATTLE_UI_SPEED_ICON, 96, 504));
	m_TextTimer = new Text(this, TextureKey::BATTLE_UI_CHARSKILL_FONT_BLUE, 180, 348, "0%");
	m_TextSpeed = new Text(this, TextureKey::BATTLE_UI_CHARSKILL_FONT_RED, 180, 504, "0%");

	// Skill
	SKILL1_COOLTIME = (m_Role == ChRole::WITCH) ? WITCH_GEMTRANS_COOLTIME : NORMAL_DASH_COOLTIME;
	SKILL2_COOLTIME = (m_Role == ChRole::WITCH) ? WITCH_HALT_COOLTIME : NORMAL_FAKEDIE_COOLTIME;

	m_RenderSphere = false;
	m_Sphere = new StaticObject(MeshKey::SPHERE, TextureKey::COMEHERE);
	m_Sphere->CreateConstantBuffer();

	// Skill UI
	if (m_Role == ChRole::WITCH) {
		AddObject(new Sprite(TextureKey::BATTLE_UI_SKILL_GEM_TRANSPARENT_ICON, 856, 960));
		AddObject(new Sprite(TextureKey::BATTLE_UI_SKILL_HALT_ICON, 1064, 960));
	}
	else {
		AddObject(new Sprite(TextureKey::BATTLE_UI_SKILL_DASH_ICON, 856, 960));
		AddObject(new Sprite(TextureKey::BATTLE_UI_SKILL_FAKE_DIE_ICON, 1064, 960));
	}
	m_Skill1Filter = new Sprite(TextureKey::BATTLE_UI_SKILL_COOLTIME_FILTER, 856, 960);
	m_Skill2Filter = new Sprite(TextureKey::BATTLE_UI_SKILL_COOLTIME_FILTER, 1064, 960);
	m_Skill1Filter->SetScale(1.f, 0.f);
	m_Skill2Filter->SetScale(1.f, 0.f);
	AddObject(m_Skill1Filter);
	AddObject(m_Skill2Filter);
	AddObject(new Sprite(TextureKey::BATTLE_UI_SKILL_FRAME, 856, 960));
	AddObject(new Sprite(TextureKey::BATTLE_UI_SKILL_FRAME, 1064, 960));

	// Cool Time
	MOUSE_LBUTTON_COOLTIME = (m_Role == ChRole::WITCH) ? 3.f : 1.f;

	// 리스폰 관련
	m_RespawnLetter = new Sprite(TextureKey::BATTLE_UI_BAR_RESPAWN, 640, 480);
	m_FakeDeathLetter = new Sprite(TextureKey::BATTLE_UI_BAR_FAKEDEATH, 640, 480);
	m_RespawnBG = new Sprite(TextureKey::BATTLE_UI_BAR_BG, 960, 540);
	m_RespawnFill = new Sprite(TextureKey::BATTLE_UI_BAR_FILL, 960, 540);

	m_RespawnLetter->CreateConstantBuffer();
	m_FakeDeathLetter->CreateConstantBuffer();
	m_RespawnBG->CreateConstantBuffer();
	m_RespawnFill->CreateConstantBuffer();

	// 사운드
	g_Sound.pChannel[BATTLE_SOUND]->setVolume(1.0f);
	g_Sound.Play(BATTLE_SOUND);
}

void BattleScene::Update(float frame_time)
{
	m_SceneTime += frame_time;

	// 배틀 데이터 가져오기
	BattleData bd;
	ReadBattleData(bd);

	// 카메라 업데이트
	CharacterInfo& my_info = bd.players[m_ClientID];
	XMFLOAT3 c_pos{ my_info.m_Position };
	c_pos.y += 1.2f;
	m_Camera.SetPosition(c_pos);
	m_Camera.SetDirection(my_info.m_DirVector);

	// 노말 리스폰, 죽은척 처리
	if (m_Role == ChRole::NORMAL) {
		if (my_info.m_RespawnTime > 0.f) {
			m_RespawnFill->SetScale(my_info.m_RespawnTime / 5.f, 1.f);
		}
		else {	// 살아났을 때
			if (m_Death == true) {
				m_Death = false;
				my_info.m_RespawnTime = 0.f;
				RemoveObject(m_RespawnLetter);
				RemoveObject(m_RespawnBG);
				RemoveObject(m_RespawnFill);
			}
		}

		float fd_dur = bd.skill2[m_ClientID].m_DurationTime;
		if (fd_dur > 0.f) {
			m_RespawnFill->SetScale(fd_dur / 10.f, 1.f);
		}
		else {
			if (m_FakeDeath == true) {
				m_FakeDeath = false;
				RemoveObject(m_FakeDeathLetter);
				RemoveObject(m_RespawnBG);
				RemoveObject(m_RespawnFill);
			}
		}
	}

	// 애니메이션 업데이트
	queue<AniEvent> ani_queue;
	g_AniUpdateLock.lock();
	ani_queue = g_AniUpdateQueue;
	while (false == g_AniUpdateQueue.empty()) g_AniUpdateQueue.pop();
	g_AniUpdateLock.unlock();

	while (false == ani_queue.empty()) {
		AniEvent e = ani_queue.front();
		ani_queue.pop();

		if (e.id == m_ClientID) {
			switch (e.stat) {
			case A_ST_DASH:	m_Hand->PlayOnce("Dash"); break;
			case A_ST_GEM_INVISIBLE: m_Hand->PlayOnce("Invisible"); break;
			case A_ST_HALT_1: m_Hand->SetAnimation("Halt01"); break;
			case A_ST_HALT_2: {
				m_Hand->PlayOnce("Halt02");
				m_Hand->SetAnimation("Idle_NW");
				break;
			}
			case A_ST_DEAD:
				m_Death = true;
				ReaddObject(m_RespawnLetter);
				ReaddObject(m_RespawnBG);
				ReaddObject(m_RespawnFill);
				break;
			case A_ST_FAKE_DEATH:
				m_FakeDeath = true;
				ReaddObject(m_FakeDeathLetter);
				ReaddObject(m_RespawnBG);
				ReaddObject(m_RespawnFill);
				break;
			default: break;
			}
		}
		else {
			string ani_name = ANI_STRING[e.stat];
			if (e.stat == A_ST_GEM_THROW ||
				e.stat == A_ST_GEM_PICK ||
				e.stat == A_ST_ATTACK) {
				m_Characters[e.id]->PlayOnce(ani_name);
			}
			else {
				m_Characters[e.id]->SetAnimation(ani_name);
			}
		}
	}

	// skill speed
	if (!g_PowerUpdateQueue.empty()) {
		POINTF power = GetPowerEventUpdateMessage();
		int skill = static_cast<int>(power.x);
		int speed = static_cast<int>(power.y);
		string str_skill = to_string(skill) + "%";
		string str_speed = to_string(speed) + "%";
		(*m_TextTimer) = str_skill;
		(*m_TextSpeed) = str_speed;
	}


	// 손 업데이트
	m_Hand->Animate(frame_time);

	// pc 업데이트
	for (auto& [id, cha] : m_Characters) {
		CharacterInfo pc_info = bd.players[id];

		cha->SetPosition(pc_info.m_Position);
		cha->SetRotation(pc_info.m_Rotation, true);
		cha->Animate(frame_time);		// 애니 업데이트
	}

	// 젬 업데이트
	for (int i = 0; i < 16; ++i)
	{
		if (bd.gems[i].m_Player_id != 6 && bd.gems[i].m_Player_id != m_ClientID) {
			m_Gems[i]->m_Visible = false;
		}
		else {
			m_Gems[i]->m_Visible = !bd.gems[i].m_Transparent;
		}
		
		m_Gems[i]->SetPosition(bd.gems[i].m_Position);
		m_Gems[i]->SetRotation(bd.gems[i].m_Rotation);
	}
	
	// 위치스톤 업데이트
	// 이게 아니라 큐로 한다.
	if (false == g_WitchStoneUpdateQueue.empty()) {
		GetWitchStoneUpdateMessage();

		for (int i = 0; i < 4; ++i)
		{
			auto& ws = bd.witchStoneState[i];
			if (ws.m_Alive != m_WitchStoneAlive[i]) {
				// 죽은 순간
				m_WitchStoneAlive[i] = false;
				RemoveObject(m_WitchStoneUIs[ws.m_Color]);
				AddObject(m_WitchStoneBrokenUIs[ws.m_Color]);
				m_WitchStones[ws.m_Color]->m_Visible = false;	
			}
		}
	}

	// 스킬
	float filter1 = bd.skill1[m_ClientID].m_CoolTime / SKILL1_COOLTIME;
	float filter2 = bd.skill2[m_ClientID].m_CoolTime / SKILL2_COOLTIME;

	m_Skill1Filter->SetScale(1.f, filter1);
	m_Skill2Filter->SetScale(1.f, filter2);


	for (int i = 0; i < GAME_START_NUMBER; ++i) {
		auto& s1 = bd.skill1[i];
		// 처리할 거 있나

		auto& s2 = bd.skill2[i];
		if (ChRole::WITCH == bd.players[i].m_Role) {
			if (s2.m_Exist) {
				m_Sphere->SetPosition(s2.m_Position);
				if (false == m_RenderSphere) {
					ReaddObject(m_Sphere);
					m_RenderSphere = true;
				}
			}
			else {
				if (true == m_RenderSphere) {
					RemoveObject(m_Sphere);
					m_RenderSphere = false;
				}
			}
		}
	}

	// UI 업데이트
	(*m_SoulCount) = to_string(bd.soulCount);

	// cool 업데이트
	if (m_LBtnCurrCool > 0.f) m_LBtnCurrCool -= frame_time;
}

void BattleScene::HandleInput(InputSystem& input)
{
	if (m_Death) {
		UINT m_msg;
		int x, y;
		// 입력 쌓이지 않게 pop 해주기
		MouseManager::PopMouseInput(m_msg, x, y);
		KeyboardEvent evt = KeyboardManager::GetEvent();
		return;
	}

	// 마우스 이동량 Send
	auto frame_delta = MouseManager::GetDelta();
	if (frame_delta.x != 0 || frame_delta.y != 0)
		NetworkSystem::SendMousePacket(frame_delta.x, frame_delta.y);


	// 마우스 이벤트 처리
	UINT m_msg;
	int x, y;
	if (MouseManager::PopMouseInput(m_msg, x, y)) 
	{
		switch (m_msg) {
		case WM_LBUTTONDOWN: {
			yhl::dout << "lbuttondown" << yhl::fls;

			if (m_LBtnCurrCool > 0.f) break;

			if (m_Role == ChRole::WITCH) {
				m_Hand->PlayOnce("Attack");
				NetworkSystem::SendAttackPacket();
				yhl::dout << "Witch Attack Packet" << yhl::fls;
			}
			else {
				CharacterInfo info;
				ReadCharacterInfo(m_ClientID, info);

				if (info.gem)
				{
					NetworkSystem::SendGemThrow();
					m_Hand->PlayOnce("ThrowGem");
				}
				else {
					NetworkSystem::SendGemPickUp();
					m_Hand->PlayOnce("PickGem");
				}
			}
			m_LBtnCurrCool = MOUSE_LBUTTON_COOLTIME;

			break;
		}
		case WM_LBUTTONUP:
			yhl::dout << "lbuttonup" << yhl::fls;
			break;
		case WM_RBUTTONDOWN:
			yhl::dout << "rbuttondown" << yhl::fls;
			NetworkSystem::SendUseSkill2();
			break;
		case WM_RBUTTONUP:
			yhl::dout << "rbuttonup" << yhl::fls;
			break;
		default:
			break;
		}
	}


	// key 이벤트 처리
	KeyboardEvent evt = KeyboardManager::GetEvent();
	if (KeyboardEvent::NONE == evt) return;

	switch (evt) {
		//이동
	case KeyboardEvent::KEY_DOWN_W:
		NetworkSystem::SendInputPacket(MOVE_FOWARD);
		m_MoveKeyDownCount += 1;
		if (1 == m_MoveKeyDownCount) {
			m_Hand->SetAnimation("Walk_NW");
		}
		break;
	case KeyboardEvent::KEY_DOWN_A:
		NetworkSystem::SendInputPacket(MOVE_LEFT);
		m_MoveKeyDownCount += 1;
		if (1 == m_MoveKeyDownCount) {
			m_Hand->SetAnimation("Walk_NW");
		}
		break;
	case KeyboardEvent::KEY_DOWN_S:
		NetworkSystem::SendInputPacket(MOVE_BACKWARD);
		m_MoveKeyDownCount += 1;
		if (1 == m_MoveKeyDownCount) {
			m_Hand->SetAnimation("Walk_NW");
		}
		break;
	case KeyboardEvent::KEY_DOWN_D:
		NetworkSystem::SendInputPacket(MOVE_RIGHT);
		m_MoveKeyDownCount += 1;
		if (1 == m_MoveKeyDownCount) {
			m_Hand->SetAnimation("Walk_NW");
		}
		break;

	case KeyboardEvent::KEY_UP_W:
		NetworkSystem::SendInputPacket(STOP_MOVE_FORWARD);
		m_MoveKeyDownCount -= 1;
		if (0 == m_MoveKeyDownCount) {
			m_Hand->SetAnimation("Idle_NW");
		}
		break;
	case KeyboardEvent::KEY_UP_A:
		NetworkSystem::SendInputPacket(STOP_MOVE_LEFT);
		m_MoveKeyDownCount -= 1;
		if (0 == m_MoveKeyDownCount) {
			m_Hand->SetAnimation("Idle_NW");
		}
		break;
	case KeyboardEvent::KEY_UP_S:
		NetworkSystem::SendInputPacket(STOP_MOVE_BACKWARD);
		m_MoveKeyDownCount -= 1;
		if (0 == m_MoveKeyDownCount) {
			m_Hand->SetAnimation("Idle_NW");
		}
		break;
	case KeyboardEvent::KEY_UP_D:
		NetworkSystem::SendInputPacket(STOP_MOVE_RIGHT);
		m_MoveKeyDownCount -= 1;
		if (0 == m_MoveKeyDownCount) {
			m_Hand->SetAnimation("Idle_NW");
		}
		break;

		// 스킬
	case KeyboardEvent::KEY_DOWN_Q:
		NetworkSystem::SendInputPacket(KD_SKILL1);
		break;
	case KeyboardEvent::KEY_UP_Q:
		NetworkSystem::SendInputPacket(KU_SKILL1);
		break;

		// 치트키

	default:
		break;
	}
}

void BattleScene::Destroy()
{
	MouseManager::ChangeMouseMode(MouseMode::DEFAULT_MODE);

	m_ClientID = -1;
	m_Role = ChRole::NORMAL;
	m_Characters.clear();
	m_Gems.clear();
	m_WitchStones.clear();
	m_WitchStoneUIs.clear();
	m_WitchStoneBrokenUIs.clear();
	for (int i = 0; i < 4; ++i) m_WitchStoneAlive[i] = true;
	m_Hand = nullptr;
	m_Font = nullptr;
	m_EngFont = nullptr;
	m_SoulCount = nullptr;
	m_Sphere = nullptr;
	m_RenderSphere = false;

	m_TextSpeed = nullptr;
	m_TextTimer = nullptr;

	for (auto& [pso, vec] : m_Objects) {
		for (auto& obj : vec) {
			delete obj;
		}
		vec.clear();
	}
	m_Objects.clear();

	// sound
	// g_Sound.pSound[BATTLE_SOUND]->release();
	g_Sound.pChannel[BATTLE_SOUND]->stop();
}
