#pragma once
#include "SystemNode.h"
#include "GraphicsCore.h"

using namespace Graphics;

class SystemCommunicator;

class GraphicsSystem : SystemNode
{
public:
	explicit GraphicsSystem(SystemCommunicator* const communicator);
	~GraphicsSystem();

public:
	void Initialize() override;
	
	void Update(Scene* scene);

private:
	static bool s_Instantiated;
};
