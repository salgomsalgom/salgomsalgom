#pragma once
#include "GpuResource.h"

class DepthBuffer : public GpuResource
{
public:
	DepthBuffer();
	~DepthBuffer() { }

public:
	void Create();
};
