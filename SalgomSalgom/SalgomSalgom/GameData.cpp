#include "pch.h"
#include "GameData.h"

#include "Mesh.h"
#include "Texture.h"

#include "Vertex.h"
#include "Animation.h"

#include "D2b.h"
#include "HeightMap.h"

namespace StaticGameData
{
	std::map<MeshKey, Mesh*>			g_Meshs;
	std::map<TextureKey, Texture*>		g_Textures;
	std::map<MeshKey, Animation*>		g_Animations;
}

void LoadTextures()  
{
	StaticGameData::g_Textures.try_emplace(TextureKey::LOBBY_BG, new Texture(L"LOBBY_BG.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::LOBBY_BG_FWD, new Texture(L"LOBBY_BG_FWD.dds"));

	StaticGameData::g_Textures.try_emplace(TextureKey::SKYBOX_SIDE, new Texture(L"NightSkySide.dds"));

	StaticGameData::g_Textures.try_emplace(TextureKey::TERN, new Texture(L"TERN.dds"));

	StaticGameData::g_Textures.try_emplace(TextureKey::PKN1_PKN2, new Texture(L"PKN1.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::MSH1, new Texture(L"MSH1.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::TREE, new Texture(L"TREE.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::ARCH, new Texture(L"ARCH.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::PKN3, new Texture(L"PKN3.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BASN, new Texture(L"BASN.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::ONIO, new Texture(L"ONIO.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::MSH2, new Texture(L"MSH2.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::ROK1, new Texture(L"ROK1.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::ROK2, new Texture(L"ROK2.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::LTRE, new Texture(L"LTRE.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::SNAL, new Texture(L"SNAL.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::CATS, new Texture(L"CATS.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::SOFA, new Texture(L"SOFA.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::TX97, new Texture(L"TX97.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BPOT, new Texture(L"BPOT.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::STL1_STL2, new Texture(L"STL1.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::HFPK, new Texture(L"HFPK.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::FENC, new Texture(L"FENC.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BASN, new Texture(L"BASN.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BSF1, new Texture(L"BSF1.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BSF2, new Texture(L"BSF2.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::FBU1, new Texture(L"FBU1.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::FBU2, new Texture(L"FBU2.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::TX64, new Texture(L"TX64.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::MOPP, new Texture(L"MOPP.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::INLI, new Texture(L"INLI.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::SIGN, new Texture(L"SIGN.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::SPMP, new Texture(L"SPMP.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::LEV1, new Texture(L"LEV1.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::LEV2, new Texture(L"LEV2.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::DRW2, new Texture(L"DRW2.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::DRW1, new Texture(L"DRW1.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::LEAV, new Texture(L"LEAV.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::GEMB, new Texture(L"GEMB.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::GEMP, new Texture(L"GEMP.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::GEMR, new Texture(L"GEMR.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::GEMW, new Texture(L"GEMW.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::MSTB, new Texture(L"MSTB.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::MSTP, new Texture(L"MSTP.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::MSTR, new Texture(L"MSTR.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::MSTW, new Texture(L"MSTW.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::FENC, new Texture(L"FENC.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BENC, new Texture(L"BENC.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::MPED, new Texture(L"MPED.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::DESK, new Texture(L"DESK.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::PDW1, new Texture(L"PDW1.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::PDW2, new Texture(L"PDW2.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::WICH, new Texture(L"WICH.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::NMGD, new Texture(L"NMGD.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::TXNM, new Texture(L"TXNM.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::SCS1, new Texture(L"SCS1.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::SCS2, new Texture(L"SCS2.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::ROOT, new Texture(L"ROOT.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::CUSN, new Texture(L"TX97.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::COMEHERE, new Texture(L"COMEHERE.dds"));

	// UI
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_WS_LEFT, new Texture(L"UI_WS_LEFT.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_WS_RIGHT, new Texture(L"UI_WS_RIGHT.dds"));

	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_WS_BLUE, new Texture(L"UI_WS_BLUE.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_WS_PURPLE, new Texture(L"UI_WS_PURPLE.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_WS_RED, new Texture(L"UI_WS_RED.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_WS_WHITE, new Texture(L"UI_WS_WHITE.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_WS_BLUE_BROKEN, new Texture(L"UI_WS_BLUE_BROKEN.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_WS_PURPLE_BROKEN, new Texture(L"UI_WS_PURPLE_BROKEN.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_WS_RED_BROKEN, new Texture(L"UI_WS_RED_BROKEN.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_WS_WHITE_BROKEN, new Texture(L"UI_WS_WHITE_BROKEN.dds"));

	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_SKILL_FRAME, new Texture(L"UI_SKILL_FRAME.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_SKILL_DASH_ICON, new Texture(L"UI_SKILL_DASH_ICON.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_SKILL_FAKE_DIE_ICON, new Texture(L"UI_SKILL_FAKE_DIE_ICON.dds"));
	
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_SOUL_FONT, new Texture(L"UI_SOUL_FONT.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_SOUL_ICON, new Texture(L"UI_SOUL_ICON.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_SKILL_HALT_ICON, new Texture(L"UI_SKILL_HALT_ICON.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_SKILL_GEM_TRANSPARENT_ICON, new Texture(L"UI_SKILL_GEMTRANS_ICON.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_SPEED_ICON, new Texture(L"UI_SPEED_ICON.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_TIMER_ICON, new Texture(L"UI_TIMER_ICON.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_ENGFONT, new Texture(L"UI_ENGFONT.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_SKILL_COOLTIME_FILTER, new Texture(L"BATTLE_UI_SKILL_COOLTIME_FILTER.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_BAR_BG, new Texture(L"BATTLE_UI_BAR_BG.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_BAR_FILL, new Texture(L"BATTLE_UI_BAR_FILL.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_BAR_FAKEDEATH, new Texture(L"BATTLE_UI_BAR_FAKEDEATH.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_BAR_RESPAWN, new Texture(L"BATTLE_UI_BAR_RESPAWN.dds"));

	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_CHARSKILL_BG, new Texture(L"CHARSKILL_BG.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_CHARSKILL_FONT_BLUE, new Texture(L"CHARSKILL_BLUE_FONT.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::BATTLE_UI_CHARSKILL_FONT_RED, new Texture(L"CHARSKILL_RED_FONT.dds"));

	StaticGameData::g_Textures.try_emplace(TextureKey::LOBBY_GAME_START_BUTTON, new Texture(L"UI_GAME_START_BUTTON.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::LOBBY_GAME_QUIT_BUTTON, new Texture(L"UI_GAME_QUIT_BUTTON.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::LOBBY_ID_ICON, new Texture(L"UI_ID_ICON.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::LOBBY_ID_BG, new Texture(L"UI_ID_BG.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::LOBBY_LOADING_ICON, new Texture(L"UI_LOADING_ICON.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::LOBBY_QUIT_FINDING_GAME, new Texture(L"UI_QUIT_FINDING_GAME.dds"));
	
	//스프라이트 애니메이션즈
	StaticGameData::g_Textures.try_emplace(TextureKey::LOBBY_SPANI_SPIDER, new Texture(L"SPANI_LOBBY_SPIDER.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::LOBBY_SPANI_WITCH, new Texture(L"SPANI_LOBBY_WITCH.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::LOBBY_SPANI_NMGD1, new Texture(L"SPANI_LOBBY_NMGD1.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::LOBBY_SPANI_NMGD2, new Texture(L"SPANI_LOBBY_NMGD2.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::LOBBY_SPANI_NMGD3, new Texture(L"SPANI_LOBBY_NMGD3.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::LOBBY_SPANI_NMGD4, new Texture(L"SPANI_LOBBY_NMGD4.dds"));

	StaticGameData::g_Textures.try_emplace(TextureKey::LOGIN_BG, new Texture(L"LOGIN_BG.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::LOGIN_LOGO, new Texture(L"LOGIN_LOGO.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::LOGIN_SELECTED, new Texture(L"LOGIN_SELECTED.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::LOGIN_LOGIN_BOX, new Texture(L"LOGIN_LOGIN_BOX.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::LOGIN_REGISTER_BOX, new Texture(L"LOGIN_REGISTER_BOX.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::LOGIN_QUIT_BUTTON, new Texture(L"LOGIN_QUIT_BUTTON.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::LOGIN_LOGIN_BUTTON, new Texture(L"LOGIN_LOGIN_BUTTON.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::LOGIN_REGISTER_BUTTON, new Texture(L"LOGIN_REGISTER_BUTTON.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::LOGIN_BACK_BUTTON, new Texture(L"LOGIN_BACK_BUTTON.dds"));

	StaticGameData::g_Textures.try_emplace(TextureKey::NMWIN_BG, new Texture(L"NMWIN_BG.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::NMWIN_WICH, new Texture(L"NMWIN_WICH.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::NMWIN_NM1, new Texture(L"NMWIN_NM1.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::NMWIN_NM2, new Texture(L"NMWIN_NM2.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::NMWIN_NM3, new Texture(L"NMWIN_NM3.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::NMWIN_NM4, new Texture(L"NMWIN_NM4.dds"));

	StaticGameData::g_Textures.try_emplace(TextureKey::WCWIN_BG, new Texture(L"WCWIN_BG.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::WCWIN_WICH, new Texture(L"WCWIN_WICH.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::WCWIN_NM1, new Texture(L"WCWIN_NM1.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::WCWIN_NM2, new Texture(L"WCWIN_NM2.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::WCWIN_SOUL, new Texture(L"WCWIN_SOUL.dds"));

	StaticGameData::g_Textures.try_emplace(TextureKey::EF_CONFETTI, new Texture(L"EF_CONFETTI.dds"));
	StaticGameData::g_Textures.try_emplace(TextureKey::RESULT_TOLOBBY_ICON, new Texture(L"RESULT_TOLOBBY_ICON.dds"));
}

void StoreD2bData(MeshKey key, const string& filepath) {
	D2bBuf buf;
	buf.LoadD2b(filepath);
	StaticGameData::g_Meshs.emplace(key, new Mesh(buf));
	if (buf.header.rigged) 
		StaticGameData::g_Animations.emplace(key, new Animation(buf));
}

void BuildGeometryMesh()
{
	HeightMap* hm = new HeightMap("./Assets/Map/TER2.hm"s);
	StaticGameData::g_Meshs.emplace(MeshKey::TERN, new GridMesh(XMFLOAT3(1.f, HEIGHTMAP_Y_FACTOR, 1.f), hm));

	StaticGameData::g_Meshs.emplace(MeshKey::SKYBOX, new CubeMesh(5));

	StaticGameData::g_Meshs.emplace(MeshKey::CUBE, new CubeMesh(1));
	
	StaticGameData::g_Meshs.emplace(MeshKey::RECTANGLE, new RectangleMesh(2, 2));

	yhl::dout << "build geometry mesh" << yhl::fls;
}

void LoadD2bFiles()
{
	StoreD2bData(MeshKey::TREE, "./Assets/D2b/TREE.d2b");
	StoreD2bData(MeshKey::PKN1, "./Assets/D2b/PKN1.d2b");
	StoreD2bData(MeshKey::ARCH, "./Assets/D2b/ARCH.d2b");
	StoreD2bData(MeshKey::PKN2_PKN3, "./Assets/D2b/PKN3.d2b");
	StoreD2bData(MeshKey::HFPK, "./Assets/D2b/HFPK.d2b");
	StoreD2bData(MeshKey::ONIO, "./Assets/D2b/ONIO.d2b");
	StoreD2bData(MeshKey::MSH1, "./Assets/D2b/MSH1.d2b");
	StoreD2bData(MeshKey::MSH2, "./Assets/D2b/MSH2.d2b");
	StoreD2bData(MeshKey::ROK1, "./Assets/D2b/ROK1.d2b");
	StoreD2bData(MeshKey::ROK2, "./Assets/D2b/ROK2.d2b");
	StoreD2bData(MeshKey::LTRE, "./Assets/D2b/LTRE.d2b");
	StoreD2bData(MeshKey::SNAL, "./Assets/D2b/SNAL.d2b");
	StoreD2bData(MeshKey::CATS, "./Assets/D2b/CATS.d2b");
	StoreD2bData(MeshKey::SOFA, "./Assets/D2b/SOFA.d2b");
	StoreD2bData(MeshKey::CHAR, "./Assets/D2b/CHAR.d2b");
	StoreD2bData(MeshKey::BPOT, "./Assets/D2b/BPOT.d2b");
	StoreD2bData(MeshKey::STL1, "./Assets/D2b/STL1.d2b");
	StoreD2bData(MeshKey::STL2, "./Assets/D2b/STL2.d2b");
	StoreD2bData(MeshKey::BSF1, "./Assets/D2b/BSF1.d2b");
	StoreD2bData(MeshKey::BSF2, "./Assets/D2b/BSF2.d2b");
	StoreD2bData(MeshKey::FBU1_FBU2, "./Assets/D2b/FBU1.d2b");
	StoreD2bData(MeshKey::SCS1, "./Assets/D2b/SCS1.d2b");
	StoreD2bData(MeshKey::SCS2, "./Assets/D2b/SCS2.d2b");
	StoreD2bData(MeshKey::MOPP, "./Assets/D2b/MOPP.d2b");
	StoreD2bData(MeshKey::INLI, "./Assets/D2b/INLI.d2b");
	StoreD2bData(MeshKey::ROT1, "./Assets/D2b/ROT1.d2b");
	StoreD2bData(MeshKey::ROT2, "./Assets/D2b/ROT2.d2b");
	StoreD2bData(MeshKey::ROT3, "./Assets/D2b/ROT3.d2b");
	StoreD2bData(MeshKey::ROT4, "./Assets/D2b/ROT4.d2b");
	StoreD2bData(MeshKey::CUSN, "./Assets/D2b/CUSN.d2b");
	StoreD2bData(MeshKey::SIGN, "./Assets/D2b/SIGN.d2b");
	StoreD2bData(MeshKey::SPMP, "./Assets/D2b/SPMP.d2b");
	StoreD2bData(MeshKey::LEV1, "./Assets/D2b/LEV1.d2b");
	StoreD2bData(MeshKey::LEV2, "./Assets/D2b/LEV2.d2b");
	StoreD2bData(MeshKey::DRW1, "./Assets/D2b/DRW1.d2b");
	StoreD2bData(MeshKey::DRW2, "./Assets/D2b/DRW2.d2b");
	StoreD2bData(MeshKey::NOST, "./Assets/D2b/NOST.d2b");
	StoreD2bData(MeshKey::MGST, "./Assets/D2b/MGST.d2b");
	StoreD2bData(MeshKey::LEAV, "./Assets/D2b/LEAV.d2b");
	StoreD2bData(MeshKey::FENC, "./Assets/D2b/FENC.d2b");
	StoreD2bData(MeshKey::MPED, "./Assets/D2b/MPED.d2b");
	StoreD2bData(MeshKey::BENC, "./Assets/D2b/BENC.d2b");
	StoreD2bData(MeshKey::PDW1, "./Assets/D2b/PDW1.d2b");
	StoreD2bData(MeshKey::PDW2, "./Assets/D2b/PDW2.d2b");
	StoreD2bData(MeshKey::WIST, "./Assets/D2b/WIST.d2b");
	StoreD2bData(MeshKey::MGEM, "./Assets/D2b/MGEM.d2b");
	StoreD2bData(MeshKey::BASN, "./Assets/D2b/BASN.d2b");
	StoreD2bData(MeshKey::DESK, "./Assets/D2b/DESK.d2b");

	// 스킬
	StoreD2bData(MeshKey::SPHERE, "./Assets/D2b/COMEHERE.d2b");

	// 곰시키들
	StoreD2bData(MeshKey::NMGD, "./Assets/D2b/NMGD.d2b");
	StoreD2bData(MeshKey::WICH, "./Assets/D2b/WICH.d2b");

	StoreD2bData(MeshKey::NM_HAND, "./Assets/D2b/NMGD_HAND.d2b");
	StoreD2bData(MeshKey::WC_HAND, "./Assets/D2b/WICH_HAND.d2b");
}
