#include "pch.h"
#include "Mesh.h"

#include "Vertex.h"
#include "GameData.h"
#include "D2b.h"

Mesh::Mesh(D3D12_PRIMITIVE_TOPOLOGY primitive_topology) :
	m_VertexBuffer(),
	m_IndexBuffer(),
	m_VertexUploadBuffer(),
	m_IndexUploadBuffer(),
	m_VertexBufferView(),
	m_IndexBufferView(),
	m_PrimitiveTopology(primitive_topology)
{
}

Mesh::Mesh(UINT v_size, UINT v_count, void* pV, UINT i_count, void* pI, ID3D12GraphicsCommandList* command_list)
{
	m_VertexBuffer.Create(v_size * v_count);
	m_VertexUploadBuffer.Create(v_size * v_count);
	m_IndexBuffer.Create(sizeof(short) * i_count);
	m_IndexUploadBuffer.Create(sizeof(short) * i_count);

	// 업로드 버퍼에 데이터 복사
	UINT8* data_begin = reinterpret_cast<UINT8*>(m_VertexUploadBuffer.GetCPUPointer());
	memcpy(data_begin, pV, v_size * v_count);
	m_VertexUploadBuffer->Unmap(NULL, NULL);

	// 버텍스 버퍼에 데이터 복사
	command_list->CopyResource(m_VertexBuffer.Get(), m_VertexUploadBuffer.Get());

	// 버퍼 뷰 설정
	m_VertexBufferView.BufferLocation = m_VertexBuffer->GetGPUVirtualAddress();
	m_VertexBufferView.SizeInBytes = v_size * v_count;
	m_VertexBufferView.StrideInBytes = v_size;


	// 업로드 버퍼에 데이터 복사
	data_begin = reinterpret_cast<UINT8*>(m_IndexUploadBuffer.GetCPUPointer());
	memcpy(data_begin, pI, sizeof(short) * i_count);
	m_IndexUploadBuffer->Unmap(NULL, NULL);

	// 인덱스 버퍼에 데이터 복사
	command_list->CopyResource(m_IndexBuffer.Get(), m_IndexUploadBuffer.Get());

	// 버퍼 뷰 설정
	m_IndexBufferView.BufferLocation = m_IndexBuffer->GetGPUVirtualAddress();
	m_IndexBufferView.Format = DXGI_FORMAT_R16_UINT;
	m_IndexBufferView.SizeInBytes = sizeof(short) * i_count;

	// 리소스의 상태를 바꿔주어야 한다. ( COPY_DEST -> 각각 BUFFER )
	D3D12_RESOURCE_BARRIER resource_barrier[2];
	resource_barrier[0].Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	resource_barrier[0].Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	resource_barrier[0].Transition.pResource = m_VertexBuffer.Get();
	resource_barrier[0].Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_DEST;
	resource_barrier[0].Transition.StateAfter = D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER;
	resource_barrier[0].Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;

	resource_barrier[1].Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	resource_barrier[1].Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	resource_barrier[1].Transition.pResource = m_IndexBuffer.Get();
	resource_barrier[1].Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_DEST;
	resource_barrier[1].Transition.StateAfter = D3D12_RESOURCE_STATE_INDEX_BUFFER;
	resource_barrier[1].Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
	command_list->ResourceBarrier(2, resource_barrier);
}

Mesh::Mesh(D2bBuf& buf)
	: m_PrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST)
	, m_VertexCount(buf.header.v_count)
	, m_IndexCount(buf.header.i_count)
{
	UINT64 v_buf_size = buf.header.v_count * buf.GetVertexBytes();
	UINT64 i_buf_size = buf.header.i_count * sizeof(buf.i_buf[0]);

	// 버퍼 생성
	m_VertexBuffer.Create(v_buf_size);
	m_IndexBuffer.Create(i_buf_size);

	// 업로드 버퍼 생성
	m_VertexUploadBuffer.Create(v_buf_size);
	m_IndexUploadBuffer.Create(i_buf_size);

	// 업로드 버퍼에 데이터 카피
	m_VertexUploadBuffer.CopyData(buf.v_buf);
	m_IndexUploadBuffer.CopyData(buf.i_buf);

	// 버텍스 버퍼에 업로드 데이터 카피
	m_VertexBuffer.CopyData(m_VertexUploadBuffer);
	m_IndexBuffer.CopyData(m_IndexUploadBuffer);

	// Buffer View 채우기
	m_VertexBufferView.BufferLocation = m_VertexBuffer->GetGPUVirtualAddress();
	m_VertexBufferView.SizeInBytes = v_buf_size;
	m_VertexBufferView.StrideInBytes = buf.GetVertexBytes();

	m_IndexBufferView.BufferLocation = m_IndexBuffer->GetGPUVirtualAddress();
	m_IndexBufferView.SizeInBytes = i_buf_size;
	m_IndexBufferView.Format = DXGI_FORMAT_R16_UINT;

	m_VertexBuffer.TransitionResourceState(D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
	m_IndexBuffer.TransitionResourceState(D3D12_RESOURCE_STATE_INDEX_BUFFER);
}

#include "GraphicsCore.h"

RectangleMesh::RectangleMesh(float width, float height)
	: Mesh(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST)
{
	m_VertexCount = 4;
	m_IndexCount = 6;

	PosTex vertices[4];

	float l	= -(width / 2);
	float r	= +(width / 2);
	float b	= -(height / 2);
	float t	= +(height / 2);

	// float l = -1;
	// float r = l + (width);
	// float b = -1;
	// float t = b + (height);


	vertices[0].m_Position = XMFLOAT3(l, t, 0);
	vertices[0].m_Texcoord = XMFLOAT2(0, 0);
	vertices[1].m_Position = XMFLOAT3(r, t, 0);
	vertices[1].m_Texcoord = XMFLOAT2(1, 0);
	vertices[2].m_Position = XMFLOAT3(l, b, 0);
	vertices[2].m_Texcoord = XMFLOAT2(0, 1);
	vertices[3].m_Position = XMFLOAT3(r, b, 0);
	vertices[3].m_Texcoord = XMFLOAT2(1, 1);

	unsigned short indices[6] = { 0, 1, 2, 2, 1, 3 };

	// 버퍼 세팅
	m_VertexBuffer.Create(sizeof(PosTex) * m_VertexCount);
	m_IndexBuffer.Create(sizeof(short) * m_IndexCount);
	m_VertexUploadBuffer.Create(sizeof(PosTex) * m_VertexCount);
	m_IndexUploadBuffer.Create(sizeof(short) * m_IndexCount);

	// 버텍스 버퍼 설정
	UINT8* data_begin = reinterpret_cast<UINT8*>(m_VertexUploadBuffer.GetCPUPointer());
	memcpy(data_begin, vertices, sizeof(PosTex) * m_VertexCount);
	m_VertexUploadBuffer->Unmap(NULL, NULL);

	// 인덱스 버퍼 설정
	data_begin = reinterpret_cast<UINT8*>(m_IndexUploadBuffer.GetCPUPointer());
	memcpy(data_begin, indices, sizeof(short) * m_IndexCount);
	m_IndexUploadBuffer->Unmap(NULL, NULL);

	InitBuffers();
}

void RectangleMesh::InitBuffers(/*ID3D12GraphicsCommandList* command_list*/)
{
	auto command_list = Graphics::GraphicsCore::GetCommandList();

	command_list->CopyResource(m_VertexBuffer.Get(), m_VertexUploadBuffer.Get());
	command_list->CopyResource(m_IndexBuffer.Get(), m_IndexUploadBuffer.Get());

	D3D12_RESOURCE_BARRIER resource_barrier[2];
	resource_barrier[0].Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	resource_barrier[0].Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	resource_barrier[0].Transition.pResource = m_VertexBuffer.Get();
	resource_barrier[0].Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_DEST;
	resource_barrier[0].Transition.StateAfter = D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER;
	resource_barrier[0].Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;

	resource_barrier[1].Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	resource_barrier[1].Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	resource_barrier[1].Transition.pResource = m_IndexBuffer.Get();
	resource_barrier[1].Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_DEST;
	resource_barrier[1].Transition.StateAfter = D3D12_RESOURCE_STATE_INDEX_BUFFER;
	resource_barrier[1].Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
	command_list->ResourceBarrier(2, resource_barrier);


	m_VertexBufferView.BufferLocation = m_VertexBuffer->GetGPUVirtualAddress();
	m_VertexBufferView.SizeInBytes = sizeof(PosTex) * m_VertexCount;
	m_VertexBufferView.StrideInBytes = sizeof(PosTex);

	m_IndexBufferView.BufferLocation = m_IndexBuffer->GetGPUVirtualAddress();
	m_IndexBufferView.Format = DXGI_FORMAT_R16_UINT;
	m_IndexBufferView.SizeInBytes = sizeof(short) * m_IndexCount;
}

CubeMesh::CubeMesh(float size) :
	Mesh(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST)
{
	m_VertexCount = 24;
	m_IndexCount = 36;

	auto command_list = Graphics::GraphicsCore::GetCommandList();

	m_VertexBuffer.Create(sizeof(PosTex) * m_VertexCount);
	m_IndexBuffer.Create(sizeof(short) * m_IndexCount);
	m_VertexUploadBuffer.Create(sizeof(PosTex) * m_VertexCount);
	m_IndexUploadBuffer.Create(sizeof(short) * m_IndexCount);

	float half = size / 2;
	
	PosTex vertices[24];

	{
		// +x plane: 0 ~ 3
		vertices[0].m_Position = XMFLOAT3(+half, +half, +half);
		vertices[1].m_Position = XMFLOAT3(+half, +half, -half);
		vertices[2].m_Position = XMFLOAT3(+half, -half, -half);
		vertices[3].m_Position = XMFLOAT3(+half, -half, +half);
		vertices[0].m_Texcoord = XMFLOAT2(0.0f, 0.0f);
		vertices[1].m_Texcoord = XMFLOAT2(1.0f, 0.0f);
		vertices[2].m_Texcoord = XMFLOAT2(1.0f, 1.0f);
		vertices[3].m_Texcoord = XMFLOAT2(0.0f, 1.0f);

		// -x plane: 4 ~ 7
		vertices[4].m_Position = XMFLOAT3(-half, +half, +half);
		vertices[5].m_Position = XMFLOAT3(-half, +half, -half);
		vertices[6].m_Position = XMFLOAT3(-half, -half, -half);
		vertices[7].m_Position = XMFLOAT3(-half, -half, +half);
		vertices[4].m_Texcoord = XMFLOAT2(1.0f, 0.0f);
		vertices[5].m_Texcoord = XMFLOAT2(0.0f, 0.0f);
		vertices[6].m_Texcoord = XMFLOAT2(0.0f, 1.0f);
		vertices[7].m_Texcoord = XMFLOAT2(1.0f, 1.0f);

		// +y plane: 8 ~ 11
		vertices[8].m_Position = XMFLOAT3(+half, +half, +half);
		vertices[9].m_Position = XMFLOAT3(+half, +half, -half);
		vertices[10].m_Position = XMFLOAT3(-half, +half, -half);
		vertices[11].m_Position = XMFLOAT3(-half, +half, +half);
		vertices[8].m_Texcoord = XMFLOAT2(0.0f, 0.0f);
		vertices[9].m_Texcoord = XMFLOAT2(1.0f, 0.0f);
		vertices[10].m_Texcoord = XMFLOAT2(0.0f, 1.0f);
		vertices[11].m_Texcoord = XMFLOAT2(1.0f, 1.0f);

		// -y plane: 12 ~ 15
		vertices[12].m_Position = XMFLOAT3(+half, -half, +half);
		vertices[13].m_Position = XMFLOAT3(+half, -half, -half);
		vertices[14].m_Position = XMFLOAT3(-half, -half, -half);
		vertices[15].m_Position = XMFLOAT3(-half, -half, +half);
		vertices[12].m_Texcoord = XMFLOAT2(0.0f, 0.0f);
		vertices[13].m_Texcoord = XMFLOAT2(0.0f, 0.0f);
		vertices[14].m_Texcoord = XMFLOAT2(0.0f, 0.0f);
		vertices[15].m_Texcoord = XMFLOAT2(0.0f, 0.0f);

		// +z plane: 15 ~ 19
		vertices[16].m_Position = XMFLOAT3(+half, +half, +half);
		vertices[17].m_Position = XMFLOAT3(+half, -half, +half);
		vertices[18].m_Position = XMFLOAT3(-half, -half, +half);
		vertices[19].m_Position = XMFLOAT3(-half, +half, +half);
		vertices[16].m_Texcoord = XMFLOAT2(1.0f, 0.0f);
		vertices[17].m_Texcoord = XMFLOAT2(1.0f, 1.0f);
		vertices[18].m_Texcoord = XMFLOAT2(0.0f, 1.0f);
		vertices[19].m_Texcoord = XMFLOAT2(0.0f, 0.0f);

		// -z plane: 20 ~ 23
		vertices[20].m_Position = XMFLOAT3(+half, +half, -half);
		vertices[21].m_Position = XMFLOAT3(+half, -half, -half);
		vertices[22].m_Position = XMFLOAT3(-half, -half, -half);
		vertices[23].m_Position = XMFLOAT3(-half, +half, -half);
		vertices[20].m_Texcoord = XMFLOAT2(0.0f, 0.0f);
		vertices[21].m_Texcoord = XMFLOAT2(0.0f, 1.0f);
		vertices[22].m_Texcoord = XMFLOAT2(1.0f, 1.0f);
		vertices[23].m_Texcoord = XMFLOAT2(1.0f, 0.0f);
	}

	// 업로드 버퍼에 데이터 복사
	UINT8* data_begin = reinterpret_cast<UINT8*>(m_VertexUploadBuffer.GetCPUPointer());
	memcpy(data_begin, vertices, sizeof(PosTex) * m_VertexCount);
	m_VertexUploadBuffer->Unmap(NULL, NULL);

	// 버텍스 버퍼에 데이터 복사
	command_list->CopyResource(m_VertexBuffer.Get(), m_VertexUploadBuffer.Get());

	// 버퍼 뷰 설정
	m_VertexBufferView.BufferLocation = m_VertexBuffer->GetGPUVirtualAddress();
	m_VertexBufferView.SizeInBytes = sizeof(PosTex) * m_VertexCount;
	m_VertexBufferView.StrideInBytes = sizeof(PosTex);

	// 인덱스 정보 구성
	// 향하는 면을 신경써야함
	UINT16 indices[36] = {
		0, 1, 2, 3, 0, 2,
		4, 7, 5, 5, 7, 6,
		9, 8, 10, 10, 8, 11,
		12, 13, 15, 15, 13, 14,
		16, 17, 19, 19, 17, 18,
		20, 23, 21, 21, 23, 22
	};

	// 업로드 버퍼에 데이터 복사
	data_begin = reinterpret_cast<UINT8*>(m_IndexUploadBuffer.GetCPUPointer());
	memcpy(data_begin, indices, sizeof(short) * m_IndexCount);
	m_IndexUploadBuffer->Unmap(NULL, NULL);

	// 인덱스 버퍼에 데이터 복사
	command_list->CopyResource(m_IndexBuffer.Get(), m_IndexUploadBuffer.Get());

	// 버퍼 뷰 설정
	m_IndexBufferView.BufferLocation = m_IndexBuffer->GetGPUVirtualAddress();
	m_IndexBufferView.Format = DXGI_FORMAT_R16_UINT;
	m_IndexBufferView.SizeInBytes = sizeof(short) * m_IndexCount;
	
	// 리소스의 상태를 바꿔주어야 한다. ( COPY_DEST -> 각각 BUFFER )
	D3D12_RESOURCE_BARRIER resource_barrier[2];
	resource_barrier[0].Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	resource_barrier[0].Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	resource_barrier[0].Transition.pResource = m_VertexBuffer.Get();
	resource_barrier[0].Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_DEST;
	resource_barrier[0].Transition.StateAfter = D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER;
	resource_barrier[0].Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
	
	resource_barrier[1].Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	resource_barrier[1].Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	resource_barrier[1].Transition.pResource = m_IndexBuffer.Get();
	resource_barrier[1].Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_DEST;
	resource_barrier[1].Transition.StateAfter = D3D12_RESOURCE_STATE_INDEX_BUFFER;
	resource_barrier[1].Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
	command_list->ResourceBarrier(2, resource_barrier);
}

#include "HeightMap.h"

GridMesh::GridMesh(const DirectX::XMFLOAT3& scale, HeightMap* heightmap)
	: Mesh(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP), m_HeightMap(heightmap)
{
	assert(nullptr != heightmap);
	assert(heightmap->Valid());

	auto command_list = Graphics::GraphicsCore::GetCommandList();

	int row_len = static_cast<int>(heightmap->row_length);
	int col_len = static_cast<int>(heightmap->col_length);

	m_VertexCount = row_len * col_len;
	// m_IndexCount = ((row_len - 1) * (col_len - 1)) * 2 * 3;
	m_IndexCount = ((row_len * 2) * (col_len - 1)) + (col_len - 2);

	int vtxbuf_size = sizeof(TerrainVertex) * m_VertexCount;
	int idxbuf_size = sizeof(short) * m_IndexCount;

	m_VertexBuffer.Create(vtxbuf_size);
	m_IndexBuffer.Create(idxbuf_size);
	m_VertexUploadBuffer.Create(vtxbuf_size);
	m_IndexUploadBuffer.Create(idxbuf_size);

	vector<TerrainVertex> vertices;
	vertices.reserve(m_VertexCount);

	for (int r = 0; r < row_len; ++r) {
		for (int c = 0; c < col_len; ++c) {
			float x = static_cast<float>(r) - (row_len / 2);
			float y = static_cast<float>(m_HeightMap->GetHeight(r, c)) - 129;
			float z = static_cast<float>(c) - (col_len / 2);

			DirectX:XMFLOAT3 normal = m_HeightMap->GetNormalFactor(r, c);

			// 스케일 적용
			x *= scale.x;
			y *= scale.y;
			z *= scale.z;

			normal.x /= scale.x;		// scale이 늘어남에 따라 한 칸이었을 때 변화하는 양
			normal.z /= scale.z;		// scale이 늘어남에 따라 한 칸이었을 때 변화하는 양
			XMMath::Normalize(normal);

			TerrainVertex v;
			v.position = XMFLOAT3(x, y, z);
			v.normal = normal;
			v.texcoord = XMFLOAT2(1.f - (float(r) / float(row_len - 1)), 1.f - float(c) / float(col_len - 1));

			vertices.push_back(v);
		}
	}

	// 업로드 버퍼에 데이터 복사
	UINT8* data_begin = reinterpret_cast<UINT8*>(m_VertexUploadBuffer.GetCPUPointer());
	memcpy(data_begin, vertices.data(), vtxbuf_size);
	m_VertexUploadBuffer->Unmap(NULL, NULL);

	// 버텍스 버퍼에 데이터 복사
	command_list->CopyResource(m_VertexBuffer.Get(), m_VertexUploadBuffer.Get());

	// 버퍼 뷰 설정
	m_VertexBufferView.BufferLocation = m_VertexBuffer->GetGPUVirtualAddress();
	m_VertexBufferView.SizeInBytes = vtxbuf_size;
	m_VertexBufferView.StrideInBytes = sizeof(TerrainVertex);


	// 인덱스 정보 구성
	UINT16* indices = new UINT16[m_IndexCount];

	for (int i = 0, c = 0; c < col_len - 1; c++)
	{
		if ((c % 2) == 0)
		{
			for (int r = 0; r < row_len; r++)
			{
				if ((r == 0) && (c > 0)) indices[i++] = (UINT)(r + (c * row_len));
				indices[i++] = (UINT)(r + (c * row_len));
				indices[i++] = (UINT)((r + (c * row_len)) + row_len);
			}
		}
		else
		{
			for (int x = row_len - 1; x >= 0; x--)
			{
				if (x == (row_len - 1)) indices[i++] = (UINT)(x + (c * row_len));
				indices[i++] = (UINT)(x + (c * row_len));
				indices[i++] = (UINT)((x + (c * row_len)) + row_len);
			}
		}
	}

	// 업로드 버퍼에 데이터 복사
	data_begin = reinterpret_cast<UINT8*>(m_IndexUploadBuffer.GetCPUPointer());
	memcpy(data_begin, indices, idxbuf_size);
	m_IndexUploadBuffer->Unmap(NULL, NULL);

	// 인덱스 버퍼에 데이터 복사
	command_list->CopyResource(m_IndexBuffer.Get(), m_IndexUploadBuffer.Get());

	// 버퍼 뷰 설정
	m_IndexBufferView.BufferLocation = m_IndexBuffer->GetGPUVirtualAddress();
	m_IndexBufferView.Format = DXGI_FORMAT_R16_UINT;
	m_IndexBufferView.SizeInBytes = idxbuf_size;

	m_VertexBuffer.TransitionResourceState(D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
	m_IndexBuffer.TransitionResourceState(D3D12_RESOURCE_STATE_INDEX_BUFFER);
}

GridMesh::~GridMesh()
{
	if (m_HeightMap) delete m_HeightMap;
}
