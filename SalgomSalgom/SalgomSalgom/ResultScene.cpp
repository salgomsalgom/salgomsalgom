#include "pch.h"
#include "ResultScene.h"

#include "Game.h"
#include "InputSystem.h"
#include "GraphicsObject.h"
#include "SpriteAnimation.h"
#include "SpriteFont.h"
#include "Sound.h"


void NormalWinScene::Initialize() {

	//NM_WIN SCENE
	AddObject(new Sprite(1980, 1080, TextureKey::NMWIN_BG));
	
	m_ResultObjs.push_back(new SpriteAnimation(TextureKey::NMWIN_NM1, 205, 590));
	m_ResultObjs.push_back(new SpriteAnimation(TextureKey::NMWIN_NM2, 426, 590));
	m_ResultObjs.push_back(new SpriteAnimation(TextureKey::NMWIN_NM3, 965, 565));
	m_ResultObjs.push_back(new SpriteAnimation(TextureKey::NMWIN_NM4, 1450, 1000));
	m_ResultObjs.push_back(new SpriteAnimation(TextureKey::NMWIN_WICH, 1710, 615));
	m_ResultObjs.push_back(new SpriteAnimation(TextureKey::EF_CONFETTI, 965, 580));
	
	for (int i = 0; i < m_ResultObjs.size(); ++i) {
		AddObject(m_ResultObjs[i]);
	}

	AddObject(new Sprite(TextureKey::RESULT_TOLOBBY_ICON, 100, 100));


	// 사운드
	g_Sound.pChannel[NORMAL_WIN_SOUND]->setVolume(1.0f);
	g_Sound.Play(NORMAL_WIN_SOUND);
}

void NormalWinScene::Update(float frame_time)
{
	m_SceneTime += frame_time;

	for (int i = 0; i < m_ResultObjs.size(); ++i) {
		m_ResultObjs[i]->UpdateIndex();
	}

	if (m_SceneTime > 30.f)
	{
		PushSystemMessage(GameCore::SystemMessage::LOBBY_START);
	}
}

void NormalWinScene::HandleInput(InputSystem& input) {
	// 마우스
	UINT m_msg;
	int x, y;
	if (MouseManager::PopMouseInput(m_msg, x, y))
	{
		switch (m_msg) {
		case WM_LBUTTONDOWN:
			yhl::dout << "lbuttondown" << yhl::fls;
			break;
		case WM_LBUTTONUP:
			yhl::dout << "lbuttonup" << yhl::fls;
			break;
		case WM_RBUTTONDOWN:
			yhl::dout << "rbuttondown" << yhl::fls;
			break;
		case WM_RBUTTONUP:
			yhl::dout << "rbuttonup" << yhl::fls;
			break;
		default:
			break;
		}
	}

	// 키보드
	KeyboardEvent evt = KeyboardManager::GetEvent();
	if (KeyboardEvent::NONE == evt) return;

	switch (evt) {
	case KeyboardEvent::KEY_DOWN_SPACE:
		PushSystemMessage(GameCore::SystemMessage::LOBBY_START);
		break;
	default:
		break;
	}
}

void NormalWinScene::Destroy()
{
	m_SceneTime = 0.f;
	g_Sound.pChannel[NORMAL_WIN_SOUND]->setVolume(0.f);
	//g_Sound.pSound[NORMAL_WIN_SOUND]->release();
	g_Sound.pChannel[NORMAL_WIN_SOUND]->stop();
}

void WitchWinScene::Initialize()
{
	//WC_WIN SCENE
	AddObject(new Sprite(1980, 1080, TextureKey::WCWIN_BG));

	m_ResultObjs.push_back(new SpriteAnimation(TextureKey::WCWIN_WICH, 1400, 745));
	m_ResultObjs.push_back(new SpriteAnimation(TextureKey::WCWIN_NM1, 318, 980));
	m_ResultObjs.push_back(new SpriteAnimation(TextureKey::WCWIN_NM2, 857, 844));
	m_ResultObjs.push_back(new SpriteAnimation(TextureKey::WCWIN_SOUL, 450, 530));

	for (int i = 0; i < m_ResultObjs.size(); ++i) {
		AddObject(m_ResultObjs[i]);
	}

	AddObject(new Sprite(TextureKey::RESULT_TOLOBBY_ICON, 100, 100));
	//g_Sound.pChannel[WITCH_WIN_SOUND]->setVolume(1.0f);
	g_Sound.Play(WITCH_WIN_SOUND);
}

void WitchWinScene::Update(float frame_time)
{
	m_SceneTime += frame_time;

	for (int i = 0; i < m_ResultObjs.size(); ++i) {
		m_ResultObjs[i]->UpdateIndex();
	}

	if (m_SceneTime > 30.f)
	{
		PushSystemMessage(GameCore::SystemMessage::LOBBY_START);
	}
}

void WitchWinScene::HandleInput(InputSystem& input)
{
	// 마우스
	UINT m_msg;
	int x, y;
	if (MouseManager::PopMouseInput(m_msg, x, y))
	{
		switch (m_msg) {
		case WM_LBUTTONDOWN:
			yhl::dout << "lbuttondown" << yhl::fls;
			break;
		case WM_LBUTTONUP:
			yhl::dout << "lbuttonup" << yhl::fls;
			break;
		case WM_RBUTTONDOWN:
			yhl::dout << "rbuttondown" << yhl::fls;
			break;
		case WM_RBUTTONUP:
			yhl::dout << "rbuttonup" << yhl::fls;
			break;
		default:
			break;
		}
	}

	// 키보드
	KeyboardEvent evt = KeyboardManager::GetEvent();
	if (KeyboardEvent::NONE == evt) return;

	switch (evt) {
	case KeyboardEvent::KEY_DOWN_SPACE:
		PushSystemMessage(GameCore::SystemMessage::LOBBY_START);
		break;
	default:
		break;
	}
}

void WitchWinScene::Destroy()
{
	m_SceneTime = 0.f;
	g_Sound.pChannel[WITCH_WIN_SOUND]->setVolume(0.f);
	//g_Sound.pSound[WITCH_WIN_SOUND]->release();
	g_Sound.pChannel[WITCH_WIN_SOUND]->stop();
}
