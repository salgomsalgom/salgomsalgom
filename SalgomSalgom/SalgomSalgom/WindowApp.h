#pragma once

constexpr UINT RESOLUTION_WIDHT = 1920;
constexpr UINT RESOLUTION_HEIGHT = 1080;
constexpr float WINDOW_MODE_RATE = 1.f;

constexpr RECT FULLSCREEN_MODE_RECT = {
	0, 0, RESOLUTION_WIDHT, RESOLUTION_HEIGHT
};

constexpr RECT WINDOW_MODE_RECT = {
	0, 
	0, 
	static_cast<UINT>(RESOLUTION_WIDHT * WINDOW_MODE_RATE),
	static_cast<UINT>(RESOLUTION_HEIGHT * WINDOW_MODE_RATE)
};

namespace GameCore { class Game; };
class D3DRenderer;

class WindowApp
{
public:
	static int Run(HINSTANCE instance, GameCore::Game& game, const TCHAR* className);
	static HWND GetHwnd() { return m_Wnd; }
	static RECT GetClientRect() { return m_ClientRect; }
	static UINT GetResolutionWidth() { return RESOLUTION_WIDHT; }
	static UINT GetResolutionHeight() { return RESOLUTION_HEIGHT; }

public:
	static void ToggleFullScreenWindow();
	static void ToggleFullScreenWindow(IDXGISwapChain3*);
	static bool IsFullScreen() { return m_bFullScreen; }

private:
	static LRESULT CALLBACK WndProc
	(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

private:
	static HWND m_Wnd;
	static BOOL m_bFullScreen;
	static RECT m_ClientRect;

	static const UINT m_WindowStyle = (WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU);

private:
	static void DebugOutClientSize();
};
