#pragma once
#include "Scene.h"

void PushChar(char ch);



class LoginScene :
    public Scene
{
public:
    explicit LoginScene(SceneKey key);
 	virtual ~LoginScene() { }
 
 public:
 	void Initialize() override;
 	void Update(float frame_time) override;
 	void HandleInput(InputSystem& input) override;
    void Destroy() override;

public:
    void ShowRegister();
    void HideRegister();
 
 public:
     SpriteAnimation* m_LoginObj;
     Sprite* m_SelectedEntry;
     Text** m_CurrActiveText;
     bool m_Register = false;

     // Login
     Text* m_TextID;
     Text* m_TextPassword;
     string m_Password;

     Sprite* m_LoginButton;
     Sprite* m_RegisterButton;
     Sprite* m_QuitButton;

     // Sign in
     Sprite* m_RegisterBox;

     Text* m_TextRegisterID;
     Text* m_TextRegisterPassword;
     Text* m_TextRegisterPasswordConfirm;
     string m_RegisterPassword;
     string m_RegisterPasswordConfirm;

     Sprite* m_JoinInButton;
     Sprite* m_BackButton;

};

