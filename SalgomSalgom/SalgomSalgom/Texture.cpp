#include "pch.h"
#include "Texture.h"

#include "GraphicsCore.h"
#include "Util/DDSTextureLoader12.h"

constexpr wchar_t g_TextureFileDirectory[] = L"./Assets/Images/";


HRESULT CreateTextureResourceFromDDSFile(
	ID3D12Device* pd3dDevice, 
	ID3D12GraphicsCommandList* pd3dCommandList,
	ID3D12Resource** texture,
	const wchar_t* pszFileName, 
	ID3D12Resource** ppd3dUploadBuffer, 
	D3D12_RESOURCE_STATES d3dResourceStates
)
{
	std::unique_ptr<uint8_t[]> ddsData;
	std::vector<D3D12_SUBRESOURCE_DATA> vSubresources;
	DirectX::DDS_ALPHA_MODE ddsAlphaMode = DirectX::DDS_ALPHA_MODE_UNKNOWN;
	bool bIsCubeMap = false;

	HRESULT rtv = DirectX::LoadDDSTextureFromFileEx(
		pd3dDevice, pszFileName, 0, D3D12_RESOURCE_FLAG_NONE, DirectX::DDS_LOADER_DEFAULT, 
		texture, ddsData, vSubresources, &ddsAlphaMode, &bIsCubeMap
	);

	/* 인자로 넣은 pd3dTexture에 Committed Resource를 만들어줌 */
	/* 그렇다고 pd3dTexture에 파일의 정보가 들어있는 것은 아닐 것임 */
	/* 업로드 버퍼를 생성하지 않았기 때문 */
	/* 그럼 실제 데이터는 어디에 있냐? */
	/* 서브리소스 데이터가 수상하다. 알아보자 */
	/* 실제 파일의 데이터는 서브리소스 데이터에 있는 것으로 보임 */


	// 여기 밑에는 업로드 버퍼를 만드는 과정
	D3D12_HEAP_PROPERTIES d3dHeapPropertiesDesc;
	::ZeroMemory(&d3dHeapPropertiesDesc, sizeof(D3D12_HEAP_PROPERTIES));
	d3dHeapPropertiesDesc.Type = D3D12_HEAP_TYPE_UPLOAD;
	d3dHeapPropertiesDesc.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	d3dHeapPropertiesDesc.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	d3dHeapPropertiesDesc.CreationNodeMask = 1;
	d3dHeapPropertiesDesc.VisibleNodeMask = 1;

	//D3D12_RESOURCE_DESC d3dTextureResourceDesc = pd3dTexture->GetDesc();
	//UINT nSubResources = d3dTextureResourceDesc.DepthOrArraySize * d3dTextureResourceDesc.MipLevels;
	UINT nSubResources = (UINT)vSubresources.size();
	//UINT64 nBytes = 0;
	//pd3dDevice->GetCopyableFootprints(&d3dResourceDesc, 0, nSubResources, 0, NULL, NULL, NULL, &nBytes);
	UINT64 nBytes = GetRequiredIntermediateSize(*texture, 0, nSubResources);

	D3D12_RESOURCE_DESC d3dUploadResourceDesc;
	::ZeroMemory(&d3dUploadResourceDesc, sizeof(D3D12_RESOURCE_DESC));
	d3dUploadResourceDesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER; //Upload Heap에는 텍스쳐를 생성할 수 없음
	d3dUploadResourceDesc.Alignment = 0;
	d3dUploadResourceDesc.Width = nBytes;
	d3dUploadResourceDesc.Height = 1;
	d3dUploadResourceDesc.DepthOrArraySize = 1;
	d3dUploadResourceDesc.MipLevels = 1;
	d3dUploadResourceDesc.Format = DXGI_FORMAT_UNKNOWN;
	d3dUploadResourceDesc.SampleDesc.Count = 1;
	d3dUploadResourceDesc.SampleDesc.Quality = 0;
	d3dUploadResourceDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
	d3dUploadResourceDesc.Flags = D3D12_RESOURCE_FLAG_NONE;

	pd3dDevice->CreateCommittedResource(&d3dHeapPropertiesDesc, D3D12_HEAP_FLAG_NONE, &d3dUploadResourceDesc, D3D12_RESOURCE_STATE_GENERIC_READ, NULL, __uuidof(ID3D12Resource), (void**)ppd3dUploadBuffer);

	//UINT nSubResources = (UINT)vSubresources.size();
	//D3D12_SUBRESOURCE_DATA *pd3dSubResourceData = new D3D12_SUBRESOURCE_DATA[nSubResources];
	//for (UINT i = 0; i < nSubResources; i++) pd3dSubResourceData[i] = vSubresources.at(i);

	//	std::vector<D3D12_SUBRESOURCE_DATA>::pointer ptr = &vSubresources[0];
	::UpdateSubresources(pd3dCommandList, *texture, *ppd3dUploadBuffer, 0, 0, nSubResources, &vSubresources[0]);


	// 리소스의 상태를 변경해준다.
	D3D12_RESOURCE_BARRIER d3dResourceBarrier;
	::ZeroMemory(&d3dResourceBarrier, sizeof(D3D12_RESOURCE_BARRIER));
	d3dResourceBarrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	d3dResourceBarrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	d3dResourceBarrier.Transition.pResource = *texture;
	d3dResourceBarrier.Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_DEST;
	d3dResourceBarrier.Transition.StateAfter = d3dResourceStates;
	d3dResourceBarrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
	pd3dCommandList->ResourceBarrier(1, &d3dResourceBarrier);

	//	delete[] pd3dSubResourceData;

	return rtv;
}



bool Texture::LoadTextureFromFile(const wchar_t* filename)
{
	std::wstring filepath{ g_TextureFileDirectory };
	filepath.append(filename);

	ID3D12Resource* t_upload_buffer;

	/* 왜 업로드 버퍼의 포인터를 넣어야 하는 거지? */
	HRESULT rtv = CreateTextureResourceFromDDSFile(
		Graphics::GraphicsCore::GetDevice(),
		Graphics::GraphicsCore::GetCommandList(),
		m_Resource.GetAddressOf(),
		filepath.c_str(),
		&t_upload_buffer,
		D3D12_RESOURCE_STATE_GENERIC_READ
	);

	/* 이 부분을 BindHeap에서 할당받는 것으로 한다.*/
	//---------------------------------------------------------------
	// 이 텍스처에 대한 SRV 서술자를 생성해야한다.
	// D3D12_DESCRIPTOR_HEAP_DESC srv_heap_desc{ };
	// srv_heap_desc.NumDescriptors = 1;
	// srv_heap_desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	// srv_heap_desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;	// NONE과 SHADER_VISIVLE이 있는데 뭐가 다름?
	// 
	// 
	// // SRV 힙 생성
	// rtv = Graphics::GraphicsCore::GetDevice()->CreateDescriptorHeap(&srv_heap_desc, IID_PPV_ARGS(&m_DescriptorHeap));
	// auto descriptor_handle = m_DescriptorHeap->GetCPUDescriptorHandleForHeapStart();
	//---------------------------------------------------------------

	Graphics::GraphicsCore::AllocateSRVDescriptor(&m_CPUDescriptorHandle, &m_GPUDescriptorHandle);

	// SRV 서술자 생성
	D3D12_TEX2D_SRV tex2d_srv;
	tex2d_srv.MostDetailedMip = 0;
	tex2d_srv.MipLevels = 1;
	tex2d_srv.PlaneSlice = 0;
	tex2d_srv.ResourceMinLODClamp = 0.0f;	

	D3D12_SHADER_RESOURCE_VIEW_DESC srv_desc;
	srv_desc.Format = m_Resource->GetDesc().Format;
	srv_desc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	srv_desc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srv_desc.Texture2D = tex2d_srv;
	
	// 얘는 왜 이름이 VIEW 만들기냐
	// 만든 건 어딧지
	Graphics::GraphicsCore::GetDevice()->CreateShaderResourceView(m_Resource.Get(), &srv_desc, m_CPUDescriptorHandle);
	
	if (S_OK != rtv) {
		yhl::dout << filename << ": fail to load." << yhl::fls;
		return false;
	}

	auto desc = m_Resource->GetDesc();
	yhl::dout << filename << ": w - " << desc.Width << ", h - " << desc.Height << yhl::fls;

	return true;
}

Texture::Texture(const wchar_t* filename)
{
	m_Valid = LoadTextureFromFile(filename); 
}
