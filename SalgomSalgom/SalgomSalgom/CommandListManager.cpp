#include "pch.h"
#include "CommandListManager.h"

CommandListManager::CommandListManager() :
	m_CommandQueue(nullptr),
	m_CommandAllocator(nullptr),
	m_CommandList(nullptr),
	m_Fence()
{
}

CommandListManager::~CommandListManager()
{
}

void CommandListManager::Initialize(ID3D12Device* const device)
{
	D3D12_COMMAND_QUEUE_DESC queue_desc{ };
	queue_desc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
	queue_desc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;

	device->CreateCommandQueue(&queue_desc, IID_PPV_ARGS(&m_CommandQueue));
	device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&m_CommandAllocator));
	device->CreateCommandList(
		0,
		D3D12_COMMAND_LIST_TYPE_DIRECT,
		m_CommandAllocator.Get(),			// 연관된 명령 할당자
		nullptr,							// 초기 파이프라인 상태 객체
		IID_PPV_ARGS(&m_CommandList)
	);

	m_CommandList->Close();		// 열린 상태로 생성됨, 닫아준다.

	m_Fence.Initialize(device);
}
