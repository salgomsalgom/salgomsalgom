#include "pch.h"
#include "DescriptorAllocator.h"
#include "GraphicsCore.h"

using namespace Microsoft::WRL;

UINT g_NumDescriptorsPerHeap = 256;
vector<ComPtr<ID3D12DescriptorHeap>> g_DescriptorHeapPool;


D3D12_CPU_DESCRIPTOR_HANDLE DescriptorAllocator::Allocate(UINT count)
{
    if (m_CurrHeap == nullptr || m_RemainingFreeHandles < count)
    {
        m_CurrHeap = RequestNewHeap(m_Type);
        m_CurrHandle = m_CurrHeap->GetCPUDescriptorHandleForHeapStart();
        m_RemainingFreeHandles = g_NumDescriptorsPerHeap;

        if (m_DescriptorSize == 0)
            m_DescriptorSize = Graphics::GraphicsCore::GetDevice()->GetDescriptorHandleIncrementSize(m_Type);
    }

    D3D12_CPU_DESCRIPTOR_HANDLE ret = m_CurrHandle;
    m_CurrHandle.ptr += count * m_DescriptorSize;
    m_RemainingFreeHandles -= count;
    return ret;
}

UINT DescriptorAllocator::GetHeapCount()
{
    return g_DescriptorHeapPool.size();
}

ID3D12DescriptorHeap* DescriptorAllocator::RequestNewHeap(D3D12_DESCRIPTOR_HEAP_TYPE Type)
{
    D3D12_DESCRIPTOR_HEAP_DESC Desc;
    Desc.Type = Type;
    Desc.NumDescriptors = g_NumDescriptorsPerHeap;
    Desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
    Desc.NodeMask = 1;

    Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> pHeap;
    Graphics::GraphicsCore::GetDevice()->CreateDescriptorHeap(&Desc, IID_PPV_ARGS(&pHeap));
    g_DescriptorHeapPool.emplace_back(pHeap);
    return pHeap.Get();
}
