#include "pch.h"
#include "HeightMap.h"

HeightMap::HeightMap(const std::string& filename)
	: m_Valid(false), m_Heights(), row_length(HEIGHTMAP_SIZE), col_length(HEIGHTMAP_SIZE)
{
	std::ifstream in{ filename, std::ios_base::binary };
	assert(in);

	int file_size = row_length * col_length;
	m_Heights.resize(file_size);
	in.read(reinterpret_cast<char*>(m_Heights.data()), file_size);
	in.close();

	m_Valid = true;
}

float HeightMap::GetHeight(int row, int column)
{
	return static_cast<float>(m_Heights[row * col_length + column]);
}

DirectX::XMFLOAT3 HeightMap::GetNormalFactor(int row, int column)
{
	DirectX::XMFLOAT3 normal{ 0, 1, 0 };	// delta x, delta z 값에 2를 나누는 대신 y값에 2를 곱함

	int delta_row = col_length;
	int delta_col = 1;

	int idx = row * delta_row + column * delta_col;
	
	// 모서리, 변
	if (0 == row || 0 == column || (row_length - 1) == row || (col_length - 1) == column)
		return normal;

	// 내부
	normal.x = (m_Heights[idx - delta_row] - m_Heights[idx + delta_row]) / 2;
	normal.z = (m_Heights[idx - delta_col] - m_Heights[idx + delta_col]) / 2;
	
	return normal;

	// 교수님 알고리즘
	/* 
	float y1 = (float)m_Heights[idx];
	float y2 = (float)m_Heights[idx + delta_row];
	float y3 = (float)m_Heights[idx + delta_col];
	
	DirectX::XMFLOAT3 xmf3Edge1 = DirectX::XMFLOAT3(0.0f, y3 - y1, 1);
	DirectX::XMFLOAT3 xmf3Edge2 = DirectX::XMFLOAT3(1, y2 - y1, 0.0f);
	
	DirectX::XMFLOAT3 xmf3Normal = CrossProduct(xmf3Edge1, xmf3Edge2, true);
	
	return(xmf3Normal);
	*/
}
