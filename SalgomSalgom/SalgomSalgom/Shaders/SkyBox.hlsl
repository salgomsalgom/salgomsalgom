cbuffer CameraInfo : register(b0)
{
	matrix g_view : packoffset(c0);
	matrix g_projection : packoffset(c4);
	float3 g_CameraEyePos : packoffset(c8);
};

 cbuffer ObjectInfo : register(b1)
 {
 	matrix g_Transform : packoffset(c0);
 };


struct VS_INPUT
{
	float3 position	: POSITION;
	float2 texcoord	: TEXCOORD;
};

struct VS_OUTPUT
{
	float4 position : SV_POSITION;
	float2 texcoord : TEXCOORD;
};


VS_OUTPUT VSMain(VS_INPUT input)
{
	VS_OUTPUT output;
	input.position += g_CameraEyePos;
	output.position = mul(mul(mul(float4(input.position, 1.f), g_Transform), g_view), g_projection);
	output.texcoord = input.texcoord;

	return output;
}

SamplerState g_Sampler: register(s0);
Texture2D g_SkyBoxSide: register(t0);


float4 PSMain(VS_OUTPUT input) : SV_TARGET
{
	float4 color = g_SkyBoxSide.Sample(g_Sampler, input.texcoord);
	return color;
}
