cbuffer CameraInfo : register(b0)
{
	matrix g_view : packoffset(c0);
	matrix g_projection : packoffset(c4);
};

struct VS_INPUT
{
	float3 position	: POSITION;
	float3 normal	: NORMAL;
};

struct VS_OUTPUT
{
	float4 position : SV_POSITION;
	float3 positionW : POSITION;
	float3 normal	: NORMAL;
};


VS_OUTPUT VSMain(VS_INPUT input)
{
	VS_OUTPUT output;
	output.position = mul(mul(float4(input.position, 1.f), g_view), g_projection);
	output.positionW = input.position;
	output.normal = input.normal;
	
	return output;
}

float4 PSMain(VS_OUTPUT input) : SV_TARGET
{
	float3 light_position = float3(5.f, 0.f, 5.f);
	float3 to_light = normalize(light_position - input.positionW);

	float diffuse_factor = dot(to_light, input.normal);		// �갡 cos(����)��
	if (diffuse_factor < 0) diffuse_factor = 0;

	float ambient_factor = 0.5;


	return diffuse_factor;
}
