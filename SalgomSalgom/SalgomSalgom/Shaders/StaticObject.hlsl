cbuffer CameraInfo : register(b0)
{
	matrix g_view : packoffset(c0);
	matrix g_projection : packoffset(c4);
	float3 g_CameraEyePos : packoffset(c8);
};

cbuffer ObjectInfo : register(b1)
{
	matrix g_Transform : packoffset(c0);
};


// 변하는 데이터가 아닌데 굳이 상수버퍼에 둘 필요 있나...?
// 상수버퍼는 CPU가 프레임당 한번 갱신하는 것이 일반적이다.
// 그래서 default힙이 아닌 upload힙에 만들어야 한다.

struct VS_INPUT
{
	float3 position	: POSITION;
	float3 normal	: NORMAL;
	float2 texcoord	: TEXCOORD;
};

struct VS_OUTPUT
{
	float4 position : SV_POSITION;
	float3 posW : POSITIONW;
	float3 normalW	: NORMAL;
	float2 texcoord : TEXCOORD;
};

float4 ComputeDirectionalLight(float3 lightPos, float3 pos, float3 normal)
{
	float3 md = float3(1.f, 1.f, 1.f);
	float3 ms = float3(1.f, 1.f, 1.f);
	float shininess = 20.f;

	float3 lit_color = float3(1.f, 1.f, 1.f);
	float3 v_light = normalize(-lightPos);

	// diffuse
	float ndotl = max(dot(v_light, normal), 0.f);
	float3 diffuse = (ndotl * lit_color) * md;

	// specular
	float3 v_view = normalize(g_CameraEyePos - pos);						// 시선
	float3 v_reflection = (2.f * dot(normal, v_light) * normal) - v_light;	// 반사

	float3 specular = pow(max(dot(v_reflection, v_view), 0.f), 20) * ms;

	// ambient
	float3 ambient = float3(0.5f, 0.5f, 0.5f);

	return float4(diffuse /*+ specular */+ ambient, 1.f);
	// return float4(specular, 1.f);
}

VS_OUTPUT VSMain(VS_INPUT input)
{
	VS_OUTPUT output;

	float4 posW = mul(float4(input.position, 1.f), g_Transform);

	output.position = mul(mul(posW, g_view), g_projection);
	output.posW = posW.xyz;
	output.normalW = mul(input.normal, (float3x3)g_Transform);
	output.texcoord = input.texcoord;

	return output;
}

SamplerState g_Sampler: register(s0);
Texture2D g_Texture: register(t0);

float4 PSMain(VS_OUTPUT input) : SV_TARGET
{
	input.normalW = normalize(input.normalW);

	float4 color = g_Texture.Sample(g_Sampler, input.texcoord);

	float3 light_pos = float3(1.f, 0.f, 0.f);
	float4 directional_light = ComputeDirectionalLight(light_pos, input.posW.xyz, input.normalW);

	// float3 nor_color = input.normalW;
	// return float4(nor_color, 1.f);
	return directional_light * color;
}
