cbuffer CameraInfo : register(b0)
{
	matrix g_view : packoffset(c0);
	matrix g_projection : packoffset(c4);
};

cbuffer ObjectInfo : register(b1)
{
	matrix g_Transform : packoffset(c0);
};

cbuffer SkinnedInfo : register(b2)
{
	// 이게 256바이트인가?
	float4x4 g_BoneTransforms[96];
}


struct VS_INPUT
{
	float3 position				: POSITION;
	float3 normal				: NORMAL;
	float2 texcoord				: TEXCOORD;
	float3 weights				: WEIGHTS;
	min16uint4 boneIndices		: BONEINDICES;
};

struct VS_OUTPUT
{
	float4 position : SV_POSITION;
	float3 positionW : POSITIONW;
	float3 normal	: NORMAL;
	float2 texcoord : TEXCOORD;
};


VS_OUTPUT VSMain(VS_INPUT input)
{
	VS_OUTPUT output;

	// 가중치 구하기
	float weights[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
	weights[0] = input.weights.x;
	weights[1] = input.weights.y;
	weights[2] = input.weights.z;
	weights[3] = 1.0f - weights[0] - weights[1] - weights[2];

	float3 posL = float3(0.0f, 0.0f, 0.0f);
	float3 normalL = float3(0.0f, 0.0f, 0.0f);

	// 정점 블렌딩
	for (int i = 0; i < 4; ++i) {
		posL += weights[i] * mul(float4(input.position, 1.0f), g_BoneTransforms[input.boneIndices[i]]).xyz;
		normalL += weights[i] * mul(input.normal, (float3x3)g_BoneTransforms[input.boneIndices[i]]);
	}

	output.position = mul(mul(float4(posL, 1.f), g_Transform), g_projection);
	// output.position = mul(mul(float4(input.position, 1.f), g_Transform), g_projection);
	output.positionW = input.position;
	output.normal = mul(normalL, (float3x3)g_Transform);
	output.texcoord = input.texcoord;

	return output;
}

SamplerState g_Sampler: register(s0);
Texture2D g_Texture: register(t0);

float4 PSMain(VS_OUTPUT input) : SV_TARGET
{
	float4 color = g_Texture.Sample(g_Sampler, input.texcoord);

	float3 light_position = float3(0.f, 10.f, 0.f);
	float3 to_light = normalize(light_position - input.positionW);

	float diffuse_factor = dot(to_light, input.normal);		// 얘가 cos(각도)임
	if (diffuse_factor < 0) diffuse_factor = 0;

	float ambient_factor = 0.3;

	return color * (diffuse_factor + ambient_factor);
}
