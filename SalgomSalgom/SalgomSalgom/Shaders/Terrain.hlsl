cbuffer CameraInfo : register(b0)
{
	matrix g_view : packoffset(c0);
	matrix g_projection : packoffset(c4);
};

struct VS_INPUT
{
	float3 position	: POSITION;
	float3 normal   : NORMAL;
	float2 texcoord	: TEXCOORD;
};

struct VS_OUTPUT
{
	float4 position : SV_POSITION;
	float3 positionW : POSITION;
	float3 normal   : NORMAL;
	float2 texcoord : TEXCOORD;
};


VS_OUTPUT VSMain(VS_INPUT input)
{
	VS_OUTPUT output;
	output.position = mul(mul(float4(input.position, 1.f), g_view), g_projection);
	output.positionW = input.position;
	output.normal = input.normal;
	output.texcoord = input.texcoord;

	return output;
}

SamplerState g_Sampler: register(s0);
Texture2D g_Texture: register(t0);

float4 PSMain(VS_OUTPUT input) : SV_TARGET
{
	float4 color = g_Texture.Sample(g_Sampler, input.texcoord);

	 float3 light_position = float3(0.f, 50.f, 0.f);
	 float3 to_light = normalize(light_position - input.positionW);

	 float diffuse_factor = dot(to_light, input.normal);		// �갡 cos(����)��
	 if (diffuse_factor < 0) diffuse_factor = 0;
	 diffuse_factor = diffuse_factor * 0.5;

	 float ambient_factor = 0.2;

	 return color * (diffuse_factor + ambient_factor);

}
