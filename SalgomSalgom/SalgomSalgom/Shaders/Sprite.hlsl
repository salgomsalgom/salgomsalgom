cbuffer CameraInfo : register(b0)
{
	matrix g_view : packoffset(c0);
	matrix g_projection : packoffset(c4);
};

 cbuffer ObjectInfo : register(b1)
 {
	matrix g_Transform;
	matrix g_TexTransform;
 };


struct VS_INPUT
{
	float3 position	: POSITION;
	float2 texcoord	: TEXCOORD;
};

struct VS_OUTPUT
{
	float4 position : SV_POSITION;
	float2 texcoord : TEXCOORD;
};


VS_OUTPUT VSMain(VS_INPUT input)
{
	VS_OUTPUT output;
	output.position = mul(float4(input.position, 1.f), g_Transform);
	output.texcoord = mul(float4(input.texcoord, 0.0f, 1.f), g_TexTransform).xy;

	return output;
}

SamplerState g_Sampler: register(s0);
Texture2D g_Sprite: register(t0);
// Texture2D g_tSprite: register(t1);


float4 PSMain(VS_OUTPUT input) : SV_TARGET
{
	float4 color = g_Sprite.Sample(g_Sampler, input.texcoord);
	return color;
}
