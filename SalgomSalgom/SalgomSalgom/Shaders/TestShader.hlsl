cbuffer CameraInfo : register(b0)
{
	matrix g_view : packoffset(c0);
	matrix g_projection : packoffset(c4);
};

struct VS_INPUT
{
	float3 position	: POSITION;
	float3 color	: COLOR;
};

struct VS_OUTPUT
{
	float4 position : SV_POSITION;
	float3 color	: COLOR;
};


VS_OUTPUT VSMain(VS_INPUT input)
{
	VS_OUTPUT output;
	output.position = mul(mul(float4(input.position, 1.f), g_view), g_projection);
	output.color = input.color;

	return output;
}

float4 PSMain(VS_OUTPUT input) : SV_TARGET
{
	float4 color = float4(input.color, 1);
	return color;
}
