#include "pch.h"
#include "D3DRenderer.h"
#include "WindowApp.h"
#include "Util/DebugOut.h"

#include "Camera.h"
#include "Mesh.h"

void D3DRenderer::Initialize()
{

#if defined(_DEBUG)
	// ID3D12Debug: Debug Layer를 활성화하기 위한 인터페이스
	Microsoft::WRL::ComPtr<ID3D12Debug> d3dDebug;
	D3D12GetDebugInterface(IID_PPV_ARGS(&d3dDebug));
	if (d3dDebug) d3dDebug->EnableDebugLayer();
#endif

	CreateDXGIFactory2(DXGI_CREATE_FACTORY_DEBUG, IID_PPV_ARGS(&m_Factory));

	// ALT-ENTER 명령을 비활성화한다.
	m_Factory->MakeWindowAssociation(WindowApp::GetHwnd(), DXGI_MWA_NO_ALT_ENTER);

	CreateDevice();

	m_RtvDescriptorSize = m_Device
		->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	m_DsvDescriptorSize = m_Device
		->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);
	m_SbvSrvDescriptorSize = m_Device
		->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

	CreateCommandObject();
	CreateSwapChain();

	CreateDescriptorHeap();
	CreateRenderTargetView();
	CreateDepthStencilView();

	// fence
	m_FenceEvent = ::CreateEvent(NULL, FALSE, FALSE, NULL);

	m_Device->CreateFence(
		(UINT64) 0,
		(D3D12_FENCE_FLAGS) D3D12_FENCE_FLAG_NONE,
		IID_PPV_ARGS(m_Fence.GetAddressOf())
	);
	m_FenceValue = 0;

	CreateGraphicRootSignature();
	BuildShaders();
	CreatePSO();
	// BuildObject();

	m_Camera = new Camera();
	m_CubeMesh = new CubeMesh(m_Device.Get(), 20, m_CommandList.Get());

	LogAdapterOutputs();
}

void D3DRenderer::CreateDevice()
{
	Microsoft::WRL::ComPtr<IDXGIAdapter1> adapter;

	for (int i = 0; DXGI_ERROR_NOT_FOUND != m_Factory->EnumAdapters1(i, &adapter); ++i) 
	{
		DXGI_ADAPTER_DESC1 adapater_desc;
		adapter->GetDesc1(&adapater_desc);

		if (adapater_desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) continue;

		HRESULT rst = D3D12CreateDevice(
			adapter.Get(),
			D3D_FEATURE_LEVEL_12_0,
			IID_PPV_ARGS(&m_Device)
		);

		if (SUCCEEDED(rst)) break;
	}

	if (!adapter)
	{
		m_Factory->EnumWarpAdapter(IID_PPV_ARGS(&adapter));
		D3D12CreateDevice(adapter.Get(), D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&m_Device));
	}
}

void D3DRenderer::CreateCommandObject()
{
	D3D12_COMMAND_QUEUE_DESC queue_desc = {};
	queue_desc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
	queue_desc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;

	m_Device->CreateCommandQueue(&queue_desc, IID_PPV_ARGS(&m_CommandQueue));
	m_Device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&m_CommandAllocator));
	m_Device->CreateCommandList(
		0,
		D3D12_COMMAND_LIST_TYPE_DIRECT,
		m_CommandAllocator.Get(),			// 연관된 명령 할당자
		nullptr,							// 초기 파이프라인 상태 객체
		IID_PPV_ARGS(&m_CommandList)
	);

	m_CommandList->Close();		// 열린 상태로 생성됨, 닫아준다.
}

void D3DRenderer::CreateSwapChain()
{
	m_SwapChain.Reset();

	DXGI_SWAP_CHAIN_DESC1 swap_chain_desc = {};
	swap_chain_desc.Width = WindowApp::GetResolutionWidth();
	swap_chain_desc.Height = WindowApp::GetResolutionHeight();
	swap_chain_desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swap_chain_desc.SampleDesc.Count = 1; // (m_MSAA4xEnable) ? 4 : 1;
	swap_chain_desc.SampleDesc.Quality = 0; // (m_MSAA4xEnable) ? m_MSAA4xQualityLevels - 1 : 0;
	swap_chain_desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swap_chain_desc.BufferCount = 2;
	swap_chain_desc.Scaling = DXGI_SCALING_NONE;
	swap_chain_desc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	swap_chain_desc.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;
	swap_chain_desc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	DXGI_SWAP_CHAIN_FULLSCREEN_DESC sc_fullscreen_desc = {};
	sc_fullscreen_desc.RefreshRate.Numerator = 60;
	sc_fullscreen_desc.RefreshRate.Denominator = 1;
	sc_fullscreen_desc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	sc_fullscreen_desc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	sc_fullscreen_desc.Windowed = TRUE;

	HRESULT rst =  m_Factory->CreateSwapChainForHwnd(
		m_CommandQueue.Get(),
		WindowApp::GetHwnd(),
		&swap_chain_desc,
		&sc_fullscreen_desc,
		(IDXGIOutput*) nullptr,
		(IDXGISwapChain1**)m_SwapChain.GetAddressOf()
	);

	m_SwapChainBufferIdx = m_SwapChain->GetCurrentBackBufferIndex();
	// m_SwapChainBufferIdx = 0; // m_SwapChain->GetCurrentBackBufferIndex();
}

void D3DRenderer::CreateDescriptorHeap()
{
	D3D12_DESCRIPTOR_HEAP_DESC descriptor_heap_desc = {};

	descriptor_heap_desc.NumDescriptors = m_SwapChainBuffersCount;
	descriptor_heap_desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	descriptor_heap_desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	descriptor_heap_desc.NodeMask = 0;
	m_Device->CreateDescriptorHeap(
		&descriptor_heap_desc,
		IID_PPV_ARGS(m_RtvDescriptorHeap.GetAddressOf())
	);

	descriptor_heap_desc.NumDescriptors = 1;
	descriptor_heap_desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
	descriptor_heap_desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	descriptor_heap_desc.NodeMask = 0;
	m_Device->CreateDescriptorHeap(
		&descriptor_heap_desc,
		IID_PPV_ARGS(m_DsvDescriptorHeap.GetAddressOf())
	);
}

void D3DRenderer::CreateRenderTargetView()
{
	D3D12_CPU_DESCRIPTOR_HANDLE desc_handle
		= m_RtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();

	for (UINT i = 0; i < m_SwapChainBuffersCount; ++i)
	{
		m_SwapChain->GetBuffer(
			i,
			IID_PPV_ARGS(&m_RenderTargetBuffers[i])
		);
		m_Device->CreateRenderTargetView(
			m_RenderTargetBuffers[i].Get(),
			nullptr,
			desc_handle
		);
		desc_handle.ptr += m_RtvDescriptorSize;
	}
}

void D3DRenderer::CreateDepthStencilView()
{
	D3D12_RESOURCE_DESC resource_desc = { };
	resource_desc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	resource_desc.Alignment = 0;
	resource_desc.Width = WindowApp::GetResolutionWidth();
	resource_desc.Height = WindowApp::GetResolutionWidth();
	resource_desc.DepthOrArraySize = 1;
	resource_desc.MipLevels = 1;
	resource_desc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	resource_desc.SampleDesc.Count = 1;// (m_MSAA4xEnable) ? 4 : 1;
	resource_desc.SampleDesc.Quality = 0;// (m_MSAA4xEnable) ? (m_MSAA4xQualityLevels - 1) : 0;
	resource_desc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	resource_desc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

	D3D12_HEAP_PROPERTIES heap_properties{ };
	heap_properties.Type = D3D12_HEAP_TYPE_DEFAULT;
	heap_properties.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	heap_properties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	heap_properties.CreationNodeMask = 1;
	heap_properties.VisibleNodeMask = 1;

	D3D12_CLEAR_VALUE clear_value{ };
	clear_value.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	clear_value.DepthStencil.Depth = 1.0f;
	clear_value.DepthStencil.Stencil = 0;

	m_Device->CreateCommittedResource(
		&heap_properties, D3D12_HEAP_FLAG_NONE,
		&resource_desc, D3D12_RESOURCE_STATE_DEPTH_WRITE,
		&clear_value,
		IID_PPV_ARGS(m_DepthStencilBuffer.GetAddressOf())
	);	// DepthStencilBuffer를 생성한다.

	D3D12_CPU_DESCRIPTOR_HANDLE desc_handle =
		m_DsvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();

	m_Device->CreateDepthStencilView(
		m_DepthStencilBuffer.Get(),
		nullptr,
		desc_handle
	);
} 

void D3DRenderer::SetFullScreen(bool bIsFullScreen, RECT clientRect)
{
	SetFence();

	UINT width = clientRect.right - clientRect.left;
	UINT height = clientRect.bottom - clientRect.top;

	m_SwapChain->SetFullscreenState(bIsFullScreen, nullptr);

	DXGI_MODE_DESC display_mode_desc{ };
	display_mode_desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	display_mode_desc.Width = width;
	display_mode_desc.Height = height;
	display_mode_desc.RefreshRate = { 0 , 0 };		// 여기를 0으로 설정하면 알아서 계산해준다고 함...!
	display_mode_desc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	display_mode_desc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	m_SwapChain->ResizeTarget(&display_mode_desc);

	for (int i = 0; i < 2; ++i) {
		m_RenderTargetBuffers[i] = nullptr;
	}

	// SwapChain의 버퍼를 Resize한다.
	DXGI_SWAP_CHAIN_DESC swap_chain_desc{ };
	m_SwapChain->GetDesc(&swap_chain_desc);
	m_SwapChain->ResizeBuffers(
		m_SwapChainBuffersCount,
		width,
		height,
		swap_chain_desc.BufferDesc.Format,
		swap_chain_desc.Flags
	);

	// 버퍼의 인덱스를 얻어온다.
	m_SwapChainBufferIdx = m_SwapChain->GetCurrentBackBufferIndex();

	// RTV를 다시 만들어준다.
	CreateRenderTargetView();

	m_Camera->SetViewportAndScissorRect();
}

void D3DRenderer::Render()
{
	m_CommandAllocator->Reset();
	m_CommandList->Reset(m_CommandAllocator.Get(), NULL);

	// Bind an array of viewports to the rasterizer stage of the pipeline.
	m_CommandList->RSSetViewports(1, m_Camera->GetViewport());
	// Binds an array of scissor rectangles to the rasterizer stage.
	m_CommandList->RSSetScissorRects(1, m_Camera->GetScissorRect());

	D3D12_RESOURCE_BARRIER resource_barrier{ };
	resource_barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	resource_barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	resource_barrier.Transition.pResource = m_RenderTargetBuffers[m_SwapChainBufferIdx].Get();
	resource_barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
	resource_barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_RENDER_TARGET;
	resource_barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
	m_CommandList->ResourceBarrier(1, &resource_barrier);

	D3D12_CPU_DESCRIPTOR_HANDLE rtv_cpu_desc_handle 
		= m_RtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();

	rtv_cpu_desc_handle.ptr += (m_SwapChainBufferIdx * m_RtvDescriptorSize);

	D3D12_CPU_DESCRIPTOR_HANDLE dsv_cpu_desc_handle =
		m_DsvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();

	m_CommandList->OMSetRenderTargets(
		1,
		&rtv_cpu_desc_handle,
		FALSE,
		&dsv_cpu_desc_handle
	);

	float clear_color[4] = { 0.0f, 0.125f, 0.3f, 1.0f };

	m_CommandList->ClearRenderTargetView(
		rtv_cpu_desc_handle,
		clear_color,
		0,
		NULL
	);

	m_CommandList->ClearDepthStencilView(
		dsv_cpu_desc_handle,
		D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL,
		1.0f,
		0,
		0,
		NULL
	);

	m_CommandList->SetGraphicsRootSignature(m_RootSignature.Get());

	// 뭐가 달라?
	// m_CommandList->SetGraphicsRootDescriptorTable();

	m_CommandList->SetPipelineState(m_PipelineState.Get());



	// Update Camera-------------------------------------
	m_Camera->SetViewMatrix(
		XMFLOAT3(0.f, 0.f, 0.f),
		XMFLOAT3(0.f, 1.f, 0.f),
		XMFLOAT3(0.f, 1.f, 0.f)
	);
	m_Camera->SetProjectionMatrix();
	m_Camera->UpdateShaderVariables(m_CommandList.Get());
	//---------------------------------------------------

	m_CommandList->IASetPrimitiveTopology(
		D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST
	);

	m_CommandList->IASetVertexBuffers(0, 1, &m_CubeMesh->m_VertexBufferView);
	// m_CommandList->DrawInstanced(12, 1, 0, 0);

	m_CommandList->IASetIndexBuffer(&m_CubeMesh->m_IndexBufferView);
	m_CommandList->DrawIndexedInstanced(36, 1, 0, 0, 0);

	// 여기서 베리어 한 번 더
	resource_barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET;
	resource_barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PRESENT;
	m_CommandList->ResourceBarrier(1, &resource_barrier);

	m_CommandList->Close();
	m_CommandQueue->ExecuteCommandLists(1, (ID3D12CommandList* const*)m_CommandList.GetAddressOf());
	SetFence();

	m_SwapChain->Present(0, 0);
	m_SwapChainBufferIdx = m_SwapChain->GetCurrentBackBufferIndex();
}

void D3DRenderer::SetFence()
{
	m_FenceValue++;
	const UINT64 fence_value = m_FenceValue;
	m_CommandQueue->Signal(m_Fence.Get(), fence_value);
	// Signal: 새 울타리 지점을 설정하는 명령

	if (m_Fence->GetCompletedValue() < fence_value) {
		m_Fence->SetEventOnCompletion(fence_value, m_FenceEvent);
		::WaitForSingleObject(m_FenceEvent, INFINITE);
	}
}


void D3DRenderer::CreateGraphicRootSignature()
{
	// 카메라의 view와 projection 행렬
	D3D12_ROOT_PARAMETER root_parameter{ };
	root_parameter.ParameterType = D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS;
	root_parameter.Constants.Num32BitValues = 32;
	root_parameter.Constants.ShaderRegister = 0;
	root_parameter.Constants.RegisterSpace = 0;
	root_parameter.ShaderVisibility = D3D12_SHADER_VISIBILITY_VERTEX;

	D3D12_ROOT_SIGNATURE_DESC d3dRootSignatureDesc{ };
	d3dRootSignatureDesc.NumParameters = 1;
	d3dRootSignatureDesc.pParameters = &root_parameter;
	d3dRootSignatureDesc.NumStaticSamplers = 0;
	d3dRootSignatureDesc.pStaticSamplers = NULL;
	d3dRootSignatureDesc.Flags = D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT;

	ID3DBlob* pd3dSignatureBlob = NULL;
	ID3DBlob* pd3dErrorBlob = NULL;

	::D3D12SerializeRootSignature(
		&d3dRootSignatureDesc,
		D3D_ROOT_SIGNATURE_VERSION_1,
		&pd3dSignatureBlob,
		&pd3dErrorBlob
	);

	m_Device->CreateRootSignature(
		0,
		pd3dSignatureBlob->GetBufferPointer(),
		pd3dSignatureBlob->GetBufferSize(),
		IID_PPV_ARGS(m_RootSignature.GetAddressOf())
	);

	if (pd3dSignatureBlob)	pd3dSignatureBlob->Release();
	if (pd3dErrorBlob)		pd3dErrorBlob->Release();
}

void D3DRenderer::BuildShaders()
{
	//정점 셰이더와 픽셀 셰이더를 생성한다. 
	UINT compile_flags = 0;

#if defined(_DEBUG)
	compile_flags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

	ID3DBlob* compile_error_blob = NULL;
	m_VertexShaderBlob = nullptr;
	HRESULT rst = D3DCompileFromFile(
		L"./Shaders/Shaders.hlsl",
		NULL,
		NULL,
		"VSMain",
		"vs_5_1",
		compile_flags,
		0,
		&m_VertexShaderBlob,
		&compile_error_blob
	);

	if (compile_error_blob)
		yhl::dout << (char*)compile_error_blob->GetBufferPointer() << yhl::fls;

	D3DCompileFromFile(
		L"./Shaders/Shaders.hlsl",
		NULL,
		NULL,
		"PSMain",
		"ps_5_1",
		compile_flags,
		0,
		&m_PixelShaderBlob,
		NULL
	);

	if (compile_error_blob)
		yhl::dout << (char*)compile_error_blob->GetBufferPointer() << yhl::fls;
}

void D3DRenderer::CreatePSO()
{
	//래스터라이저 상태를 설정한다.
	D3D12_RASTERIZER_DESC d3dRasterizerDesc{ };
	d3dRasterizerDesc.FillMode = D3D12_FILL_MODE_SOLID;
	d3dRasterizerDesc.CullMode = D3D12_CULL_MODE_NONE;
	d3dRasterizerDesc.FrontCounterClockwise = FALSE;
	d3dRasterizerDesc.DepthBias = 0;
	d3dRasterizerDesc.DepthBiasClamp = 0.0f;
	d3dRasterizerDesc.SlopeScaledDepthBias = 0.0f;
	d3dRasterizerDesc.DepthClipEnable = TRUE;
	d3dRasterizerDesc.MultisampleEnable = FALSE;
	d3dRasterizerDesc.AntialiasedLineEnable = FALSE;
	d3dRasterizerDesc.ForcedSampleCount = 0;
	d3dRasterizerDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

	//블렌드 상태를 설정한다. 
	D3D12_BLEND_DESC d3dBlendDesc{ };
	d3dBlendDesc.AlphaToCoverageEnable = FALSE;
	d3dBlendDesc.IndependentBlendEnable = FALSE;
	d3dBlendDesc.RenderTarget[0].BlendEnable = FALSE;
	d3dBlendDesc.RenderTarget[0].LogicOpEnable = FALSE;
	d3dBlendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_ONE;
	d3dBlendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_ZERO;
	d3dBlendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
	d3dBlendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
	d3dBlendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
	d3dBlendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
	d3dBlendDesc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
	d3dBlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;

	D3D12_DEPTH_STENCIL_DESC d3dDepthStencilDesc;
	::ZeroMemory(&d3dDepthStencilDesc, sizeof(D3D12_DEPTH_STENCIL_DESC));
	d3dDepthStencilDesc.DepthEnable = TRUE;
	d3dDepthStencilDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
	d3dDepthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
	d3dDepthStencilDesc.StencilEnable = FALSE;
	d3dDepthStencilDesc.StencilReadMask = 0x00;
	d3dDepthStencilDesc.StencilWriteMask = 0x00;
	d3dDepthStencilDesc.FrontFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	d3dDepthStencilDesc.FrontFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	d3dDepthStencilDesc.FrontFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	d3dDepthStencilDesc.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;
	d3dDepthStencilDesc.BackFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	d3dDepthStencilDesc.BackFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	d3dDepthStencilDesc.BackFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	d3dDepthStencilDesc.BackFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;

	D3D12_INPUT_ELEMENT_DESC ele[] = {
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
	};

	//그래픽 파이프라인 상태를 설정한다.
	D3D12_GRAPHICS_PIPELINE_STATE_DESC d3dPipelineStateDesc{ };
	::ZeroMemory(&d3dPipelineStateDesc, sizeof(D3D12_GRAPHICS_PIPELINE_STATE_DESC));
	d3dPipelineStateDesc.pRootSignature = m_RootSignature.Get();
	d3dPipelineStateDesc.VS.pShaderBytecode = m_VertexShaderBlob->GetBufferPointer();
	d3dPipelineStateDesc.VS.BytecodeLength = m_VertexShaderBlob->GetBufferSize();
	d3dPipelineStateDesc.PS.pShaderBytecode = m_PixelShaderBlob->GetBufferPointer();
	d3dPipelineStateDesc.PS.BytecodeLength = m_PixelShaderBlob->GetBufferSize();
	d3dPipelineStateDesc.RasterizerState = d3dRasterizerDesc;
	d3dPipelineStateDesc.BlendState = d3dBlendDesc;
	d3dPipelineStateDesc.DepthStencilState = d3dDepthStencilDesc;
	d3dPipelineStateDesc.InputLayout.pInputElementDescs = ele;
	d3dPipelineStateDesc.InputLayout.NumElements = 2;
	d3dPipelineStateDesc.SampleMask = UINT_MAX;
	d3dPipelineStateDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	d3dPipelineStateDesc.NumRenderTargets = 1;
	d3dPipelineStateDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	d3dPipelineStateDesc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
	d3dPipelineStateDesc.SampleDesc.Count = 1;
	d3dPipelineStateDesc.SampleDesc.Quality = 0;

	m_Device->CreateGraphicsPipelineState(
		&d3dPipelineStateDesc,
		IID_PPV_ARGS(m_PipelineState.GetAddressOf())
	);

	/*	
	if (pd3dVertexShaderBlob) pd3dVertexShaderBlob->Release();
	if (pd3dPixelShaderBlob) pd3dPixelShaderBlob->Release();
	*/
}

D3D12_HEAP_PROPERTIES GetDefaultHeapProperties() {
	D3D12_HEAP_PROPERTIES heap_properties{ };
	heap_properties.Type = D3D12_HEAP_TYPE_DEFAULT;
	heap_properties.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	heap_properties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	heap_properties.CreationNodeMask = 1;
	heap_properties.VisibleNodeMask = 1;

	return heap_properties;
}

D3D12_HEAP_PROPERTIES GetUploadHeapProperties() {
	D3D12_HEAP_PROPERTIES heap_properties{ };
	heap_properties.Type = D3D12_HEAP_TYPE_UPLOAD;
	heap_properties.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	heap_properties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	heap_properties.CreationNodeMask = 1;
	heap_properties.VisibleNodeMask = 1;

	return heap_properties;
}

struct Vertex1 {
	XMFLOAT3 position;
	XMFLOAT3 normal;
};

void D3DRenderer::BuildObject()
{
	//D2bLoader test_model;

	//test_model.Load("mush.d2b");
	//m_VertexCount = test_model.m_VertexBuffer.count;

	//Vertex1* vertices = new Vertex1[m_VertexCount];

	//D3D12_HEAP_PROPERTIES default_heap_prop = GetDefaultHeapProperties();
	//D3D12_HEAP_PROPERTIES upload_heap_prop = GetUploadHeapProperties();
	//
	//for (int i = 0; i < m_VertexCount; ++i) {
	//	vertices[i].position.x = test_model.m_VertexBuffer.verticies[i].position.x;
	//	vertices[i].position.y = test_model.m_VertexBuffer.verticies[i].position.y;
	//	vertices[i].position.z = test_model.m_VertexBuffer.verticies[i].position.z;

	//	vertices[i].normal.x = test_model.m_VertexBuffer.verticies[i].normal.x;
	//	vertices[i].normal.y = test_model.m_VertexBuffer.verticies[i].normal.y;
	//	vertices[i].normal.z = test_model.m_VertexBuffer.verticies[i].normal.z;
	//}

	//D3D12_RESOURCE_DESC resource_desc{ };
	//resource_desc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
	//resource_desc.Alignment = 0;
	//resource_desc.Width = /*static_cast<UINT64>*/(m_VertexCount) * sizeof(Vertex1);
	//resource_desc.Height = 1;
	//resource_desc.DepthOrArraySize = 1;
	//resource_desc.MipLevels = 1;
	//resource_desc.Format = DXGI_FORMAT_UNKNOWN;
	//resource_desc.SampleDesc.Count = 1;
	//resource_desc.SampleDesc.Quality = 0;
	//resource_desc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
	//resource_desc.Flags = D3D12_RESOURCE_FLAG_NONE;

	//// 정점버퍼 생성
	//m_Device->CreateCommittedResource(
	//	&default_heap_prop,
	//	D3D12_HEAP_FLAG_NONE,
	//	&resource_desc,
	//	D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER,
	//	NULL,
	//	IID_PPV_ARGS(VertexBufferGPU.GetAddressOf())
	//);

	//// 업로드 버퍼 생성
	//m_Device->CreateCommittedResource(
	//	&upload_heap_prop,
	//	D3D12_HEAP_FLAG_NONE,
	//	&resource_desc,
	//	D3D12_RESOURCE_STATE_GENERIC_READ,
	//	NULL,
	//	IID_PPV_ARGS(VertexBufferUploader.GetAddressOf())
	//);


	//// 업로드 버퍼를 매핑하여 초기화 데이터를 업로드 버퍼에 복사한다
	//D3D12_RANGE read_range = { 0, 0 };
	//UINT8* data_begin = nullptr;

	//// Map과 Unmap 코드 이해가 안감
	//VertexBufferUploader->Map(0, &read_range, (void**)&data_begin);		// 이건 CPU 포인터를 얻어오려고 한 것 같은데
	//memcpy(data_begin, vertices, sizeof(Vertex1) * m_VertexCount);
	//VertexBufferUploader->Unmap(0, NULL);

	//// 업로드 버퍼의 내용을 디폴트 버퍼에 복사한다
	//m_CommandList->CopyResource(VertexBufferGPU.Get(), VertexBufferUploader.Get());

	//// 왜 이렇게 해야하는거지??

	//// 리소스배리어 해줌
	//D3D12_RESOURCE_BARRIER resource_barrier{ };
	//resource_barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	//resource_barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	//resource_barrier.Transition.pResource = VertexBufferGPU.Get();
	//resource_barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_DEST;
	//resource_barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER;
	//resource_barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;

	//// 리소스 베리어는 왜 하지??

	//m_CommandList->ResourceBarrier(1, &resource_barrier);

	// 버퍼 뷰의 내용을 채운다.
	//m_TestBufferView.BufferLocation = VertexBufferGPU->GetGPUVirtualAddress();
	//m_TestBufferView.SizeInBytes = sizeof(Vertex1) * m_VertexCount;		// 버퍼의 크기
	//m_TestBufferView.StrideInBytes = sizeof(Vertex1);		// 버퍼에 담긴 한 정점 원소의 크기



	//m_IndexCount = test_model.m_IndexBuffer.count;
	//UINT* indices = new UINT[m_IndexCount];
	//for (int i = 0; i < m_IndexCount; ++i) {
	//	indices[i] = test_model.m_IndexBuffer.indicies[i];
	//}

	////for (int i = 0; i < m_IndexCount; ++i) {
	////	yhl::dout << indices[i] << yhl::fls;
	////}

	//{// 인덱스 버퍼 생성
	//	D3D12_RESOURCE_DESC resource_desc{ };
	//	resource_desc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
	//	resource_desc.Alignment = 0;
	//	resource_desc.Width = m_IndexCount * sizeof(UINT);
	//	resource_desc.Height = 1;
	//	resource_desc.DepthOrArraySize = 1;
	//	resource_desc.MipLevels = 1;
	//	resource_desc.Format = DXGI_FORMAT_UNKNOWN;
	//	resource_desc.SampleDesc.Count = 1;
	//	resource_desc.SampleDesc.Quality = 0;
	//	resource_desc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
	//	resource_desc.Flags = D3D12_RESOURCE_FLAG_NONE;

	//	// 인덱스 버퍼 생성
	//	m_Device->CreateCommittedResource(
	//		&default_heap_prop,
	//		D3D12_HEAP_FLAG_NONE,
	//		&resource_desc,
	//		D3D12_RESOURCE_STATE_INDEX_BUFFER,
	//		NULL,
	//		IID_PPV_ARGS(IndexBufferGPU.GetAddressOf())
	//	);

	//	// 업로드 버퍼 생성
	//	m_Device->CreateCommittedResource(
	//		&upload_heap_prop,
	//		D3D12_HEAP_FLAG_NONE,
	//		&resource_desc,
	//		D3D12_RESOURCE_STATE_GENERIC_READ,
	//		NULL,
	//		IID_PPV_ARGS(IndexBufferUploader.GetAddressOf())
	//	);

	//	// 업로드 버퍼를 매핑하여 초기화 데이터를 업로드 버퍼에 복사한다
	//	D3D12_RANGE read_range = { 0, 0 };
	//	UINT8* data_begin = nullptr;
	//	IndexBufferUploader->Map(0, &read_range, (void**)&data_begin);
	//	memcpy(data_begin, indices, sizeof(UINT) * m_IndexCount);
	//	IndexBufferUploader->Unmap(0, NULL);

	//	// 업로드 버퍼의 내용을 디폴트 버퍼에 복사한다
	//	m_CommandList->CopyResource(IndexBufferGPU.Get(), IndexBufferUploader.Get());

	//	// 리소스배리어 해줌
	//	//D3D12_RESOURCE_BARRIER resource_barrier{ };
	//	//resource_barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	//	//resource_barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	//	//resource_barrier.Transition.pResource = IndexBufferGPU.Get();
	//	//resource_barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_DEST;
	//	//resource_barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_INDEX_BUFFER;
	//	//resource_barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;

	//	//m_CommandList->ResourceBarrier(1, &resource_barrier);
	//}

	//m_TestIndexBufferView.BufferLocation = IndexBufferGPU->GetGPUVirtualAddress();
	//m_TestIndexBufferView.Format = DXGI_FORMAT_R32_UINT;
	//m_TestIndexBufferView.SizeInBytes = sizeof(UINT) * m_IndexCount;

	//m_CommandList->Close();
	//m_CommandQueue->ExecuteCommandLists(1, (ID3D12CommandList* const*)m_CommandList.GetAddressOf());
	//SetFence();
}

void D3DRenderer::LogAdapters()
{
	UINT i = 0;
	Microsoft::WRL::ComPtr<IDXGIAdapter1> adapter;
	DXGI_ADAPTER_DESC adapter_desc;

	while (m_Factory->EnumAdapters1(i++, &adapter) != DXGI_ERROR_NOT_FOUND) {
		adapter->GetDesc(&adapter_desc);
		yhl::dout << "**Adapter: " << adapter_desc.Description << yhl::fls;
	}
}

void D3DRenderer::LogAdapterOutputs()
{
	UINT i = 0;
	UINT j = 0;
	
	Microsoft::WRL::ComPtr<IDXGIAdapter1> adapter;
	DXGI_ADAPTER_DESC adapter_desc;

	Microsoft::WRL::ComPtr<IDXGIOutput> output;
	DXGI_OUTPUT_DESC output_desc;

	while (m_Factory->EnumAdapters1(i++, &adapter) != DXGI_ERROR_NOT_FOUND) {
		while (adapter->EnumOutputs(j++, &output) != DXGI_ERROR_NOT_FOUND) {
			output->GetDesc(&output_desc);
			yhl::dout << "**Output: " << output_desc.DeviceName << yhl::fls;

			// Get Count
			UINT count = 0;
			output->GetDisplayModeList(DXGI_FORMAT_B8G8R8A8_UNORM, 0, &count, nullptr);

			// Get Mode Desc
			DXGI_MODE_DESC* mode_desc = new DXGI_MODE_DESC[count];
			output->GetDisplayModeList(DXGI_FORMAT_B8G8R8A8_UNORM, 0, &count, mode_desc);

			for (int i = 0; i < count; ++i) {
				auto mode = mode_desc[i];
				yhl::dout << "Width: " << mode.Width << " "
					<< "Height: " << mode.Height << " "
					<< "Refresh Rate: " << mode.RefreshRate.Numerator << "/" << mode.RefreshRate.Denominator << yhl::fls;
			}
		}
		j = 0;
	}
}
