#pragma once
#include "Scene.h"
#include "GameData.h"
#include "GraphicsObject.h"

class TestScene : public Scene
{
public:
	explicit TestScene() : Scene(SceneKey::TEST) { }
	virtual ~TestScene() { }

public:
	void Initialize() override
	{
		m_HandPos = XMFLOAT3(0.f, 0.f, 0.f);

		m_Hand = new Body(MeshKey::NM_HAND, TextureKey::TXNM);
		m_Hand->SetScale(0.5f);
		m_Hand->SetPosition(m_HandPos);
		AddObject(m_Hand);

		// m_Character = new Character(0, MeshKey::NM_HAND, TextureKey::TXNM);
		// AddObject(m_Character);

		XMFLOAT3 c_pos{ 0, 0, -5 };
		XMFLOAT3 c_dir{ 0, 0, 1 };

		m_Camera.SetPosition(c_pos);
		m_Camera.SetDirection(c_dir);

		m_HandScl = 1.f;
	};

	void Update(float frame_time) override {
		m_SceneTime += frame_time;

		m_Hand->Animate(frame_time);

		// m_Character->Animate(m_SceneTime);

		// static float pitch = 0.f;
		// pitch += 0.01;
		// 
		// m_Hand->SetRotation(XMFLOAT3(0.F, pitch, 0.f));
	};
	
	void HandleInput(InputSystem& input) override { 
		KeyboardEvent evt = KeyboardManager::GetEvent();
		if (KeyboardEvent::NONE == evt) return;

		auto pos = m_Camera.GetPosition();
		switch (evt) {
		//case KeyboardEvent::KEY_DOWN_W:
		//	pos.y += 0.1f;
		//	break;
		//case KeyboardEvent::KEY_DOWN_S:
		//	pos.y -= 0.1f;
		//	break;
		//case KeyboardEvent::KEY_DOWN_A:
		//	pos.x -= 0.1f;
		//	break;
		//case KeyboardEvent::KEY_DOWN_D:
		//	pos.x += 0.1f;
		//	break;
		case KeyboardEvent::KEY_DOWN_W:
			m_HandPos.y += 0.001f;
			break;
		case KeyboardEvent::KEY_DOWN_S:
			m_HandPos.y -= 0.001f;
			break;

		case KeyboardEvent::KEY_DOWN_1:
			m_HandPos.z += 0.05f;
			break;
		case KeyboardEvent::KEY_DOWN_2:
			m_HandPos.z -= 0.05f;
			break;
		case KeyboardEvent::KEY_DOWN_A:
			m_HandScl += 0.05f;
			break;
		case KeyboardEvent::KEY_DOWN_D:
			m_HandScl -= 0.05f;
			break;
		default:
			break;
		}

		m_Hand->SetPosition(m_HandPos);
		// m_Hand->SetScale(m_HandScl);

		m_Camera.SetPosition(pos);
	};

public:
	Character* m_Character;
	Body* m_Hand;
	XMFLOAT3 m_HandPos;
	float m_HandScl;
};
