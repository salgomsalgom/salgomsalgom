#pragma once

enum class DescriptorHeapType {
	CBV_SRV_UAV,
	SAMPLER,
	RTV,
	DSV,
};

class DescriptorHeap
{
public:
	explicit DescriptorHeap();
	explicit DescriptorHeap(DescriptorHeapType type, UINT count);
	virtual ~DescriptorHeap();

	ID3D12DescriptorHeap* operator->() { return m_DescriptorHeap.Get(); }
	const ID3D12DescriptorHeap* operator->() const { return m_DescriptorHeap.Get(); }

public:
	void Create(DescriptorHeapType type, UINT count);
	D3D12_CPU_DESCRIPTOR_HANDLE GetHandle(int index = 0);
	ID3D12DescriptorHeap* Get() { return m_DescriptorHeap.Get(); }
	ID3D12DescriptorHeap** GetAddressOf() { return m_DescriptorHeap.GetAddressOf(); }

private:
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> m_DescriptorHeap;
	D3D12_CPU_DESCRIPTOR_HANDLE m_Handle;
	
	UINT m_DescriptorSize;
};
