#include "pch.h"
#include "ShaderVisibleDescriptorHeap.h"

void ShaderVisibleDescriptorHeap::Create(ID3D12Device* device, UINT count)
{
	D3D12_DESCRIPTOR_HEAP_DESC desc;
	desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	desc.NumDescriptors = count;
	desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	desc.NodeMask = 0;
	
	HRESULT ret = device->CreateDescriptorHeap(&desc, IID_PPV_ARGS(m_Heap.GetAddressOf()));
	assert(ret == S_OK);

	m_CurrCPUDescriptorHandle = m_Heap->GetCPUDescriptorHandleForHeapStart();
	m_CurrGPUDescriptorHandle = m_Heap->GetGPUDescriptorHandleForHeapStart();
	m_DescriptorSize = device->GetDescriptorHandleIncrementSize(desc.Type);
	m_FreeHandle = count;
}

void ShaderVisibleDescriptorHeap::Allocate(D3D12_CPU_DESCRIPTOR_HANDLE* out_cpu_handle, D3D12_GPU_DESCRIPTOR_HANDLE* out_gpu_handle)
{
	assert(0 != m_FreeHandle);
	m_FreeHandle -= 1;
	yhl::dout << "freehandle " << m_FreeHandle << yhl::fls;

	*out_cpu_handle = m_CurrCPUDescriptorHandle;
	*out_gpu_handle = m_CurrGPUDescriptorHandle;
	
	m_CurrCPUDescriptorHandle.ptr += m_DescriptorSize;
	m_CurrGPUDescriptorHandle.ptr += m_DescriptorSize;
	return;
}
