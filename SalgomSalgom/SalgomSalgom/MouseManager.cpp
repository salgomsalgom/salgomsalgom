#include "pch.h"
#include "MouseManager.h"
#include "WindowApp.h"


std::queue<WindowInput> MouseManager::m_MouseInput;
MouseMode MouseManager::m_Mode = MouseMode::DEFAULT_MODE;
void (*MouseManager::m_UpdateMouseMode)(void) = MouseManager::UpdateDefault;

POINTF MouseManager::s_Delta{ 0, 0 };
ClientPoint MouseManager::s_LastMousePos{ 0, 0 };		// 이거 초기화 시점에 현재 마우스 좌표로 설정해주어야 할 것 같음


void MouseManager::Initialize()
{
	ClientPoint mpos;
	::GetCursorPos(&mpos);
	::ScreenToClient(WindowApp::GetHwnd(), &mpos);

	s_LastMousePos = mpos;
}

void MouseManager::PushWindowInput(UINT message, WPARAM wParam, LPARAM lParam)
{
	m_MouseInput.emplace(message, wParam, lParam);
}

void MouseManager::Update()
{
	ClientPoint mpos;
	::GetCursorPos(&mpos);
	::ScreenToClient(WindowApp::GetHwnd(), &mpos);

	auto wr = WindowApp::GetClientRect();
	float wnd_w = (float)(wr.left - wr.right);
	float wnd_h = (float)(wr.bottom - wr.top);

	s_Delta.x = static_cast<float>(mpos.x - s_LastMousePos.x) / wnd_w;
	s_Delta.y = static_cast<float>(mpos.y - s_LastMousePos.y) / wnd_h;

	s_LastMousePos = mpos;

	m_UpdateMouseMode();
}

void MouseManager::ChangeMouseMode(const MouseMode mode)
{
	if (mode == m_Mode) return;

	OutputDebugStringA("Mouse Mode Change: ");

	switch (mode) {
	case MouseMode::DEFAULT_MODE: {
		// ::ReleaseCapture();
		::ClipCursor(NULL);
		::ShowCursor(TRUE);

		m_UpdateMouseMode = UpdateDefault;

		OutputDebugStringA("default.\n");
		break;
	}
	case MouseMode::CAMERACONTROL_MODE: {
		// ::SetCapture(WindowApp::GetHwnd());
		RECT cl_rect;
		::GetWindowRect(WindowApp::GetHwnd(), &cl_rect);
		::ClipCursor(&cl_rect);
		::ShowCursor(FALSE);

		m_UpdateMouseMode = UpdateCameraControl;

		OutputDebugStringA("camera control.\n");
		break;
	}
	default: 
		break;
	}

	m_Mode = mode;
}

bool MouseManager::PopMouseInput(UINT& message, int& x, int& y)
{
	if (m_MouseInput.empty()) return false;

	WindowInput input = m_MouseInput.front();
	m_MouseInput.pop();

	message = input.message;
	x = GET_X_LPARAM(input.lParam);
	y = GET_Y_LPARAM(input.lParam);
	return true;
}


void MouseManager::UpdateDefault()
{
}

void MouseManager::UpdateCameraControl()
{
	// 여기 최적화할 수 있을걸...???
	RECT cl_rect;
	::GetClientRect(WindowApp::GetHwnd(), &cl_rect);

	ClientPoint cl_center;
	cl_center.x = (cl_rect.left + cl_rect.right) / 2;
	cl_center.y = (cl_rect.top + cl_rect.bottom) / 2;

	s_LastMousePos.x = cl_center.x;
	s_LastMousePos.y = cl_center.y;

	::ClientToScreen(WindowApp::GetHwnd(), &cl_center);	// 스크린 좌표로 바꿈
	::SetCursorPos(cl_center.x, cl_center.y);
}
