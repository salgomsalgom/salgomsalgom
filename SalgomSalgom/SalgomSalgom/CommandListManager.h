#pragma once
#include "Fence.h"

#include "WindowApp.h"

using namespace Microsoft::WRL;


class CommandListManager
{
public:
	CommandListManager();
	~CommandListManager();

public:
	void Initialize(ID3D12Device* const device);

	ID3D12CommandQueue* GetCommandQueue() const {
		return m_CommandQueue.Get();
	}

	void ClearRenderTarget(D3D12_CPU_DESCRIPTOR_HANDLE rtv_handle) {

		float clear_color[4] = { 0.0f, 0.125f, 0.3f, 1.0f };

		m_CommandList->ClearRenderTargetView(
			rtv_handle,
			clear_color,
			0,
			NULL
		);
	}

	void Reset() {
		m_CommandAllocator->Reset();
		m_CommandList->Reset(m_CommandAllocator.Get(), NULL);
	}

	void SetRS() {
		D3D12_VIEWPORT viewport;
		D3D12_RECT scissor_rect;
		
		RECT client_rect = WindowApp::GetClientRect();

		int width = client_rect.right - client_rect.left;
		int height = client_rect.bottom - client_rect.top;

		viewport.Width = (FLOAT)width;
		viewport.Height = (FLOAT)height;
		viewport.TopLeftX = 0;
		viewport.TopLeftY = 0;
		viewport.MaxDepth = 1;
		viewport.MinDepth = 0;

		scissor_rect.left = 0;
		scissor_rect.right = (LONG)width;
		scissor_rect.top = 0;
		scissor_rect.bottom = (LONG)height;

		m_CommandList->RSSetViewports(1, &viewport);
		m_CommandList->RSSetScissorRects(1, &scissor_rect);
	}

	void ExcuteCommandList() {
		m_CommandList->Close();
		ID3D12CommandList* command_lists[] = { m_CommandList.Get() };

		m_CommandQueue->ExecuteCommandLists(1, command_lists);
		m_Fence.Wait(m_CommandQueue.Get());
	}

	void SetOM(D3D12_CPU_DESCRIPTOR_HANDLE& rtd, D3D12_CPU_DESCRIPTOR_HANDLE& dsd) {
		// const FLOAT clear_color[4] = { 0.615f, 0.764f, 0.902f ,1 };//{ 0.0f, 0.125f, 0.3f, 1.0f };
		// const FLOAT clear_color[4] = { 0.f, 0.f, 0.f, 1.f };	// 검은색
		const FLOAT clear_color[4] = { 1.f, 1.f, 1.f, 1.f };
		D3D12_CLEAR_FLAGS clear_flags{ D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL };

		// 여긴 왜 value로 넘기냐
		m_CommandList->ClearRenderTargetView(rtd, clear_color, 0, NULL);
		m_CommandList->ClearDepthStencilView(dsd, clear_flags, 1.0f, 0, 0, NULL);

		m_CommandList->OMSetRenderTargets(1, &rtd, FALSE, &dsd);
		// m_CommandList->OMSetBlendFactor();
		// m_CommandList->OMSetStencilRef();
	}

	ID3D12GraphicsCommandList* GetCommandList() {
		return m_CommandList.Get();
	}

	void ResourceBarrier(D3D12_RESOURCE_BARRIER& barrier) {
		m_CommandList->ResourceBarrier(1, &barrier);
	}

	void SetFence() {
		m_Fence.Wait(m_CommandQueue.Get());
	}

private:
	ComPtr<ID3D12CommandQueue> m_CommandQueue;
	ComPtr<ID3D12CommandAllocator> m_CommandAllocator;
	ComPtr<ID3D12GraphicsCommandList> m_CommandList;
	
	Fence m_Fence;
};
