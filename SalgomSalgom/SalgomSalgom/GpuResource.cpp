#include "pch.h"
#include "GpuResource.h"

#include "GraphicsCore.h"
#include "UploadBuffer.h"

GpuResource::GpuResource() :
	m_Resource()
{
	// 여기서 만들면 디바이스를 통해 생성할 수 없다.
	// 리소스를 특정할 수 없기 때문에
}

GpuResource::~GpuResource()
{
}

void GpuBuffer::Create(UINT64 bytes)
{
	HeapProperties heap_properties{ D3D12_HEAP_TYPE_DEFAULT };
	ResourceDesc resource_desc{ (UINT64)bytes };

	Graphics::GraphicsCore::GetDevice()->CreateCommittedResource(
		heap_properties.Get(),
		D3D12_HEAP_FLAG_NONE,
		resource_desc.Get(),
		D3D12_RESOURCE_STATE_COPY_DEST,
		NULL,
		IID_PPV_ARGS(m_Resource.GetAddressOf())
	);
}

void GpuBuffer::Create(UINT64 bytes, bool cb)
{
	HeapProperties heap_properties{ D3D12_HEAP_TYPE_UPLOAD };
	ResourceDesc resource_desc{ (UINT64)bytes };

	Graphics::GraphicsCore::GetDevice()->CreateCommittedResource(
		heap_properties.Get(),
		D3D12_HEAP_FLAG_NONE,
		resource_desc.Get(),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		NULL,
		IID_PPV_ARGS(m_Resource.GetAddressOf())
	);
}



void GpuBuffer::CopyData(UploadBuffer& uploader)
{
	auto cl = Graphics::GraphicsCore::GetCommandList();
	cl->CopyResource(m_Resource.Get(), uploader.Get());
}

void GpuBuffer::TransitionResourceState(D3D12_RESOURCE_STATES state)
{
	D3D12_RESOURCE_BARRIER resource_barrier;
	resource_barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	resource_barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	resource_barrier.Transition.pResource = m_Resource.Get();
	resource_barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_COPY_DEST;
	resource_barrier.Transition.StateAfter = state;
	resource_barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;

	auto cl = Graphics::GraphicsCore::GetCommandList();
	cl->ResourceBarrier(1, &resource_barrier);
}
