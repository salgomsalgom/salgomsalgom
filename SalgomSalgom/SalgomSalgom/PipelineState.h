#pragma once

/*
class PipelineState
{
	explicit PipelineState();
	 ~PipelineState();

private:
	Microsoft::WRL::ComPtr<ID3D12PipelineState> m_PipelineState;

};
*/

// 그리기 순서에 맞춘다.
enum class PSOType {
	SKYBOX,
	TERRAIN,
	STATIC_OBJECT,
	COLLISION_BOX,
	CHARACTER,

	BODY,
	SPRITE,

	TEST,
};

class GraphicsPSO {
public:
	GraphicsPSO(const PSOType type);
	~GraphicsPSO() { }

public:
	ID3D12PipelineState* Get() { return m_PipelineState.Get(); }
	void Create(ID3D12Device* device, ID3D12RootSignature* rootSig);

private:
	PSOType m_Type;

	Microsoft::WRL::ComPtr<ID3D12PipelineState> m_PipelineState;
};

class ComputePSO {

};
