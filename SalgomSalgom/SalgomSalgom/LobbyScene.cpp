#include "pch.h"
#include "LobbyScene.h"

#include "NetworkSystem.h"
#include "InputSystem.h"
#include "GraphicsObject.h"
#include "Sound.h"

string g_UserName = "TEST";

void WriteUserName(char* name)
{
	g_UserName = string(name);
}


LobbyScene::LobbyScene(SceneKey key)
	: Scene(key)
{
}

LobbyScene::~LobbyScene()
{
}

void LobbyScene::Initialize()
{
	// 배경, 버튼, 텍스트 추가
	AddObject(new Sprite(1980, 1080, TextureKey::LOBBY_BG));

	//애니메이션 및 포워드레이어 추가
	m_LobbyObjs.push_back(new SpriteAnimation(TextureKey::LOBBY_SPANI_SPIDER, 787, 130.5));
	m_LobbyObjs.push_back(new SpriteAnimation(TextureKey::LOBBY_SPANI_NMGD1, 495, 578));
	m_LobbyObjs.push_back(new SpriteAnimation(TextureKey::LOBBY_SPANI_NMGD2, 794, 500));
	m_LobbyObjs.push_back(new SpriteAnimation(TextureKey::LOBBY_SPANI_NMGD3, 350, 50));
	m_LobbyObjs.push_back(new SpriteAnimation(TextureKey::LOBBY_SPANI_NMGD4, 1640, 707));
	// m_LobbyObjs.push_back(new SpriteAnimation(TextureKey::LOBBY_LOADING_ICON, 420, 200));

	for (int i = 0; i < m_LobbyObjs.size(); ++i) {
		AddObject(m_LobbyObjs[i]);
	}

	AddObject(new Sprite(1980, 1080, TextureKey::LOBBY_BG_FWD));

	m_LobbyObjs.push_back(new SpriteAnimation(TextureKey::LOBBY_SPANI_WITCH, 725, 823));
	AddObject(m_LobbyObjs.back());

	// 버튼
	m_ReadyButton = new Sprite(TextureKey::LOBBY_GAME_START_BUTTON, 240, 200);
	m_QuitButton = new Sprite(TextureKey::LOBBY_GAME_QUIT_BUTTON, 240, 320);
	m_SearchQuitButton = new Sprite(TextureKey::LOBBY_QUIT_FINDING_GAME, 240, 200);
	m_LoadingIcon = new SpriteAnimation(TextureKey::LOBBY_LOADING_ICON, 420, 200);
	m_SearchQuitButton->CreateConstantBuffer();
	m_LoadingIcon->CreateConstantBuffer();

	AddObject(m_ReadyButton);
	AddObject(m_QuitButton);

	// ID 관련
	AddObject(new Sprite(TextureKey::LOBBY_ID_BG, 1560, 100));
	AddObject(new Sprite(TextureKey::LOBBY_ID_ICON, 1230, 100));

	m_Test = new Text(this, TextureKey::BATTLE_UI_ENGFONT, 1310, 110, g_UserName);

	g_Sound.pChannel[LOBBY_SOUND]->setVolume(1.0f);
	g_Sound.Play(LOBBY_SOUND);
}

void LobbyScene::Update(float frame_time)
{
	for (int i = 0; i < m_LobbyObjs.size(); ++i) {
		m_LobbyObjs[i]->UpdateIndex();
	}

	m_LoadingIcon->UpdateIndex();

	// 버튼 업데이트
	POINT m_pos = MouseManager::GetMousePos();

	if (!Ready) {
		if (!m_ReadyButtonMouseIn && m_ReadyButton->InMousePointer(m_pos)) {
			m_ReadyButtonMouseIn = true;
			m_ReadyButton->SetScale(1.2F, 1.2F);
		}

		if (m_ReadyButtonMouseIn && !m_ReadyButton->InMousePointer(m_pos)) {
			m_ReadyButtonMouseIn = false;
			m_ReadyButton->SetScale(1.F, 1.F);
		}
	}
	else {
		if (!m_ReadyButtonMouseIn && m_ReadyButton->InMousePointer(m_pos)) {
			m_ReadyButtonMouseIn = true;
			m_SearchQuitButton->SetScale(1.2F, 1.2F);
		}

		if (m_ReadyButtonMouseIn && !m_ReadyButton->InMousePointer(m_pos)) {
			m_ReadyButtonMouseIn = false;
			m_SearchQuitButton->SetScale(1.F, 1.F);
		}
	}

	if (!m_QuitButtonMouseIn && m_QuitButton->InMousePointer(m_pos)) {
		m_QuitButtonMouseIn = true;
		m_QuitButton->SetScale(1.2F, 1.2F);
	}

	if (m_QuitButtonMouseIn && !m_QuitButton->InMousePointer(m_pos)) {
		m_QuitButtonMouseIn = false;
		m_QuitButton->SetScale(1.F, 1.F);
	}
}

void LobbyScene::HandleInput(InputSystem& input)
{
	UINT m_msg;
	int x, y;
	POINT m_pos = MouseManager::GetMousePos();

	if (MouseManager::PopMouseInput(m_msg, x, y))
	{
		switch (m_msg) {
		case WM_LBUTTONDOWN:
			yhl::dout << "lbuttondown" << yhl::fls;

			// Ready
			if (!Ready) {
				if (false == m_ReadyDown && m_ReadyButton->InMousePointer(m_pos)) {
					m_ReadyDown = true;
					m_ReadyButton->SetScale(0.9F, 0.9F);
				}
			}
			else {
				if (false == m_ReadyDown && m_ReadyButton->InMousePointer(m_pos)) {
					m_ReadyDown = true;
					m_SearchQuitButton->SetScale(0.9F, 0.9F);
				}
			}

			// Quit
			if (false == m_QuitDown && m_QuitButton->InMousePointer(m_pos)) {
				m_QuitDown = true;
				m_QuitButton->SetScale(0.9F, 0.9F);
			}

			break;
		case WM_LBUTTONUP:
			yhl::dout << "lbuttonup" << yhl::fls;
			// Ready
			if (!Ready) {
				if (true == m_ReadyDown) {
					if (m_ReadyButton->InMousePointer(m_pos)) {
						Ready = true;
						NetworkSystem::SendRedayPacket(Ready);

						m_ReadyButton->SetScale(1.f, 1.f);
						RemoveObject(m_ReadyButton);
						ReaddObject(m_SearchQuitButton);
						ReaddObject(m_LoadingIcon);
					}
					else {
						m_ReadyButton->SetScale(1.f, 1.f);
					}
					m_ReadyDown = false;
				}
			}
			else {
				if (true == m_ReadyDown) {
					if (m_ReadyButton->InMousePointer(m_pos)) {
						Ready = false;
						NetworkSystem::SendRedayPacket(Ready);

						m_SearchQuitButton->SetScale(1.f, 1.f);
						ReaddObject(m_ReadyButton);
						RemoveObject(m_SearchQuitButton);
						RemoveObject(m_LoadingIcon);
					}
					else {
						m_SearchQuitButton->SetScale(1.f, 1.f);
					}
					m_ReadyDown = false;
				}
			}

			// Quit
			if (true == m_QuitDown) {
				if (m_QuitButton->InMousePointer(m_pos)) {
					exit(0);
				}
				else {
					m_QuitButton->SetScale(1.f, 1.f);
				}
				m_QuitDown = false;
			}

			break;
		case WM_RBUTTONDOWN:
			yhl::dout << "rbuttondown" << yhl::fls;
			break;
		case WM_RBUTTONUP:
			yhl::dout << "rbuttonup" << yhl::fls;
			break;
		default:
			break;
		}
	}


	KeyboardEvent evt = KeyboardManager::GetEvent();
	if (KeyboardEvent::NONE == evt) return;

	switch (evt) {
	case KeyboardEvent::KEY_DOWN_SPACE: {
		NetworkSystem::SendRedayPacket(true);
		yhl::dout << "ready" << yhl::fls;
		break;
	}
	case KeyboardEvent::KEY_DOWN_0: {
		m_Test->PushBack('a');
		break;
	}
	case KeyboardEvent::KEY_DOWN_1: {
		m_Test->PopBack();
		break;
	}
	case KeyboardEvent::KEY_DOWN_F11: {
		yhl::dout << "F11" << yhl::fls;

		// 여기서 풀스크린 전환
		Graphics::GraphicsCore::ToggleFullScreen();
		m_Camera.SetViewportAndScissorRect();
		break;
	}
	default:
		break;
	}
}

void LobbyScene::Destroy()
{
	m_Test = nullptr;
	m_LobbyObjs.clear();
	m_ReadyButton = nullptr;
	m_QuitButton = nullptr;
	m_SearchQuitButton = nullptr;
	m_LoadingIcon = nullptr;

	Ready = false;
	m_ReadyDown = false;
	m_QuitDown = false;
	m_ReadyButtonMouseIn = false;
	m_QuitButtonMouseIn = false;

	for (auto& [pso, vec] : m_Objects) {
		for (auto& obj : vec) {
			delete obj;
		}
		vec.clear();
	}
	m_Objects.clear();
	//g_Sound.pSound[LOBBY_SOUND]->release();
	g_Sound.pChannel[LOBBY_SOUND]->setVolume(0.f);
	g_Sound.pChannel[LOBBY_SOUND]->stop();
}
