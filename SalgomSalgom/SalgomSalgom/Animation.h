#pragma once

struct Bone
{
	int						index;
	int						parentIndex;
	DirectX::XMFLOAT4X4		childToParent;
	DirectX::XMFLOAT4X4		offsetMat;
};

struct Keyframe
{
public:
	float time;
	DirectX::XMFLOAT3 translation;
	DirectX::XMFLOAT4 rotation;
	DirectX::XMFLOAT3 scale;
};

struct BoneAnimation
{
public:
	void Interpolate(float t, DirectX::XMFLOAT4X4& M) const;

public:
	std::vector<Keyframe> m_KeyFrames;
};

struct AnimationClip
{
public:
	void Interpolate(float t, std::vector<DirectX::XMFLOAT4X4>& boneTransforms) const;

public:
	string m_ClipName;
	float m_TimeLength;
	unsigned short m_NumKeyframes;
	std::map<int, BoneAnimation> m_BoneAnimations;

	float m_PlayTime;		// 이 값을 언제 초기화하고 업데이트 하냐
};

struct D2bBuf;
class Animation
{
public:
	Animation(D2bBuf& buf);
	void GetFinalTransforms(float timePos, DirectX::XMFLOAT4X4* finalTransforms);
	void SetAnimation(const string& name);

	void PlayOnce(const string& name);
	

	std::vector<Bone>						bones;
	std::map<std::string, AnimationClip>	animationClips;
	
	string m_PrevClip;
	string m_CurrClip;
	unsigned int m_PlayCount;
	float m_PlayTime;
	bool m_PlayOnce;
};
