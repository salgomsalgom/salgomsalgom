#pragma once
#include "GraphicsObject.h"


class SpriteFont : public GraphicsObject
{
public:
	explicit SpriteFont(TextureKey key, float width, float height);
	explicit SpriteFont(int num_r, int num_c, TextureKey key, float sprite_w, float sprite_h, float pixel_x, float pixel_y);
	explicit SpriteFont(char c, TextureKey key, float sprite_w, float sprite_h, float pixel_x, float pixel_y);

public:
	virtual PSOType GetPSOType() override;

	virtual void CreateConstantBuffer() override;
	virtual void UpdateConstantBuffer(ID3D12GraphicsCommandList* cl) override;

	void SetTexturePos(float x, float y);
	void SetTextureScl(float x, float y);


private:
	XMFLOAT2 m_TexPos;
	XMFLOAT2 m_TexScl;
};

struct SpriteFontInfo
{
	int num_r;
	int num_c;
	float width;
	float height;
};

class Scene;
class Text
{
public:
	explicit Text(Scene* scene, TextureKey key, float x, float y, string contents);

	void PushBack(char letter);
	void PopBack();
	int Size() { return m_Contents.size(); }
	string GetString() { return m_Contents; }

	void operator=(const string& text);
	void operator=(string&& text);


private:
	void CreateLettersFromContents();

private:
	Scene				*m_Scene;
	TextureKey			m_TextureKey;
	POINTF				m_Pos;
	string				m_Contents;
	list<SpriteFont*>	m_Letters;

	SpriteFontInfo m_FontInfo;
	unordered_map<char, pair<int, int>> m_CharRowCol;
};
