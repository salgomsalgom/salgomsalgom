#include "pch.h"
#include "LoginScene.h"

#include "Game.h"
#include "NetworkSystem.h"
#include "InputSystem.h"
#include "GraphicsObject.h"
#include "Sound.h"

queue<char> g_CharQueue;

void PushChar(char ch)
{
	g_CharQueue.push(ch);
}

enum class TextEntryKey
{
	LOGIN_ID,
	LOGIN_PASSWORD,
	REGISTER_ID,
	REGISTER_PASSWORD,
	REGISTER_PASSWORD_CONFIRM,
};

map<TextEntryKey, POINTF> g_TextEntryPos;

void BuildTextEntryPos()
{
	g_TextEntryPos[TextEntryKey::LOGIN_ID]					= POINTF(1530.f, 380.f);
	g_TextEntryPos[TextEntryKey::LOGIN_PASSWORD]			= POINTF(1530.f, 515.f);
	g_TextEntryPos[TextEntryKey::REGISTER_ID]				= POINTF(1530.f, 315.f);
	g_TextEntryPos[TextEntryKey::REGISTER_PASSWORD]			= POINTF(1530.f, 450.f);
	g_TextEntryPos[TextEntryKey::REGISTER_PASSWORD_CONFIRM] = POINTF(1530.f, 585.f);
}

LoginScene::LoginScene(SceneKey key) : Scene(key)
{
	BuildTextEntryPos();
}



void LoginScene::Initialize()
{
	Graphics::GraphicsCore::ToggleFullScreen();
	m_Camera.SetViewportAndScissorRect();

	AddObject(new Sprite(1980, 1080, TextureKey::LOGIN_BG));

	m_LoginObj = new SpriteAnimation(TextureKey::LOGIN_LOGO, 500, 540);
	AddObject(m_LoginObj);

	
	AddObject(new Sprite(TextureKey::LOGIN_LOGIN_BOX, 1404, 480));


	// Button
	m_LoginButton = new Sprite(TextureKey::LOGIN_LOGIN_BUTTON, 1100, 646);
	m_RegisterButton = new Sprite(TextureKey::LOGIN_REGISTER_BUTTON, 1405, 646);
	m_QuitButton = new Sprite(TextureKey::LOGIN_QUIT_BUTTON, 1710, 646);
	AddObject(m_LoginButton);
	AddObject(m_RegisterButton);
	AddObject(m_QuitButton);

	m_TextID = new Text(this, TextureKey::BATTLE_UI_ENGFONT, 1270, 395, "");
	m_TextPassword = new Text(this, TextureKey::BATTLE_UI_ENGFONT, 1270, 530, "");
	m_Password = "";

	m_CurrActiveText = &m_TextID;


	//회원가입창_ 선택칸 스프라이트 위치
    // new Text(this, TextureKey::BATTLE_UI_ENGFONT, 1270, 330, "ABCjqriK012345");
    // new Text(this, TextureKey::BATTLE_UI_ENGFONT, 1270, 470, "*********");
	// new Text(this, TextureKey::BATTLE_UI_ENGFONT, 1270, 600, "*********");

	m_RegisterBox = new Sprite(TextureKey::LOGIN_REGISTER_BOX, 1404, 480);
	m_JoinInButton = new Sprite(TextureKey::LOGIN_REGISTER_BUTTON, 1100, 726);
	m_BackButton = new Sprite(TextureKey::LOGIN_BACK_BUTTON, 1710, 726);

	m_RegisterBox->CreateConstantBuffer();
	m_JoinInButton->CreateConstantBuffer();
	m_BackButton->CreateConstantBuffer();

	m_TextRegisterID = new Text(this, TextureKey::BATTLE_UI_ENGFONT, 1270, 330, "");
	m_TextRegisterPassword = new Text(this, TextureKey::BATTLE_UI_ENGFONT, 1270, 470, "");
	m_TextRegisterPasswordConfirm = new Text(this, TextureKey::BATTLE_UI_ENGFONT, 1270, 600, "");
	m_RegisterPassword.clear();
	m_RegisterPassword.clear();


	// 선택창
	m_SelectedEntry = new Sprite(TextureKey::LOGIN_SELECTED, 1530, 380);
	AddObject(m_SelectedEntry);

	g_Sound.pChannel[LOGIN_SOUND]->setVolume(1.f);
	g_Sound.Play(LOGIN_SOUND);


}

void LoginScene::Update(float frame_time)
{
	// 여기서 업데이트?

	m_LoginObj->UpdateIndex();

	if (false == g_CharQueue.empty())
	{
		char ch = g_CharQueue.front();
		g_CharQueue.pop();

		if ('\r' == ch) return;

		if ('\t' == ch) {
			if (false == m_Register) {// 로그인
				if (m_CurrActiveText == &m_TextID) {
					auto pos = g_TextEntryPos[TextEntryKey::LOGIN_PASSWORD];
					m_SelectedEntry->SetPosition(pos.x, pos.y);
					m_CurrActiveText = &m_TextPassword;
				}
				else {
					auto pos = g_TextEntryPos[TextEntryKey::LOGIN_ID];
					m_SelectedEntry->SetPosition(pos.x, pos.y);
					m_CurrActiveText = &m_TextID;
				}
			}
			else {// 회원가입
				if (m_CurrActiveText == &m_TextRegisterID) {
					auto pos = g_TextEntryPos[TextEntryKey::REGISTER_PASSWORD];
					m_SelectedEntry->SetPosition(pos.x, pos.y);
					m_CurrActiveText = &m_TextRegisterPassword;
				}
				else if (m_CurrActiveText == &m_TextRegisterPassword) {
					auto pos = g_TextEntryPos[TextEntryKey::REGISTER_PASSWORD_CONFIRM];
					m_SelectedEntry->SetPosition(pos.x, pos.y);
					m_CurrActiveText = &m_TextRegisterPasswordConfirm;
				}
				else {
					auto pos = g_TextEntryPos[TextEntryKey::REGISTER_ID];
					m_SelectedEntry->SetPosition(pos.x, pos.y);
					m_CurrActiveText = &m_TextRegisterID;
				}
			}
			return;
		}

		if ('\b' == ch) {
			if(0 == (*m_CurrActiveText)->Size()) return;
			(*m_CurrActiveText)->PopBack();
			if (m_CurrActiveText == &m_TextPassword) m_Password.pop_back();
			if (m_CurrActiveText == &m_TextRegisterPassword) m_RegisterPassword.pop_back();
			if (m_CurrActiveText == &m_TextRegisterPasswordConfirm) m_RegisterPasswordConfirm.pop_back();
			return;
		}

		if ((*m_CurrActiveText)->Size() < 16) {
			if (m_CurrActiveText == &m_TextPassword) {
				m_Password.push_back(ch);
				ch = '*';
			}
			if (m_CurrActiveText == &m_TextRegisterPassword) {
				m_RegisterPassword.push_back(ch);
				ch = '*';
			}
			if (m_CurrActiveText == &m_TextRegisterPasswordConfirm) {
				m_RegisterPasswordConfirm.push_back(ch);
				ch = '*';
			}
			(*m_CurrActiveText)->PushBack(ch);
		}
	}

}

void LoginScene::HandleInput(InputSystem& input)
{
	// 마우스
	UINT m_msg;
	int x, y;
	POINT pos = MouseManager::GetMousePos();

	if (MouseManager::PopMouseInput(m_msg, x, y))
	{
		switch (m_msg) {
		case WM_LBUTTONDOWN:
			yhl::dout << "lbuttondown" << yhl::fls;
			
			if (false == m_Register) {	// 로그인 창에서 버튼 클릭 반응
				if (m_RegisterButton->InMousePointer(pos))
				{
					ShowRegister();
				}
				if (m_QuitButton->InMousePointer(pos))
				{
					exit(0);
				}
			}
			else {						// 회원가입 창에서 버튼 클릭 반응
				if (m_JoinInButton->InMousePointer(pos))
				{
				}
				if (m_BackButton->InMousePointer(pos)) 
				{
					HideRegister();
				}
			}
			
			break;
		case WM_LBUTTONUP:
			yhl::dout << "lbuttonup" << yhl::fls;
			break;
		case WM_RBUTTONDOWN:
			yhl::dout << "rbuttondown" << yhl::fls;
			break;
		case WM_RBUTTONUP:
			yhl::dout << "rbuttonup" << yhl::fls;
			break;
		default:
			break;
		}
	}

	// 키보드
	KeyboardEvent evt = KeyboardManager::GetEvent();
	if (KeyboardEvent::NONE == evt) return;

	switch (evt) {
	case KeyboardEvent::KEY_DOWN_ENTER: {
		if (false == m_Register) {		// 로그인
			string id = m_TextID->GetString();
			NetworkSystem::Send_L_Login(id.c_str(), m_Password.c_str());
		}
		else {							// 회원가입
			string id = m_TextRegisterID->GetString();
			if (m_RegisterPassword == m_RegisterPasswordConfirm) {
				NetworkSystem::Send_L_Join(id.c_str(), m_RegisterPassword.c_str());
			}
			else {
				(*m_TextRegisterPassword) = string();
				(*m_TextRegisterPasswordConfirm) = string();
				m_RegisterPassword.clear();
				m_RegisterPasswordConfirm.clear();
			}
		}
		break;
	}
	default:
		break;
	}
}

void LoginScene::Destroy()
{
	// g_Sound.pSound[LOGIN_SOUND]->release();
	g_Sound.pChannel[LOGIN_SOUND]->setVolume(0.f);
	g_Sound.pChannel[LOGIN_SOUND]->stop();
}

void LoginScene::ShowRegister()
{
	m_Register = true;

	ReaddObject(m_RegisterBox);
	ReaddObject(m_JoinInButton);
	ReaddObject(m_BackButton);

	auto pos = g_TextEntryPos[TextEntryKey::REGISTER_ID];
	m_SelectedEntry->SetPosition(pos.x, pos.y);
	m_CurrActiveText = (&m_TextRegisterID);

	RemoveObject(m_SelectedEntry);
	ReaddObject(m_SelectedEntry);

	// 텍스트 비우기
	(*m_TextID) = string();
	(*m_TextPassword) = string();
	m_Password.clear();
}

void LoginScene::HideRegister()
{
	m_Register = false;

	RemoveObject(m_RegisterBox);
	RemoveObject(m_JoinInButton);
	RemoveObject(m_BackButton);

	auto pos = g_TextEntryPos[TextEntryKey::LOGIN_ID];
	m_SelectedEntry->SetPosition(pos.x, pos.y);
	m_CurrActiveText = (&m_TextID);

	RemoveObject(m_SelectedEntry);
	ReaddObject(m_SelectedEntry);

	(*m_TextRegisterID) = string();
	(*m_TextRegisterPassword) = string();
	(*m_TextRegisterPasswordConfirm) = string();
	m_RegisterPassword.clear();
	m_RegisterPasswordConfirm.clear();
}
