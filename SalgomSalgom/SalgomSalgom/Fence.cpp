#include "pch.h"
#include "Fence.h"

Fence::Fence() : 
	m_Fence(nullptr),
	m_FenceValue(0),
	m_FenceEvent()
{
}

Fence::~Fence()
{
}

void Fence::Initialize(ID3D12Device* const device)
{
	m_FenceValue = 0;

	device->CreateFence(
		/*InitValue*/ m_FenceValue,
		/*FenceFlag*/ D3D12_FENCE_FLAG_NONE,
		IID_PPV_ARGS(m_Fence.GetAddressOf())
	);

	m_FenceEvent = ::CreateEvent(nullptr, FALSE, FALSE, nullptr);
}

void Fence::Wait(ID3D12CommandQueue* const commandQueue)
{
	++m_FenceValue;
	commandQueue->Signal(m_Fence.Get(), m_FenceValue);

	if (m_Fence->GetCompletedValue() < m_FenceValue) {
		m_Fence->SetEventOnCompletion(m_FenceValue, m_FenceEvent);
		::WaitForSingleObject(m_FenceEvent, INFINITE);
	}
}
