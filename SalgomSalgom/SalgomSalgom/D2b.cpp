#include "pch.h"
#include "D2b.h"

#include "Vertex.h"
#include "Animation.h"

D2bBuf::D2bBuf()
{
	memset(&header, 0, sizeof(header));

	v_buf = nullptr;
	i_buf = nullptr;
	b_buf = nullptr;
	ani_buf = nullptr;
	key_buf = nullptr;
}

D2bBuf::~D2bBuf()
{
	if (nullptr != v_buf) delete[] v_buf;
	if (nullptr != i_buf) delete[] i_buf;
	if (nullptr != b_buf) delete[] b_buf;
	if (nullptr != ani_buf) delete[] ani_buf;
	if (nullptr != key_buf) delete[] key_buf;

	v_buf = nullptr;
	i_buf = nullptr;
	b_buf = nullptr;
	ani_buf = nullptr;
	key_buf = nullptr;
}


void D2bBuf::LoadD2b(const std::string& filename)
{
	std::ifstream in{ filename, std::ios_base::binary };
	assert(in);

	ResetBuf();

	in.read(reinterpret_cast<char*>(&header), sizeof(header));
	AllocBuf();

	in.read(reinterpret_cast<char*>(v_buf), GetVertexBytes() * header.v_count);
	in.read(reinterpret_cast<char*>(i_buf), sizeof(unsigned short) * header.i_count);

	if (header.rigged) {
		in.read(reinterpret_cast<char*>(b_buf), sizeof(Bone) * header.b_count);
		in.read(reinterpret_cast<char*>(ani_buf), sizeof(AniInfo) * header.ani_count);
		in.read(reinterpret_cast<char*>(key_buf), sizeof(Keyframe) * header.key_count);
	}

	in.close();
}


void D2bBuf::AllocBuf()
{
	switch (header.v_type) {
	case VertexType::STATIC:
		v_buf = new StaticVertex[header.v_count];
		break;
	case VertexType::SKINNED:
		v_buf = new SkinnedVertex[header.v_count];
		break;
	default: break;
	}

	i_buf = new Index[header.i_count];

	if (header.rigged) {
		b_buf = new Bone[header.b_count];
		ani_buf = new AniInfo[header.ani_count];
		key_buf = new Keyframe[header.key_count];
	}
}

void D2bBuf::ResetBuf()
{
	memset(&header, 0, sizeof(header));

	if (nullptr != v_buf) delete[] v_buf;
	if (nullptr != i_buf) delete[] i_buf;
	if (nullptr != b_buf) delete[] b_buf;
	if (nullptr != ani_buf) delete[] ani_buf;
	if (nullptr != key_buf) delete[] key_buf;

	v_buf = nullptr;
	i_buf = nullptr;
	b_buf = nullptr;
	ani_buf = nullptr;
	key_buf = nullptr;
}

int D2bBuf::GetVertexBytes()
{
	switch (header.v_type) {
	case VertexType::STATIC:	return sizeof(StaticVertex);
	case VertexType::SKINNED:	return sizeof(SkinnedVertex);
	default:					return 0;
	}
}
