#include "pch.h"
#include "TimerSystem.h"

bool TimerSystem::s_Instantiated = false;

TimerSystem::TimerSystem(SystemCommunicator* const communicator) :
	SystemNode(communicator),
	m_TimeScale(0),
	m_EntryCount(0),
	m_CurrCount(0),
	m_LastCount(0),
	m_ElapsedTime(0),
	m_TimeSample(),
	m_SampleIdx(0),
	m_CurrFrameRate(0),
	m_FrameRate(0)
{
	assert(!s_Instantiated);
	s_Instantiated = true;
}

TimerSystem::~TimerSystem()
{
}

void TimerSystem::Initialize()
{
	__int64 freq;
	::QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
	m_TimeScale = 1.f / freq;

	::QueryPerformanceCounter((LARGE_INTEGER*)&m_EntryCount);
	m_CurrCount = m_LastCount = m_EntryCount;

	yhl::dout << "Initialized Timer System." << yhl::fls;
}

void TimerSystem::Tick()
{
	::QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&m_CurrCount));
	float elapsed_time = (m_CurrCount - m_LastCount) * m_TimeScale;

	while (elapsed_time < LOCK_SPF) {
		::QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&m_CurrCount));
		elapsed_time = (m_CurrCount - m_LastCount) * m_TimeScale;
	}

	m_LastCount = m_CurrCount;

	m_ElapsedTime = elapsed_time;

	UpdateFrameRate(elapsed_time);
}

float TimerSystem::GetElapsedTime()
{
	return m_ElapsedTime;
}

float TimerSystem::GetBaseTime()
{
	return (m_CurrCount - m_EntryCount) * m_TimeScale;
}

float TimerSystem::GetFrameRate()
{
	return m_FrameRate;
}

void TimerSystem::UpdateFrameRate(float elapsed_time)
{
	m_TimeSample[m_SampleIdx++] = elapsed_time;

	if (FPS_SAMPLE_COUNT == m_SampleIdx) {
		float total_time = std::accumulate(m_TimeSample.begin(), m_TimeSample.end(), 0.f);
		m_FrameRate = (float)FPS_SAMPLE_COUNT / total_time;

		m_SampleIdx = 0;
	}
}
