#include "pch.h"
#include "SpriteAnimation.h"

#include "WindowApp.h"
#include "PipelineState.h"

#include "Scene.h"


//SpriteAnimation::SpriteAnimation(TextureKey key, float pixel_x, float pixel_y)
//	: GraphicsObject()
//	, m_CurrentIdx(0)
//	, m_TexPos(0.f, 0.f)
//	, m_TexScl(1.f, 1.f)
//
//{
//	//처음 add했을 때 기본 설정 하고 인덱스 업데이트 시작
//
//	//m_AniInfo = GetAniInfo(key);
//
//	m_Mesh = StaticGameData::g_Meshs[MeshKey::RECTANGLE];
//	m_Texture = StaticGameData::g_Textures[key];
//
//	auto wr = WindowApp::GetClientRect();
//	float wnd_w = static_cast<float>(wr.right - wr.left);
//	float wnd_h = static_cast<float>(wr.bottom - wr.top);
//
//	float scl_x = (sprite_w / wnd_w) * WINDOW_MODE_RATE;
//	float scl_y = (sprite_h / wnd_h) * WINDOW_MODE_RATE;
//	m_Scale = XMFLOAT3(scl_x, scl_y, 0);
//
//	m_TexScl.x = 1.f / num_r;
//	m_TexScl.y = 1.f / num_c;
//
//	float pos_x = (pixel_x * 2.f / wnd_w * WINDOW_MODE_RATE) - 1.f;
//	float pos_y = (pixel_y * 2.f / wnd_h * WINDOW_MODE_RATE) - 1.f;
//	m_Position = XMFLOAT3(pos_x, -pos_y, 0.f);
//
//
//}

PSOType SpriteAnimation::GetPSOType()
{
	return PSOType::SPRITE;
}

void SpriteAnimation::CreateConstantBuffer()
{
	m_DescriptorHeap.Create(DescriptorHeapType::CBV_SRV_UAV, 1);

	// 상수 버퍼는 256byte의 배수여야 함
	UINT64 bytes = static_cast<UINT64>((sizeof(CB_ObjectInfo) + 255) & ~255);

	m_ObjectInfoCB.Create(bytes, true);

	D3D12_CONSTANT_BUFFER_VIEW_DESC cbv_desc;
	cbv_desc.BufferLocation = m_ObjectInfoCB->GetGPUVirtualAddress();
	cbv_desc.SizeInBytes = static_cast<int>(bytes);

	Graphics::GraphicsCore::GetDevice()->CreateConstantBufferView(&cbv_desc, m_DescriptorHeap.GetHandle());		// 힙과 버퍼를 묶어 뷰를 생성

	auto rtv = m_ObjectInfoCB->Map(0, NULL, reinterpret_cast<void**>(&m_ObjectInfo));
}

void SpriteAnimation::UpdateConstantBuffer(ID3D12GraphicsCommandList* cl)
{
	XMVECTOR pos = XMLoadFloat3(&m_Position);
	XMVECTOR rot = XMLoadFloat4(&m_Rotation);
	XMVECTOR scl = XMLoadFloat3(&m_Scale);
	XMMATRIX world_mtx = XMMatrixAffineTransformation(scl, XMMath::XMVECTOR_ORIGIN, rot, pos);
	XMStoreFloat4x4(&(m_ObjectInfo->m_WorldTransform), XMMatrixTranspose(world_mtx));

	pos = XMLoadFloat2(&m_TexPos);
	XMVECTOR tex_rot{ 0.f, 0.f, 0.f, 1.f };

	scl = XMLoadFloat2(&m_TexScl);
	XMMATRIX tex_mtx = XMMatrixAffineTransformation(scl, XMMath::XMVECTOR_ORIGIN, tex_rot, pos);
	XMStoreFloat4x4(&(m_ObjectInfo->m_TexTransform), XMMatrixTranspose(tex_mtx));

	cl->SetGraphicsRootConstantBufferView(2, m_ObjectInfoCB->GetGPUVirtualAddress());
}

void SpriteAnimation::SetTexturePos(float x, float y)
{
	m_TexPos.x = x;
	m_TexPos.y = y;
}

void SpriteAnimation::SetTextureScl(float x, float y)
{
	m_TexScl.x = x;
	m_TexScl.y = y;
}

void SpriteAnimation::UpdateIndex()
{
	m_NumOfUpdatedFrame += 1;
	
	float new_pixel_x = 0.f;
	float new_pixel_y = 0.f;
	
	m_CurrentIdx = m_NumOfUpdatedFrame / m_AniInfo.speed;
	m_CurrentIdx %= m_AniInfo.end_idx; //max_int 범위를 넘어가지 않도록!

	new_pixel_x = (m_CurrentIdx % m_AniInfo.num_r) * (1.f / m_AniInfo.num_r); //(0~2) * 0.33...
	new_pixel_y = (m_CurrentIdx / m_AniInfo.num_r) * (1.f / m_AniInfo.num_c); //(0~6) * 0.14...
	
	//함수 내용 바꾸기?
	SetTexturePos(new_pixel_x, new_pixel_y);

}

SpriteAnimationInfo GetAniInfo(TextureKey key)
{
	SpriteAnimationInfo rtv;

	switch (key)
	{
	case TextureKey::LOGIN_LOGO:
		rtv.end_idx = 10;
		rtv.num_r = 5; //열
		rtv.num_c = 2; //행
		rtv.width = 632.f;
		rtv.height = 740.f;
		rtv.speed = 3.5f;
		break;

	case TextureKey::LOBBY_SPANI_SPIDER:
		rtv.end_idx = 36;
		rtv.num_r = 4; //열
		rtv.num_c = 9; //행
		rtv.width = 68.f;
		rtv.height = 106.f;
		rtv.speed = 3.5f;
		break;

	case TextureKey::LOBBY_SPANI_NMGD1:
		rtv.end_idx = 30;
		rtv.num_r = 5; //열
		rtv.num_c = 6; //행
		rtv.width = 183.f;
		rtv.height = 272.f;
		rtv.speed = 2.f;
		break;

	case TextureKey::LOBBY_SPANI_NMGD2:
		rtv.end_idx = 40;
		rtv.num_r = 5; //열
		rtv.num_c = 8; //행
		rtv.width = 114.f;
		rtv.height = 139.f;
		rtv.speed = 2.5f;
		break;

	case TextureKey::LOBBY_SPANI_NMGD3:
		rtv.end_idx = 40;
		rtv.num_r = 5; //열
		rtv.num_c = 8; //행
		rtv.width = 255.f;
		rtv.height = 151.f;
		rtv.speed = 2.5f;
		break;

	case TextureKey::LOBBY_SPANI_NMGD4:
		rtv.end_idx = 30;
		rtv.num_r = 5; //열
		rtv.num_c = 6; //행
		rtv.width = 242.f;
		rtv.height = 237.f;
		rtv.speed = 3.f;
		break;

	case TextureKey::LOBBY_SPANI_WITCH:
		rtv.end_idx = 40;
		rtv.num_r = 5; //열
		rtv.num_c = 8; //행
		rtv.width = 854.f;
		rtv.height = 580.f;
		rtv.speed = 2.f;
		break;

	case TextureKey::LOBBY_LOADING_ICON:
		rtv.end_idx = 24;
		rtv.num_r = 6; //열
		rtv.num_c = 4; //행
		rtv.width = 50.f;
		rtv.height = 50.f;
		rtv.speed = 2.f;
		break;

	case TextureKey::WCWIN_NM1:
		rtv.end_idx = 20;
		rtv.num_r = 5; //열
		rtv.num_c = 4; //행
		rtv.width = 352.f;
		rtv.height = 248.f;
		rtv.speed = 2.f;
		break;

	case TextureKey::WCWIN_NM2:
		rtv.end_idx = 30;
		rtv.num_r = 5; //열
		rtv.num_c = 6; //행
		rtv.width = 640.f;
		rtv.height = 524.f;
		rtv.speed = 2.f;
		break;

	case TextureKey::WCWIN_WICH:
		rtv.end_idx = 30;
		rtv.num_r = 5; //열
		rtv.num_c = 6; //행
		rtv.width = 888.f;
		rtv.height = 1000.f;
		rtv.speed = 2.5f;
		break;

	case TextureKey::WCWIN_SOUL:
		rtv.end_idx = 30;
		rtv.num_r = 5; //열
		rtv.num_c = 6; //행
		rtv.width = 200.f;
		rtv.height = 400.f;
		rtv.speed = 3.f;
		break;

	case TextureKey::NMWIN_NM1:
		rtv.end_idx = 20;
		rtv.num_r = 5; //열
		rtv.num_c = 4; //행
		rtv.width = 204.f;
		rtv.height = 352.f;
		rtv.speed = 2.f;
		break;

	case TextureKey::NMWIN_NM2:
		rtv.end_idx = 25;
		rtv.num_r = 5; //열
		rtv.num_c = 5; //행
		rtv.width = 260.f;
		rtv.height = 352.f;
		rtv.speed = 2.f;
		break;

	case TextureKey::NMWIN_NM3:
		rtv.end_idx = 30;
		rtv.num_r = 5; //열
		rtv.num_c = 6; //행
		rtv.width = 284.f;
		rtv.height = 400.f;
		rtv.speed = 2.f;
		break;

	case TextureKey::NMWIN_NM4:
		rtv.end_idx = 25;
		rtv.num_r = 5; //열
		rtv.num_c = 5; //행
		rtv.width = 438.f;
		rtv.height = 396.f;
		rtv.speed = 2.f;
		break;

	case TextureKey::NMWIN_WICH:
		rtv.end_idx = 25;
		rtv.num_r = 5; //열
		rtv.num_c = 5; //행
		rtv.width = 320.f;
		rtv.height = 320.f;
		rtv.speed = 2.f;
		break;

	case TextureKey::EF_CONFETTI:
		rtv.end_idx = 30;
		rtv.num_r = 5; //열
		rtv.num_c = 6; //행
		rtv.width = 500.f;
		rtv.height = 400.f;
		rtv.speed = 4.f;
		break;

	default:
		rtv.end_idx = 0;
		rtv.num_r = 0;
		rtv.num_c = 0;
		rtv.width = 0;
		rtv.height = 0;
		break;
	}

	return rtv;
}

SpriteAnimation::SpriteAnimation(TextureKey key, float pixel_x, float pixel_y)
	: GraphicsObject()
	, m_CurrentIdx(0)
	, m_NumOfUpdatedFrame(0)
	, m_TexPos(0.f, 0.f)
	, m_TexScl(1.f, 1.f)
{

	m_AniInfo = GetAniInfo(key);

	m_Mesh = StaticGameData::g_Meshs[MeshKey::RECTANGLE];
	m_Texture = StaticGameData::g_Textures[key];

	auto wr = WindowApp::GetClientRect();
	float wnd_w = static_cast<float>(wr.right - wr.left);
	float wnd_h = static_cast<float>(wr.bottom - wr.top);

	float scl_x = (m_AniInfo.width / wnd_w) * WINDOW_MODE_RATE;
	float scl_y = (m_AniInfo.height / wnd_h) * WINDOW_MODE_RATE;
	m_Scale = XMFLOAT3(scl_x, scl_y, 0);

	m_TexScl.x = 1.f / m_AniInfo.num_r;
	m_TexScl.y = 1.f / m_AniInfo.num_c;

	float pos_x = (pixel_x * 2.f / wnd_w * WINDOW_MODE_RATE) - 1.f;
	float pos_y = (pixel_y * 2.f / wnd_h * WINDOW_MODE_RATE) - 1.f;
	m_Position = XMFLOAT3(pos_x, -pos_y, 0.f);

	m_TexPos.x = (m_CurrentIdx % m_AniInfo.num_r) * (1.f / m_AniInfo.num_r); //(0~2) * 0.33...
	m_TexPos.y = (m_CurrentIdx / m_AniInfo.num_r) * (1.f / m_AniInfo.num_c); //(0~6) * 0.14...

}