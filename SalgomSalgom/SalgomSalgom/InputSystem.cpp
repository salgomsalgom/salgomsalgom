#include "pch.h"
#include "InputSystem.h"

#include "Util/DebugOut.h"


bool InputSystem::s_Instantiated = false;
bool InputSystem::s_WindowFocus = true;

InputSystem::InputSystem(SystemCommunicator* const communicator) :
	SystemNode(communicator)
{
	assert(!s_Instantiated);
	s_Instantiated = true;
}

void InputSystem::Initialize()
{
	MouseManager::Initialize();
	yhl::dout << "Initialized Input System." << yhl::fls;
}


void InputSystem::Update() {
	if (false == s_WindowFocus) return;
	
	MouseManager::Update();
	KeyboardManager::Update();
}

void InputSystem::SetWindowFocus(bool value)
{
	s_WindowFocus = value;
}

