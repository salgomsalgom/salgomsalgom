#include "pch.h"
#include "GraphicsCore.h"

#include "SwapChain.h"
#include "CommandListManager.h"

#include "DescriptorHeap.h"
#include "DescriptorAllocator.h"
#include "ShaderVisibleDescriptorHeap.h"
#include "DepthBuffer.h"
#include "PipelineState.h"

#include "Mesh.h"
#include "Texture.h"

#include "GameData.h"
#include "Shader.h"
#include "Camera.h"

#include "Scene.h"
#include "GraphicsObject.h"

using namespace Graphics;

ComPtr<IDXGIFactory4>		GraphicsCore::s_Factory;
SwapChain					GraphicsCore::s_SwapChain;

ComPtr<ID3D12Device>		GraphicsCore::s_Device;
CommandListManager			GraphicsCore::s_CommandListManager;

DescriptorHeap				GraphicsCore::s_RtvDescriptorHeap{ };
DescriptorHeap				GraphicsCore::s_DsvDescriptorHeap{ };
DescriptorAllocator			GraphicsCore::s_DescriptorAllocator{ D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV };
ShaderVisibleDescriptorHeap GraphicsCore::s_BindingHeap{ };

ComPtr<ID3D12Resource>		GraphicsCore::s_DisplayPlanes[GraphicsCore::s_SwapChainBufferCount];
int							GraphicsCore::s_DisplayPleneIdx = 0;
DepthBuffer					GraphicsCore::s_DepthStencilBuffer;

ComPtr<ID3D12RootSignature> GraphicsCore::s_GraphicsRS;
map<PSOType, GraphicsPSO*>		GraphicsCore::s_GraphicsPSOs;



// 텍스처는 root descriptor로 묶을 수 없다.
// 무조건 서술자 테이블에 넣을 것
void CreateGraphicRootSignature(ID3D12RootSignature** rootSignature)
{
	// Q. 쉐이더에서 b1, t1 등의 레지스터 슬롯?에 어떻게 연결되는가?
	// Q. 텍스처 2개를 보내려면 어떻게 해야하는 거지??

	// Root Parameter 성능 팁: 사용 빈도가 높은 것에서 낮은 순으로 나열한다.
	D3D12_ROOT_PARAMETER root_parameter[4];

	root_parameter[0].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	root_parameter[0].Descriptor.ShaderRegister = 0;
	root_parameter[0].Descriptor.RegisterSpace = 0;
	root_parameter[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	D3D12_DESCRIPTOR_RANGE desc_range[1];
	desc_range[0].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	desc_range[0].NumDescriptors = 1;
	desc_range[0].BaseShaderRegister = 0;
	desc_range[0].RegisterSpace = 0;
	desc_range[0].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	// 서술자 테이블에 대한
	D3D12_ROOT_DESCRIPTOR_TABLE root_descriptor_table;
	root_descriptor_table.NumDescriptorRanges = _countof(desc_range);
	root_descriptor_table.pDescriptorRanges = desc_range;

	root_parameter[1].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	root_parameter[1].DescriptorTable = root_descriptor_table;
	root_parameter[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	root_parameter[2].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	root_parameter[2].Descriptor.ShaderRegister = 1;
	root_parameter[2].Descriptor.RegisterSpace = 0;
	root_parameter[2].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	root_parameter[3].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	root_parameter[3].Descriptor.ShaderRegister = 2;
	root_parameter[3].Descriptor.RegisterSpace = 0;
	root_parameter[3].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	// D3D12_ROOT_DESCRIPTOR_TABLE	DescriptorTable;
	// D3D12_ROOT_CONSTANTS			Constants;
	// D3D12_ROOT_DESCRIPTOR		Descriptor;


	// Static Sampler를 사용한다.
	D3D12_STATIC_SAMPLER_DESC static_sampler_desc[1];
	static_sampler_desc[0].Filter = D3D12_FILTER_MIN_MAG_MIP_LINEAR;
	static_sampler_desc[0].AddressU = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
	static_sampler_desc[0].AddressV = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
	static_sampler_desc[0].AddressW = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
	static_sampler_desc[0].MipLODBias = 0;
	static_sampler_desc[0].MaxAnisotropy = 1;
	static_sampler_desc[0].ComparisonFunc = D3D12_COMPARISON_FUNC_ALWAYS;
	static_sampler_desc[0].MinLOD = 0;
	static_sampler_desc[0].MaxLOD = D3D12_FLOAT32_MAX;
	static_sampler_desc[0].ShaderRegister = 0;			// s0 Register
	static_sampler_desc[0].RegisterSpace = 0;
	static_sampler_desc[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	D3D12_ROOT_SIGNATURE_DESC d3dRootSignatureDesc{ };
	d3dRootSignatureDesc.NumParameters = _countof(root_parameter);
	d3dRootSignatureDesc.pParameters = root_parameter;
	d3dRootSignatureDesc.NumStaticSamplers = _countof(static_sampler_desc);
	d3dRootSignatureDesc.pStaticSamplers = static_sampler_desc;
	d3dRootSignatureDesc.Flags = D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT;

	ID3DBlob* pd3dSignatureBlob = NULL;
	ID3DBlob* pd3dErrorBlob = NULL;

	HRESULT rtv = ::D3D12SerializeRootSignature(
		&d3dRootSignatureDesc,
		D3D_ROOT_SIGNATURE_VERSION_1,
		&pd3dSignatureBlob,
		&pd3dErrorBlob
	);

	if (pd3dErrorBlob)
		yhl::dout << (char*)pd3dErrorBlob->GetBufferPointer() << yhl::fls;


	GraphicsCore::GetDevice()->CreateRootSignature(
		0,
		pd3dSignatureBlob->GetBufferPointer(),
		pd3dSignatureBlob->GetBufferSize(),
		IID_PPV_ARGS(rootSignature)
	);

	if (pd3dSignatureBlob)	pd3dSignatureBlob->Release();
	if (pd3dErrorBlob)		pd3dErrorBlob->Release();

	// 응용 프로그램에서 루트 매개변수에 실제로 전달하는 값을
	// Root Argument라고 부른다.
}

void GraphicsCore::Initialize()
{
	UINT factory_flag = 0;

#if defined(_DEBUG)
	{
		ComPtr<ID3D12Debug> debug;
		D3D12GetDebugInterface(IID_PPV_ARGS(&debug));
		if (debug) debug->EnableDebugLayer();
		factory_flag |= DXGI_CREATE_FACTORY_DEBUG;
	}
#endif

	CreateDXGIFactory2(
		factory_flag,
		IID_PPV_ARGS(&s_Factory)
	);

	s_Factory->MakeWindowAssociation(WindowApp::GetHwnd(), DXGI_MWA_NO_ALT_ENTER);

	CreateDeviceForHardware();

	s_CommandListManager.Initialize(s_Device.Get());

	s_SwapChain.Initailize(s_Factory.Get(), s_CommandListManager.GetCommandQueue());


	s_RtvDescriptorHeap.Create(DescriptorHeapType::RTV, s_SwapChainBufferCount);

	for (int i = 0; i < s_SwapChainBufferCount; ++i) {
		s_SwapChain->GetBuffer(i, IID_PPV_ARGS(&s_DisplayPlanes[i]));
		s_Device->CreateRenderTargetView(s_DisplayPlanes[i].Get(), nullptr, s_RtvDescriptorHeap.GetHandle(i));
	}

	s_DsvDescriptorHeap.Create(DescriptorHeapType::DSV, 1);

	s_DepthStencilBuffer.Create();

	D3D12_CPU_DESCRIPTOR_HANDLE desc_handle = s_DsvDescriptorHeap.GetHandle();

	s_Device->CreateDepthStencilView(
		s_DepthStencilBuffer.Get(),
		nullptr,
		desc_handle
	);

	s_BindingHeap.Create(s_Device.Get(), 1024);

	CreateGraphicRootSignature(s_GraphicsRS.GetAddressOf());

	CompileShaders();

	s_CommandListManager.Reset();
	BuildGeometryMesh();
	LoadD2bFiles();
	LoadTextures();

	s_CommandListManager.ExcuteCommandList();

	BuildPSO();

	yhl::dout << "Initialized Graphics Core." << yhl::fls;
}

void GraphicsCore::ShutDown()
{
}

void GraphicsCore::Resize()
{
}

void GraphicsCore::ToggleFullScreen()
{
	s_CommandListManager.SetFence();

	BOOL is_fullscreen;
	s_SwapChain->GetFullscreenState(&is_fullscreen, NULL);
	s_SwapChain->SetFullscreenState(!is_fullscreen, nullptr);

	RECT clientRect = (!is_fullscreen) ? FULLSCREEN_MODE_RECT : WINDOW_MODE_RECT;

	UINT width = clientRect.right - clientRect.left;
	UINT height = clientRect.bottom - clientRect.top;

	DXGI_MODE_DESC display_mode_desc{ };
	display_mode_desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	display_mode_desc.Width = width;
	display_mode_desc.Height = height;
	display_mode_desc.RefreshRate = { 0 , 0 };		// 여기를 0으로 설정하면 알아서 계산해준다고 함...!
	display_mode_desc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	display_mode_desc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	s_SwapChain->ResizeTarget(&display_mode_desc);

	for (int i = 0; i < s_SwapChainBufferCount; ++i) 
		s_DisplayPlanes[i].Reset();

	// SwapChain의 버퍼를 Resize한다.
	DXGI_SWAP_CHAIN_DESC swap_chain_desc{ };
	s_SwapChain->GetDesc(&swap_chain_desc);
	s_SwapChain->ResizeBuffers(
		s_SwapChainBufferCount,
		width,
		height,
		swap_chain_desc.BufferDesc.Format,
		swap_chain_desc.Flags
	);

	// 버퍼의 인덱스를 얻어온다.
	s_DisplayPleneIdx = s_SwapChain->GetCurrentBackBufferIndex();

	// RTV를 다시 만들어준다.
	for (int i = 0; i < s_SwapChainBufferCount; ++i) {
		s_SwapChain->GetBuffer(i, IID_PPV_ARGS(&s_DisplayPlanes[i]));
		s_Device->CreateRenderTargetView(s_DisplayPlanes[i].Get(), nullptr, s_RtvDescriptorHeap.GetHandle(i));
	}
}

void GraphicsCore::Present(Scene* scene)
{
	s_CommandListManager.Reset();
	s_CommandListManager.SetRS();

	UINT buffer_idx = s_SwapChain->GetCurrentBackBufferIndex();

	D3D12_RESOURCE_BARRIER resource_barrier{ };
	resource_barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	resource_barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	resource_barrier.Transition.pResource = s_DisplayPlanes[buffer_idx].Get();
	resource_barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
	resource_barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_RENDER_TARGET;
	resource_barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
	s_CommandListManager.ResourceBarrier(resource_barrier);


	// rtd와 dsd는 OM에 세팅된다.
	D3D12_CPU_DESCRIPTOR_HANDLE rtd = s_RtvDescriptorHeap.GetHandle(buffer_idx);
	D3D12_CPU_DESCRIPTOR_HANDLE dsd = s_DsvDescriptorHeap.GetHandle();
	s_CommandListManager.SetOM(rtd, dsd);

	// 루트 시그니처 Set
	ID3D12GraphicsCommandList* cl = s_CommandListManager.GetCommandList();
	cl->SetGraphicsRootSignature(s_GraphicsRS.Get());

	cl->SetDescriptorHeaps(1, s_BindingHeap.GetAddressOf());

	// 카메라 세팅
	scene->m_Camera.UpdateShaderVariables(s_CommandListManager.GetCommandList());

	// Render
	for (auto& [pso_type, objects] : scene->m_Objects) {
		cl->SetPipelineState(s_GraphicsPSOs[pso_type]->Get());

		for (auto& obj : objects)
		{
			if (obj->m_Visible == false) continue;

			obj->UpdateConstantBuffer(cl);

			auto texture = obj->GetTexture();
			if (nullptr != texture) {
				cl->SetGraphicsRootDescriptorTable(1, texture->GetGPUDescriptorHandle());
			}

			auto mesh = obj->GetMesh();
			if (nullptr != mesh) {
				cl->IASetPrimitiveTopology(mesh->GetPrimitiveTopology());
				cl->IASetVertexBuffers(0, 1, &mesh->m_VertexBufferView);
				cl->IASetIndexBuffer(&mesh->m_IndexBufferView);
				cl->DrawIndexedInstanced(mesh->m_IndexCount, 1, 0, 0, 0);
			}
		}
	}

	resource_barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET;
	resource_barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PRESENT;
	s_CommandListManager.ResourceBarrier(resource_barrier);

	s_CommandListManager.ExcuteCommandList();
	s_SwapChain->Present(0, 0);

#ifdef _DEBUG
	auto c_pos = scene->m_Camera.GetPosition();

	std::wstring pos_info;
	pos_info.append(TEXT("x: "));
	pos_info.append(std::to_wstring(c_pos.x));
	pos_info.append(TEXT(", y: "));
	pos_info.append(std::to_wstring(c_pos.y));
	pos_info.append(TEXT(", z: "));
	pos_info.append(std::to_wstring(c_pos.z));
	::SetWindowText(WindowApp::GetHwnd(), pos_info.c_str());
#endif
}



ID3D12GraphicsCommandList* Graphics::GraphicsCore::GetCommandList()
{
	return s_CommandListManager.GetCommandList();
}

void Graphics::GraphicsCore::ExcuteCommandList()
{
	s_CommandListManager.ExcuteCommandList();
}

void GraphicsCore::CreateDeviceForHardware()
{
	Microsoft::WRL::ComPtr<IDXGIAdapter1> adapter;

	for (int i = 0; DXGI_ERROR_NOT_FOUND != s_Factory->EnumAdapters1(i, &adapter); ++i)
	{
		DXGI_ADAPTER_DESC1 adapter_desc;
		adapter->GetDesc1(&adapter_desc);

		if (adapter_desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) continue;

		HRESULT rst = D3D12CreateDevice(
			adapter.Get(),
			D3D_FEATURE_LEVEL_12_0,
			IID_PPV_ARGS(&s_Device)
		);

		if (SUCCEEDED(rst)) break;
	}

	if (!adapter)
	{
		s_Factory->EnumWarpAdapter(IID_PPV_ARGS(&adapter));
		D3D12CreateDevice(adapter.Get(), D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&s_Device));
	}
}

void Graphics::GraphicsCore::BuildPSO()
{
	s_GraphicsPSOs.emplace(PSOType::SKYBOX, new GraphicsPSO(PSOType::SKYBOX));
	s_GraphicsPSOs.emplace(PSOType::TERRAIN, new GraphicsPSO(PSOType::TERRAIN));
	s_GraphicsPSOs.emplace(PSOType::STATIC_OBJECT, new GraphicsPSO(PSOType::STATIC_OBJECT));
	s_GraphicsPSOs.emplace(PSOType::CHARACTER, new GraphicsPSO(PSOType::CHARACTER));
	s_GraphicsPSOs.emplace(PSOType::BODY, new GraphicsPSO(PSOType::BODY));
	s_GraphicsPSOs.emplace(PSOType::SPRITE, new GraphicsPSO(PSOType::SPRITE));

	for (auto& [type, pso] : s_GraphicsPSOs) {
		pso->Create(s_Device.Get(), s_GraphicsRS.Get());
	}
}
