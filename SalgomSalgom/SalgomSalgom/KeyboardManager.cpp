#include "pch.h"
#include "KeyboardManager.h"

void PrintDebug(KeyboardEvent input)
{
	string outstr;

	switch (input) {
	case KeyboardEvent::KEY_UP_W:
		outstr = "KEY_UP_W";
		break;
	case KeyboardEvent::KEY_DOWN_W:
		outstr = "KEY_DOWN_W";
		break;
	case KeyboardEvent::KEY_UP_A:
		outstr = "KEY_UP_A";
		break;
	case KeyboardEvent::KEY_DOWN_A:
		outstr = "KEY_DOWN_A";
		break;
	case KeyboardEvent::KEY_UP_S:
		outstr = "KEY_UP_S";
		break;
	case KeyboardEvent::KEY_DOWN_S:
		outstr = "KEY_DOWN_S";
		break;
	case KeyboardEvent::KEY_UP_D:
		outstr = "KEY_UP_D";
		break;
	case KeyboardEvent::KEY_DOWN_D:
		outstr = "KEY_DOWN_D";
		break;
	case KeyboardEvent::KEY_DOWN_Q:
		outstr = "KEY_DOWN_Q";
		break;
	case KeyboardEvent::KEY_UP_Q:
		outstr = "KEY_UP_Q";
		break;
	default:
		break;
	};

	yhl::dout << outstr << yhl::fls;
}

// VK 코드와 내가 정의한 이벤트 매핑.
std::unordered_map<VKCODE, KeyboardEvent> g_GetKeyDownEvent;
std::unordered_map<VKCODE, KeyboardEvent> g_GetKeyUpEvent;

std::queue<KeyboardEvent> KeyboardManager::m_EventQueue;
bool KeyboardManager::m_KeyState[KeyboardManager::m_KeyCount] = { false };

void KeyboardManager::Initialize()
{
	// 여기서 맵을 초기화 해준다
	g_GetKeyDownEvent[0x41] = KeyboardEvent::KEY_DOWN_A;
}

void KeyboardManager::PushWindowKeyDownEvent(VKCODE vkcode)
{
	switch (vkcode) {
	case 0x41: /*A*/
		m_EventQueue.push(KeyboardEvent::KEY_DOWN_A);
		break;
	case 0x44: /*D*/
		m_EventQueue.push(KeyboardEvent::KEY_DOWN_D);
		break;
	case 0x53: /*S*/
		m_EventQueue.push(KeyboardEvent::KEY_DOWN_S);
		break;
	case 0x57: /*W*/
		m_EventQueue.push(KeyboardEvent::KEY_DOWN_W);
		break;
	case 0x51: /*Q*/
		m_EventQueue.push(KeyboardEvent::KEY_DOWN_Q);
		break;
	case VK_SPACE:
		m_EventQueue.push(KeyboardEvent::KEY_DOWN_SPACE);
		break;
	}
}

void KeyboardManager::PushWindowKeyUpEvent(VKCODE vkcode)
{
	switch (vkcode) {
	case 0x41: /*A*/
		m_EventQueue.push(KeyboardEvent::KEY_UP_A);
		break;
	case 0x44: /*D*/
		m_EventQueue.push(KeyboardEvent::KEY_UP_D);
		break;
	case 0x53: /*S*/
		m_EventQueue.push(KeyboardEvent::KEY_UP_S);
		break;
	case 0x57: /*W*/
		m_EventQueue.push(KeyboardEvent::KEY_UP_W);
		break;
	case 0x51: /*Q*/
		m_EventQueue.push(KeyboardEvent::KEY_UP_Q);
		break;
	case VK_SPACE:
		m_EventQueue.push(KeyboardEvent::KEY_UP_SPACE);
		break;
	}
}

KeyboardEvent KeyboardManager::GetEvent()
{
	if (m_EventQueue.empty())
		return KeyboardEvent::NONE;

	auto rtv = m_EventQueue.front();
	m_EventQueue.pop();
	return rtv;
}

// 윈도우 큐 버리기
void KeyboardManager::Update()
{
	for (int i = 0; i < m_KeyCount; ++i) {
		short rtv = ::GetAsyncKeyState(m_Code[i]);
		bool key_state = rtv & 0x8000;		// msb 추출, 눌렸을 때 true

		// 이전 프레임의 키 상태와 비교
		if (m_KeyState[i] != key_state) {
			KeyboardEvent input = static_cast<KeyboardEvent>(2 * i + static_cast<int>(key_state));
			m_EventQueue.push(input);

			yhl::dout << "key" << yhl::fls;
		}

		m_KeyState[i] = key_state;
	}
}
