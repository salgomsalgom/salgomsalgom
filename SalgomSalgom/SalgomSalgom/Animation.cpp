#include "pch.h"
#include "Animation.h"
#include "D2b.h"

using namespace DirectX;

void BoneAnimation::Interpolate(float t, DirectX::XMFLOAT4X4& M) const
{
	// 여기로 들어오는 시간은 모두 애니메이션 내의 시간임
	for (UINT i = 0; i < m_KeyFrames.size() - 1; ++i)
	{
		if (t >= m_KeyFrames[i].time && t <= m_KeyFrames[i + 1].time)
		{
			float lerpPercent = (t - m_KeyFrames[i].time) / (m_KeyFrames[i + 1].time - m_KeyFrames[i].time);

			XMVECTOR s0 = XMLoadFloat3(&m_KeyFrames[i].scale);
			XMVECTOR s1 = XMLoadFloat3(&m_KeyFrames[i + 1].scale);

			XMVECTOR p0 = XMLoadFloat3(&m_KeyFrames[i].translation);
			XMVECTOR p1 = XMLoadFloat3(&m_KeyFrames[i + 1].translation);

			XMVECTOR q0 = XMLoadFloat4(&m_KeyFrames[i].rotation);
			XMVECTOR q1 = XMLoadFloat4(&m_KeyFrames[i + 1].rotation);

			XMVECTOR S = XMVectorLerp(s0, s1, lerpPercent);
			XMVECTOR P = XMVectorLerp(p0, p1, lerpPercent);
			XMVECTOR Q = XMQuaternionSlerp(q0, q1, lerpPercent);

			XMVECTOR zero = XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);
			XMStoreFloat4x4(&M, XMMatrixAffineTransformation(S, zero, Q, P));

			break;
		}
	}
}

void AnimationClip::Interpolate(float t, std::vector<DirectX::XMFLOAT4X4>& boneTransforms) const
{
	t = fmodf(t, m_TimeLength);

	for (auto& [idx, bone_anim] : m_BoneAnimations) {
		bone_anim.Interpolate(t, boneTransforms[idx]);
	}
}


Animation::Animation(D2bBuf& buf)
	: m_PlayCount(0)
	, m_PlayTime(0.f)
	, m_PlayOnce(false)
{
	bones.resize(buf.header.b_count);
	memcpy(bones.data(), buf.b_buf, sizeof(Bone) * buf.header.b_count);

	// 행렬 뒤집기;;;;;;
	for (auto& b : bones) {
		auto t_mat = XMMatrixTranspose(XMLoadFloat4x4(&b.childToParent));
		XMStoreFloat4x4(&b.childToParent, t_mat);

		auto offset = XMMatrixTranspose(XMLoadFloat4x4(&b.offsetMat));
		XMStoreFloat4x4(&b.offsetMat, offset);
	}

	int buf_index = 0;
	for (int i = 0; i < buf.header.ani_count; ++i)
	{
		AnimationClip clip;

		clip.m_ClipName = buf.ani_buf[i].name;
		clip.m_NumKeyframes = buf.ani_buf[i].num_keys;

		for (int j = 0; j < bones.size(); ++j)
		{
			BoneAnimation bone_ani;
			bone_ani.m_KeyFrames.resize(clip.m_NumKeyframes);
			memcpy(bone_ani.m_KeyFrames.data(), &buf.key_buf[buf_index], sizeof(Keyframe) * clip.m_NumKeyframes);
			buf_index += clip.m_NumKeyframes;

			clip.m_BoneAnimations.emplace(j, bone_ani);
		}

		clip.m_TimeLength = clip.m_BoneAnimations[0].m_KeyFrames.back().time;

		animationClips.emplace(buf.ani_buf[i].name, clip);
	}

	m_PrevClip = m_CurrClip = animationClips.begin()->first;
}

void Animation::GetFinalTransforms(float timePos, DirectX::XMFLOAT4X4* finalTransforms)
{
	m_PlayTime += timePos;
	m_PlayCount = static_cast<unsigned int>(m_PlayTime / animationClips[m_CurrClip].m_TimeLength);

	if (m_PlayOnce && m_PlayCount > 0) {
		m_PlayOnce = false;
		string prev = m_PrevClip;
		SetAnimation(prev);
	}

	UINT numBones = bones.size();

	std::vector<XMFLOAT4X4> toParentTransforms(numBones);
	std::vector<XMFLOAT4X4> toRootTransforms(numBones);

	animationClips[m_CurrClip].Interpolate(m_PlayTime, toParentTransforms);

	XMMATRIX localTrans = XMLoadFloat4x4(&toParentTransforms[0]);
	XMMATRIX childToParent = XMLoadFloat4x4(&(bones[0].childToParent));

	XMMATRIX root = XMMatrixMultiply(localTrans, childToParent);
	XMStoreFloat4x4(&toRootTransforms[0], root);

	for (UINT i = 1; i < numBones; ++i)
	{
		int parentIndex = bones[i].parentIndex;

		XMMATRIX localTrans = XMLoadFloat4x4(&toParentTransforms[i]);
		XMMATRIX parentToChild = XMLoadFloat4x4(&bones[i].childToParent);
		XMMATRIX parentToRoot = XMLoadFloat4x4(&toRootTransforms[parentIndex]);

		XMMATRIX toRoot = XMMatrixMultiply(XMMatrixMultiply(localTrans, parentToChild), parentToRoot);
		XMStoreFloat4x4(&toRootTransforms[i], toRoot);
	}

	for (UINT i = 0; i < numBones; ++i)
	{
		XMMATRIX offset = XMLoadFloat4x4(&bones[i].offsetMat);
		XMMATRIX toRoot = XMLoadFloat4x4(&toRootTransforms[i]);
		XMMATRIX finalTransform = XMMatrixMultiply(offset, toRoot);
		XMStoreFloat4x4(&finalTransforms[i], XMMatrixTranspose(finalTransform));
	}
}

void Animation::SetAnimation(const string& name)
{
	if (name == m_CurrClip) return;
	if (m_PlayOnce) {
		m_PrevClip = name;
		return;
	}
	m_PrevClip = m_CurrClip;
	m_CurrClip = name;
	m_PlayCount = 0;
	m_PlayTime = 0.f;
}


void Animation::PlayOnce(const string& name)
{
	SetAnimation(name);
	m_PlayOnce = true;
}
