#pragma once

typedef unsigned short UNIT;

constexpr UNIT MAP_SIZE = 128;
constexpr UNIT HEIGHTMAP_SIZE = MAP_SIZE + 1;	// VERTEX ����

constexpr float HEIGHTMAP_Y_FACTOR = 0.069f;

class HeightMap {
public:
	HeightMap(const std::string& filename);
	~HeightMap() = default;

public:
	float GetHeight(int row, int column);
	DirectX::XMFLOAT3 GetNormalFactor(int row, int column);
	bool Valid() { return m_Valid; }

private:
	bool m_Valid;
	std::vector<BYTE> m_Heights;		// 0~255�� ��

public:
	const unsigned int row_length;
	const unsigned int col_length;
};
