#include "pch.h"
#include "GraphicsSystem.h"

#include "Scene.h"

bool GraphicsSystem::s_Instantiated = false;

GraphicsSystem::GraphicsSystem(SystemCommunicator* const communicator) :
	SystemNode(communicator)
{
	assert(!s_Instantiated);
	s_Instantiated = true;
}

GraphicsSystem::~GraphicsSystem()
{
}

void GraphicsSystem::Initialize()
{
	GraphicsCore::Initialize();

	yhl::dout << "Initialized Graphics System." << yhl::fls;
}

void GraphicsSystem::Update(Scene* scene)
{
	GraphicsCore::Present(scene);
}
