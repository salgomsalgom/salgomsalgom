#include "pch.h"
#include "Sound.h"

#pragma comment(lib, "FMOD/lib/fmod64_vc.lib")

CSound g_Sound;


CSound::CSound()
{
	FMOD::System_Create(&pSystem);
	pSystem->init(
		SOUND_NUM
		, FMOD_INIT_NORMAL
		, nullptr
	);
	for (auto& p : pSound) p = nullptr;
}


CSound::~CSound()
{
	for (int i = 0; i < SOUND_NUM; ++i)
	{
		pSound[i]->release();
	}

	pSystem->release();
	pSystem->close();
}

void CSound::Add_sound()
{
	pSystem->createStream(
		"./Assets/Sound/LOGIN_BGM.mp3"
		, FMOD_DEFAULT | FMOD_LOOP_NORMAL
		, nullptr
		, &pSound[LOGIN_SOUND]
	); // �κ�

	pSystem->createStream(
		"./Assets/Sound/LOBBY_BGM.mp3"
		, FMOD_DEFAULT | FMOD_LOOP_NORMAL
		, nullptr
		, &pSound[LOBBY_SOUND]
	); // �κ�


	pSystem->createStream(
		"./Assets/Sound/INGAME_BGM.mp3"
		, FMOD_DEFAULT | FMOD_LOOP_NORMAL
		, nullptr
		, &pSound[BATTLE_SOUND]
	); // ��Ʋ�� 

	pSystem->createStream(
		"./Assets/Sound/NORMAL_WIN.mp3"
		, FMOD_DEFAULT | FMOD_LOOP_NORMAL
		, nullptr
		, &pSound[NORMAL_WIN_SOUND]
	); // ��ְ����� �¸�

	pSystem->createStream(
		"./Assets/Sound/WITCH_WIN.mp3"
		, FMOD_DEFAULT | FMOD_LOOP_NORMAL
		, nullptr
		, &pSound[WITCH_WIN_SOUND]
	); //��������� �¸�

	//ȿ����

	pSystem->createStream(
		"./Assets/Sound/ATTACK.mp3"
		, FMOD_DEFAULT | FMOD_LOOP_OFF
		, nullptr
		, &pSound[ATTACK_SOUND]
	); //����


	pSystem->createStream(
		"./Assets/Sound/WALKING.mp3"
		, FMOD_DEFAULT | FMOD_LOOP_OFF
		, nullptr
		, &pSound[WALKING_SOUND]
	); 

	pSystem->createStream(
		"./Assets/Sound/RESPAWN.mp3"
		, FMOD_DEFAULT | FMOD_LOOP_OFF
		, nullptr
		, &pSound[RESPAWN_SOUND]
	);

	pSystem->createStream(
		"./Assets/Sound/SHIELD.mp3"
		, FMOD_DEFAULT | FMOD_LOOP_OFF
		, nullptr
		, &pSound[SHIELD_SOUND]
	);

	pSystem->createStream(
		"./Assets/Sound/MAGICSTICK.mp3"
		, FMOD_DEFAULT | FMOD_LOOP_OFF
		, nullptr
		, &pSound[MAGICSTICK_SOUND]
	);

	//pSystem->createStream(
	//	"./Assets/Sound/WITCHSTONE.mp3"
	//	, FMOD_DEFAULT | FMOD_LOOP_OFF
	//	, nullptr
	//	, &pSound[WITCHSTONE_SOUND]
	//); //�ȱ�




}

void CSound::Play(int n)
{
	pSystem->update();

	pSystem->playSound(pSound[n]
		, nullptr, false, &pChannel[n]);

}
