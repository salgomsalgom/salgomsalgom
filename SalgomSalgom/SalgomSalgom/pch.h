#pragma once

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers.
#endif

#include <Windows.h>
#include <wrl.h>
#include <tchar.h>

// DirectX
#include <dxgi1_6.h>
#include <d3d12.h>
#include <d3dcompiler.h>

#include <DirectXMath.h>
#include <DirectXPackedVector.h>

#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d12.lib")
#pragma comment(lib, "d3dcompiler.lib")

// C++ libraries
#include <array>
#include <vector>
#include <map>
#include <unordered_map>
#include <queue>
#include <string>

#include <algorithm>
#include <numeric>
#include <functional>

#include <cassert>

#include <thread>
#include <atomic>

#include <fstream>
#include <iostream>

// Utility
#include "Util/DebugOut.h"
#include "Math.h"

// Network
#include <WS2tcpip.h>
#include <MSWSock.h>
#pragma comment (lib, "WS2_32.lib")
#pragma comment (lib, "mswsock.lib")
