﻿#include "pch.h"
#include "Game.h"
#include "Scene.h"
#include "LoginScene.h"
#include "LobbyScene.h"
#include "ResultScene.h"

#include "TimerSystem.h"
#include "InputSystem.h"
#include "NetworkSystem.h"
#include "GraphicsSystem.h"
#include "Sound.h"

using namespace GameCore;

SystemCommunicator Game::s_Systems{ };

NetworkSystem		g_NetworkSystem{ &Game::s_Systems };
GraphicsSystem		g_GraphicsSystem{ &Game::s_Systems };
InputSystem			g_InputSystem{ &Game::s_Systems };
TimerSystem			g_TimerSystem{ &Game::s_Systems };

queue<SystemMessage> g_SystemMessageQueue;
mutex g_SystemMessageMutex;

void GameCore::PushSystemMessage(SystemMessage msg)
{
	g_SystemMessageMutex.lock();
	g_SystemMessageQueue.push(msg);
	g_SystemMessageMutex.unlock();
}

void PopSystemMessage()
{
	g_SystemMessageMutex.lock();
	g_SystemMessageQueue.pop();
	g_SystemMessageMutex.unlock();
}


void GameCore::Game::ChangeScene(SceneKey key)
{
	if (key == m_CurrScene->m_Key) return;
	for (auto scene : m_Scenes) {
		if (scene->m_Key == key) {
			m_CurrScene->Destroy();
			scene->Initialize();
			m_CurrScene = scene;
			break;
		}
	}
}

void GameCore::Initialize(Game& game)
{
	 game.s_Systems.Initialize();

	 game.StartUp();
}

// ������ ����
void GameCore::Update(Game& game)
{
	g_TimerSystem.Tick();

	float frame_time = g_TimerSystem.GetElapsedTime();

	g_InputSystem.Update();

	g_NetworkSystem.Update();	// Scene Write

	// queue check
	if (!g_SystemMessageQueue.empty())
	{
		auto msg = g_SystemMessageQueue.front();
		PopSystemMessage();

		switch (msg) {
		case SystemMessage::JOIN_IN_SUCCESS: {
			auto cur_s = dynamic_cast<LoginScene*>(game.GetCurrScene());
			if (cur_s != nullptr) {
				cur_s->HideRegister();
			}
			break;
		}
		case SystemMessage::BATTLE_START:
			game.ChangeScene(SceneKey::BATTLE);
			break;
		case SystemMessage::LOBBY_START:
			game.ChangeScene(SceneKey::LOBBY);
			break;
		case SystemMessage::NORMAL_WIN:
			game.ChangeScene(SceneKey::NORMAL_WIN);
			break;
		case SystemMessage::WITCH_WIN:
			game.ChangeScene(SceneKey::WITCH_WIN);
			break;
		default:
			break;
		}
	}

	game.Update(frame_time);

	game.HandleInput();

	g_GraphicsSystem.Update(game.GetCurrScene());	// Scene Read

}

void GameCore::Terminate(Game& game)
{
	game.CleanUp();
}

#include "TestScene.h"

void TestGame::StartUp()
{
	g_Sound.Add_sound();

	//m_Scenes.push_back(new TestScene());
	m_Scenes.push_back(new LoginScene(SceneKey::LOGIN));
	m_Scenes.push_back(new LobbyScene(SceneKey::LOBBY));
	m_Scenes.push_back(new BattleScene(SceneKey::BATTLE));
	m_Scenes.push_back(new NormalWinScene(SceneKey::NORMAL_WIN));
	m_Scenes.push_back(new WitchWinScene(SceneKey::WITCH_WIN));

	m_CurrScene = m_Scenes[0];
	m_CurrScene->Initialize();
}

void TestGame::CleanUp() {

}

void TestGame::Update(float frame_time) {
	m_CurrScene->Update(frame_time);
}

void TestGame::HandleInput()
{
	m_CurrScene->HandleInput(g_InputSystem);
}
