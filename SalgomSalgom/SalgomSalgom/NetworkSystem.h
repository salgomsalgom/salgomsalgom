#pragma once
#include "SystemNode.h"
#include "../Packet.h"

class SystemCommunicator;
class InputSystem;
namespace GameCore {
	class Game;
}

class NetworkSystem : SystemNode
{
public:
	explicit NetworkSystem(SystemCommunicator* const communicator);
	~NetworkSystem();

public:
	void Initialize() override;
	void Update();

public:
	static void RecvPacket();

	/* -- lobby -- */
	static void Send_L_Login(const char* name, const char* pw);
	static void Send_L_Join(const char* name, const char* pw);
	
	static void SendInputPacket(PlayerInput input);
	static void SendMousePacket(float rotate_x, float rotate_y);
	static void SendRedayPacket(bool ready);
	static void SendDataRequestPacket();
	static void SendAttackPacket();
	static void SendGemPickUp();
	static void SendGemThrow();
	static void SendRenderRequest();
	static void SendUseSkill2();

private:
	static bool s_Instantiated;
};
