#pragma once

#include "Math.h"

#define SERVER_PORT 9000
#define LOBBY_PORT	3500

enum ENUMOP { OP_RECV, OP_SEND, OP_ACCEPT, OP_UPDATE };
enum C_STATUS { ST_FREE, ST_ALLOC, ST_ACTIVE_LOBBY, ST_ACTIVE_SEARCH, ST_ACTIVE_GAME, ST_ACTIVE_RESULT,};
enum P_STATUS { ST_DEAD, ST_ALIVE, ST_CAPTURED, ST_NON_HIT, ST_DASH, ST_FALL, ST_PICKUP, ST_ATTACK};
enum A_STATUS {
	A_ST_IDLE, A_ST_MOVE_LEFT, A_ST_MOVE_RIGHT, A_ST_MOVE_FORWAD, A_ST_MOVE_BACKWARD, //공유
	A_ST_DEAD, A_ST_GEM_THROW, A_ST_GEM_PICK, A_ST_DASH, A_ST_FAKE_DEATH,						// 노말 곰돌이
	A_ST_ATTACK, A_ST_GEM_INVISIBLE, A_ST_HALT_1, A_ST_HALT_2					//마녀 곰돌이
};
// 애니메션 스테이터스 따로 만들고 보내기 
enum CH_ROLE { R_NORMAL, R_WITCH };
enum WITCH_STONE_STAUS { WST_ALIVE, WST_DESTROIED };
enum class MagicColor{ R, B, P, W };

enum LOGIN{ L_WRONG_ID, L_WRONG_PW, L_NON_EXIST_ID, L_SUCCESS };
enum JOIN { J_WRONG_ID, J_WRONG_PW, J_ID_OVERLAPPED, J_SUCCESS };

// 스킬 구현 관련
enum class SkillStatus
{
	READY, MOVE, ARRIVE,
};

#define WORLD_WIDTH		128
#define WORLD_HEIGHT	128


using namespace Math;

constexpr auto MAX_ID_LEN = 50;
constexpr auto MAX_PW_LEN = 50;
constexpr auto MAX_PACKET_SIZE = 255;
constexpr auto MAX_BUFFER_SIZE = 1024;
constexpr auto WORLD_BUFFER = 10'0000;
constexpr auto MAX_USER = 5;
constexpr auto MAX_GEM_NUMBER = 16;
constexpr auto VIEW_RADIUS = 32;
constexpr int MAX_OB_LEN = 10;
constexpr int GAME_START_NUMBER = 3;
constexpr int   SHARED_LIFE = 2;

// 스킬 쿨타임
constexpr float WITCH_GEMTRANS_COOLTIME		= 15.f;		// 1
constexpr float WITCH_HALT_COOLTIME			= 20.f;		// 2
constexpr float NORMAL_DASH_COOLTIME		= 5.f;		// 1
constexpr float NORMAL_FAKEDIE_COOLTIME		= 30.f;		// 2



//------------- C2S ---------------//
enum C2S {
	C2S_L_LOGIN,
	C2S_L_JOININ,
	C2S_LOGIN,
	
	C2S_INPUT,
	C2S_READY,
	C2S_RESULT,
	C2S_MOUSE_MOVE,
	C2S_DATA_REQUEST,
	C2S_GEM_PICKUP,
	C2S_GEM_THROW,
	C2S_SKILL2,
	C2S_WITCH_ATTACK,
	C2S_RENDER_START_REQUEST,
};

//------------- S2C ---------------//
enum S2C {
	S2C_L_LOGIN_OK,
	S2C_L_LOGIN_FAIL,
	S2C_L_JOIN_OK,
	S2C_L_JOIN_FAIL,

	S2C_LOGIN_OK,
	S2C_LEAVE,
	S2C_RESULT,
	S2C_UPDATE,
	S2C_MAP_DATA,
	S2C_START,
	S2C_RENDERSTART,
	S2C_SOULCHANGE,
	S2C_WITCHSTONE_ST_CHANGE,
	S2C_ANIMATION_CHANGE,
	S2C_CHARACTER_POWER_CHANGE,
};

enum PlayerInput {
	/*KEY DOWN*/
	MOVE_FOWARD,
	MOVE_BACKWARD,
	MOVE_LEFT,
	MOVE_RIGHT,
	RUN,
	JUMP,
	KD_SKILL1,
	KD_SKILL2,
	KD_ATTACK,

	/* KEY UP*/
	STOP_MOVE_FORWARD,
	STOP_MOVE_BACKWARD,
	STOP_MOVE_LEFT,
	STOP_MOVE_RIGHT,
	KU_RUN,
	KU_JUMP,
	KU_SKILL1,
	KU_SKILL2,
	KU_ATTACK,

	K_NONE,
};


#pragma pack (push,1)
struct OBJECTINFO {
	char name[MAX_OB_LEN];
	FloatVector3 m_position;
	FloatVector3 m_rotation;
	FloatVector3 m_scale;
	FloatVector3 m_cbox;
};

struct PacketGem {
	MagicColor m_color;
	FloatVector3 m_location;
	FloatVector3 m_rot;
	bool transparency;
	bool picked;
	int	 player_id;
};

struct PacketWitchStone {
	MagicColor m_color;
	char m_hp;
	bool m_alive;
	bool m_shield;
	FloatVector3 m_location;
};

struct playerInfo {
	char m_id;
	FloatVector3 m_location;
	FloatVector3 m_dirVector;
	FloatVector3 m_rotation;
	CH_ROLE m_role;
	bool	gem_picked;
	float	m_respawn_time;
};

struct skillinfo {
	bool m_exist;
	FloatVector3 m_location;
	SkillStatus m_Stat;
	float coolTime;
	float duration;
};


//---------sc------------//


//---------Lobby SC--------//
struct SC_L_PacketJoinFail {
	unsigned int size;
	unsigned short type;
	JOIN reason;
};

struct SC_L_PacketJoinOK {
	unsigned int size;
	unsigned short type;

};


struct SC_L_PacketLoginOk {
	unsigned int size;
	unsigned short type;
	char name[MAX_ID_LEN];
	int id;
};

struct SC_L_PacketLoginFail {
	unsigned int size;
	unsigned short type;
	int id;
	LOGIN reason;
};

struct SC_L_GameStart {
	unsigned int size;
	unsigned short type;
};

//-------battle--------//

struct SC_PacketLoginOk {
	unsigned int size;
	unsigned short type;
	int id;
};

struct SC_PacketLoginFail {
	unsigned int size;
	unsigned short type;
	int id;
};

struct SC_PacketStart {
	unsigned int size;
	unsigned short type;
	int number_of_objects;
	OBJECTINFO objects[1000];
};

struct SC_PacketRenderStart {
	unsigned int size;
	unsigned short type;
	playerInfo  players[5];
	PacketGem gems[16];
	PacketWitchStone witchStoneState[4];
	char number_of_player;
	char soul_left;
	A_STATUS astatus[5];
};

struct SC_PacketUpdate {
	unsigned int size;
	unsigned short type;
	playerInfo players[5];
	PacketGem gems[16];
	skillinfo skill_1[5];
	skillinfo skill_2[5];
};

struct SC_JoinFail {
	unsigned int size;
	unsigned short type;
};

struct SC_JoinSuccess {
	unsigned int size;
	unsigned short type;
};

struct SC_PacketResult {
	unsigned int size;
	unsigned short type;
	CH_ROLE win;

};

struct SC_PacketLeave {
	unsigned int size;
	unsigned short type;
	int id;
};

struct SC_PakcetWitchStone_ST_Change {
	unsigned int size;
	unsigned short type;
	PacketWitchStone witchStoneState[4];
};

struct SC_PacketSoulChagne {
	unsigned int size;
	unsigned short type;
	char soul_left;
};

struct SC_PacketAnimationChange {
	unsigned int size;
	unsigned short type;
	A_STATUS animation_status;
	int id;
};


struct SC_Packet_CH_PowerChange {
	unsigned int size;
	unsigned short type;
	int skill_power;
	int speed_power;
};



//-----------cs-----------//
struct CS_PacketLogin {
	unsigned int size;
	unsigned short type;
	char name[MAX_ID_LEN];
};


struct CS_PacketInput {
	unsigned int size;
	unsigned short type;
	PlayerInput input;
};

struct CS_PacketDataRequest {
	unsigned int size;
	unsigned short type;
};

struct CS_PacketRenderRequest {
	unsigned int size;
	unsigned short type;

};

struct CS_PacketSkill2 {
	unsigned int size;
	unsigned short type;
};

struct CS_PacketReady {
	unsigned int size;
	unsigned short type;
	bool is_ready;
};

struct CS_PacketMouse {
	unsigned int size;
	unsigned short type;
	FloatVector2 delta;
};

struct CS_PacketPickGem {
	unsigned int size;
	unsigned short type;
};

struct CS_PacketThrowGem {
	unsigned int size;
	unsigned short type;

};

struct CS_PacketAttack {
	unsigned int size;
	unsigned short type;
};

//---------Lobby CS------//

struct CS_L_PacketLogin {
	unsigned int size;
	unsigned short type;
	char name[MAX_ID_LEN];
	char password[MAX_PW_LEN];

};

struct CS_L_PacketJoin {
	unsigned int size;
	unsigned short type;
	char name[MAX_ID_LEN];
	char password[MAX_PW_LEN];
};

#pragma pack (pop)
