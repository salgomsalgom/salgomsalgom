#pragma once
#include <cmath>

namespace Math {

	class FloatVector2
	{
	public:
		FloatVector2() : x(0.f), y(0.f) { }
		FloatVector2(float x, float y) : x(x), y(y) { }
		
	public:
		bool operator==(const FloatVector2& rhs)
		{
			return (x == rhs.x) && (y == rhs.y);
		}
		
	public:
		float x;
		float y;
	};

	class FloatVector3
	{
	public:
		FloatVector3() : x(0.f), y(0.f), z(0.f) { }
		FloatVector3(float x, float y, float z) : x(x), y(y), z(z) { }

	public:
		void Normalize() {
			if (x == 0 && y == 0 && z == 0) return;
			float dst = sqrt(x * x + y * y + z * z);
			x /= dst;
			y /= dst;
			z /= dst;
		}

		FloatVector3 CrossProduct(const FloatVector3& v) const {
			return FloatVector3(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x);
		}

		FloatVector3 CrossProduct(const FloatVector3&& v) const {
			return FloatVector3(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x);
		}

		bool operator==(const FloatVector3& rhs)
		{
			return (x == rhs.x) && (y == rhs.y) && (z == rhs.z);
		}

		FloatVector3 operator*(float scalar) const {
			return FloatVector3(x * scalar, y * scalar, z * scalar);
		}

		FloatVector3 operator+(const FloatVector3& rhs) const {
			return FloatVector3(x + rhs.x, y + rhs.y, z + rhs.z);
		}

		FloatVector3 operator=(const FloatVector3& rhs) {
			x = rhs.x;
			y = rhs.y;
			z = rhs.z;
			return (*this);
		}

	public:
		float x;
		float y;
		float z;
	};

	class FloatVector4
	{
	public:
		float x;
		float y;
		float z;
		float w;
	};
}