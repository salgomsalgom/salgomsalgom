#include "pch.h"
#include "InputSystem.h"

bool InputSystem::s_Instantiated = false;
std::queue<KEYBOARDINFO>  InputSystem:: m_Keyboard;
std::queue<MOUSEINFO> InputSystem::m_Mouse;
std::queue<int> InputSystem::m_pickup;

mutex InputSystem::m_MouseLock;
mutex InputSystem::m_KeyboardLock;


mutex pickup_lock;
int store_pickup_id{6};


InputSystem::InputSystem(SystemCommunicator* const communicator) :
	SystemNode(communicator)
{
	assert(!s_Instantiated);
	s_Instantiated = true;
}

void InputSystem::Initialize()
{
	std::cout << "Initialized Input System." << std::endl;
}

void InputSystem::PickupUpdate()
{
	if (!m_pickup.empty()) {
		pickup_lock.lock();
		store_pickup_id = m_pickup.front();
		m_pickup.pop();
		pickup_lock.unlock();
	}
}

void InputSystem::Update()
{


}


void InputSystem::AddComponent(KEYBOARDINFO p_Info)
{
	m_KeyboardLock.lock();
	m_Keyboard.emplace(p_Info);
	m_KeyboardLock.unlock();
}

void InputSystem::AddComponent(float rad_x, float rad_y, int id) 
{
	m_MouseLock.lock();
	MOUSEINFO mouse_rad;
	mouse_rad.m_id = id;
	mouse_rad.m_rad.x = rad_x;
	mouse_rad.m_rad.y = rad_y;
	m_Mouse.emplace(mouse_rad);
	m_MouseLock.unlock();
}

void InputSystem::AddComponent(int id)
{
	pickup_lock.lock();
	m_pickup.emplace(id);
	pickup_lock.unlock();
}


bool InputSystem::GetMouseMessage(MOUSEINFO& mouseInfo)
{
	m_MouseLock.lock();
	if (m_Mouse.empty()) {
		m_MouseLock.unlock();
		return false;
	}
	mouseInfo = m_Mouse.front();
	m_Mouse.pop();
	m_MouseLock.unlock();
	return true;
}

bool InputSystem::GetKeyboardMessaage(KEYBOARDINFO& keyInfo)
{
	if (m_Keyboard.empty()) return false;
	m_KeyboardLock.lock();
	keyInfo = m_Keyboard.front();
	m_Keyboard.pop();
	m_KeyboardLock.unlock();
	return true;
}

bool InputSystem::GetPickupMessaage()
{
	if (!m_pickup.empty()) {
		return true;
	}
	return false;
}
