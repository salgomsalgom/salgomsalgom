#pragma once
#include "SystemNode.h"

enum class SystemMassage {
	NONE,
};

// 어차피 시스템은 다 전역이야!!!
class SystemCommunicator 
{
public:
	explicit SystemCommunicator() { }
	~SystemCommunicator() { }

public:
	void Initialize();
	void RegisterSystem(SystemNode* node);
	void RemoveSystem(SystemNode* node);
	void SendSystemMessage(const SystemMassage message);

private:
	std::list<SystemNode*>		m_SystemList;
	std::queue<SystemMassage>	m_MessageQueue;
};
