#include "pch.h"
#include "HeightMap.h"

// p1,p2를 d1:d2로 분할하는 p를 리턴한다. (단, d1+d2=1)
// float lerp(float p1, float p2, float d1) {
// 	return (1 - d1) * p1 + d1 * p2;
// }

HeightMap::HeightMap(const std::string& filename)
	: m_Valid(false), m_Heights(), row_length(HEIGHTMAP_SIZE), col_length(HEIGHTMAP_SIZE)
{
	std::ifstream in{ filename, std::ios_base::binary };
	assert(in);

	int file_size = row_length * col_length;
	m_Heights.resize(file_size);
	in.read(reinterpret_cast<char*>(m_Heights.data()), file_size);
	in.close();

	m_Valid = true;
}

float HeightMap::GetHeight(int row, int column)
{
	return static_cast<float>(m_Heights[row * col_length + column]);
}

float HeightMap::GetLerpHeight(float x, float z)
{
	int floor_x = floor(x);
	int floor_z = floor(z);

	int a = GetHeight(floor_x,		floor_z);
	int b = GetHeight(floor_x + 1,	floor_z);
	int c = GetHeight(floor_x,		floor_z + 1);
	int d = GetHeight(floor_x + 1,	floor_z + 1);

	float t = x - floor_x;
	float r = z - floor_z;

	float lerp1 = lerp(a, b, t);
	float lerp2 = lerp(c, d, t);
	return lerp(lerp1, lerp2, r);
}

DirectX::XMFLOAT3 HeightMap::GetNormalFactor(int row, int column)
{
	DirectX::XMFLOAT3 normal{ 0, 1, 0 };	// delta x, delta z 값에 2를 나누는 대신 y값에 2를 곱함

	int delta_row = col_length;
	int delta_col = 1;

	int idx = row * delta_row + column * delta_col;
	
	// 모서리, 변
	if (0 == row || 0 == column || (row_length - 1) == row || (col_length - 1) == column)
		return normal;

	// 내부
	normal.x = (m_Heights[idx - delta_row] - m_Heights[idx + delta_row]) / 2;
	normal.z = (m_Heights[idx - delta_col] - m_Heights[idx + delta_col]) / 2;
	
	return normal;
}
