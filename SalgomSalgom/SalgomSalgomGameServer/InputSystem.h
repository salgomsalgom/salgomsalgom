#pragma once
#include "SystemNode.h"
struct KEYBOARDINFO;
struct MOUSEINFO;

class SystemCommunicator;

class InputSystem : SystemNode
{
public:
	explicit InputSystem(SystemCommunicator* const communicator);
	virtual ~InputSystem() { }

public:
	void Initialize() override;
	void Update();

	void PickupUpdate();

	void AddComponent(KEYBOARDINFO);
	void AddComponent(float, float, int);
	void AddComponent(int);
	
	bool GetMouseMessage(MOUSEINFO& mouseInfo);
	bool GetKeyboardMessaage(KEYBOARDINFO& keyInfo);
	bool GetPickupMessaage();

private:
	static bool s_Instantiated;

	static mutex m_MouseLock;
	static mutex m_KeyboardLock;

	static std::queue<KEYBOARDINFO> m_Keyboard;
	static std::queue<MOUSEINFO> m_Mouse;
	static std::queue<int> m_pickup;
};
