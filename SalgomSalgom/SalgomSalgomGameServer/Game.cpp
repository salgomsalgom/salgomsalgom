#include "pch.h"
#include "Game.h"
#include "TimerSystem.h"
#include "InputSystem.h"
#include "NetworkSystem.h"

using namespace GameCore;
using namespace DirectX;
using namespace Math;
SystemCommunicator Game::s_Systems{ };

TimerSystem			g_TimerSystem{ &Game::s_Systems };
InputSystem			g_InputSystem{ &Game::s_Systems };
NetworkSystem		g_NetworkSystem{ &Game::s_Systems };

#include "HeightMap.h"

HeightMap g_HeightMap{ "./Assets/TERN.hm" };

void GameCore::Initialize(Game& game)
{
	 game.s_Systems.Initialize();

	 game.StartUp();
}

void GameCore::Update(Game& game)
{
	g_TimerSystem.Tick();

	float timer = g_TimerSystem.GetElapsedTime();

	//std::cout << "Base Time: " << g_TimerSystem.GetBaseTime() 
	//	<< ", Elapsed Time: " << g_TimerSystem.GetElapsedTime() << std::endl;

	auto test_game = dynamic_cast<TestGame*>(&game);
	test_game->Update(&g_InputSystem, timer);
}

void GameCore::Terminate(Game& game)
{
	game.CleanUp();
}

#include "SKill.h"


extern int store_pickup_id;
extern CLIENT g_Clients[MAX_USER + 1];
extern OBSTACLE g_Obstacle[1000];
extern FloatVector3 g_player_respwan_location[MAX_USER];
extern mutex g_SkillLock;
extern mutex pickup_lock;
extern Gem g_Gems[MAX_GEM_NUMBER];
extern FloatVector3 g_gems_origin_location[MAX_GEM_NUMBER];
extern WitchStone g_WitchStones[4];
extern THROW_GEM g_throw_gem[5];
extern ATTACK g_attack;
extern char g_number_of_witch_stone;
extern char g_number_of_regen;
extern Skill g_Skill_1_Flag[MAX_USER];
extern Skill g_Skill_2_Flag[MAX_USER];
extern Halt g_WitchSkill2;
extern int g_witch_id;
extern float before_x[5];
extern float before_y[5];
extern float before_z[5];
extern mutex soul_lock;

constexpr float WITCH_SPEED = 4.8;
constexpr float WITCH_SPEED_SIDE = 3.6;
constexpr float WITCH_SPEED_BACK = 2.4;
constexpr float NORMAL_SPEED = 4;
constexpr float NORMAL_SPEED_SIDE = 3;
constexpr float NORMAL_SPEED_BACK = 2;

collide_info player_next_location[5];

constexpr float STATUS_CHAGE_MAX_PERCENTAGE = 0.5; //  50% 
constexpr float STATUS_INCREASE_PERCENTAGE = 0.05; //  5% 
constexpr float REVIVE_COOLTIME = 5;

struct DistCalculateItems {
	float Distance[3];
	float C[3][3]; // matrix C = A^T B c_{i,j} = Dot(A_i,B_j)
	float absC[3][3]; // C의 절댓값
	float ADotDistance[3]; //A dot D

	DistCalculateItems(FloatVector3 _values){
		Distance[0] = _values.x;
		Distance[1] = _values.y;
		Distance[2] = _values.z;
	};
};

int BoxBoxColliedCheck(collide_info&, OBSTACLE&);
int BoxBoxColliedCheck(collide_info&, collide_info&);

float DotProduct(const float v0[3], const float v1[3])
{//=v0.v1
	return v0[0] * v1[0] + v0[1] * v1[1] + v0[2] * v1[2];
}

bool CalculateDistance_A(collide_info &obj1, collide_info& obj2, DistCalculateItems _dcitems){

	float R0, R1, R;
	float R01; // R0+ R1;
	for (int i = 0; i < 3; ++i) {
		_dcitems.ADotDistance[i] = DotProduct(obj1.m_axis[i], _dcitems.Distance);
		R = (float)fabsf(_dcitems.ADotDistance[i]);
		R1 = obj2.m_extent[0] * _dcitems.absC[i][0] + obj2.m_extent[1] *
			_dcitems.absC[i][1] + obj2.m_extent[2] * _dcitems.absC[i][2];
		R01 = obj1.m_extent[i] + R1;
		if (R > R01) return true;
	}
	return false;
}

bool CalculateDistance_A(collide_info& client, OBSTACLE& object, DistCalculateItems _dcitems) {

	float R0, R1, R;
	float R01; // R0+ R1;
	for (int i = 0; i < 3; ++i) {
		_dcitems.ADotDistance[i] = DotProduct(client.m_axis[i], _dcitems.Distance);
		R = (float)fabsf(_dcitems.ADotDistance[i]);
		R1 = object.m_extent[0] * _dcitems.absC[i][0] + object.m_extent[1] *
			_dcitems.absC[i][1] + object.m_extent[2] * _dcitems.absC[i][2];
		R01 = client.m_extent[i] + R1;
		if (R > R01) return true;
	}
	return false;
}

bool CalculateDistance_B(collide_info& obj1, collide_info& obj2, DistCalculateItems _dcitems) {
	float R0, R;
	float R01; // R0+ R1;
	for (int i = 0; i < 3; ++i) {
		R = (float)fabsf(DotProduct(obj2.m_axis[i], _dcitems.Distance));
		R0 = obj1.m_extent[0] * _dcitems.absC[0][i] + obj1.m_extent[1] *
			_dcitems.absC[1][i] + obj1.m_extent[2] * _dcitems.absC[2][i];
		R01 = R0 + obj2.m_extent[i];
		if (R > R01) return true;
	}
	return false;
}

bool CalculateDistance_B(collide_info &client, OBSTACLE &object, DistCalculateItems _dcitems) {
	float R0, R;
	float R01; // R0+ R1;
	for (int i = 0; i < 3; ++i) {
		R = (float)fabsf(DotProduct(object.m_axis[i], _dcitems.Distance));
		R0 = client.m_extent[0] * _dcitems.absC[0][i] + client.m_extent[1] *
			_dcitems.absC[1][i] + client.m_extent[2] * _dcitems.absC[2][i];
		R01 = R0 + object.m_extent[i];
		if (R > R01) return true;
	}
	return false;
}

bool CalculateDistance_AB(collide_info& obj1, collide_info& obj2, DistCalculateItems _dcitems) {
	//이거 경우 개많음..
	float R0 = 0.0f, R = 0.0f, R1 = 0.0f; // 거리 계산 값들
	float R01 = 0.0f; // R0+ R1;

	for (int i = 0; i < 3; ++i) {

		for (int j = 0; j < 3; ++j) {


			if (i == 0) { //ao
				R = (float)fabsf(_dcitems.ADotDistance[2] * _dcitems.C[1][j] - _dcitems.ADotDistance[1] * _dcitems.C[2][j]);
				R0 = obj1.m_extent[1] * _dcitems.absC[2][j] + obj1.m_extent[2] * _dcitems.absC[1][j];
			}

			if (i == 1) { //a1
				R = (float)fabsf(_dcitems.ADotDistance[0] * _dcitems.C[2][j] - _dcitems.ADotDistance[2] * _dcitems.C[0][j]);
				R0 = obj1.m_extent[0] * _dcitems.absC[2][j] + obj1.m_extent[2] * _dcitems.absC[0][j];
			}

			if (i == 2) {
				R = (float)fabsf(_dcitems.ADotDistance[1] * _dcitems.C[0][j] - _dcitems.ADotDistance[0] * _dcitems.C[1][j]);
				R0 = obj1.m_extent[0] * _dcitems.absC[1][j] + obj1.m_extent[1] * _dcitems.absC[0][j];
			}


			if (j == 0) {
				R1 = obj2.m_extent[1] * _dcitems.absC[i][2] + obj2.m_extent[2] * _dcitems.absC[i][1];
			}

			if (j == 1) {
				R1 = obj2.m_extent[0] * _dcitems.absC[i][2] + obj2.m_extent[2] * _dcitems.absC[i][0];
			}

			if (j == 2) {
				R1 = obj2.m_extent[0] * _dcitems.absC[i][1] + obj2.m_extent[1] * _dcitems.absC[i][0];
			}

			R01 = R0 + R1;
			if (R > R01) return false;

		}

	}

	return true;
}

bool CalculateDistance_AB(collide_info& client, OBSTACLE& object, DistCalculateItems _dcitems) {
	//이거 경우 개많음..
	float R0 = 0.0f, R = 0.0f, R1 = 0.0f; // 거리 계산 값들
	float R01 = 0.0f; // R0+ R1;

	for (int i = 0; i < 3; ++i) {

		for (int j = 0; j < 3; ++j) {


			if (i == 0) { //ao
				R = (float)fabsf(_dcitems.ADotDistance[2] * _dcitems.C[1][j] - _dcitems.ADotDistance[1] * _dcitems.C[2][j]);
				R0 = client.m_extent[1] * _dcitems.absC[2][j] + client.m_extent[2] * _dcitems.absC[1][j];
			}

			if (i == 1) { //a1
				R = (float)fabsf(_dcitems.ADotDistance[0] * _dcitems.C[2][j] - _dcitems.ADotDistance[2] * _dcitems.C[0][j]);
				R0 = client.m_extent[0] * _dcitems.absC[2][j] + client.m_extent[2] * _dcitems.absC[0][j];
			}

			if (i == 2) {
				R = (float)fabsf(_dcitems.ADotDistance[1] * _dcitems.C[0][j] - _dcitems.ADotDistance[0] * _dcitems.C[1][j]);
				R0 = client.m_extent[0] * _dcitems.absC[1][j] + client.m_extent[1] * _dcitems.absC[0][j];
			}


			if (j == 0) {
				R1 = object.m_extent[1] * _dcitems.absC[i][2] + object.m_extent[2] * _dcitems.absC[i][1];
			}

			if (j == 1) {
				R1 = object.m_extent[0] * _dcitems.absC[i][2] + object.m_extent[2] * _dcitems.absC[i][0];
			}

			if (j == 2) {
				R1 = object.m_extent[0] * _dcitems.absC[i][1] + object.m_extent[1] * _dcitems.absC[i][0];
			}

			R01 = R0 + R1;
			if (R > R01) return false;

		}


	}

	return true;
}

int BoxBoxColliedCheck(collide_info& object1, collide_info& object2)
{
	FloatVector3 temp_dist_vec = FloatVector3(object1.m_location.x - object2.m_location.x,
		object1.m_location.y - object2.m_location.y, object1.m_location.z - object2.m_location.z);
	DistCalculateItems DCvalues(temp_dist_vec);

	float R0, R1, R; // 거리 계산 값들
	float R01; // R0+ R1;

	for (int i = 0; i < 3; ++i) {

		for (int j = 0; j < 3; ++j) {

			DCvalues.C[i][j] = DotProduct(object1.m_axis[i], object2.m_axis[j]);
			DCvalues.absC[i][j] = (float)fabsf(DCvalues.C[i][j]);

		}

	}

	for (int i = 0; i < 3; ++i) {

		if (CalculateDistance_A(object1, object2, DCvalues)) {
			return 0;
		}

		if (CalculateDistance_B(object1, object2, DCvalues)) {
			return 0;
		}

		if (CalculateDistance_AB(object1, object2, DCvalues)) {
			return 0;
		}

		else {
			return 1;
		}
	}

}

int BoxBoxColliedCheck(collide_info& client, OBSTACLE& object)
{
	FloatVector3 temp_dist_vec = FloatVector3(client.m_location.x - object.m_location.x,
		client.m_location.y - object.m_location.y, client.m_location.z - object.m_location.z);
	DistCalculateItems DCvalues(temp_dist_vec);

	float R0, R1, R; // 거리 계산 값들
	float R01; // R0+ R1;

	for (int i = 0; i < 3; ++i) {

		for (int j = 0; j < 3; ++j) {

			DCvalues.C[i][j] = DotProduct(client.m_axis[i], object.m_axis[j]);
			DCvalues.absC[i][j] = (float)fabsf(DCvalues.C[i][j]);

		}

	}

	for (int i = 0; i < 3; ++i) {

		if (CalculateDistance_A(client, object, DCvalues)) {
			return 0;
		}

		if (CalculateDistance_B(client, object, DCvalues)) {
			return 0;
		}

		if (CalculateDistance_AB(client, object, DCvalues)) {
			return 0;
		}

		else {
			return 1;
		}
	}

}

float DistanceComehereNoramlBear(XMFLOAT3 come_here, FloatVector3 normal_bear)
{
	float result;
	result = sqrt( pow((come_here.x - normal_bear.x), 2) + pow((come_here.y - normal_bear.y), 2) + pow((come_here.z - normal_bear.z), 2));
	return result;
}

FloatVector3 DirVectorComehereNormalBear(XMFLOAT3 come_here, FloatVector3 normal_bear)
{
	
	FloatVector3 result{ (come_here.x - normal_bear.x), (come_here.y - normal_bear.y), (come_here.z - normal_bear.z) };

	return result;
}

inline XMFLOAT3 CrossProduct(const XMFLOAT3& xmf3Vector1, const XMFLOAT3& xmf3Vector2, bool bNormalize = true)
{
	XMFLOAT3 xmf3Result;
	if (bNormalize)
		XMStoreFloat3(&xmf3Result, XMVector3Normalize(XMVector3Cross(XMLoadFloat3(&xmf3Vector1), XMLoadFloat3(&xmf3Vector2))));
	else
		XMStoreFloat3(&xmf3Result, XMVector3Cross(XMLoadFloat3(&xmf3Vector1), XMLoadFloat3(&xmf3Vector2)));
	return(xmf3Result);
}

void useSkill_2(int id, float timer);


void useSkill_1(int id, float timer)
{
	if (g_Clients[id].m_role == R_NORMAL) { // 노말 곰돌이 대쉬
		g_Clients[id].m_cl.lock();
		if(g_Clients[id].m_pstatus == ST_ALIVE)
			g_Skill_1_Flag[id].dir = g_Clients[id].m_DirVector;
		g_Clients[id].m_pstatus = ST_DASH;
		g_Clients[id].m_astatus = A_ST_DASH;
		g_Skill_1_Flag[id].cool_time = NORMAL_DASH_COOLTIME;
		g_Skill_1_Flag[id].duration_time -= timer;
		float dash_velocicy = (3 * g_Clients[id].m_speed) * g_Clients[id].m_velocity_accel;
		auto rot = g_Skill_1_Flag[id].dir;

		g_Clients[id].m_location.x = g_Clients[id].m_location.x + (rot.x) * (dash_velocicy * timer);
		g_Clients[id].m_location.z = g_Clients[id].m_location.z + (rot.z) * (dash_velocicy * timer);
		g_Clients[id].m_location.y = (g_HeightMap.GetLerpHeight(g_Clients[id].m_location.x +64, g_Clients[id].m_location.z+64) - 129) * 0.1;

		player_next_location[id].m_location.x = g_Clients[id].m_location.x + (rot.x) * (g_Clients[id].m_speed * timer );
		player_next_location[id].m_location.z = g_Clients[id].m_location.z + (rot.z) * (g_Clients[id].m_speed * timer );


		if (g_Skill_1_Flag[id].duration_time <= 0) g_Clients[id].m_pstatus = ST_ALIVE;

		g_Clients[id].m_cl.unlock();
	}
	else {
		g_Clients[id].m_cl.lock();
		//마녀 곰돌이 잼 투명화
		//가까이 있는 잼이 있으면 스킬 발동 아니면 발동 x
		int check{0};
		collide_info client_box;
		client_box.m_location = g_Clients[id].m_location;
		client_box.m_extent[0] = g_Clients[id].m_extent[0];
		client_box.m_extent[1] = g_Clients[id].m_extent[1];
		client_box.m_extent[2] = g_Clients[id].m_extent[2];
		for (auto& gems : g_Gems) {
			bool transparency = BoxBoxColliedCheck(client_box, gems.m_cbox);
			if (transparency) {
				gems.m_lock.lock();
				if (!gems.transparency) {
					gems.transparency = true;
					gems.transparency_time = 10;

				}
				gems.m_lock.unlock();
				g_Skill_1_Flag[id].cool_time = WITCH_GEMTRANS_COOLTIME;
				g_Clients[id].m_astatus = A_ST_GEM_INVISIBLE;
			}
			if (gems.transparency_time <= 0) {
				gems.m_lock.lock();
				gems.transparency = false;
				gems.transparency_time = 0;
				gems.m_lock.unlock();
			}
			else {
				gems.m_lock.lock();
				gems.transparency_time -= timer;
				gems.m_lock.unlock();
				check++;
			}
			if (check >= 1) {
				g_Skill_1_Flag[id].duration_time = 1;
			}
			else {
				g_Skill_1_Flag[id].duration_time = 0;
			}
		}
		g_Clients[id].m_cl.unlock();
	}
}

// 기본 플레이어의 속도, 게임 데이터로 따로 정리해놓기
constexpr float FORWARD_MOVE_WEIGHT = 1.f;
constexpr float RIGHT_MOVE_WEIGHT = 1.f;

void TestGame::Update(InputSystem* input, float timer)
{
	// 마우스용 업데이트 키보드용 업데이트
	MOUSEINFO mouse_info;
	while (input->GetMouseMessage(mouse_info)) {
		// 방향벡터 계산
		Math::FloatVector3 dir_v{ cos(mouse_info.m_rad.x), sin(-mouse_info.m_rad.y), sin(mouse_info.m_rad.x) };
		dir_v.Normalize();

		g_Clients[mouse_info.m_id].m_cl.lock();
		g_Clients[mouse_info.m_id].m_DirVector = dir_v;
		g_Clients[mouse_info.m_id].m_cl.unlock();
	}

	KEYBOARDINFO key_info;
	while (input->GetKeyboardMessaage(key_info)) {
		switch (key_info.m_input) {
		case MOVE_FOWARD:
		case STOP_MOVE_BACKWARD:
			g_Clients[key_info.m_id].m_ForwardSpeed += FORWARD_MOVE_WEIGHT;

			break;
		case MOVE_BACKWARD:
		case STOP_MOVE_FORWARD:
			g_Clients[key_info.m_id].m_ForwardSpeed -= FORWARD_MOVE_WEIGHT;

			break;
		case MOVE_RIGHT:
		case STOP_MOVE_LEFT:
			g_Clients[key_info.m_id].m_RightSpeed += RIGHT_MOVE_WEIGHT;

			break;
		case MOVE_LEFT:
		case STOP_MOVE_RIGHT:
			g_Clients[key_info.m_id].m_RightSpeed -= RIGHT_MOVE_WEIGHT;

			break;
		case KD_SKILL1: {// 노말은 달리기 마녀는 잼 투명화
			//쿨타임 이 아닐때만 true
			if (g_Skill_1_Flag[key_info.m_id].cool_time == 0) {
				if (g_Clients[key_info.m_id].m_role == R_NORMAL)
					g_Skill_1_Flag[key_info.m_id].duration_time = 0.5;
				useSkill_1(key_info.m_id, timer);
			}
			break;
		}
		case KU_SKILL1: {
			//g_Skill1Flag[store_keyboard.m_id].flag = false;
			break;
		}
		default: break;
		}
	}



	// 잼중에 충돌된거 있으면 집기
	bool pickup_empty = input->GetPickupMessaage();
	while (pickup_empty) {
		pickup_lock.lock();

		collide_info client_box;
		client_box.m_location = g_Clients[store_pickup_id].m_location;
		client_box.m_extent[0] = g_Clients[store_pickup_id].m_extent[0];
		client_box.m_extent[1] = g_Clients[store_pickup_id].m_extent[1];
		client_box.m_extent[2] = g_Clients[store_pickup_id].m_extent[2];
		for (auto& gems : g_Gems) {
			bool pickup = BoxBoxColliedCheck(client_box, gems.m_cbox);
			if (pickup && !g_Clients[store_pickup_id].picked_up && !gems.transparency) {
				g_Clients[store_pickup_id].picked_up = true;
				g_Clients[store_pickup_id].m_pstatus = ST_PICKUP;
				gems.picked = true;
				gems.picked_id = store_pickup_id;
				g_throw_gem[store_pickup_id].gem_id = gems.m_id;
				gems.m_cbox.m_location = g_Clients[store_pickup_id].m_location;

			}

		}
		pickup_lock.unlock();
		pickup_empty = input->GetPickupMessaage();
		input->PickupUpdate();
	}


	// 다음 프레임 타임의 위치랑 충돌을 하면 못가게 한다.

	for (int i = 0; i < MAX_USER; ++i) {
		g_Clients[i].m_cl.lock();
		before_x[i] = g_Clients[i].m_location.x;
		before_y[i] = g_Clients[i].m_location.y;
		before_z[i] = g_Clients[i].m_location.z;
		g_Clients[i].m_cl.unlock();
	}

	// 플레이어 업데이트
	for (int i = 0; i < MAX_USER; ++i)
	{
		g_Clients[i].m_AliveTime += timer;

		// 능력치 업데이트
		if (g_Clients[i].m_role == R_NORMAL && g_Clients[i].m_AliveTime > 120.f) {
			// 더하는 방식이 아니라 단계별로 수치를 정해놓고 할당하는 방식이 더 나음
			g_Clients[i].m_velocity_accel += STATUS_INCREASE_PERCENTAGE;
			g_Clients[i].m_skill_accel += STATUS_INCREASE_PERCENTAGE;
			g_Clients[i].m_char_power += STATUS_INCREASE_PERCENTAGE;
			clamp(g_Clients[i].m_velocity_accel, 0.f, 1.5f);
			clamp(g_Clients[i].m_skill_accel, 0.f, 1.5f);
			g_Clients[i].m_AliveTime = 0.f;
		}
		else if (g_Clients[i].m_role == R_WITCH) {
			// 영혼 수확했을 때
			g_Clients[i].m_velocity_accel = 1 + (SHARED_LIFE - g_number_of_regen) * STATUS_INCREASE_PERCENTAGE;
			g_Clients[i].m_skill_accel = 1 + (SHARED_LIFE - g_number_of_regen) * STATUS_INCREASE_PERCENTAGE;
			g_Clients[i].m_char_power = (SHARED_LIFE - g_number_of_regen) * STATUS_INCREASE_PERCENTAGE;
		}

		// 데이터 가져오기
		g_Clients[i].m_cl.lock();
		auto curr_loc = g_Clients[i].m_location;
		auto v_dir = g_Clients[i].m_DirVector;
		P_STATUS p_st = g_Clients[i].m_pstatus;
		A_STATUS a_st = g_Clients[i].m_astatus;
		float forward_speed = g_Clients[i].m_ForwardSpeed;
		float right_speed = g_Clients[i].m_RightSpeed;
		g_Clients[i].m_cl.unlock();

		// 애니메이션 상태들 보내기
		if (g_Clients[i].m_pstatus == ST_DEAD) {
			a_st = A_ST_DEAD;
		}
		else if (g_Clients[i].m_pstatus == ST_NON_HIT) {
			a_st = A_ST_FAKE_DEATH;
		}
		else if (forward_speed > 0) {
			a_st = A_ST_MOVE_FORWAD;
		}
		else if (forward_speed < 0) {
			a_st = A_ST_MOVE_BACKWARD;
		}
		else if (right_speed > 0) {
			a_st = A_ST_MOVE_RIGHT;
		}
		else if (right_speed < 0) {
			a_st = A_ST_MOVE_LEFT;
		}
		else if (forward_speed == 0 && right_speed == 0) {
			a_st = A_ST_IDLE;
		}

		if (g_Clients[i].m_pstatus == ST_ATTACK) {
			a_st = A_ST_ATTACK;
			g_Clients[i].m_pstatus = ST_ALIVE;
		}

		if (g_Clients[i].m_pstatus == ST_PICKUP) {
			a_st = A_ST_GEM_PICK;
			g_Clients[i].m_pstatus = ST_ALIVE;
		}

		g_Clients[i].m_cl.lock();
		g_Clients[i].m_astatus = a_st;
		g_Clients[i].m_cl.unlock();

		switch (p_st) {
		case ST_ALIVE:
		{

			// right 벡터 구하기
			auto xm_v_right = CrossProduct(XMFLOAT3(0, 1, 0), XMFLOAT3(v_dir.x, v_dir.y, v_dir.z));
			FloatVector3 v_right{ xm_v_right.x, xm_v_right.y, xm_v_right.z };
			v_right.Normalize();

			// 이동방향과 위치 구하기
			auto v_move = (v_dir * forward_speed) + (v_right * right_speed);
			v_move.Normalize();
			auto new_loc = curr_loc + (v_move * g_Clients[i].m_speed * g_Clients[i].m_velocity_accel * timer);
			player_next_location[i].m_location = curr_loc + (v_move * g_Clients[i].m_speed * timer * 2);

			// 지형 높이 구하기
			float height_x = new_loc.x + 64;
			float height_z = new_loc.z + 64;
			float h = (g_HeightMap.GetLerpHeight(height_x, height_z) - 129) * HEIGHTMAP_Y_FACTOR;

			// 새로운 위치 저장

			g_Clients[i].m_cl.lock();
			g_Clients[i].m_location = FloatVector3(new_loc.x, h, new_loc.z);
			g_Clients[i].m_cl.unlock();


			break;
		}

		case ST_CAPTURED:
		{
			// 공의 위치까지 이동후 떨어지게
			// 거리 계산하고 1초 만에 이동으로 보간
			XMFLOAT3 come_here{};
			come_here = g_WitchSkill2.GetPos();
			float D[MAX_USER];
			D[i] = DistanceComehereNoramlBear(come_here, curr_loc);
			FloatVector3 captured_dir = DirVectorComehereNormalBear(come_here, curr_loc);
			captured_dir.Normalize();
			// 거리에 따라 이동

			static float V[MAX_USER]{};
			if (V[i] == 0)
				V[i] = D[i] / HALT_PULLTIME; // 1초에 D만큼 이동

			float stop_distance{ 1 };
			if (D[i] < stop_distance) {
				g_Clients[i].m_cl.lock();
				g_Clients[i].m_pstatus = ST_FALL;
				g_Clients[i].m_cl.unlock();
				V[i] = 0;
			}
			else {
				auto new_loc = curr_loc + (captured_dir * timer * V[i]);
				// 충돌처리하고 막히면 fall로 변경
				for (auto& obj : g_Obstacle) {
					int collide = BoxBoxColliedCheck(player_next_location[i], obj);
					if (collide) {
						new_loc.x = before_x[i];
						new_loc.y = before_y[i];
						new_loc.z = before_z[i];
						g_Clients[i].m_pstatus = ST_FALL;
					}
				}
				g_Clients[i].m_cl.lock();
				g_Clients[i].m_location = FloatVector3(new_loc.x, new_loc.y, new_loc.z);
				g_Clients[i].m_cl.unlock();
				player_next_location[i].m_location = new_loc + (captured_dir * timer * V[i]);

			}
			break;
		}
		case ST_FALL:
		{
			g_Clients[i].m_cl.lock();
			auto curr_loc = g_Clients[i].m_location;
			g_Clients[i].m_cl.unlock();

			// 중력을 받게 // 떨어지는 동안 이동 불가
			float gravity{ 10 };
			float height_x = curr_loc.x + 64;
			float height_z = curr_loc.z + 64;
			float h = (g_HeightMap.GetLerpHeight(height_x, height_z) - 129) * 0.1;
			auto new_loc = curr_loc.y - (10 * timer);

			g_Clients[i].m_cl.lock();
			if (new_loc <= h) {
				new_loc = h;
				g_Clients[i].m_pstatus = ST_ALIVE;
			}
			g_Clients[i].m_location.y = new_loc;
			g_Clients[i].m_cl.unlock();
			break;
		}
		}
	}

	// skill 부분
	for (int i = 0; i < 5; ++i) {
		if (g_Skill_1_Flag[i].cool_time > 0 && g_Skill_1_Flag[i].duration_time > 0)
			useSkill_1(i, timer);

		if (g_Skill_2_Flag[i].duration_time > 0)
			useSkill_2(i, timer);

		// cooltime 계산
		g_SkillLock.lock();
		if (g_Skill_1_Flag[i].cool_time > 0) {
			float cool_time = g_Skill_1_Flag[i].cool_time;
			cool_time = clamp(cool_time - timer, 0.f, cool_time);
			g_Skill_1_Flag[i].cool_time = cool_time;
		}

		if (g_Skill_2_Flag[i].cool_time > 0) {
			float cool_time = g_Skill_2_Flag[i].cool_time;
			cool_time = clamp(cool_time - timer, 0.f, cool_time);
			g_Skill_2_Flag[i].cool_time = cool_time;
		}
		g_SkillLock.unlock();

	}
	// class skill update
	bool active_pull = false;
	g_WitchSkill2.Update(timer, active_pull);
	switch (g_WitchSkill2.m_Status) {
	case SkillStatus::MOVE:
		g_Clients[g_witch_id].m_astatus = A_ST_HALT_1;
		break;
	case SkillStatus::ARRIVE:
		g_Clients[g_witch_id].m_astatus = A_ST_IDLE;
		break;
	}

	if (active_pull) {
		vector<CLIENT*> normal_bears;
		for (int i = 0; i < GAME_START_NUMBER; ++i) {
			if (R_WITCH == g_Clients[i].m_role) continue;
			normal_bears.push_back(&(g_Clients[i]));
		}
		g_WitchSkill2.InitArrive(normal_bears);
		g_Clients[g_witch_id].m_astatus = A_ST_HALT_2;
	}

	//// 플레이어와 물체 충돌처리 부분
	for (int i = 0; i < GAME_START_NUMBER; ++i) {
		g_Clients[i].m_cl.lock();
		float x = 0;
		float y = g_Clients[i].m_rot.y;
		float z = 0;
		player_next_location[i].m_axis[0][0] = cos(y);		//cos(y) * cos(z);
		player_next_location[i].m_axis[0][1] = 0;			//-cos(x) * sin(z) + sin(x)* sin(y) * cos(z);
		player_next_location[i].m_axis[0][2] = sin(y);		//sin(x) * sin(z) + cos(x) * sin(y) * cos(z);
		player_next_location[i].m_axis[1][0] = 0;			//sin(z) * cos(y);
		player_next_location[i].m_axis[1][1] = 1;			//sin(z) * sin(y) * sin(x) + cos(z) * cos(x);
		player_next_location[i].m_axis[1][2] = 0;			//sin(z) * sin(y) * cos(x) - cos(z) * sin(x);
		player_next_location[i].m_axis[2][0] = -sin(y);		//-sin(y);
		player_next_location[i].m_axis[2][1] = 0;			//cos(y) * sin(x);
		player_next_location[i].m_axis[2][2] = cos(y);		//cos(y) * cos(x);
		g_Clients[i].m_cl.unlock();

		for (auto& objs : g_Obstacle) {
			g_Clients[i].m_cl.lock();
			int collide = BoxBoxColliedCheck(player_next_location[i], objs);
			if (collide == 1) {
				g_Clients[i].m_location = FloatVector3(before_x[i], before_y[i], before_z[i]);
			}
			g_Clients[i].m_cl.unlock();
		}

		for (auto& players : g_Clients) {
			g_Clients[i].m_cl.lock();
			if (g_Clients[i].m_id == players.m_id) {
				g_Clients[i].m_cl.unlock();
				continue;
			}
			collide_info other_bear;
			copy(&other_bear.m_axis[0][0], &other_bear.m_axis[2][2], &players.m_axis[0][0]);
			other_bear.m_location = players.m_location;
			other_bear.m_extent[0] = players.m_extent[0];
			other_bear.m_extent[1] = players.m_extent[1];
			other_bear.m_extent[2] = players.m_extent[2];
			int collide = BoxBoxColliedCheck(player_next_location[i], other_bear);
			if (collide == 1) {
				g_Clients[i].m_location = FloatVector3(before_x[i], before_y[i], before_z[i]);
			}
			g_Clients[i].m_cl.unlock();
		}

		if (player_next_location[i].m_location.x > 63.5 || player_next_location[i].m_location.x < -63.5) {
			g_Clients[i].m_cl.lock();
			g_Clients[i].m_location.x = before_x[i];
			g_Clients[i].m_cl.unlock();

		}
		if (player_next_location[i].m_location.z > 63.5 || player_next_location[i].m_location.z < -63.5) {
			g_Clients[i].m_cl.lock();
			g_Clients[i].m_location.z = before_z[i];
			g_Clients[i].m_cl.unlock();
		}



	}


	// 잼중에 아이디가 같은 게 있으면 같이 이동

	for (auto& gems : g_Gems) {
		if (gems.picked == true && g_Clients[gems.picked_id].m_pstatus != ST_DEAD) {
			g_Clients[gems.picked_id].m_cl.lock();
			auto xm_v_right = CrossProduct(XMFLOAT3(0, 1, 0), XMFLOAT3(g_Clients[gems.picked_id].m_DirVector.x, g_Clients[gems.picked_id].m_DirVector.y, g_Clients[gems.picked_id].m_DirVector.z));
			FloatVector3 v_right{ xm_v_right.x, xm_v_right.y, xm_v_right.z };
			v_right.Normalize();
			FloatVector3 left{ v_right.x / 8,v_right.y / 2,v_right.z / 8 };
			FloatVector3 gem_offset{ (g_Clients[gems.picked_id].m_DirVector.x / 3) - left.x ,(g_Clients[gems.picked_id].m_DirVector.y / 3) + 1.2f , (g_Clients[gems.picked_id].m_DirVector.z / 3) - left.z };
			FloatVector3 temp_client = g_Clients[gems.picked_id].m_location;
			FloatVector3 tmep_client_rot = g_Clients[gems.picked_id].m_rot;
			g_Clients[gems.picked_id].m_cl.unlock();

			gems.m_lock.lock();
			gems.m_rotation = tmep_client_rot;
			gems.m_cbox.m_location = temp_client + gem_offset;
			gems.m_lock.unlock();
		}
		else if (g_Clients[gems.picked_id].m_pstatus == ST_DEAD) {
			gems.m_lock.lock();
			gems.m_cbox.m_location = g_gems_origin_location[gems.m_id];
			gems.m_lock.unlock();
		}
	}

	// 던지면 주움 상태해제하고 잼의 회전 값 저장
	for (auto& gem_thorwn : g_throw_gem) {
		if (gem_thorwn.p_id == MAX_USER + 1) { continue; }
		if (gem_thorwn.m_throw && g_Clients[gem_thorwn.p_id].picked_up && g_Clients[gem_thorwn.p_id].m_pstatus == ST_ALIVE) {
			g_Gems[gem_thorwn.gem_id].m_lock.lock();
			g_Gems[gem_thorwn.gem_id].picked = false;
			g_Gems[gem_thorwn.gem_id].thrown = true;
			g_Gems[gem_thorwn.gem_id].m_rotation = g_Clients[gem_thorwn.p_id].m_DirVector;
			g_Gems[gem_thorwn.gem_id].picked_id = MAX_USER + 1;
			g_Clients[gem_thorwn.p_id].picked_up = false;
			gem_thorwn.m_throw = false;
			g_Clients[gem_thorwn.p_id].m_astatus = A_ST_GEM_THROW;
			gem_thorwn.p_id = MAX_USER + 1;
			g_Gems[gem_thorwn.gem_id].m_lock.unlock();
		}
	}


	// 여기서 이제 던진 잼의 좌표 변경 만약 높이 값이 0보다 작아지면 원위치 시키기
	// 포물선으로 변경
	float v_throw = 20.0f;
	float gravity = 2.f;

	for (auto& gems : g_Gems) {
		if (gems.thrown) {
			gems.m_lock.lock();
			gems.fall_timer = gems.fall_timer + timer;
			gems.m_cbox.m_location.x = gems.m_cbox.m_location.x + (gems.m_rotation.x) * (v_throw * timer);
			gems.m_cbox.m_location.y = gems.m_cbox.m_location.y + (gems.m_rotation.y) * (v_throw * timer) - (0.5 * gravity * gems.fall_timer * gems.fall_timer);
			gems.m_cbox.m_location.z = gems.m_cbox.m_location.z + (gems.m_rotation.z) * (v_throw * timer);
			gems.m_lock.unlock();
			for (auto& stones : g_WitchStones) {
				if (gems.m_color == stones.m_color) {
					int collision_check = BoxBoxColliedCheck(gems.m_cbox, stones.m_c_box);
					if (collision_check) {
						gems.m_lock.lock();
						gems.m_cbox.m_location = FloatVector3(0, -5, 0);
						gems.thrown = false;
						gems.m_lock.unlock();
						if (!stones.m_shield) {
							stones.m_lock.lock();
							stones.m_hp--;
							int temp_stone_hp = stones.m_hp;
							stones.m_lock.unlock();
							gems.m_lock.lock();
							gems.m_cbox.m_location = FloatVector3(0, 100, 0);
							gems.transparency = true;
							gems.m_lock.unlock();
							if (temp_stone_hp == 0) {
								stones.m_alive = false;
								g_number_of_witch_stone--;
							}
							if (g_number_of_witch_stone > 1) {
								stones.m_lock.lock();
								stones.m_shield = true;
								stones.m_lock.unlock();
							}
							// 다른 스톤의 실드는 false
							for (auto& o_stone : g_WitchStones) {
								if (o_stone.m_color != gems.m_color) {
									o_stone.m_lock.lock();
									o_stone.m_shield = false;
									o_stone.m_lock.unlock();
								}
							}

						}
					}
				}
			}
		}

		if (gems.m_cbox.m_location.y < 0) {
			gems.m_cbox.m_location = g_gems_origin_location[gems.m_id];
			gems.thrown = false;
			gems.fall_timer = 0;
		}

	}

	// 공격박스랑 플레이어랑 충돌하는지 자기 제외
	if (g_attack.attatck_flag) {
		collide_info attack_box;
		for (auto& player : g_Clients) {
			attack_box.m_location.x = g_Clients[g_attack.id].m_location.x + (g_Clients[g_attack.id].m_DirVector.x) * 2;
			attack_box.m_location.y = player.m_location.y;
			attack_box.m_location.z = g_Clients[g_attack.id].m_location.z + (g_Clients[g_attack.id].m_DirVector.z) * 2;

			collide_info player_cBox;
			player_cBox.m_location = player.m_location;
			for (int i = 0; i < 3; ++i) {
				attack_box.m_extent[i] = 1.0;
				player_cBox.m_extent[i] = player.m_extent[i];
			}
			if (player.m_id != g_attack.id) { //자기 자신 제외
				int collison_check = BoxBoxColliedCheck(attack_box, player_cBox);
				if (collison_check) {
					player.m_cl.lock();
					if (player.m_pstatus != ST_DEAD && player.m_pstatus != ST_NON_HIT) {
						// 능력치 초기화
						player.m_pstatus = ST_DEAD;
						player.m_velocity_accel = 1.f;
						player.m_skill_accel = 1.f;
						player.m_AliveTime = 0;
						player.m_revive_timer = REVIVE_COOLTIME;
						player.m_location.x = before_x[player.m_id];
						player.m_location.y = before_y[player.m_id];
						player.m_location.z = before_z[player.m_id];
						soul_lock.lock();
						g_number_of_regen--;
						soul_lock.unlock();
					}
					player.m_cl.unlock();
				}
			}
			g_attack.attatck_flag = false;
		}
		//일정 시간이 지나면 false로 바꾸기
		g_attack.attatck_flag = false;
	}



	// 곰돌이 부활
	for (auto& deads : g_Clients) {
		if (deads.m_pstatus == ST_DEAD && deads.m_status == ST_ACTIVE_GAME) {
			deads.m_revive_timer -= timer;

			if (deads.m_revive_timer <= 0) {
				deads.m_cl.lock();
				deads.m_location = g_player_respwan_location[deads.m_id];
				deads.m_revive_timer = 0;
				deads.m_pstatus = ST_ALIVE;
				deads.m_astatus = A_ST_IDLE;
				deads.m_cl.unlock();
			}
		}
	}
}
