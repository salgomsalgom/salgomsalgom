#include "pch.h"
#include "SystemNode.h"
#include "SystemCommunicator.h"

SystemNode::SystemNode(SystemCommunicator* communicator) :
	m_Communicator(communicator)
{
	communicator->RegisterSystem(this);
}

SystemNode::~SystemNode()
{
	m_Communicator->RemoveSystem(this);
}
