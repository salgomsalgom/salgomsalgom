#pragma once
#include <Windows.h>
#include <codecvt>
#include <sstream>
using namespace std;

namespace yhl {

	class EndDelim { };

	class DebugOut {
		stringstream ss;

	public:
		DebugOut() { }

		DebugOut& operator<<(EndDelim) {
			ss << "\n";
			OutputDebugStringA(ss.str().c_str());
			ss.str("");
			return *this;
		}

		DebugOut& operator<< (int val) {
			ss << val;
			return *this;
		}

		DebugOut& operator<< (unsigned int val) {
			ss << val;
			return *this;
		}

		DebugOut& operator<< (short val) {
			ss << val;
			return *this;
		}

		DebugOut& operator<< (unsigned short val) {
			ss << val;
			return *this;
		}

		DebugOut& operator<< (long val) {
			ss << val;
			return *this;
		}

		DebugOut& operator<< (unsigned long val) {
			ss << val;
			return *this;
		}

		DebugOut& operator<< (long long val) {
			ss << val;
			return *this;
		}

		DebugOut& operator<< (unsigned long long val) {
			ss << val;
			return *this;
		}

		DebugOut& operator<< (float val) {
			ss << val;
			return *this;
		}

		DebugOut& operator<< (double val) {
			ss << val;
			return *this;
		}

		DebugOut& operator<< (long double val) {
			ss << val;
			return *this;
		}

		DebugOut& operator<< (bool val) {
			if (val)	ss << "true";
			else		ss << "false";
			return *this;
		}

		DebugOut& operator<< (const char* val) {
			ss << val;
			return *this;
		}

		DebugOut& operator<< (const string& val) {
			ss << val;
			return *this;
		}

		//DebugOut& operator<< (const wchar_t* val) {
		//	wstring_convert<codecvt_utf8_utf16<wchar_t>> converter; 
		//	string str{ converter.to_bytes(val)};

		//	ss << str;
		//	return *this;
		//}

		//DebugOut& operator<< (const wstring& val) {
		//	wstring_convert<codecvt_utf8_utf16<wchar_t>> converter;
		//	string str{ converter.to_bytes(val) };

		//	ss << str;
		//	return *this;
		//}

	};

	extern EndDelim fls;
	extern DebugOut dout;
}
