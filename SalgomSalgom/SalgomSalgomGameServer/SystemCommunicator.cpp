#include "pch.h"
#include "SystemCommunicator.h"


void SystemCommunicator::Initialize()
{
	for (SystemNode* node : m_SystemList) {
		node->Initialize();
	}
}

void SystemCommunicator::RegisterSystem(SystemNode* node)
{
	m_SystemList.push_back(node);

	std::cout << "Register System." << std::endl;
}

void SystemCommunicator::RemoveSystem(SystemNode* node)
{
	m_SystemList.remove(node);
}

void SystemCommunicator::SendSystemMessage(const SystemMassage message)
{
	m_MessageQueue.push(message);
}
