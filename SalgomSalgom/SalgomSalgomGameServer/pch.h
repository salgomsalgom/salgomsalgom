#pragma once

#include <tchar.h>

#include <WS2tcpip.h>
#include <MSWSock.h>
#pragma comment (lib, "WS2_32.lib")
#pragma comment (lib, "mswsock.lib")

// C++ libraries
#include <array>
#include <vector>
#include <map>
#include <queue>
#include <string>

#include <algorithm>
#include <numeric>
#include <functional>

#include <cassert>

#include <iostream>
#include <fstream>
#include <sstream>

#include <mutex>

// Utility
#include "Util/DebugOut.h"

// server
#include "../Packet.h"

#include <DirectXMath.h>



struct EXOVER {
	WSAOVERLAPPED	over;
	ENUMOP			op;
	char			io_buf[WORLD_BUFFER];
	union
	{
		WSABUF			wsabuf;
		SOCKET			c_socket;
	};
};

struct collide_info {

	FloatVector3 m_location;
	float m_axis[3][3]{ 1.0, 0.0, 0.0,
						0.0, 1.0, 0.0,
						0.0, 0.0, 1.0 };
	float m_extent[3];
};


struct CLIENT {
	CLIENT()
	{
		m_DirVector = FloatVector3(1.f, 0.f, 0.f);
	}

	mutex m_cl;
	SOCKET m_s;
	int m_id;
	EXOVER m_recv_over;
	int m_prev_size;
	char m_packe_buf[MAX_PACKET_SIZE];
	C_STATUS m_status;
	P_STATUS m_pstatus{ST_DEAD};
	A_STATUS m_astatus;
	CH_ROLE m_role;

	FloatVector3 m_location{0,0,0};
	FloatVector3 m_rot{ 0 ,DirectX::XM_PI,0};
	FloatVector3 m_DirVector;
	FloatVector3 m_MoveVector;
	atomic<float> m_ForwardSpeed;
	atomic<float> m_RightSpeed;

	float m_axis[3][3]{ 1.f, 0.f, 0.f,
						0.f, 1.f, 0.f,
						0.f, 0.f, 1.f };
	float m_extent[3]{ 0.8f, 1.7f, 0.8f };
	bool picked_up;
	char m_name[MAX_ID_LEN + 1];

	float m_char_power;
	float m_speed;
	atomic<float> m_AliveTime;
	float m_velocity_accel = 1.f;
	float m_skill_accel = 1.f;
	float m_revive_timer{ 5 };
};

struct Skill {
	float cool_time;
	float duration_time;
	FloatVector3 dir;
};


struct PICKUP_GEM {
	int id;
	bool pickup_flag;
};

struct ATTACK {
	int id;
	bool attatck_flag;
};

struct THROW_GEM {
	int p_id{ 6 };
	int gem_id{ 18 };
	MagicColor m_color;
	bool m_throw;
};

struct Gem {
	int m_id;
	MagicColor m_color;
	bool picked;
	bool thrown;
	bool transparency;
	float transparency_time;
	int picked_id;
	float fall_timer;
	FloatVector3 m_rotation;
	collide_info m_cbox;

	mutex m_lock;

};

struct WitchStone {
	mutex m_lock;
	MagicColor m_color;
	collide_info m_c_box;
	char m_hp;
	bool m_alive{ true };
	bool m_shield;
};

// location 0 0 0 ,
// ???? ??? 10 10 10
struct OBSTACLE {
	string m_name;
	FloatVector3 m_location{ 0 ,0, 0 };
	FloatVector3 m_rotation{ 0,0,0 };
	FloatVector3 m_scale{ 0,0,0 };
	float m_axis[3][3]{ 1.0, 0.0, 0.0,
						0.0, 1.0, 0.0,
						0.0, 0.0, 1.0 };
	float m_extent[3]{};
};

struct KEYBOARDINFO {
	char m_id;
	PlayerInput m_input;
};

struct MOUSEINFO {
	char m_id;
	FloatVector2 m_rad;
};