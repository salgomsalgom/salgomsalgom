#pragma once
#include "SystemNode.h"

class SystemCommunicator;
 
class TimerSystem : SystemNode
{
	static constexpr float LOCK_TICK = 30.f;
	static constexpr float LOCK_SEC = 1.f / LOCK_TICK;
	static constexpr int TICK_SAMPLE_COUNT = 30;

	static bool s_Instantiated;

public:
	explicit TimerSystem(SystemCommunicator* const communicator);
	~TimerSystem();

public:
	void Initialize() override;

	void Tick();
	float GetElapsedTime();
	float GetBaseTime();
	float GetTickRate();

private:
	inline void UpdateTickRate(float elapsed_time);

private:
	float m_TimeScale;

	__int64 m_EntryCount;
	__int64 m_CurrCount;
	__int64 m_LastCount;

	float m_ElapsedTime;

public:
	// for calculate Tick Rate
	std::array<float, TICK_SAMPLE_COUNT> m_TimeSample;
	int m_SampleIdx;
	float m_TickRate;
};
