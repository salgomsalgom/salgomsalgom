#pragma once
#include "SystemCommunicator.h"

class Scene;

namespace GameCore {
	class Game abstract
	{
	public:
		static SystemCommunicator s_Systems;

	protected:
		std::vector<Scene*>		m_Scenes;
		Scene*					m_CurrScene;

	public:
		explicit Game() { }		// ��� �ʱ�ȭ
		virtual ~Game() { }

		virtual void StartUp() = 0;
		virtual void CleanUp() = 0;

		virtual void Update() = 0;
	};

	void Initialize(Game& game);

	void Update(Game& game);
	
	void Terminate(Game& game);
}

class InputSystem;

class TestGame : public GameCore::Game {
public:
	TestGame() { }
	virtual ~TestGame() { }

	virtual void StartUp() override { }		// �� ����
	virtual void CleanUp() override { }		// �� �Ҹ�

	virtual void Update() override {};		// ���� �� ������Ʈ
	void Update(InputSystem* , float );

};
