#include "pch.h"
#include "NetworkSystem.h"
#include "../Packet.h"
#include "Math.h"
#include "HeightMap.h"
bool NetworkSystem::s_Instantiated = false;

HeightMap g_HeightMap_objects{ "./Assets/TERN.hm" };


void LoadMapInfo(const char* filename);
void LoadSize(const char* filename);
NetworkSystem::NetworkSystem(SystemCommunicator* const communicator) :
	SystemNode(communicator)
{
	assert(!s_Instantiated);
	s_Instantiated = true;
}

NetworkSystem::~NetworkSystem()
{
}



HANDLE g_iocp;
SOCKET g_ListenSocket;

void NetworkSystem::Initialize()
{
	std::cout << "Initialized Network System." << std::endl;
	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 2), &WSAData);

	g_ListenSocket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);

	SOCKADDR_IN s_address;
	memset(&s_address, 0, sizeof(s_address));
	s_address.sin_family = AF_INET;
	s_address.sin_port = htons(SERVER_PORT);
	s_address.sin_addr.S_un.S_addr = htonl(INADDR_ANY);
	::bind(g_ListenSocket, reinterpret_cast<sockaddr*>(&s_address), sizeof(s_address));

	listen(g_ListenSocket, SOMAXCONN);

	g_iocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, 0);
	
	LoadSize("dimensions.txt");
	LoadMapInfo("MapInfoB.txt");
	
}



#define TAG_LENGTH 10
using namespace std;
typedef string TagName;
extern OBSTACLE g_Obstacle[1000];
extern 	int number_of_objects;

struct ObjectSize {
	FloatVector3 m_size;
};
map<TagName, ObjectSize> g_objectSize;
struct StaticObjectInfo {

	Math::FloatVector3 position;
	Math::FloatVector3 rotation;
	Math::FloatVector3 scale;
};

struct MapInfo {
	
	//map<TagName, vector<StaticObjectInfo>> objects;
	vector<pair<TagName, vector<StaticObjectInfo>>>objects;

};

vector <StaticObjectInfo> GetObjInfos(ifstream& i, int pnum) {
	
	vector <StaticObjectInfo> v_objtransform;
	StaticObjectInfo ob;
	float getfloatvalue;
	float* getpoint = &ob.position.x;
	

	for (int j = 0; j < pnum * 9; j++) {

		i.read((char*)&getfloatvalue, sizeof(float));
		getpoint[j % 9] = getfloatvalue;

		if (j % 9 == 8) v_objtransform.push_back(ob);
	}
	return v_objtransform;
}

void LoadSize(const char* filename)
{
	ifstream in{ filename };
	float getsizeinfo;
	ObjectSize temp;
	
	
	int checker{};
	while (!in.eof()) {
		string tag_name;
		in >> tag_name;
		
		for (int i = 0; i < 3; ++i) {
			in >> getsizeinfo;
			if (checker % 3 == 0) {
				temp.m_size.x = getsizeinfo;
			}
			else if (checker % 3 == 1) {
				temp.m_size.y = getsizeinfo;
			}
			else if (checker % 3 == 2) {
				temp.m_size.z = getsizeinfo;
			}
			checker++;
		}
		g_objectSize.insert(make_pair(tag_name, temp));
		
	}
}

void LoadMapInfo(const char* filename) {
	
	ifstream in{ filename, std::ios_base::binary };
	MapInfo m_mapinfo; 
	if (!in) {
		//return false;
	}

	if (in) {
		int objnuminmap; 
		int prefabnum; 

		in.read((char*)&objnuminmap, sizeof(int));
		cout << objnuminmap << endl;

		for (int i = 0; i < objnuminmap - 1; i++) {
			
			char* buffer = new char[TAG_LENGTH];
			in.read(buffer, TAG_LENGTH);
			in.read((char*)&prefabnum, sizeof(int));
			vector<StaticObjectInfo> v_tmp = GetObjInfos(in, prefabnum);
			string str_name{ buffer };

			pair<TagName, vector<StaticObjectInfo>> p = make_pair(str_name, vector<StaticObjectInfo>());
			m_mapinfo.objects.push_back(p);

			for (auto& item : v_tmp) {
				m_mapinfo.objects.back().second.push_back(item);
			}
			delete[] buffer;
			buffer = nullptr;
		}

	}
	
	int i = 0;
	//vector<pair<TagName, vector<StaticObjectInfo>>>
	for (auto& item : m_mapinfo.objects) {

		for (auto& obj : item.second) {

			g_Obstacle[i].m_location.x = obj.position.x;
			g_Obstacle[i].m_location.z = obj.position.z;

			float height_x = obj.position.x + 64;
			float height_z = obj.position.z + 64;

			if (item.first == "MPED") {
				g_Obstacle[i].m_location.y = obj.position.y;

			}
			else if (item.first == "GEMB" || item.first == "GEMR" || item.first == "GEMW" || item.first == "GEMP") {
				g_Obstacle[i].m_location.y = ((g_HeightMap_objects.GetLerpHeight(height_x, height_z) - 129) * 0.069);
			}
			else {
				g_Obstacle[i].m_location.y = ((g_HeightMap_objects.GetLerpHeight(height_x, height_z) - 129) * 0.069) - HEIGHTMAP_Y_CORRECTION ;
			}

			g_Obstacle[i].m_extent[0] = obj.scale.x * (g_objectSize.find(item.first)->second.m_size.x / 2);
			g_Obstacle[i].m_extent[1] = obj.scale.y * (g_objectSize.find(item.first)->second.m_size.y);
			g_Obstacle[i].m_extent[2] = obj.scale.z * (g_objectSize.find(item.first)->second.m_size.z / 2);

			if (item.first == "BOXC") {
				g_Obstacle[i].m_location.y = 0;
			}

			g_Obstacle[i].m_rotation = obj.rotation;


			if (item.first == "LEV3" || item.first == "LEV4") {
				g_Obstacle[i].m_location.y = obj.position.y;
			}
			if (item.first == "SNAL") {
				g_Obstacle[i].m_rotation.y += 45;
			}
			if (item.first == "ROK1") { //60도 보정
				g_Obstacle[i].m_rotation.y += 60;
			}

			if (item.first == "ROK2") {
				g_Obstacle[i].m_rotation.y += 180;
			}

			float x = obj.rotation.x * 3.14 / 180;
			float y = obj.rotation.y * 3.14 / 180;
			float z = obj.rotation.z * 3.14 / 180;
			g_Obstacle[i].m_axis[0][0] = cos(z) * cos(y);
			g_Obstacle[i].m_axis[0][1] = cos(x) * sin(z) + (sin(x) * sin(y) * cos(z));
			g_Obstacle[i].m_axis[0][2] = sin(x) * sin(z) - (cos(x) * sin(y) * cos(z));
			g_Obstacle[i].m_axis[1][0] = -(sin(z) * cos(y));
			g_Obstacle[i].m_axis[1][1] = (cos(z) * cos(x)) - (sin(z) * sin(y) * sin(x));
			g_Obstacle[i].m_axis[1][2] = (sin(z) * sin(y) * cos(x)) + (cos(z) * sin(x));
			g_Obstacle[i].m_axis[2][0] = sin(y);
			g_Obstacle[i].m_axis[2][1] = -(cos(y) * sin(x));
			g_Obstacle[i].m_axis[2][2] = cos(y) * cos(x);



			{
				g_Obstacle[i].m_scale = obj.scale;
				g_Obstacle[i].m_name = item.first;
				++i;
			}
		}


	}
		number_of_objects = i;
}
