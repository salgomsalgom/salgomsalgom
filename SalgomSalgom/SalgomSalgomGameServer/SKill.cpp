#include "pch.h"
#include "SKill.h"

//인자로 복사받은 전역 변수의 값을 (연산해서) 바꾸고 넘기자.

//SKill::Skill()
//{
//}

//SKill::~SKill() {
//
//}


Halt::Halt()
    : m_HaltInfo(6.0f, 25.f, 25.0f, 0.99f, 6.0f, 0.5f)
{
    m_SphereScale = XMFLOAT3(1.0f, 1.0f, 1.0f);
    m_ArriveCount = 0;
    m_Status = SkillStatus::READY;
    m_Direction = XMFLOAT3(0.f, 0.f, 0.f);
    m_HaltPos = XMFLOAT3(0.f, 0.f, 0.f);
}

Halt::~Halt()
{
}


void Halt::Update(float elapsedTime, bool& move_timeover)
{
    switch (m_Status) {
    case SkillStatus::READY: {
        m_CoolTime -= elapsedTime;
        break;
    }
    case SkillStatus::MOVE: {
        m_DurationTime -= elapsedTime;
        Move(elapsedTime);

        if (m_DurationTime < 0) {
            m_DurationTime = 0.f;
            move_timeover = true;
        }
        break;
    }
    case SkillStatus::ARRIVE:
        m_DurationTime -= elapsedTime;
        if (m_DurationTime < 0) {
            m_DurationTime = 0.f;
            m_CoolTime = WITCH_HALT_COOLTIME;
            m_Status = SkillStatus::READY;
        }
        break;
    default:
        break;
    }
}

void Halt::InitMove(const FloatVector3& witchPos, const FloatVector3& witchFowardVec)
{
    memcpy(&m_Direction, &witchFowardVec, sizeof(XMFLOAT3));
    memcpy(&m_CurrPos, &witchPos, sizeof(XMFLOAT3));
    m_DurationTime = HALT_MOVETIME;
    m_Status = SkillStatus::MOVE;
}

void Halt::InitArrive(vector<CLIENT*>& normal_bears)
{
    auto pos = GetPos();

    for (auto& bear : normal_bears)
    {
        XMFLOAT3 bear_pos;
        
        bear->m_cl.lock();
        memcpy(&(bear_pos), &(bear->m_location), sizeof(XMFLOAT3));
        auto xmv_pos = XMLoadFloat3(&pos);
        auto xmv_bear_pos = XMLoadFloat3(&bear_pos);
        auto xmv_dist = XMVectorSubtract(xmv_pos, xmv_bear_pos);
        auto xmv_len_sq = XMVector4LengthSq(xmv_dist);
        
        XMFLOAT3 v_len_sq;
        XMStoreFloat3(&v_len_sq, xmv_len_sq);

        float len_sq = v_len_sq.x;

        if (len_sq < HALT_PULLRADIUS_SQ) {
            bear->m_pstatus = ST_CAPTURED;
        }

        bear->m_cl.unlock();
    }

    m_DurationTime = HALT_PULLTIME;
    m_Status = SkillStatus::ARRIVE;
}

void Halt::Move(float elapsedTime)
{
    XMVECTOR dir = XMLoadFloat3(&m_Direction);
    float move_dist = m_HaltInfo.velocity * elapsedTime;
    XMVECTOR v_move = move_dist * dir;

    XMVECTOR pos = XMLoadFloat3(&m_CurrPos);
    XMVECTOR new_pos = XMVectorAdd(pos, v_move);

    XMStoreFloat3(&m_CurrPos, new_pos);
}

XMFLOAT3 Halt::GetPos()
{
    m_Lock.lock();
    XMFLOAT3 pos = m_CurrPos;
    m_Lock.unlock();
    return pos;
}
