#pragma once

enum class SceneKeys : unsigned short;

class Scene abstract {
protected:
	SceneKeys m_Key;

public:
	Scene(SceneKeys key);
	virtual ~Scene() { }

};
