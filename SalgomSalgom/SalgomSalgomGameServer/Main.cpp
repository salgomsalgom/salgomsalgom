﻿#include "pch.h"
#include "Game.h"
#include "InputSystem.h"
#include "NetworkSystem.h"
#include "TimerSystem.h"
#include "SKill.h"
using namespace GameCore;

extern HANDLE g_iocp;
extern SOCKET g_ListenSocket;

extern TimerSystem   g_TimerSystem;
GameCore::Game* game = new TestGame();

bool g_UpdateFlag;
constexpr float GEMSIZE = 1.0f;
constexpr float WITCHSTOENSIZE = 1.f;
constexpr char  WITCHSTONE_HP = 1;

constexpr float WITCH_SPEED = 4.8;
constexpr float WITCH_SPEED_SIDE = 3.6;
constexpr float WITCH_SPEED_BACK = 2.4;
constexpr float NORMAL_SPEED = 4;
constexpr float NORMAL_SPEED_SIDE = 3;
constexpr float NORMAL_SPEED_BACK = 2;

OBSTACLE g_Obstacle[1000];
CLIENT g_Clients[MAX_USER + 1];
Gem g_Gems[MAX_GEM_NUMBER];
FloatVector3 g_gems_origin_location[MAX_GEM_NUMBER];
FloatVector3 g_player_respwan_location[MAX_USER];
WitchStone g_WitchStones[4];

atomic<int> g_player_count;
int number_of_objects;

float g_rad_x[MAX_USER];
float g_rad_y[MAX_USER];
ATTACK g_attack;
THROW_GEM g_throw_gem[5]{};
PICKUP_GEM g_pickup[5];
char g_number_of_witch_stone = 4;
constexpr char g_MAX_WITCH_STONE_NUMBER = 4;
char g_number_of_regen = SHARED_LIFE;
mutex soul_lock;
bool witch_flag;
int  g_witch_id;
Skill g_Skill_1_Flag[MAX_USER];
Skill g_Skill_2_Flag[MAX_USER];
mutex g_SkillLock;

Halt g_WitchSkill2;

float before_x[5]{};
float before_y[5]{};
float before_z[5]{};


void sendUpdatePacket(int);

void display_error(const char* msg, int error)
{
    WCHAR* lpMsgBuf;
    FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
        NULL, error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPTSTR)&lpMsgBuf, 0, NULL);
    cout << msg;
    wcout << L"���� " << lpMsgBuf << '\n';
    exit(-1);
    LocalFree(lpMsgBuf);
}

void useSkill_2(int id, float timer) {
    if (g_Clients[id].m_role == R_NORMAL) {

        g_Clients[id].m_cl.lock();
        g_Skill_2_Flag[id].cool_time = NORMAL_FAKEDIE_COOLTIME;
        g_Clients[id].m_pstatus = ST_NON_HIT;
        g_Clients[id].m_location.x = before_x[id];
        g_Clients[id].m_location.y = before_y[id];
        g_Clients[id].m_location.z = before_z[id];
        g_Skill_2_Flag[id].duration_time -= timer;

        if (g_Skill_2_Flag[id].duration_time <= 0) {
            g_Clients[id].m_pstatus = ST_ALIVE;
        }

        g_Clients[id].m_cl.unlock();
    }
    else {
        g_Skill_2_Flag[id].duration_time = 1;
        g_Skill_2_Flag[id].cool_time = WITCH_HALT_COOLTIME;
        // 여기가 아니라 Game.cpp에서 업데이트
        // witchSkill2.Update();
    }
}


void sendPacket(int user_id, void* p, int size)
{
    CLIENT& u = g_Clients[user_id];
    EXOVER* exover = new EXOVER;
    exover->op = OP_SEND;
    ZeroMemory(&exover->over, sizeof(exover->over));
    exover->wsabuf.buf = exover->io_buf;
    exover->wsabuf.len = size;
    memcpy(exover->io_buf, p, size);
    WSASend(u.m_s, &exover->wsabuf, 1, NULL, 0, &exover->over, NULL);

}

void sendPacketRender(int user_id) {
    SC_PacketRenderStart p;
    p.size = sizeof(p);
    p.type = S2C_RENDERSTART;
    // 플레이어
    for (int i = 0; i < MAX_USER; ++i) {
        g_Clients[i].m_cl.lock();
        p.players[i].m_id = i;
        p.players[i].m_location = g_Clients[i].m_location;
        p.players[i].m_role = g_Clients[i].m_role;
        p.players[i].m_rotation = g_Clients[i].m_rot;
        p.players[i].m_dirVector = g_Clients[i].m_DirVector;
        p.astatus[i] = g_Clients[i].m_astatus;
        g_Clients[i].m_cl.unlock();
    }
    // 잼
    for (int i = 0; i < MAX_GEM_NUMBER; ++i) {
        g_Gems[i].m_lock.lock();
        p.gems[i].m_color = g_Gems[i].m_color;
        p.gems[i].m_location = g_Gems[i].m_cbox.m_location;
        p.gems[i].transparency = g_Gems[i].transparency;
        g_Gems[i].m_lock.unlock();
    }

    // 위치스톤
    for (int i = 0; i < g_MAX_WITCH_STONE_NUMBER; ++i) {
        g_WitchStones[i].m_lock.lock();
        p.witchStoneState[i].m_shield = g_WitchStones[i].m_shield;
        p.witchStoneState[i].m_location = g_WitchStones[i].m_c_box.m_location;
        p.witchStoneState[i].m_alive = g_WitchStones[i].m_alive;
        g_WitchStones[i].m_lock.unlock();

    }
    p.soul_left = g_number_of_regen;
    p.number_of_player = GAME_START_NUMBER;
    sendPacket(user_id, &p, p.size);
}

void sendPacketLoginOK(int user_id) {
    SC_PacketLoginOk p;
    p.size = sizeof(p);
    p.type = S2C_LOGIN_OK;
    p.id = user_id;
    sendPacket(user_id, &p, p.size);
}

void sendPacketWorldData(int user_id) {
    SC_PacketStart* map_data = new SC_PacketStart;
    map_data->size = sizeof(SC_PacketStart);
    map_data->type = S2C_MAP_DATA;
    int i{};
    for (auto& obj : g_Obstacle) {

        strcpy_s(map_data->objects[i].name, obj.m_name.c_str());
        map_data->objects[i].m_position = obj.m_location;

        map_data->objects[i].m_rotation = obj.m_rotation;

        map_data->objects[i].m_scale = obj.m_scale;

        map_data->objects[i].m_cbox.x = obj.m_extent[0] * 2;
        map_data->objects[i].m_cbox.y = obj.m_extent[1];
        map_data->objects[i].m_cbox.z = obj.m_extent[2] * 2;

        ++i;
    }
    map_data->number_of_objects = number_of_objects;
    sendPacket(user_id, map_data, map_data->size);
    delete map_data;
}

void processPakcet(int user_id, char* buf)
{
    CS_PacketInput* packet = reinterpret_cast<CS_PacketInput*>(buf);

    switch (packet->type) {
    case C2S_LOGIN:
    {
        CS_PacketLogin* packet = reinterpret_cast<CS_PacketLogin*>(buf);

        sendPacketLoginOK(user_id);

        ++g_player_count;

        break;
    }
    case C2S_DATA_REQUEST:
    {
        sendPacketWorldData(user_id);
        break;
    }
    case C2S_RENDER_START_REQUEST:
    {
        if (g_player_count == GAME_START_NUMBER) {
            for (int i = 0; i < GAME_START_NUMBER; ++i) {
                sendPacketRender(i);
                cout << i << "send: C2S_RENDER_START_REQUEST\n";
            }

            if (!g_UpdateFlag) {
                EXOVER updateover;
                updateover.op = OP_UPDATE;
                g_UpdateFlag = true;
                PostQueuedCompletionStatus(g_iocp, 1, MAX_USER, &updateover.over);
                cout << "update start\n";
            }
        }
        break;
    }
    case C2S_WITCH_ATTACK:
    {

        CS_PacketAttack* packet = reinterpret_cast<CS_PacketAttack*> (buf);
        g_attack.attatck_flag = true;
        g_attack.id = user_id;
        g_Clients[user_id].m_pstatus = ST_ATTACK;
    }
    break;

    case C2S_GEM_PICKUP:
    {
        CS_PacketPickGem* packet = reinterpret_cast<CS_PacketPickGem*> (buf);

        InputSystem* inputsystem{};
        int pickup_id = user_id;
        inputsystem->AddComponent(pickup_id);
    }
    break;

    case C2S_GEM_THROW:
    {
        CS_PacketThrowGem* packet = reinterpret_cast<CS_PacketThrowGem*> (buf);

        g_throw_gem[user_id].m_throw = true;
        g_throw_gem[user_id].p_id = user_id;

    }
    break;

    case C2S_SKILL2:
    {
        CS_PacketSkill2* packet = reinterpret_cast<CS_PacketSkill2*>(buf);
        if (g_Skill_2_Flag[user_id].cool_time != 0) {
            g_Skill_2_Flag[user_id].duration_time = 0;
            g_Clients[user_id].m_pstatus = ST_ALIVE;
            g_Clients[user_id].m_astatus = A_ST_IDLE;
        }
        else if (g_Skill_2_Flag[user_id].cool_time == 0 && g_Clients[user_id].m_role == R_NORMAL) {

            g_Skill_2_Flag[user_id].duration_time = 10.f;

        }
        else if (g_WitchSkill2.m_CoolTime <= 0) {
            g_Clients[0].m_cl.lock();
            FloatVector3 pos = g_Clients[0].m_location;
            FloatVector3 dir = g_Clients[0].m_DirVector;
            g_Clients[0].m_cl.unlock();

            // 마녀 시야 위치
            pos = pos + dir;
            pos.y += 1.2f;

            switch (g_WitchSkill2.m_Status) {
            case SkillStatus::READY: {
                g_WitchSkill2.InitMove(pos, dir);
                g_Clients[user_id].m_astatus = A_ST_HALT_1;
                break;
            }
            case SkillStatus::MOVE: {
                // 끌어당길 곰돌이 선별
                vector<CLIENT*> normal_bears;
                for (int i = 0; i < GAME_START_NUMBER; ++i) {
                    if (R_WITCH == g_Clients[i].m_role) continue;
                    normal_bears.push_back(&(g_Clients[i]));
                }
                g_WitchSkill2.InitArrive(normal_bears);
                g_Clients[user_id].m_astatus = A_ST_HALT_2;
                break;
            }
            case SkillStatus::ARRIVE:
                // 아무것도 안함
                break;
            default:
                break;
            }
        }

       
    }
    break;
    case C2S_INPUT:
    {
        InputSystem* inputsystem{};
        KEYBOARDINFO player_info;
        player_info.m_id = user_id;
        player_info.m_input = packet->input;
        inputsystem->AddComponent(player_info);
        break;
    }
    case C2S_MOUSE_MOVE:
    {
        CS_PacketMouse* packet = reinterpret_cast<CS_PacketMouse*>(buf);

        InputSystem* inputsystem{};
        FloatVector2 mouse_d = packet->delta;

        float rad_x = g_rad_x[user_id];
        float rad_y = g_rad_y[user_id];


        rad_x += (mouse_d.x * DirectX::XM_PIDIV4);
        rad_y += (mouse_d.y * DirectX::XM_PIDIV4);

        rad_y = clamp(rad_y, -DirectX::XM_PIDIV4, DirectX::XM_PIDIV4);
        inputsystem->AddComponent(rad_x, rad_y, user_id);

        g_rad_x[user_id] = rad_x;
        g_rad_y[user_id] = rad_y;

        g_Clients->m_cl.lock();
        g_Clients[user_id].m_rot.y += -(mouse_d.x * DirectX::XM_PIDIV4);
        g_Clients->m_cl.unlock();
        break;
    }
    case C2S_RESULT:
    {
        SC_PacketResult* packet = reinterpret_cast<SC_PacketResult*>(buf);
        break;
    }
    default:
        cout << "unknown packet type error\n";
    }
}

void sendLeavePacket(int user_id, int o_id)
{
    SC_PacketLeave p;
    p.id = o_id;
    p.size = sizeof(p);
    p.type = S2C_LEAVE;
    sendPacket(user_id, &p, p.size);
}

void sendResultPacket(int user_id, CH_ROLE win) {
    SC_PacketResult p;
    p.size = sizeof(SC_PacketResult);
    p.type = S2C_RESULT;
    p.win = win;
    sendPacket(user_id, &p, p.size);
}

void sendCHPowerChagenPacket(int user_id, int speed, int skill) {
    SC_Packet_CH_PowerChange p;
    p.size = sizeof(SC_Packet_CH_PowerChange);
    p.type = S2C_CHARACTER_POWER_CHANGE;
    p.speed_power = speed;
    p.skill_power = skill;
    sendPacket(user_id, &p, p.size);
}

void sendSoulLeftPacket(int user_id) {
    SC_PacketSoulChagne p;
    p.size = sizeof(SC_PacketSoulChagne);
    p.type = S2C_SOULCHANGE;
    p.soul_left = g_number_of_regen;
    sendPacket(user_id, &p, p.size);
}

void sendAnimationChagnePacket(int user_id, int other_id ) {
    SC_PacketAnimationChange p;
    p.size = sizeof(SC_PacketAnimationChange);
    p.type = S2C_ANIMATION_CHANGE;
    g_Clients[other_id].m_cl.lock();
    p.animation_status = g_Clients[other_id].m_astatus;
    g_Clients[other_id].m_cl.unlock();
    p.id = other_id;
    sendPacket(user_id, &p, p.size);
}

void sendWitchStoneSTChange(int user_id) {
    SC_PakcetWitchStone_ST_Change p;
    p.size = sizeof(SC_PakcetWitchStone_ST_Change);
    p.type = S2C_WITCHSTONE_ST_CHANGE;
    for (int i = 0; i < g_MAX_WITCH_STONE_NUMBER; ++i) {
        g_WitchStones[i].m_lock.lock();
        p.witchStoneState[i].m_alive = g_WitchStones[i].m_alive;
        p.witchStoneState[i].m_color = g_WitchStones[i].m_color;
        p.witchStoneState[i].m_hp = g_WitchStones[i].m_hp;
        p.witchStoneState[i].m_shield = g_WitchStones[i].m_shield;
        p.witchStoneState[i].m_location = g_WitchStones[i].m_c_box.m_location;
        g_WitchStones[i].m_lock.unlock();
    }
    sendPacket(user_id, &p, p.size);
}

void sendUpdatePacket(int user_id)
{
    SC_PacketUpdate p;
    p.type = S2C_UPDATE;
    p.size = sizeof(SC_PacketUpdate);
    for (int i = 0; i < MAX_USER; ++i) {
        g_Clients[i].m_cl.lock();
        p.players[i].m_id = i;
        p.players[i].m_location = g_Clients[i].m_location;
        p.players[i].m_role = g_Clients[i].m_role;
        p.players[i].m_rotation = g_Clients[i].m_rot;
        p.players[i].m_dirVector = g_Clients[i].m_DirVector;
        p.players[i].gem_picked = g_Clients[i].picked_up;
        p.players[i].m_respawn_time = g_Clients[i].m_revive_timer;
        g_Clients[i].m_cl.unlock();
    }
    for (int i = 0; i < MAX_GEM_NUMBER; ++i) {
        g_Gems[i].m_lock.lock();
        p.gems[i].m_color = g_Gems[i].m_color;
        p.gems[i].m_location = g_Gems[i].m_cbox.m_location;
        p.gems[i].transparency = g_Gems[i].transparency;
        p.gems[i].m_rot = g_Gems[i].m_rotation;
        p.gems[i].picked = g_Gems[i].picked;
        p.gems[i].player_id = g_Gems[i].picked_id;
        g_Gems[i].m_lock.unlock();
    }

    for (int i = 0; i < MAX_USER; ++i) {
        g_SkillLock.lock();
        p.skill_1[i].coolTime = g_Skill_1_Flag[i].cool_time;
        p.skill_1[i].duration = g_Skill_1_Flag[i].duration_time;
        p.skill_2[i].coolTime = g_Skill_2_Flag[i].cool_time;
        p.skill_2[i].duration = g_Skill_2_Flag[i].duration_time;

        if (g_Clients[i].m_role == R_WITCH) {
            XMFLOAT3 pos = g_WitchSkill2.GetPos();
            auto stat = g_WitchSkill2.GetSKillStatus();
            p.skill_2[i].coolTime = g_WitchSkill2.m_CoolTime;
            memcpy(&(p.skill_2[i].m_location), &pos, sizeof(XMFLOAT3));
            p.skill_2[i].m_Stat = stat;
            p.skill_2[i].m_exist = (SkillStatus::READY == stat) ? false : true;
        }
        else { // 이건 뭐냐
            p.skill_2[i].m_location = FloatVector3(0, -50, 0);
        }
        g_SkillLock.unlock();
    }
    sendPacket(user_id, &p, p.size);
}

void recvPacketConstruct(int user_id, DWORD io_byte)
{
    CLIENT& cu = g_Clients[user_id];
    EXOVER& r_o = cu.m_recv_over;

    int rest_byte = io_byte;
    char* p = r_o.io_buf;

    int packet_size = 0;
    if (0 != cu.m_prev_size) packet_size = cu.m_packe_buf[0];

    while (rest_byte > 0) {
        if (0 == packet_size) packet_size = *p;
        if (packet_size <= rest_byte + cu.m_prev_size) {
            memcpy(cu.m_packe_buf + cu.m_prev_size, p, packet_size - cu.m_prev_size);
            p += packet_size - cu.m_prev_size;
            rest_byte -= packet_size - cu.m_prev_size;
            packet_size = 0;
            processPakcet(user_id, cu.m_packe_buf);
            cu.m_prev_size = 0;

        }
        else {
            memcpy(cu.m_packe_buf + cu.m_prev_size, p, rest_byte);
            cu.m_prev_size += rest_byte;
            rest_byte = 0;
            p += rest_byte;
        }
    }
}

void InitializeClient(int user_id) {
    CLIENT& nc = g_Clients[user_id];
    
    nc.m_id = user_id;
    nc.m_prev_size = 0;
    nc.m_recv_over.op = OP_ACCEPT;
    ZeroMemory(&nc.m_recv_over.over, sizeof(nc.m_recv_over.over));
    ZeroMemory(&nc.m_name, sizeof(nc.m_name));
    nc.m_recv_over.wsabuf.buf = nc.m_recv_over.io_buf;
    nc.m_recv_over.wsabuf.len = MAX_BUFFER_SIZE;
    nc.m_location = g_player_respwan_location[user_id];
    nc.m_rot = FloatVector3(0.f, 0.f, 0.f);
    nc.m_DirVector = FloatVector3(1.f, 0.f, 0.f);
    nc.m_MoveVector = FloatVector3(0.f, 0.f, 0.f);
    nc.m_status = ST_FREE;
    nc.m_role = R_NORMAL;
    nc.m_pstatus = ST_DEAD;
    nc.m_astatus = A_ST_IDLE;
    nc.m_revive_timer = 5;
    nc.m_velocity_accel = 1;
    nc.m_skill_accel = 1;
    nc.m_axis[0][0] = 1;
    nc.m_axis[0][1] = 0;
    nc.m_axis[0][2] = 0;
    nc.m_axis[1][0] = 0;
    nc.m_axis[1][1] = 1;
    nc.m_axis[1][2] = 0;
    nc.m_axis[2][0] = 0;
    nc.m_axis[2][1] = 0;
    nc.m_axis[2][2] = 1;
    nc.m_AliveTime = 0;
    nc.m_ForwardSpeed = 0;
    nc.m_RightSpeed = 0;
    nc.m_prev_size = 0;
    nc.picked_up = false;
    g_rad_x[user_id] = 0;
    g_rad_y[user_id] = 0;
}

void InitializeGame() {

    int i{};
    int j{};

    for (auto& obj : g_Obstacle) {
        if (obj.m_name == "GEMB") {
            g_Gems[i].m_lock.lock();
            g_Gems[i].m_cbox.m_location = obj.m_location;
            g_gems_origin_location[i] = obj.m_location;
            g_Gems[i].m_cbox.m_extent[0] = GEMSIZE;
            g_Gems[i].m_cbox.m_extent[1] = GEMSIZE;
            g_Gems[i].m_cbox.m_extent[2] = GEMSIZE;
            g_Gems[i].m_id = i;
            g_Gems[i].transparency_time = 0;
            g_Gems[i].transparency = false;
            g_Gems[i].fall_timer = 0;
            g_Gems[i].picked = false;
            g_Gems[i].thrown = false;
            g_Gems[i].picked_id = MAX_USER + 1;
            g_Gems[i].m_color = MagicColor::B;
            g_Gems[i].m_lock.unlock();
            ++i;
        }
        if (obj.m_name == "MSTB") {
            g_WitchStones[j].m_lock.lock();
            g_WitchStones[j].m_hp = WITCHSTONE_HP;
            g_WitchStones[j].m_c_box.m_location = obj.m_location;
            g_WitchStones[j].m_c_box.m_extent[0] = WITCHSTOENSIZE;
            g_WitchStones[j].m_c_box.m_extent[1] = WITCHSTOENSIZE;
            g_WitchStones[j].m_c_box.m_extent[2] = WITCHSTOENSIZE;
            g_WitchStones[j].m_shield = false;
            g_WitchStones[j].m_alive = true;
            g_WitchStones[j].m_color = MagicColor::B;
            g_WitchStones[j].m_lock.unlock();
            ++j;
        }

        if (obj.m_name == "GEMR") {
            g_Gems[i].m_lock.lock();
            g_Gems[i].m_cbox.m_location = obj.m_location;
            g_gems_origin_location[i] = obj.m_location;
            g_Gems[i].m_cbox.m_extent[0] = GEMSIZE;
            g_Gems[i].m_cbox.m_extent[1] = GEMSIZE;
            g_Gems[i].m_cbox.m_extent[2] = GEMSIZE;
            g_Gems[i].m_id = i;
            g_Gems[i].transparency_time = 0;
            g_Gems[i].transparency = false;
            g_Gems[i].fall_timer = 0;
            g_Gems[i].picked = false;
            g_Gems[i].thrown = false;
            g_Gems[i].picked_id = MAX_USER + 1;
            g_Gems[i].m_color = MagicColor::R;
            g_Gems[i].m_lock.unlock();
            ++i;
        }
        if (obj.m_name == "MSTR") {
            g_WitchStones[j].m_lock.lock();
            g_WitchStones[j].m_hp = WITCHSTONE_HP;
            g_WitchStones[j].m_alive = true;
            g_WitchStones[j].m_c_box.m_location = obj.m_location;
            g_WitchStones[j].m_c_box.m_extent[0] = WITCHSTOENSIZE;
            g_WitchStones[j].m_c_box.m_extent[1] = WITCHSTOENSIZE;
            g_WitchStones[j].m_c_box.m_extent[2] = WITCHSTOENSIZE;
            g_WitchStones[j].m_shield = false;
            g_WitchStones[j].m_color = MagicColor::R;
            g_WitchStones[j].m_lock.unlock();

            ++j;
        }

        if (obj.m_name == "GEMW") {
            g_Gems[i].m_lock.lock();
            g_Gems[i].m_cbox.m_location = obj.m_location;
            g_gems_origin_location[i] = obj.m_location;
            g_Gems[i].m_cbox.m_extent[0] = GEMSIZE;
            g_Gems[i].m_cbox.m_extent[1] = GEMSIZE;
            g_Gems[i].m_cbox.m_extent[2] = GEMSIZE;
            g_Gems[i].m_id = i;
            g_Gems[i].transparency_time = 0;
            g_Gems[i].transparency = false;
            g_Gems[i].fall_timer = 0;
            g_Gems[i].picked = false;
            g_Gems[i].thrown = false;
            g_Gems[i].picked_id = MAX_USER + 1;
            g_Gems[i].m_color = MagicColor::W;
            g_Gems[i].m_lock.unlock();
            ++i;
        }
        if (obj.m_name == "MSTW") {
            g_WitchStones[j].m_lock.lock();
            g_WitchStones[j].m_hp = WITCHSTONE_HP;
            g_WitchStones[j].m_alive = true;
            g_WitchStones[j].m_c_box.m_location = obj.m_location;
            g_WitchStones[j].m_c_box.m_extent[0] = WITCHSTOENSIZE;
            g_WitchStones[j].m_c_box.m_extent[1] = WITCHSTOENSIZE;
            g_WitchStones[j].m_c_box.m_extent[2] = WITCHSTOENSIZE;
            g_WitchStones[j].m_shield = false;
            g_WitchStones[j].m_color = MagicColor::W;
            g_WitchStones[j].m_lock.unlock();
            ++j;
        }


        if (obj.m_name == "GEMP") {
            g_Gems[i].m_lock.lock();
            g_Gems[i].m_cbox.m_location = obj.m_location;
            g_gems_origin_location[i] = obj.m_location;
            g_Gems[i].m_cbox.m_extent[0] = GEMSIZE;
            g_Gems[i].m_cbox.m_extent[1] = GEMSIZE;
            g_Gems[i].m_cbox.m_extent[2] = GEMSIZE;
            g_Gems[i].m_id = i;
            g_Gems[i].transparency_time = 0;
            g_Gems[i].transparency = false;
            g_Gems[i].fall_timer = 0;
            g_Gems[i].picked = false;
            g_Gems[i].thrown = false;
            g_Gems[i].picked_id = MAX_USER + 1;
            g_Gems[i].m_color = MagicColor::P;
            g_Gems[i].m_lock.unlock();
            ++i;
        }
        if (obj.m_name == "MSTP") {
            g_WitchStones[j].m_lock.lock();
            g_WitchStones[j].m_hp = WITCHSTONE_HP;
            g_WitchStones[j].m_c_box.m_location = obj.m_location;
            g_WitchStones[j].m_c_box.m_extent[0] = WITCHSTOENSIZE;
            g_WitchStones[j].m_c_box.m_extent[1] = WITCHSTOENSIZE;
            g_WitchStones[j].m_c_box.m_extent[2] = WITCHSTOENSIZE;
            g_WitchStones[j].m_shield = false;
            g_WitchStones[j].m_alive = true;
            g_WitchStones[j].m_color = MagicColor::P;
            g_WitchStones[j].m_lock.unlock();
            ++j;
        }
    }
    for (int i = 0; i < MAX_USER; ++i) {

        g_Skill_1_Flag[i].cool_time = 0;
        g_Skill_1_Flag[i].duration_time = 0;
        g_Skill_2_Flag[i].cool_time = 0;
        g_Skill_2_Flag[i].duration_time = 0;
    }
    g_number_of_witch_stone = g_MAX_WITCH_STONE_NUMBER;
    g_WitchSkill2.SetHaltStatus(SkillStatus::READY);
    g_WitchSkill2.m_CoolTime = 0;
    g_number_of_regen = SHARED_LIFE;
}

void disconnect(int user_id)
{
    sendLeavePacket(user_id, user_id);
    g_Clients[user_id].m_cl.lock();
    closesocket(g_Clients[user_id].m_s);
    InitializeClient(user_id);
    for (auto& cl : g_Clients) {
        if (cl.m_id == user_id) continue;
        //cl.m_cl.lock();
        if (ST_ACTIVE_GAME == g_Clients[cl.m_id].m_status)
            sendLeavePacket(cl.m_id, user_id);
        //cl.m_cl.unlock();
    }
    g_Clients[user_id].m_status = ST_FREE;
    g_Clients[user_id].m_cl.unlock();

    g_player_count--;

    cout << "disconnected\n";
}


void workerThread() {

    while (true) {

        DWORD io_byte;
        ULONG_PTR key;
        WSAOVERLAPPED* over;
        GetQueuedCompletionStatus(g_iocp, &io_byte, &key, &over, INFINITE);
        EXOVER* exover = reinterpret_cast<EXOVER*> (over);
        int user_id = static_cast<int>(key);
        CLIENT& cl = g_Clients[user_id];


        switch (exover->op) {
        case OP_RECV: {
            if (0 == io_byte)
                disconnect(user_id);
            else {
                recvPacketConstruct(user_id, io_byte);
                ZeroMemory(&cl.m_recv_over.over, sizeof(cl.m_recv_over.over));
                DWORD flags = 0;
                WSARecv(cl.m_s, &cl.m_recv_over.wsabuf, 1, NULL, &flags, &cl.m_recv_over.over, NULL);
            }
            break;
        }

        case OP_SEND:
            if (0 == io_byte)
                disconnect(user_id);
            delete exover;
            break;


        case OP_ACCEPT:
        {
            int user_id = -1;
            for (int i = 0; i < MAX_USER; ++i) {
                lock_guard<mutex> gl{ g_Clients[i].m_cl };
                if (ST_FREE == g_Clients[i].m_status) {
                    g_Clients[i].m_status = ST_ALLOC;
                    user_id = i;
                    break;
                }
            }
            cout << "ACCETED:" << user_id << '\n';

            SOCKET c_socket = exover->c_socket;
            if (-1 == user_id)
                closesocket(c_socket);
            else {

                CreateIoCompletionPort(reinterpret_cast<HANDLE>(c_socket), g_iocp, user_id, 0);
                CLIENT& nc = g_Clients[user_id];
                nc.m_cl.lock();
                nc.m_prev_size = 0;
                nc.m_recv_over.op = OP_RECV;
                ZeroMemory(&nc.m_recv_over.over, sizeof(nc.m_recv_over.over));
                nc.m_recv_over.wsabuf.buf = nc.m_recv_over.io_buf;
                nc.m_recv_over.wsabuf.len = MAX_BUFFER_SIZE;
                nc.m_s = c_socket;
                nc.m_location = g_player_respwan_location[user_id];
                //cout << user_id << ": x:" << nc.m_location.x << ", y:" << nc.m_location.y << ", z:" << nc.m_location.z << "\n";
                nc.m_id = user_id;
                nc.m_status = ST_ACTIVE_GAME;
                nc.m_astatus = A_ST_IDLE;
                nc.m_pstatus = ST_ALIVE;
                nc.m_DirVector = FloatVector3(1.f, 0.f, 0.f);
                if (!witch_flag) {
                    nc.m_role = R_WITCH;
                    nc.m_speed = WITCH_SPEED;
                    g_witch_id= user_id;
                    witch_flag = true;
                }
                else {
                    nc.m_role = R_NORMAL;
                    nc.m_speed = NORMAL_SPEED;
                }
                ZeroMemory(&nc.m_recv_over.over, sizeof(nc.m_recv_over.over));
                nc.m_recv_over.wsabuf.buf = nc.m_recv_over.io_buf + nc.m_prev_size;
                nc.m_recv_over.wsabuf.len = MAX_BUFFER_SIZE - nc.m_prev_size;
                nc.m_cl.unlock();
                DWORD flags = 0;
                WSARecv(nc.m_s, &nc.m_recv_over.wsabuf, 1, NULL, &flags, &nc.m_recv_over.over, NULL);

            }
            c_socket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
            exover->c_socket = c_socket;
            ZeroMemory(&exover->over, sizeof(exover->over));
            AcceptEx(g_ListenSocket, c_socket, exover->io_buf, NULL, sizeof(sockaddr_in) + 16, sizeof(sockaddr_in) + 16, NULL, &exover->over);


        }
        break;
        case OP_UPDATE:
        {
            char actvie_player_num{};
            soul_lock.lock();
            int soul_left = g_number_of_regen;
            soul_lock.unlock();
            static A_STATUS previous_st[MAX_USER];
            static float previois_ch_power[MAX_USER];
            int witch_stone_hp_sum{};
            for (int i = 0; i < MAX_USER; ++i) {
                g_Clients[i].m_cl.lock();
                previous_st[i] = g_Clients[i].m_astatus;
                previois_ch_power[i] = g_Clients[i].m_char_power;
                g_Clients[i].m_cl.unlock();
            }


            for (int i = 0; i < g_MAX_WITCH_STONE_NUMBER; ++i) {
                g_WitchStones[i].m_lock.lock();
                witch_stone_hp_sum += g_WitchStones[i].m_hp;
                g_WitchStones[i].m_lock.unlock();
            }


            GameCore::Update(*game);

            int after_witch_stone_hp_sum{};
            for (int i = 0; i < g_MAX_WITCH_STONE_NUMBER; ++i) {
                g_WitchStones[i].m_lock.lock();
                after_witch_stone_hp_sum += g_WitchStones[i].m_hp;
                g_WitchStones[i].m_lock.unlock();
            }

            for (int i = 0; i < GAME_START_NUMBER; ++i) {
                if (g_Clients[i].m_status == ST_ACTIVE_GAME) {
                    sendUpdatePacket(i);
                    actvie_player_num++;
                }

                if (previois_ch_power[i] != g_Clients[i].m_char_power) {
                    float temp_speed_power = static_cast<int>((g_Clients[i].m_velocity_accel - 1) * 100);
                    float temp_skill_power = static_cast<int>((g_Clients[i].m_skill_accel - 1) * 100);
                    sendCHPowerChagenPacket(i, temp_speed_power, temp_skill_power);

                }

                if (soul_left != g_number_of_regen)
                    sendSoulLeftPacket(i);

                if (witch_stone_hp_sum != after_witch_stone_hp_sum) {
                    sendWitchStoneSTChange(i);
                }

                if (previous_st[i] != g_Clients[i].m_astatus)
                    for(int j=0; j < GAME_START_NUMBER; ++j)
                        sendAnimationChagnePacket(j,i);
                
            }

            EXOVER updateover;
            updateover.op = OP_UPDATE;

            if (g_number_of_witch_stone <= 2) {

                for (int i = 0; i < GAME_START_NUMBER; ++i) {
                    if (g_Clients[i].m_status == ST_ACTIVE_GAME) {
                        sendResultPacket(i, R_NORMAL);
                        g_UpdateFlag = false;
                        witch_flag = false;
                        InitializeGame();
                    }
                }

            }
            else if (g_number_of_regen <= 0) {
                for (int i = 0; i < GAME_START_NUMBER; ++i) {
                    if (g_Clients[i].m_status == ST_ACTIVE_GAME) {
                        sendResultPacket(i, R_WITCH);
                        g_UpdateFlag = false;
                        witch_flag = false;
                        InitializeGame();
                    }
                }
            }  else if (actvie_player_num == 0) {
                g_UpdateFlag = false;
                witch_flag = false;
                InitializeGame();
                break;
            }
            else {

                PostQueuedCompletionStatus(g_iocp, 1, MAX_USER, &updateover.over);
            }


            break;
        }
        default:
            break;
        }
    }
}

void set_respwan()
{
    //g_player_respwan_location[0] = FloatVector3(0, 0, 40);
    //g_player_respwan_location[1] = FloatVector3(21, 0, -50);
    //g_player_respwan_location[2] = FloatVector3(30, 0, -50);
    //g_player_respwan_location[3] = FloatVector3(50, 0, -30);
    //g_player_respwan_location[4] = FloatVector3(50, 0, -22);

    g_player_respwan_location[0] = FloatVector3(0, 0, 30);      // 마녀..?
    g_player_respwan_location[1] = FloatVector3(0, 0, 40);
    g_player_respwan_location[2] = FloatVector3(30, 0, -50);
    g_player_respwan_location[3] = FloatVector3(50, 0, -30);
    g_player_respwan_location[4] = FloatVector3(50, 0, -22);

}

int main()
{
    GameCore::Initialize(*game);
    set_respwan();
    InitializeGame();
    CreateIoCompletionPort(reinterpret_cast<HANDLE> (g_ListenSocket), g_iocp, 999, 0);
    SOCKET c_socket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
    unique_ptr <EXOVER> accept_over(new EXOVER);
    //EXOVER  accept_over;

    ZeroMemory(&accept_over->over, sizeof(accept_over->over));
    accept_over->op = OP_ACCEPT;
    accept_over->c_socket = c_socket;
    AcceptEx(g_ListenSocket, c_socket, accept_over->io_buf, NULL, sizeof(sockaddr_in) + 16, sizeof(sockaddr_in) + 16, NULL, &accept_over->over);

    vector <thread> worker_threads;
    for (unsigned int i = 0; i < thread::hardware_concurrency(); ++i) worker_threads.emplace_back(workerThread);
    for (auto& th : worker_threads) th.join();

    WSACleanup();
}