#pragma once
#include "SystemNode.h"

class SystemCommunicator;

class NetworkSystem : SystemNode
{



public:
	explicit NetworkSystem(SystemCommunicator* const communicator);
	~NetworkSystem();

public:
	void Initialize() override;

private:
	static bool s_Instantiated;

};
