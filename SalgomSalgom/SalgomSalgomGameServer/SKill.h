#pragma once

//
//class SKill abstract
//{
//protected:
//    //Skill();
//	//virtual ~SKill();
//
//    //쿨타임을 여기서 정의하고 getcooltime같은거 쓰고싶은데 구조상 뭔가..?
//    //스킬 1 2 플래그로 따로 접근해야 하니 일단 기다리자.
//
//private:
//    //쿨타임 값
//    //time 값
//};

using namespace DirectX;

struct Sphere {
    float high; //구체가 공중에 뜨는 높이(바닥으로부터)
    float dist; //구체 거리(플레이어와 xz평면상으로 얼마나 떨어져있나??)
    float velocity; //발사되고 끌어당겨지는 속도

    float spScale; //구체 스케일이 줄어드는 비율
    float radius; //적 끌어당기는 범위 r
    float areaGap;

    Sphere(float _high, float _dist, float _speed, float _spScale, float _radius, float _areaGap) :
        high(_high), dist(_dist), velocity(_speed), spScale(_spScale), radius(_radius), areaGap(_areaGap) {};

};

constexpr float HALT_MOVETIME = 1.f;
constexpr float HALT_PULLTIME = 0.5f;
constexpr float HALT_PULLRADIUS_SQ = 6.f * 6.f;


//꼼짝마!
class Halt {
public:
    Halt();
    ~Halt();

public:
    void InitMove(const FloatVector3& witchPos, const FloatVector3& witchFowardVec);

    void InitArrive(vector<CLIENT*>& normal_bears);

    void Update(float elapsedTime, bool& move_timeover);
    void Move(float elapsedTime);


public:    
    SkillStatus GetSKillStatus() { return m_Status; }
    void SetHaltStatus (SkillStatus curstatus) { m_Status = curstatus; }

    XMFLOAT3 GetPos();

public:
    atomic<SkillStatus> m_Status;
    float m_DurationTime;
    float m_CoolTime;
    mutex m_Lock;

private:
    XMFLOAT3 m_CurrPos;

    XMFLOAT3 m_HaltPos;
    XMFLOAT3 m_Direction;
    XMFLOAT3 m_SphereScale;
    int m_ArriveCount;      // 구체에 도착한 노말곰 수

    map<int, XMVECTOR> m_NormalPosInfo;
    Sphere m_HaltInfo;


};
