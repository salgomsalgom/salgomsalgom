#pragma once

class SystemCommunicator;

class SystemNode abstract
{
protected:
	explicit SystemNode(SystemCommunicator* communicator);
	virtual ~SystemNode();

public:
	virtual void Initialize() = 0;

private:
	SystemCommunicator* const m_Communicator;
};
