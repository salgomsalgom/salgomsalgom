#pragma warning(disable:4996)
#include "pch.h"
#include "DaeParser.h"

using namespace std;

DaeParser::DaeParser()
    : m_Mesh(nullptr)
    , m_Skin(nullptr)
    , rigged(false)
{

}

DaeParser::~DaeParser() { }

void DaeParser::Parse(const string& filename)
{
    TiXmlDocument document;
    string with_ex = filename + ".dae"s;

    if (false == document.LoadFile(with_ex.c_str())) {
        cout << with_ex << " - 파일을 찾을 수 없음" << endl;
        return;
    }

    const TiXmlElement* library_geometries = document.RootElement()->FirstChildElement("library_geometries");
    const TiXmlElement* library_controllers = document.RootElement()->FirstChildElement("library_controllers");
    const TiXmlElement* library_visual_scenes = document.RootElement()->FirstChildElement("library_visual_scenes");

    // 있는 것만 데이터 가져온다.
    if (nullptr != library_geometries) ParseLibraryGeometries(library_geometries);
    if (nullptr != library_controllers) {
        rigged = true;
        ParseLibraryControllers(library_controllers);
        ParseLibraryVisualScenes(library_visual_scenes);
    }
}

void DaeParser::ParseAnifile(const string& filename)
{
    ifstream in{ filename + ".ani", ios_base::binary };
    if (!in) return;

    // bone matrix
    int num_bones;
    in.read((char*)&num_bones, sizeof(int));

    for (int i = 0; i < num_bones; ++i)
    {
        char bone_name[11];
        bone_name[10] = '\0';
        DirectX::XMFLOAT4X4 mat;

        in.read(bone_name, sizeof(char) * 10);
        in.read((char*)&mat, sizeof(mat));

        m_ChildToParentMat[bone_name] = mat;
    }

    // key
    int num_ani;
    in.read((char*)&num_ani, sizeof(int));

    for (int i = 0; i < num_ani; ++i)
    {
        char ani_name[11];
        ani_name[10] = '\0';
        int num_keys;

        in.read(ani_name, sizeof(char) * 10);
        in.read((char*)&num_keys, sizeof(int));

        vector<pair<string, vector<Keyframe>>> action;

        for (int j = 0; j < num_bones; ++j)
        {
            char bone_name[11];
            bone_name[10] = '\0';
            in.read(bone_name, sizeof(char) * 10);

            vector<Keyframe> keyframes;

            for (int k = 0; k < num_keys; ++k)
            {
                Keyframe key;
                in.read((char*)&key, sizeof(key));
                keyframes.push_back(key);
            }

            action.push_back(make_pair(bone_name, keyframes));
        }

        m_AnifileData.push_back(make_pair(ani_name, action));

        // 애니 인포 따로 저장
        AniInfo info;
        memcpy(info.name, ani_name, sizeof(char) * 10);
        info.num_keys = num_keys;

        m_AniInfo.push_back(info);
    }
}

inline DirectX::XMFLOAT3 Normalize(const DirectX::XMFLOAT3& xmf3Vector)
{
    using namespace DirectX;
    XMFLOAT3 m_xmf3Normal;
    XMStoreFloat3(&m_xmf3Normal, XMVector3Normalize(XMLoadFloat3(&xmf3Vector)));
    return(m_xmf3Normal);
}

void DaeParser::GenerateD2b(D2bBuf& dst)
{
    if (rigged) {
        // skinned vertex로 구성해야함
        std::vector<float> v;
        std::vector<float> n;
        std::vector<float> t;
        std::vector<unsigned int> indices;
        std::vector<float> weights;
        std::vector<int> joints;

        m_Mesh->GetSourceData(Semantic::POSITION(), v, true);
        m_Mesh->GetSourceData(Semantic::NORMAL(), n, true);
        m_Mesh->GetSourceData(Semantic::TEXCOORD(), t, true);
        m_Mesh->GetIndexData(indices);
        m_Skin->GetWeightData(weights);
        m_Skin->GetBoneData(joints);


        int loop_size = m_Mesh->GetNumTriangles() * 3;

        // 노말 계산
        vector<unsigned int> vertex_indices;                            // position의 인덱스만 저장
        m_Mesh->GetIndexData(Semantic::VERTEX(), vertex_indices);
        std::vector<DirectX::XMFLOAT3> vn;
        vn.resize(v.size() / 3);      // vertex normal

        for (int i = 0; i < n.size() / 3; ++i) {
            int mapped_idx = i * 3;

            // 삼각형을 이루는 점들의 인덱스를 가져온다.
            int idx0 = vertex_indices.at(mapped_idx + 0);     // 0,3,6,...
            int idx1 = vertex_indices.at(mapped_idx + 1);     // 1,4,7...
            int idx2 = vertex_indices.at(mapped_idx + 2);     // 2,5,8....

            DirectX::XMFLOAT3 face_normal = DirectX::XMFLOAT3(n[mapped_idx], n[mapped_idx + 1], n[mapped_idx + 2]);

            // 해당 점에다가 삼각형의 노말을 더한다. 
            vn[idx0].x += face_normal.x;
            vn[idx1].x += face_normal.x;
            vn[idx2].x += face_normal.x;
            vn[idx0].y += face_normal.y;
            vn[idx1].y += face_normal.y;
            vn[idx2].y += face_normal.y;
            vn[idx0].z += face_normal.z;
            vn[idx1].z += face_normal.z;
            vn[idx2].z += face_normal.z;
        }

        for (int i = 0; i < vn.size(); i++) vn[i] = Normalize(vn[i]);

        vector<SkinnedVertex> triangle_v;
        triangle_v.reserve(loop_size);

        for (int i = 0; i < loop_size; ++i) {
            int v_idx = indices[i * 3 + 0];
            // int n_idx = indices[i * 3 + 1];
            int n_idx = v_idx;
            int t_idx = indices[i * 3 + 2];

            SkinnedVertex sv;
            sv.pos.x = v[v_idx * 3 + 0];
            sv.pos.y = v[v_idx * 3 + 1];
            sv.pos.z = v[v_idx * 3 + 2];

            sv.nor.x = vn[n_idx].x;
            sv.nor.y = vn[n_idx].y;
            sv.nor.z = vn[n_idx].z;

            sv.tex.x = t[t_idx * 2 + 0];
            sv.tex.y = t[t_idx * 2 + 1];

            sv.weights.x = weights[v_idx * 4 + 0];
            sv.weights.y = weights[v_idx * 4 + 1];
            sv.weights.z = weights[v_idx * 4 + 2];

            sv.boneIdx[0] = joints[v_idx * 4 + 0];
            sv.boneIdx[1] = joints[v_idx * 4 + 1];
            sv.boneIdx[2] = joints[v_idx * 4 + 2];
            sv.boneIdx[3] = joints[v_idx * 4 + 3];

            triangle_v.push_back(sv);
        }

        // 버텍스, 인덱스 버퍼 만들기
        vector<SkinnedVertex> dst_v;
        vector<unsigned short> dst_i;

        int index = 0;
        for (int i = 0; i < loop_size; ++i) {
            bool overlap = false;

            for (int j = 0; j < dst_v.size(); ++j) {
                if (dst_v[j] == triangle_v[i]) {
                    dst_i.push_back(j);
                    overlap = true;
                    break;
                }
            }

            if (overlap) continue;

            dst_v.push_back(triangle_v[i]);
            dst_i.push_back(index++);
        }

        // bone 구성
        vector<Bone> bone_info;
        for (auto& b : m_BoneInfo)
        {
            Bone bone;
            bone.index = b.index;
            bone.parentIndex = b.parentIndex;
            bone.offsetMat = b.offsetMat;
            bone.childToParent = m_ChildToParentMat[b.name];

            bone_info.push_back(bone);
        }

        // ani 구성
        vector<Keyframe> keyframes;
        int total_key_count = 0;
        for (auto& [ani_name, action] : m_AnifileData) {
            for (auto& [b_name, b_ani] : action) {
                total_key_count += b_ani.size();
            }

            // bone 순서에 맞춰야함
            for (auto& b : m_BoneInfo) {
                for (auto& [b_name, b_ani] : action) {
                    if (b.name == b_name) {
                        for (auto& key : b_ani) keyframes.push_back(key);
                        break;
                    }
                }
            }
        }

        // dst 채우기
        dst.header.v_type = VertexType::SKINNED;
        dst.header.rigged = rigged;
        dst.header.v_count = dst_v.size();
        dst.header.i_count = dst_i.size();
        dst.header.b_count = bone_info.size();
        dst.header.ani_count = m_AniInfo.size();
        dst.header.key_count = total_key_count;

        dst.v_buf = new SkinnedVertex[dst_v.size()];
        dst.i_buf = new Index[dst_i.size()];
        dst.b_buf = new Bone[m_BoneInfo.size()];
        dst.ani_buf = new AniInfo[m_AniInfo.size()];
        dst.key_buf = new Keyframe[total_key_count];
        memcpy(dst.v_buf, dst_v.data(), sizeof(SkinnedVertex) * dst_v.size());
        memcpy(dst.i_buf, dst_i.data(), sizeof(Index) * dst_i.size());
        memcpy(dst.b_buf, bone_info.data(), sizeof(Bone) * m_BoneInfo.size());
        memcpy(dst.ani_buf, m_AniInfo.data(), sizeof(AniInfo) * m_AniInfo.size());
        memcpy(dst.key_buf, keyframes.data(), sizeof(Keyframe) * keyframes.size());
    }
    else {
        // static vertex로 구성해야함
        std::vector<float> v;
        std::vector<float> n;
        std::vector<float> t;

        m_Mesh->GetSourceData(Semantic::POSITION(), v, true);
        m_Mesh->GetSourceData(Semantic::NORMAL(), n, true);
        m_Mesh->GetSourceData(Semantic::TEXCOORD(), t, true);

        std::vector<unsigned int> indices;
        m_Mesh->GetIndexData(indices);

        int pos_size = v.size() / 3;
        int loop_size = m_Mesh->GetNumTriangles() * 3;

        // 노말 구하기
        vector<unsigned int> vertex_indices;
        m_Mesh->GetIndexData(Semantic::VERTEX(), vertex_indices); // position의 인덱스만 저장

        std::vector<DirectX::XMFLOAT3> vn;      // vertex normal
        vn.resize(pos_size);

        for (Index i = 0; i < n.size() / 3; ++i) {
            Index mapped_idx = i * 3;

            // 삼각형을 이루는 점들의 인덱스를 가져온다.
            Index idx0 = vertex_indices.at(mapped_idx + 0);     // 0,3,6,...
            Index idx1 = vertex_indices.at(mapped_idx + 1);     // 1,4,7...
            Index idx2 = vertex_indices.at(mapped_idx + 2);     // 2,5,8....

            DirectX::XMFLOAT3 face_normal = DirectX::XMFLOAT3(n[mapped_idx], n[mapped_idx + 1], n[mapped_idx + 2]);

            // 해당 점에다가 삼각형의 노말을 더한다.
            vn[idx0].x += face_normal.x;
            vn[idx1].x += face_normal.x;
            vn[idx2].x += face_normal.x;
            vn[idx0].y += face_normal.y;
            vn[idx1].y += face_normal.y;
            vn[idx2].y += face_normal.y;
            vn[idx0].z += face_normal.z;
            vn[idx1].z += face_normal.z;
            vn[idx2].z += face_normal.z;
        }

        for (Index i = 0; i < vn.size(); i++) vn[i] = Normalize(vn[i]);

        std::vector<StaticVertex> triangle_v;
        triangle_v.reserve(loop_size);

        for (int i = 0; i < loop_size; ++i) {
            int v_idx = indices[i * 3 + 0];
            int n_idx = v_idx;
            int t_idx = indices[i * 3 + 2];

            StaticVertex vertex;
            vertex.pos.x = v[v_idx * 3 + 0];
            vertex.pos.y = v[v_idx * 3 + 1];
            vertex.pos.z = -v[v_idx * 3 + 2];

            // 노말의 인덱스는 점의 인덱스와 같다.
            vertex.nor.x = vn[n_idx].x;
            vertex.nor.y = vn[n_idx].y;
            vertex.nor.z = -vn[n_idx].z;

            vertex.tex.x = t[t_idx * 2 + 0];
            vertex.tex.y = t[t_idx * 2 + 1];

            triangle_v.push_back(vertex);
        }

        for (int i = 0; i < triangle_v.size(); i += 3)
        {
            auto& p1 = triangle_v[i + 0];
            auto& p2 = triangle_v[i + 1];
            auto& p3 = triangle_v[i + 2];

            auto tmp = p2;
            p2 = p3;
            p3 = tmp;
        }

        // 버텍스, 인덱스 버퍼 만들기
        vector<StaticVertex> dst_v;
        vector<unsigned short> dst_i;

        int index = 0;
        for (int i = 0; i < loop_size; ++i) {
            bool overlap = false;

            for (int j = 0; j < dst_v.size(); ++j) {
                if (dst_v[j] == triangle_v[i]) {
                    dst_i.push_back(j);
                    overlap = true;
                    break;
                }
            }

            if (overlap) continue;

            dst_v.push_back(triangle_v[i]);
            dst_i.push_back(index++);
        }

        // dst 채우기
        dst.header.v_type = VertexType::STATIC;
        dst.header.rigged = rigged;
        dst.header.v_count = dst_v.size();
        dst.header.i_count = dst_i.size();
        dst.header.b_count = 0;
        dst.header.ani_count = 0;
        dst.header.key_count = 0;

        dst.v_buf = new StaticVertex[dst_v.size()];
        dst.i_buf = new Index[dst_i.size()];
        dst.b_buf = nullptr;
        dst.ani_buf = nullptr;
        dst.key_buf = nullptr;
        memcpy(dst.v_buf, dst_v.data(), sizeof(StaticVertex)* dst_v.size());
        memcpy(dst.i_buf, dst_i.data(), sizeof(Index)* dst_i.size());
    }
}

void DaeParser::GenerateD2bNoNormalCalc(D2bBuf& dst)
{
    // static vertex로 구성해야함
    std::vector<float> v;
    std::vector<float> n;
    std::vector<float> t;

    m_Mesh->GetSourceData(Semantic::POSITION(), v, true);
    m_Mesh->GetSourceData(Semantic::NORMAL(), n, true);
    m_Mesh->GetSourceData(Semantic::TEXCOORD(), t, true);

    std::vector<unsigned int> indices;
    m_Mesh->GetIndexData(indices);

    int loop_size = m_Mesh->GetNumTriangles() * 3;

    std::vector<StaticVertex> triangle_v;
    triangle_v.reserve(loop_size);

    for (int i = 0; i < loop_size; ++i) {
        int v_idx = indices[i * 3 + 0];
        int n_idx = indices[i * 3 + 1];
        int t_idx = indices[i * 3 + 2];

        StaticVertex vertex;
        vertex.pos.x = v[v_idx * 3 + 0];
        vertex.pos.y = v[v_idx * 3 + 1];
        vertex.pos.z = -v[v_idx * 3 + 2];

        // 노말의 인덱스는 점의 인덱스와 같다.
        vertex.nor.x = n[n_idx * 3 + 0];
        vertex.nor.y = n[n_idx * 3 + 1];
        vertex.nor.z = -n[n_idx * 3 + 2];

        vertex.tex.x = t[t_idx * 2 + 0];
        vertex.tex.y = t[t_idx * 2 + 1];

        triangle_v.push_back(vertex);
    }

    for (int i = 0; i < triangle_v.size(); i += 3)
    {
        auto& p1 = triangle_v[i + 0];
        auto& p2 = triangle_v[i + 1];
        auto& p3 = triangle_v[i + 2];
    
        auto tmp = p2;
        p2 = p3;
        p3 = tmp;
    }

    // 버텍스, 인덱스 버퍼 만들기
    vector<StaticVertex> dst_v;
    vector<unsigned short> dst_i;

    int index = 0;
    for (int i = 0; i < loop_size; ++i) {
        bool overlap = false;

        for (int j = 0; j < dst_v.size(); ++j) {
            if (dst_v[j] == triangle_v[i]) {
                dst_i.push_back(j);
                overlap = true;
                break;
            }
        }

        if (overlap) continue;

        dst_v.push_back(triangle_v[i]);
        dst_i.push_back(index++);
    }

    // dst 채우기
    dst.header.v_type = VertexType::STATIC;
    dst.header.rigged = rigged;
    dst.header.v_count = dst_v.size();
    dst.header.i_count = dst_i.size();
    dst.header.b_count = 0;
    dst.header.ani_count = 0;
    dst.header.key_count = 0;

    dst.v_buf = new StaticVertex[dst_v.size()];
    dst.i_buf = new Index[dst_i.size()];
    dst.b_buf = nullptr;
    dst.ani_buf = nullptr;
    dst.key_buf = nullptr;
    memcpy(dst.v_buf, dst_v.data(), sizeof(StaticVertex) * dst_v.size());
    memcpy(dst.i_buf, dst_i.data(), sizeof(Index) * dst_i.size());
}

void DaeParser::ParseLibraryGeometries(const TiXmlElement* const library)
{
    if (nullptr == library) return;
    if ("library_geometries" == library->Value()) return;

    const TiXmlElement* geometry = library->FirstChildElement("geometry");
         
    const TiXmlElement* mesh = geometry->FirstChildElement("mesh");

    m_Mesh = new DaeType::MeshElement(mesh);

    // m_Mesh->PrintData();
}

DirectX::XMFLOAT4X4 GetMatrix4x4(const TiXmlElement* matrixElement) {
    stringstream ss{ matrixElement->GetText() };
    DirectX::XMFLOAT4X4 xm_mat;

    for (int r = 0; r < 4; ++r) {
        for (int c = 0; c < 4; ++c) {
            float ele;
            ss >> ele;
            xm_mat.m[r][c] = ele;
        }
    }

    return xm_mat;
}

void GetMatrix4x4(const TiXmlElement* matrixElement, vector<DirectX::XMFLOAT4X4>& dst) {
    string str_count{ matrixElement->Attribute("count") };
    int mat_count{ stoi(str_count) / 16 };

    stringstream ss{ matrixElement->GetText() };
    DirectX::XMFLOAT4X4 xm_mat;

    for (int i = 0; i < mat_count; ++i) {
        for (int r = 0; r < 4; ++r) {
            for (int c = 0; c < 4; ++c) {
                float ele;
                ss >> ele;
                xm_mat.m[r][c] = ele;
            }
        }
        dst.push_back(xm_mat);
    }

}


void DaeParser::ParseLibraryControllers(const TiXmlElement* const library)
{
    if (nullptr == library) return;
    if ("library_controllers" == library->Value()) return;

    // 여기서 뼈 이름과 skin bind pose, weight를 얻을 수 있다.

    const TiXmlElement* controller = library->FirstChildElement("controller");

    const TiXmlElement* skin = controller->FirstChildElement("skin");

    m_Skin = new DaeType::SkinElement(skin);

    // 뼈 이름 알아오기
    const TiXmlElement* name_array = skin->FirstChildElement("source")
        ->FirstChildElement("Name_array");

    stringstream contents{ name_array->GetText() };
    vector<string> names;
    string name;

    while (contents >> name) names.push_back(name);

    for (int i = 0; i < names.size(); ++i)
        m_BoneIndexMapping.emplace(names[i], i);

    // InvBindMatrix 얻기
    const TiXmlElement* source_mat = skin->FirstChildElement("source")->NextSiblingElement()
        ->FirstChildElement("float_array");


    vector<DirectX::XMFLOAT4X4> inv_bind_matrixs;
    GetMatrix4x4(source_mat, inv_bind_matrixs);

    // 이름 순서에 맞춤
    for (int i = 0; i < names.size(); ++i)
        m_BoneInvBindMatrixMapping.emplace(names[i], inv_bind_matrixs[i]);

}

void DaeParser::ParseLibraryVisualScenes(const TiXmlElement* const library)
{
    // 계층 구조를 얻기 위한 파싱
    if (nullptr == library) return;
    if ("library_visual_scenes" == library->Value()) return;

    auto vs = library->FirstChildElement("visual_scene");
    auto arma = vs->FirstChildElement("node");

    // 루트본이라고 확신할 수 없음
    auto root_bone = arma->FirstChildElement("node");
    auto transform_mat = root_bone->FirstChildElement("matrix");
    DaeBone bone;
    strcpy(bone.name, root_bone->Attribute("name"));
    bone.index = 0;
    bone.parentIndex = -1;
    bone.offsetMat = m_BoneInvBindMatrixMapping[bone.name];


    // 둘이 같은 인덱스
    m_BoneInfo.push_back(bone);

    m_BoneNameHierarchy.emplace_back(string(root_bone->Attribute("name")), "ROOTBONE"s);

    StoreChildBones(root_bone);

    // cout << "bone count: " << m_BoneInfo.size() << endl;
    // for (auto& ele : m_BoneInfo) {
    //     cout << "parent - " << ele.parentIndex << ", child - " << ele.index << endl;
    // }
    // 
    // for (auto& [ch, p] : m_BoneNameHierarchy) {
    //     cout << "parent - " << p << ", child - " << ch << endl;
    // }
}

void DaeParser::StoreChildBones(const TiXmlElement* parant) {
    auto child = parant->FirstChildElement("node");
    
    while (true) {
        if (nullptr == child) return;   // 자식이 없음
        if (string("JOINT") != child->Attribute("type")) return;

        string ch_name = child->Attribute("sid");
        string p_name = parant->Attribute("sid");

        int ch_idx = m_BoneIndexMapping[ch_name];
        int p_idx = m_BoneIndexMapping[p_name];

        auto transform_mat = child->FirstChildElement("matrix");

        DaeBone bone;
        strcpy(bone.name, child->Attribute("name"));
        bone.index = ch_idx;
        bone.parentIndex = p_idx;
        bone.offsetMat = m_BoneInvBindMatrixMapping[ch_name];

        m_BoneInfo.push_back(bone);
        
        m_BoneNameHierarchy.emplace_back(ch_name, p_name);

        StoreChildBones(child);

        child = child->NextSiblingElement("node");
    }
}
