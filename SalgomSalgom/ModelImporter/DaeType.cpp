#include "pch.h"
#include "DaeType.h"


using namespace std;

DaeType::SourceElement::SourceElement(const TiXmlElement* const sourceElement) :
	m_SourceID(sourceElement->Attribute("id")),
	m_ArrayCount(0),
	m_Array(nullptr)
{
	// array [0..1]
	const TiXmlElement* float_array = sourceElement->FirstChildElement("float_array");
	if (nullptr == float_array) return;

	stringstream ss{ float_array->GetText() };
	m_ArrayCount = stoi(string(float_array->Attribute("count")));

	m_Array = new float[m_ArrayCount];
	for (int i = 0; i < m_ArrayCount; ++i) {
		ss >> m_Array[i];
	}
}

DaeType::SourceElement::SourceElement(const SourceElement& other) :
	m_SourceID(other.m_SourceID),
	m_ArrayCount(other.m_ArrayCount),
	m_Array(nullptr)
{
	m_Array = new float[m_ArrayCount];
	memcpy(m_Array, other.m_Array, sizeof(float) * m_ArrayCount);
}

DaeType::SourceElement::SourceElement(SourceElement&& other) noexcept :
	m_SourceID(other.m_SourceID),
	m_ArrayCount(other.m_ArrayCount), 
	m_Array(nullptr)
{
	if (this == &other) return;

	m_Array = other.m_Array;
	other.m_Array = nullptr;
}

DaeType::SourceElement::~SourceElement()
{
	if (nullptr != m_Array)
		delete[] m_Array;
	m_Array = nullptr;
}

void DaeType::SourceElement::PrintData() const
{
	cout << "-Source Info: " << m_SourceID << ", count: " << m_ArrayCount << endl;
}

DaeType::InputElement::InputElement(const TiXmlElement* const inputElement) :
	m_Semantic(Semantic::Generate(string(inputElement->Attribute("semantic")))),
	m_Source(string(inputElement->Attribute("source"))),
	m_Offset(0),
	m_Set(0)
{
	const char* offset = inputElement->Attribute("offset");

	if (nullptr != offset) {
		m_Offset = stoul(string(offset));
	}

	const char* set = inputElement->Attribute("set");

	if (nullptr != set) {
		m_Set = stoul(string(set));
	}
}

void DaeType::InputElement::PrintData() const
{
	cout << "-Input Info: " << m_Semantic.GetValue() << " " << m_Source << endl;
}


DaeType::VerticesElement::VerticesElement(const TiXmlElement* const verticesElement) :
	m_Input(verticesElement->FirstChildElement("input"))
{
}

DaeType::VerticesElement::~VerticesElement()
{
}

void DaeType::VerticesElement::PrintData()
{
	cout << "Vertices Info: " << endl;
	m_Input.PrintData();
}

const DaeType::InputElement* DaeType::VerticesElement::GetInput(const Semantic& semantic) const
{
	if (semantic == m_Input.GetSemantic()) return &m_Input;
	return nullptr;
}

DaeType::TrianglesElement::TrianglesElement(const TiXmlElement* const trianglesElement) :
	m_Count(0),
	m_Inputs(),
	m_Primitive()
{
	m_Count = stoi(string(trianglesElement->Attribute("count")));
	
	const TiXmlElement* input = trianglesElement->FirstChildElement("input");

	while (true) {
		if (nullptr == input) break;
		if ("input"s != string(input->Value())) break;

		m_Inputs.emplace_back(input);

		input = input->NextSiblingElement();
	}

	const TiXmlElement* p = trianglesElement->FirstChildElement("p");
	stringstream ss{ p->GetText() };

	unsigned int i;
	while (ss >> i)
		m_Primitive.push_back(i);
}

DaeType::TrianglesElement::~TrianglesElement()
{
}

void DaeType::TrianglesElement::PrintData()
{
	cout << "Triangle Info: " << endl;
	cout << m_Count << endl;

	for (auto& input : m_Inputs) {
		input.PrintData();
	}
}

const DaeType::InputElement* DaeType::TrianglesElement::GetInput(const Semantic& semantic) const
{
	for (const auto& input : m_Inputs) {
		if (semantic == input.GetSemantic()) return &input;
	}
	return nullptr;
}

void DaeType::TrianglesElement::GetIndexSet(const Semantic& semantic, vector<unsigned int>& dst)
{
	// 1. semantic에 맞는 input을 찾는다.
	DaeType::InputElement* target_input{ nullptr };
	
	for (auto& input : m_Inputs) {
		if (input.GetSemantic() == semantic)
			target_input = &input;
	}

	if (nullptr == target_input) return;

	// 2. input data 얻어오기
	string sourceID = target_input->GetSource();
	unsigned int offset = target_input->GetOffset();

	// 3. data set 구성
	for (int i = 0; i < m_Count * 3; ++i) {
		dst.push_back(m_Primitive[i * m_Inputs.size() + offset]);
	}
}

DaeType::JointsElement::JointsElement(const TiXmlElement* const jointsElement) :
	m_Inputs()
{
	const TiXmlElement* input = jointsElement->FirstChildElement("input");

	while (true) {
		if (nullptr == input) break;
		if ("input"s != string(input->Value())) break;

		m_Inputs.emplace_back(input);

		input = input->NextSiblingElement();
	}

}

DaeType::JointsElement::~JointsElement()
{
}

void DaeType::JointsElement::PrintData() const
{
	cout << "Joints Info: \n";
	for (const auto& input : m_Inputs)
		input.PrintData();
}

DaeType::VertexWeightsElement::VertexWeightsElement(const TiXmlElement* const vertexWightElement) :
	m_Count(stoul(string(vertexWightElement->Attribute("count")))),
	m_Inputs(),
	m_Vcount(),
	m_V()
{
	const TiXmlElement* input = vertexWightElement->FirstChildElement("input");

	while (true) {
		if (nullptr == input) break;
		if ("input"s != string(input->Value())) break;

		m_Inputs.emplace_back(input);

		input = input->NextSiblingElement();
	}

	stringstream ss{ };
	unsigned int value;
	
	const TiXmlElement* vcount = vertexWightElement->FirstChildElement("vcount");

	if (nullptr != vcount) {
		ss.clear();
		ss.str(string(vcount->GetText()));

		while (ss >> value)
			m_Vcount.push_back(value);
	}

	const TiXmlElement* v = vertexWightElement->FirstChildElement("v");

	if (nullptr != v) {
		ss.clear();
		ss.str(string(v->GetText()));

		while (ss >> value)
			m_V.push_back(value);
	}
}

DaeType::VertexWeightsElement::~VertexWeightsElement()
{
}

void DaeType::VertexWeightsElement::PrintData() const
{
	cout << "Vertex Weights Info: \n";
	cout << "Count: " << m_Count << endl;
	
	for (const auto& input : m_Inputs)
		input.PrintData();

	cout << "<vcount> : \n";
	for (auto i : m_Vcount)
		cout << i << " ";
	cout << endl;

	cout << "<v> : \n";
	for (auto i : m_V)
		cout << i << " ";
	cout << endl;
}

const DaeType::InputElement* DaeType::VertexWeightsElement::GetInput(const Semantic& semantic) const
{
	for (auto& input : m_Inputs) {
		if (semantic == input.GetSemantic()) return &input;
	}
	return nullptr;
}

void DaeType::VertexWeightsElement::GetIndexSet(const Semantic& semantic, vector<unsigned int>& dst)
{
	DaeType::InputElement* target_input{ nullptr };

	// 1.
	for (auto& input : m_Inputs) {
		if (input.GetSemantic() == semantic)
			target_input = &input;
	}

	if (nullptr == target_input) return;

	// 2. input data 얻어오기
	string sourceID = target_input->GetSource();
	unsigned int offset = target_input->GetOffset();

	// 3. data set 구성
	int v_idx = 0;
	for (int vertex_idx = 0; vertex_idx < m_Count; ++vertex_idx) {
		
		int bone_count = m_Vcount[vertex_idx];

		for (int bone_idx = 0; bone_idx < 4; ++bone_idx) { // 4개의 가중치까지 지원함
			if (bone_idx >= bone_count) {
				dst.push_back(0);
				continue;
			}

			int mapped_idx = v_idx * m_Inputs.size() + offset;
			
			dst.push_back(m_V[mapped_idx]);

			++v_idx;
		}
	}
}


DaeType::MeshElement::MeshElement(const TiXmlElement* const meshElement) :
	m_Sources(),
	m_Vertices(meshElement->FirstChildElement("vertices")),
	m_Triangles(meshElement->FirstChildElement("triangles"))
{
	const TiXmlElement* source = meshElement->FirstChildElement("source");

	while (true) {
		if (nullptr == source) break;
		if ("source"s != string(source->Value())) break;

		m_Sources.emplace_back(source);

		source = source->NextSiblingElement();
	}
}

DaeType::MeshElement::~MeshElement()
{
}

void DaeType::MeshElement::PrintData()
{
	cout << "Mesh Info: " << endl;
	for (auto& source : m_Sources) {
		source.PrintData();
	}
	m_Vertices.PrintData();
	m_Triangles.PrintData();
}

void DaeType::MeshElement::GetSourceData(const Semantic& semantic, vector<float>& dst, bool raw)
{
	// 1. semantic에 맞는 인풋을 찾는다.
	const InputElement* target_input{ nullptr };

	if (nullptr == target_input) {
		target_input = m_Vertices.GetInput(semantic);
	}

	if (nullptr == target_input) {
		target_input = m_Triangles.GetInput(semantic);
	}

	// 2. target input에 맞는 source를 찾는다.
	const SourceElement* target_source{ nullptr };
	string ref_source = target_input->GetSource();			// # + source_id
	string target_source_id{ ref_source.begin() + 1, ref_source.end() };

	for (const auto& source : m_Sources) {
		if (target_source_id == source.GetSourceID())
			target_source = &source;
	}

	if (nullptr == target_source) return;

	if (raw) {
		int size = target_source->GetCount();
		float* src = target_source->GetArrayPointer();
		dst.resize(size);

		memcpy(dst.data(), src, sizeof(float) * size);
		return;
	}

	// 3. data 구성
	vector<unsigned int> index_buffer;
	m_Triangles.GetIndexSet(semantic, index_buffer);

	if (index_buffer.empty()) {
		float* arr = target_source->GetArrayPointer();
		int count = target_source->GetCount();
		dst.reserve(count);

		for (int i = 0; i < count; ++i) {
			dst.push_back(arr[i]);
		}
	}
	else { // triangle의 인덱스 값을 참조함
		float* arr = target_source->GetArrayPointer();
		int count = index_buffer.size();

		for (int i = 0; i < count; ++i) {
			dst.push_back(arr[index_buffer[i] * 3 + (i % 3)]);
		}
	}
}

void DaeType::MeshElement::GetIndexData(const Semantic& semantic, vector<unsigned int>& dst)
{
	// verticies인 경우 nullptr이므로 처리하지 않는다.
	m_Triangles.GetIndexSet(semantic, dst);
}

void DaeType::MeshElement::GetIndexData(vector<unsigned int>& dst)
{
	auto buf_size = m_Triangles.m_Primitive.size();
	dst.resize(buf_size);

	memcpy(dst.data(), m_Triangles.m_Primitive.data(), sizeof(unsigned int) * buf_size);
}

DaeType::SkinElement::SkinElement(const TiXmlElement* const skinElement) : 
	m_SourceID(skinElement->Attribute("source")),
	m_BindShapeMatrix(0),
	m_Sources(),
	m_Joints(skinElement->FirstChildElement("joints")),
	m_VertexWeight(skinElement->FirstChildElement("vertex_weights"))
{
	// 3개 있을 걸? 아마?
	const TiXmlElement* source = skinElement->FirstChildElement("source");

	while (true) {
		if (nullptr == source) break;
		if ("source"s != string(source->Value())) break;

		m_Sources.emplace_back(source);

		source = source->NextSiblingElement();
	}
}

DaeType::SkinElement::~SkinElement()
{
}

void DaeType::SkinElement::PrintData()
{
	cout << "Skin Info: " << endl;
	for (auto& source : m_Sources) {
		source.PrintData();
	}
	m_Joints.PrintData();
	m_VertexWeight.PrintData();
}

void DaeType::SkinElement::GetWeightData(vector<float>& dst)
{
	// 1. input 찾기
	const InputElement* target_input = m_VertexWeight.GetInput(Semantic::WEIGHT());

	// 2. source 찾기
	const SourceElement* target_source{ nullptr };
	string ref_source = target_input->GetSource();
	string target_source_id{ ref_source.begin() + 1, ref_source.end() };

	for (const auto& source : m_Sources) {
		if (target_source_id == source.GetSourceID())
			target_source = &source;
	}

	if (nullptr == target_source) return;

	// 3. data 구성
	float* sources = target_source->GetArrayPointer();
	int offset = target_input->GetOffset();
	int count = m_VertexWeight.m_Count;


	int v_idx = 0;
	for (int vertex_idx = 0; vertex_idx < count; ++vertex_idx) {

		int bone_count = m_VertexWeight.m_Vcount[vertex_idx];

		for (int bone_idx = 0; bone_idx < 4; ++bone_idx) { // 4개의 가중치까지 지원함
			if (bone_idx >= bone_count) {
				dst.push_back(0);
				continue;
			}

			int mapped_idx = v_idx * m_VertexWeight.m_Inputs.size() + offset;
			float value = sources[m_VertexWeight.m_V[mapped_idx]];
			
			dst.push_back(value);

			++v_idx;
		}
	}

	// cout << "Weight Info\n";
	// for (auto& n : dst) {
	// 	cout << n << " ";
	// } cout << endl;
}

void DaeType::SkinElement::GetBoneData(vector<int>& dst)
{
	// source를 참조할 필요 없음 아마도...?ㅎㅎ
	const InputElement* target_input = m_VertexWeight.GetInput(Semantic::JOINT());

	int offset = target_input->GetOffset();
	int count = m_VertexWeight.m_Count;

	int v_idx = 0;
	for (int vertex_idx = 0; vertex_idx < count; ++vertex_idx) {

		int bone_count = m_VertexWeight.m_Vcount[vertex_idx];

		for (int bone_idx = 0; bone_idx < 4; ++bone_idx) { // 4개의 가중치까지 지원함
			if (bone_idx >= bone_count) {
				dst.push_back(0);
				continue;
			}

			int mapped_idx = v_idx * m_VertexWeight.m_Inputs.size() + offset;
			int value = m_VertexWeight.m_V[mapped_idx];

			dst.push_back(value);

			++v_idx;
		}
	}

	// cout << "Bone Info\n";
	// for (auto& n : dst) {
	// 	cout << n << " ";
	// } cout << endl;
}

void DaeType::SkinElement::GetJointNameData(vector<string>& dst)
{
	// const InputElement* target_input = m_VertexWeight.GetInput(Semantic::JOINT());
	// string source{ target_input->GetSource() };
	// source.erase(source.begin());
	// 
	// for (auto& src : m_Sources) {
	// 	if (source == src.GetSourceID()) {
	// 		
	// 	}
	// }
}


DaeType::AnimationElement::AnimationElement(const TiXmlElement* const animationElement) :
	m_Sources(),
	m_Sampler(animationElement->FirstChildElement("sampler")),
	m_Channel(animationElement->FirstChildElement("channel"))
{
	const TiXmlElement* source = animationElement->FirstChildElement("source");

	while (true) {
		if (nullptr == source) break;
		if ("source"s != string(source->Value())) break;

		m_Sources.emplace_back(source);

		source = source->NextSiblingElement();
	}
}

DaeType::AnimationElement::~AnimationElement()
{
}

void DaeType::AnimationElement::PrintData() const
{
	cout << "Animation Info: \n";
	for (auto& source : m_Sources) {
		source.PrintData();
	}
	
	m_Sampler.PrintData();
	m_Channel.PrintData();
}

void DaeType::AnimationElement::GetSourceData(const Semantic& semantic, vector<float>& dst)
{
	InputElement* target_input{ nullptr };

	for (auto& input : m_Sampler.m_Inputs) {
		if (semantic == input.GetSemantic()) {
			target_input = &input;
		}
	}
	
	if (nullptr == target_input) return;

	SourceElement* target_source{ nullptr };
	string ref_source{ target_input->GetSource() };
	string source_id{ ref_source.begin() + 1, ref_source.end() };
	
	for (auto& source : m_Sources) {
		if (source_id == source.GetSourceID()) {
			target_source = &source;
		}
	}

	if (nullptr == target_source) return;

	float* source{ target_source->GetArrayPointer() };
	int count = target_source->GetCount();

	for (int i = 0; i < count; ++i) {
		dst.push_back(source[i]);
	}
}

DaeType::ChannelElement::ChannelElement(const TiXmlElement* const channelElement) :
	m_Source(channelElement->Attribute("source")),
	m_Target(channelElement->Attribute("target"))
{
}

DaeType::ChannelElement::~ChannelElement()
{
}

void DaeType::ChannelElement::PrintData() const
{
	cout << "Channel Info: \n";
	cout << "Source: " << m_Source << endl;
	cout << "Target: " << m_Target << endl;
}

DaeType::SamplerElement::SamplerElement(const TiXmlElement* const samplerElement) :
	m_Inputs()
{
	const TiXmlElement* input = samplerElement->FirstChildElement("input");

	while (true) {
		if (nullptr == input) break;
		if ("input"s != string(input->Value())) break;

		m_Inputs.emplace_back(input);

		input = input->NextSiblingElement();
	}
}

DaeType::SamplerElement::~SamplerElement()
{
}

void DaeType::SamplerElement::PrintData() const
{
	cout << "Sampler Info: \n";
	for (auto& input : m_Inputs) {
		input.PrintData();
	}
}

DaeType::Animation::Animation(const TiXmlElement* const animationElement)
{
	const TiXmlElement* ani = animationElement->FirstChildElement("animation");

	while (true) {
		if (nullptr == ani) break;
		if ("animation"s != string(ani->Value())) break;

		string name{ ani->Attribute("name") };
		cout << name << endl;

		m_BoneAnimation.try_emplace(name);
		m_BoneAnimation[name].emplace_back(ani);

		ani = ani->NextSiblingElement();
	}
}

void DaeType::Animation::PrintData()
{
	for (auto& [name, anims] : m_BoneAnimation) {
		for (auto& ani : anims) {

			ani.PrintData();
		}
	}
}

typedef bool KeyFrame;

void DaeType::Animation::Test()
{

	vector<float> test;
	//for (auto& [name, anims] : m_BoneAnimation) {
	//	for (auto& ani : anims) {
	//		// Input 정보는 모두 같음
	//		// 각 Animation 정보에서 output 정보를 빼와라

	//		ani.GetSourceData(Semantic::OUTPUT(), test);
	//		// ani.PrintData();
	//	}
	//}

	// 뼈 별로 묶인 곳은 여기니까 여기서 나눠서 처리해아함

	const int q = 1;
	KeyFrame key_frames[q];

	for (auto ani : m_BoneAnimation["Armature_Bone"s]) {
		ani.GetSourceData(Semantic::OUTPUT(), test);

		// location xyz
		// rotation xyz
		// scale xyz
		
		for (int i = 0; i < q; ++i) {
			// key_frames[i].location.x = test[i];
			// ㅋㅋ..
		}
	}
	

	for (auto& d : test)
		cout << d << " ";
	cout << endl;
}
