#pragma once
#include <vector>
#include <string>
#include <fstream>
#include <DirectXMath.h>


// 이건 버텍스에 있음
enum class VertexType : char { STATIC, SKINNED };
struct Vertex abstract { };
struct StaticVertex : public Vertex
{
	DirectX::XMFLOAT3 pos;
	DirectX::XMFLOAT3 nor;
	DirectX::XMFLOAT2 tex;

	bool operator==(const StaticVertex& rhs) {
		if (pos.x == rhs.pos.x && pos.y == rhs.pos.y && pos.z == rhs.pos.z &&
			nor.x == rhs.nor.x && nor.y == rhs.nor.y && nor.z == rhs.nor.z &&
			tex.x == rhs.tex.x && tex.y == rhs.tex.y) return true;
		return false;
	}
};
struct SkinnedVertex : public Vertex
{
	DirectX::XMFLOAT3 pos;
	DirectX::XMFLOAT3 nor;
	DirectX::XMFLOAT2 tex;
	DirectX::XMFLOAT3 weights;
	unsigned short boneIdx[4];
	
	bool operator==(const SkinnedVertex& rhs) {
		if (pos.x == rhs.pos.x && pos.y == rhs.pos.y && pos.z == rhs.pos.z &&
			nor.x == rhs.nor.x && nor.y == rhs.nor.y && nor.z == rhs.nor.z &&
			tex.x == rhs.tex.x && tex.y == rhs.tex.y) return true;
		return false;
	}
};

using Index = unsigned short;

struct Bone
{
	int						index;
	int						parentIndex;
	DirectX::XMFLOAT4X4		childToParent;
	DirectX::XMFLOAT4X4		offsetMat;
};

struct Keyframe
{
	float time;
	DirectX::XMFLOAT3 translation;
	DirectX::XMFLOAT4 rotation;
	DirectX::XMFLOAT3 scale;
};

struct AniInfo
{
	char			name[10];
	unsigned short	num_keys;
};

struct D2bBuf
{
public:
	// 메소드

	void LoadD2b(const std::string& filename)
	{
		std::ifstream in{ filename, std::ios_base::binary };
		assert(in);

		in.read(reinterpret_cast<char*>(&header), sizeof(header));
		AllocBuf();

		in.read(reinterpret_cast<char*>(v_buf), GetVertexBytes() * header.v_count);
		in.read(reinterpret_cast<char*>(i_buf), sizeof(Index) * header.i_count);

		if (header.rigged) {
			in.read(reinterpret_cast<char*>(b_buf), sizeof(Bone) * header.b_count);
			in.read(reinterpret_cast<char*>(ani_buf), sizeof(AniInfo) * header.ani_count);
			in.read(reinterpret_cast<char*>(key_buf), sizeof(Keyframe) * header.key_count);
		}

		in.close();
	}

	void AllocBuf() {
		switch (header.v_type) {
		case VertexType::STATIC:
			v_buf = new StaticVertex[header.v_count];
			break;
		case VertexType::SKINNED:
			v_buf = new SkinnedVertex[header.v_count];
			break;
		default: break;
		}

		i_buf = new Index[header.i_count];

		if (header.rigged) {
			b_buf = new Bone[header.b_count];
			ani_buf = new AniInfo[header.ani_count];
			key_buf = new Keyframe[header.key_count];
		}
	}

public:
	int GetVertexBytes()
	{
		switch (header.v_type) {
		case VertexType::STATIC:	return sizeof(StaticVertex);
		case VertexType::SKINNED:	return sizeof(SkinnedVertex);
		default:					return 0;
		}
	}

public:
#pragma pack(push, 1)
	struct Header {
		VertexType v_type;
		bool rigged;

		int v_count;
		int i_count;
		int b_count;
		int ani_count;
		int key_count;
	};
#pragma pack(pop)

	Header header;

	// geo info
	Vertex* v_buf;
	Index* i_buf;

	// ani info
	Bone* b_buf;
	AniInfo* ani_buf;
	Keyframe* key_buf;			// (시그마 (key_counts)) * b_count 개수만큼 있음
};
