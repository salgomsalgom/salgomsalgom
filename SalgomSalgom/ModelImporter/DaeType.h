	#pragma once
#include <vector>
#include <map>
#include "Semantic.h"

class TiXmlElement;

namespace DaeType {
	using namespace std;

	class SourceElement
	{
	public:
		explicit SourceElement(const TiXmlElement* const sourceElement);
		explicit SourceElement(const SourceElement& other);
		explicit SourceElement(SourceElement&& other) noexcept;
		~SourceElement();

	public:
		void PrintData() const;

		string	GetSourceID()		const { return m_SourceID; }
		int		GetCount()			const { return m_ArrayCount; }
		float*	GetArrayPointer()	const { return m_Array; }

	private:
		string		m_SourceID;
		int			m_ArrayCount;
		float*		m_Array;
	};

	class InputElement {
	public:
		explicit InputElement(const TiXmlElement* const inputElement);

	public:

		const Semantic GetSemantic() const { return (const Semantic)m_Semantic; }
		const string GetSource() const { return (const string)m_Source; }
		const unsigned int GetOffset() const { return (const unsigned int)m_Offset; }

	public:
		void PrintData() const;

	private:
		Semantic		m_Semantic;
		string			m_Source;
		unsigned int	m_Offset;
		unsigned int	m_Set;
	};

//-----------------------------------------------------------------------------

	class VerticesElement {
	public:
		explicit VerticesElement(const TiXmlElement* const verticesElement);
		~VerticesElement();

	public:
		void PrintData();
		const InputElement* GetInput(const Semantic& semantic) const;
		// void GetIndexSet(const Semantic& semantic);

	private:
		InputElement m_Input;
	};

	class TrianglesElement {
	public:
		explicit TrianglesElement(const TiXmlElement* const trianglesElement);
		~TrianglesElement();

	public:
		void PrintData();
		const InputElement* GetInput(const Semantic& semantic) const;

		void GetIndexSet(const Semantic& semantic, vector<unsigned int>& dst);

	public:
		int m_Count;
		std::vector<InputElement> m_Inputs;
		std::vector<unsigned int> m_Primitive;
	};

	class JointsElement {
	public:
		JointsElement(const TiXmlElement* const jointsElement);
		~JointsElement();

	public:


	public:	// for debug
		void PrintData() const;

	private:
		// JOINT INV_BIND_MATRIX: 뼈와 그 행렬 (2개)
		vector<InputElement> m_Inputs;
	};

	class VertexWeightsElement {
	public:
		VertexWeightsElement(const TiXmlElement* const vertexWightElement);
		~VertexWeightsElement();


	public:	// for debug
		void PrintData() const;

	public:
		const InputElement* GetInput(const Semantic& semantic) const;
		void GetIndexSet(const Semantic& semantic, vector<unsigned int>& dst);

	public:
		unsigned int m_Count; // vertex count

		// JOINT WEIGHT: 뼈와 가중치 (점을 위한 정보)
		vector<InputElement> m_Inputs;
		vector<unsigned int> m_Vcount;		// 각각의 정점이 몇개의 뼈와 연관되어 있는 지
		vector<int> m_V;
	};

	// 애니메이션
	class SamplerElement {
	public:
		SamplerElement(const TiXmlElement* const samplerElement);
		~SamplerElement();

	public:
		void PrintData() const;

	public:
		vector<InputElement> m_Inputs;
	};

	class ChannelElement {
	public:
		ChannelElement(const TiXmlElement* const channelElement);
		~ChannelElement();

	public:
		void PrintData() const;

	public:
		std::string m_Source;
		std::string m_Target;
	};

//-----------------------------------------------------------------------------

	class MeshElement {
	public:
		MeshElement(const TiXmlElement* const meshElement);
		~MeshElement();

	public:
		void PrintData();

	public:
		void GetSourceData(const Semantic& semantic, vector<float>& dst, bool raw = false);

		void GetIndexData(vector<unsigned int>& dst);
		void GetIndexData(const Semantic& semantic, vector<unsigned int>& dst);

		int GetNumTriangles() {
			return m_Triangles.m_Count;
		}

	private:
		std::vector<SourceElement> m_Sources;
		VerticesElement m_Vertices;
		TrianglesElement m_Triangles;	/* 삼각형 프리미티브만 지원함 */
	};

	class SkinElement {
	public:
		SkinElement(const TiXmlElement* const skinElement);
		~SkinElement();

	public:
		void PrintData();
		
		void GetWeightData(vector<float>& dst);
		void GetBoneData(vector<int>& dst);

		void GetJointNameData(vector<string>& dst);

	private:
		string m_SourceID;

		int							m_BindShapeMatrix;		// mesh의 transform

		std::vector<SourceElement>	m_Sources;
		JointsElement				m_Joints;
		VertexWeightsElement		m_VertexWeight;			// 얘가 인덱스 정보를 주면
	};


	// 해놓고 보면 별거 아닐걸!!
	class AnimationElement {
	public:
		AnimationElement(const TiXmlElement* const animationElement);
		~AnimationElement();

	public:
		void PrintData() const;

	public:
		void GetSourceData(const Semantic& semantic, vector<float>& dst);

	private:
		std::vector<SourceElement> m_Sources;
		SamplerElement m_Sampler;
		ChannelElement m_Channel;
	};

	class Animation {
	public:
		Animation(const TiXmlElement* const animationElement);

		void PrintData();

		void Test();

	private:
		map<string, vector<AnimationElement>> m_BoneAnimation;
	};
}

