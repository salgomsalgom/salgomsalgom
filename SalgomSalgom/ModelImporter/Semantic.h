#pragma once
#include <string>

using namespace std;

constexpr unsigned int SEMANTIC_COUNT = 23;

static const char* SEMANTIC_NAME_SET[SEMANTIC_COUNT] = {
	"BINARMAL",
	"COLOR",
	"CONTINUITY",
	"IMAGE",
	"INPUT",

	"IN_TANGENT",
	"INTERPOLATION",
	"INV_BIND_MATRIX",
	"JOINT",
	"LINEAR_STEPS",

	"MORPH_TARGET",
	"MORPH_WEIGHT",
	"NORMAL",
	"OUTPUT",
	"OUT_TANGENT",

	"POSITION",
	"TANGENT",
	"TEXBINORMAL",
	"TEXCOORD",
	"TEXTANGENT",

	"UV",
	"VERTEX",
	"WEIGHT",
};

class Semantic {
private:
	explicit Semantic(int index) 
	{
		m_SemanticName = SEMANTIC_NAME_SET[index];
	}

public:
	explicit Semantic( void ) 
	{
		m_SemanticName = nullptr;
	}

	explicit Semantic(const Semantic& other)
	{
		m_SemanticName = other.m_SemanticName;
	}

	~Semantic() { }

public:
	static Semantic Generate(const std::string& semanticName) {
		for (int i = 0; i < SEMANTIC_COUNT; ++i) {
			if (string(SEMANTIC_NAME_SET[i]) == semanticName) {
				return Semantic(i);
			}
		}

		// return Semantic();
	}

	static const Semantic BINORMAL()			{ return Semantic(0); }
	static const Semantic COLOR()				{ return Semantic(1); }
	static const Semantic CONTINUITY()			{ return Semantic(2); }
	static const Semantic IMAGE()				{ return Semantic(3); }
	static const Semantic INPUT()				{ return Semantic(4); }
	static const Semantic IN_TANGENT()			{ return Semantic(5); }
	static const Semantic INTERPOLATION()		{ return Semantic(6); }
	static const Semantic INV_BIND_MATRIX()		{ return Semantic(7); }
	static const Semantic JOINT()				{ return Semantic(8); }
	static const Semantic LINEAR_STEPS()		{ return Semantic(9); }
	static const Semantic MORPH_TARGET()		{ return Semantic(10); }
	static const Semantic MORPH_WEIGHT()		{ return Semantic(11); }
	static const Semantic NORMAL()				{ return Semantic(12); }
	static const Semantic OUTPUT()				{ return Semantic(13); }
	static const Semantic OUT_TANGENT()			{ return Semantic(14); }
	static const Semantic POSITION()			{ return Semantic(15); }
	static const Semantic TANGENT()				{ return Semantic(16); }
	static const Semantic TEXBINORMAL()			{ return Semantic(17); }
	static const Semantic TEXCOORD()			{ return Semantic(18); }
	static const Semantic TEXTANGENT()			{ return Semantic(19); }
	static const Semantic UV()					{ return Semantic(20); }
	static const Semantic VERTEX()				{ return Semantic(21); }
	static const Semantic WEIGHT()				{ return Semantic(22); }

public:
	const char* GetValue() const { return m_SemanticName; }

public:
	bool operator==(const Semantic& rhs) {
		return m_SemanticName == rhs.m_SemanticName;
	}
	bool operator==(const Semantic& rhs) const {
		return m_SemanticName == rhs.m_SemanticName;
	}

private:
	const char* m_SemanticName;
};
