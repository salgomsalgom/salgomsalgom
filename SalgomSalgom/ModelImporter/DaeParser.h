#pragma once

#include "DataType.h"
#include <vector>
#include <DirectXMath.h>

class TiXmlDocument;
class TiXmlElement;


struct DaeBone
{
	char					name[10];
	int						index;
	int						parentIndex;
	DirectX::XMFLOAT4X4		offsetMat;
};

class DaeParser
{
public:
	DaeParser();
	~DaeParser();

public:
	void Parse(const string& filename);
	void ParseAnifile(const string& filename);
	
public:
	void GenerateD2b(D2bBuf& dst);
	void GenerateD2bNoNormalCalc(D2bBuf& dst);

private:
	void ParseLibraryGeometries(const TiXmlElement* const library);
	void ParseLibraryControllers(const TiXmlElement* const library);
	void ParseLibraryVisualScenes(const TiXmlElement* const library);

private:
	void StoreChildBones(const TiXmlElement* parant);

private:
	bool rigged;

	DaeType::MeshElement* m_Mesh;
	DaeType::SkinElement* m_Skin;

	std::vector<DaeBone> m_BoneInfo;

	vector<AniInfo> m_AniInfo;
	map<string, DirectX::XMFLOAT4X4> m_ChildToParentMat;
	vector<pair<string, vector<pair<string, vector<Keyframe>>>>> m_AnifileData;

public:
	vector<pair<string, string>>				m_BoneNameHierarchy;
	std::map<string, int>						m_BoneIndexMapping;
	std::map<string, DirectX::XMFLOAT4X4>		m_BoneInvBindMatrixMapping;

};
