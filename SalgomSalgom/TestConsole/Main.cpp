#include "pch.h"

#include <iostream>
using namespace DirectX;
using namespace std;

#pragma region SceneTest
enum class PSOType {
	SKYBOX,
	TERRAIN,
	STATIC_OBJECT,
	PLAYER,
};

class GraphicsObject abstract
{
protected:
	GraphicsObject() { };
	virtual ~GraphicsObject() { };

public:
	virtual PSOType GetPSOType() = 0;

protected:
	XMFLOAT3 m_Translation;
	XMFLOAT4 m_Rotation;
	XMFLOAT3 m_Scale;

	XMFLOAT4X4 m_WorldTransform;
};

class SkyBox : public GraphicsObject {
public:
	SkyBox() { };
	~SkyBox() { };

	void SetLocation(const XMFLOAT3& location);

	PSOType GetPSOType() override;

private:

};

class Terrain : public GraphicsObject {
public:
	Terrain() { }
	~Terrain() { }

	PSOType GetPSOType() override;
private:

};

void SkyBox::SetLocation(const XMFLOAT3& location)
{
	m_Translation = location;
}

PSOType SkyBox::GetPSOType()
{
	return PSOType::SKYBOX;
}

PSOType Terrain::GetPSOType()
{
	return PSOType::TERRAIN;
}

enum class SceneKey : unsigned short {
	TEST
};

class Scene
{
public:
	explicit Scene(SceneKey key);
	virtual ~Scene();

public:
	void AddObject(GraphicsObject* obj);

public:
	SceneKey m_Key;
	std::map<PSOType, std::vector<GraphicsObject*>> m_Objects;

};

Scene::Scene(SceneKey key) : m_Key(key)
{


}

Scene::~Scene() {

}

void Scene::AddObject(GraphicsObject* obj)
{
	auto pso = obj->GetPSOType();
	auto iter = m_Objects.find(pso);

	if (m_Objects.end() == iter)
		m_Objects.emplace(pso, std::vector<GraphicsObject*>({ obj }));
	else iter->second.push_back(obj);
}
#pragma endregion SceneTest


#include "Model.h"

int main() {
	Model model;
	LoadD2bModel("PKN1.d2b"s, model);


}


