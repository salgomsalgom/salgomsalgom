#pragma once
#include "Vertex.h"

typedef unsigned int NumberOf;
typedef unsigned short Index;

// public 멤버 변수는 카멜 표기법

class KeyFrame 
{
	float time;
	DirectX::XMFLOAT3 translate;
	DirectX::XMFLOAT3 scale;
	DirectX::XMFLOAT4 rotate;
};

class BoneAnimation 
{
	std::vector<KeyFrame> m_KeyFrames;
};

class AnimationClip 
{
	std::string m_ClipName;
	unsigned short m_NumKeyframes;
	std::map<int, BoneAnimation> m_BoneAnimations;
	// 맵의 키는 뼈의 인덱스
};

struct Bone
{
	int						index;
	int						parentIndex;
	DirectX::XMFLOAT4X4		offsetMat;
};

enum class VertexType {
	DEFULAT,		// static 오브젝트를 그리기 위한 버텍스 타입
	SKINNED,		// 애니메이션 오브젝트를 그리기 위한 버텍스 타입
};

class Model
{
public:
	struct Header {
		VertexType v_type;
		NumberOf m_Vertices;
		NumberOf m_Triangles;
		NumberOf m_Bones;
		NumberOf m_AnimationClips;
	};

	Header m_Header;

public:
	void ReserveBuffer() {
		// 버텍스 타입에 따라 다른 버텍스를 할당해준다.
		if (VertexType::SKINNED == m_Header.v_type) {
			vertices = new SkinnedVertex[m_Header.m_Vertices];
		}

		indices.resize(m_Header.m_Triangles * 3);

		bones.resize(m_Header.m_Bones);

		animNameSizes.resize(m_Header.m_AnimationClips);
		animationClips.resize(m_Header.m_AnimationClips);
	}

	char* GetHeaderPtr()
	{ return reinterpret_cast<char*>(&m_Header); }
	
	const unsigned int GetHeaderBytes() 
	{ return sizeof(Header); }

	char* GetVerticesPtr()
	{ return reinterpret_cast<char*>(vertices); }

	const unsigned int GetVerticesBytes()
	{ 
		unsigned int v_size = 0;

		if (VertexType::SKINNED == m_Header.v_type)
			v_size = sizeof(SkinnedVertex);

		return m_Header.m_Vertices * v_size;
	}

	char* GetIndicesPtr()
	{ return reinterpret_cast<char*>(indices.data()); }

	const unsigned int GetIndicesBytes()
	{ return m_Header.m_Triangles * 3 * sizeof(Index); }

	char* GetBonesPtr()
	{ return reinterpret_cast<char*>(bones.data()); }

	const unsigned int GetBonesBytes()
	{ return m_Header.m_Bones * sizeof(Bone); }

	char* GetAnimNameSizesPtr()
	{ return reinterpret_cast<char*>(animNameSizes.data()); }

	const unsigned int GetAnimNameSizesBytes()
	{ return m_Header.m_AnimationClips * sizeof(unsigned short); }

public:
	Vertex*						vertices;
	std::vector<Index>			indices;

	std::vector<Bone>			bones;

	std::vector<unsigned short>	animNameSizes;
	std::vector<AnimationClip>	animationClips;

};

void LoadD2bModel(const std::string& filename, Model& dst);
