#pragma once
using namespace DirectX;

// 근본적인 물음: 왜 버텍스를 만드려는거야?
// DirectX에서는 Vertex Format을 마음대로 지정할 수 있다.
// 즉 dx의 SetVertexBuffer할 때 여기에 들어가는 데이터 구조체
// 정점 구조체를 정의한 다음 DirectX에게 input layout description 형식으로 알려줘야 한다.


// 이걸 상속받는 Vertex 클래스들을 만들까?
// 이거를 할 이유가 있나? 좀 더 고민해보기

// 모델 파일에서 어떤 Vertex를 저장할 지 모르니까
// 상속을 통해 포인터로 관리할 필요가 있다.

enum class VetexType {

};

struct Vertex abstract
{
protected:
	explicit Vertex() = default;
	virtual ~Vertex() = default;
};

struct PosTex : public Vertex {
	PosTex() : m_Position(), m_Texcoord() { }
	~PosTex() { }

	static D3D12_INPUT_LAYOUT_DESC GetInputLayoutDesc();

	XMFLOAT3 m_Position;
	XMFLOAT2 m_Texcoord;
};

struct PosNor : public Vertex {
	static D3D12_INPUT_LAYOUT_DESC GetInputLayoutDesc();

	XMFLOAT3 m_Postion;
	XMFLOAT3 m_Normal;
};

struct SkinnedVertex : public Vertex {
	static D3D12_INPUT_LAYOUT_DESC GetInputLayoutDesc();

	XMFLOAT3 position;
	XMFLOAT3 normal;
	XMFLOAT3 weight;
	unsigned short boneIndices[4];
};
