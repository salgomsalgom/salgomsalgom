#include "pch.h"
#include "Model.h"
#include <fstream>

/*

우선 내가 하고 싶은 것을 명확하게 적어두자.

모델은 D2b 파일에서 얻어올 수 있는 모든 정보를 담는 구조체이다.

d2b 파일에 모델 데이터에는 크게 메쉬와 뼈, 애니메이션 클립 정보가 있다.

메쉬는 버텍스 버터와 인덱스 버퍼를 구성하는 정보이고,

뼈는 뼈 계층구조와 뼈의 offset 정보이고

애니메이션 클립은 각각 뼈들에 대한 키프레임 정보이다.

여기서 고려해야할 사항
1. 버텍스는 구성 형식이 다를 수 있으니 상속을 하거나 각각 다른 방식으로 로드해야한다.
2. 애니메이션이 없는 모델이라면 뼈와 애니메이션 클립이 없다.

*/

void LoadD2bModel(const std::string& filename, Model& dst)
{
	std::ifstream in{ filename, std::ios_base::binary };

	if (!in) {
		std::cout << filename << " - 파일을 열 수 없습니다." << std::endl;
		return;
	}

	in.read(dst.GetHeaderPtr(), dst.GetHeaderBytes());
	dst.ReserveBuffer();

	// 버텍스
	in.read(dst.GetVerticesPtr(), dst.GetVerticesBytes());

	// 인덱스
	in.read(dst.GetIndicesPtr(), dst.GetIndicesBytes());

	// 애니메이션이 있을 경우에만
	if (0 != dst.m_Header.m_AnimationClips) {
		// 뼈
		in.read(dst.GetBonesPtr(), dst.GetBonesBytes());

		// 애니메이션 이름 개수
		in.read(dst.GetAnimNameSizesPtr(), dst.GetAnimNameSizesBytes());

		for (int i = 0; i < dst.m_Header.m_AnimationClips; ++i) {
			int name_size = 0 * sizeof(char);
			std::string clip_name;
			clip_name.resize(name_size);
			unsigned short num_keyframes;

			// 이름 
			in.read(const_cast<char*>(clip_name.data()), name_size);

			// 키프레임 개수
			in.read(reinterpret_cast<char*>(&num_keyframes), sizeof(num_keyframes));

			int clip_size = (sizeof(short) + sizeof(KeyFrame) * num_keyframes) * dst.m_Header.m_Bones;

			char* clip_buf = new char[clip_size];
			in.read(clip_buf, clip_size);

			// 여기서 클립 정보 분리해서 넣기
		}
	}

	std::cout << "파일 읽기 완료" << std::endl;
}
