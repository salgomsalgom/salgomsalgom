#include <vector>
#include <string>
#include <fstream>
#include <filesystem>
using namespace std;

typedef unsigned char BYTE;

bool MakeHeightMap(const string& filename) {
	string in_fn = filename + ".raw"s;
	string out_fn = filename + ".hm"s;

	ifstream in{ in_fn, ios_base::binary };
	if (!in) return false;

	int file_size = filesystem::file_size(in_fn);

	std::vector<BYTE> buf;
	buf.resize(file_size);
	in.read((char*)buf.data(), file_size);
	in.close();

	ofstream out{ out_fn, ios_base::binary };
	for (int i = 0; i < file_size; i += 3) {
		out << buf[i];
	}
	out.close();

	return true;
}

#include <iostream>
int main() {
	string fn;
	cout << "** [ .raw -> .hm ] Converter **" << endl;

	while (true) {
		cout << "파일 이름 입력(확장자 제외): ";
		cin >> fn;

		if (MakeHeightMap(fn)) cout << "convert success" << endl;
		else cout << "convert fail" << endl;
	}
}