#include <iostream>
#include <fstream>
#include <string>
#include <filesystem>
#include "DaeParser.h"

using namespace std;

void ExportD2b(const string& filename)
{
	DaeParser d;
	d.Parse(filename);
	d.ParseAnifile(filename);

	D2bBuf buf;
	d.GenerateD2b(buf);

	ofstream out{ filename + ".d2b", ios_base::binary };

	out.write((char*)&buf.header, sizeof(buf.header));
	if (nullptr != buf.v_buf) out.write((char*)buf.v_buf, buf.GetVertexBytes()* buf.header.v_count);
	if (nullptr != buf.i_buf) out.write((char*)buf.i_buf, sizeof(buf.i_buf[0]) * buf.header.i_count);
	if (nullptr != buf.b_buf) out.write((char*)buf.b_buf, sizeof(buf.b_buf[0]) * buf.header.b_count);
	if (nullptr != buf.ani_buf) out.write((char*)buf.ani_buf, sizeof(buf.ani_buf[0]) * buf.header.ani_count);
	if (nullptr != buf.key_buf) out.write((char*)buf.key_buf, sizeof(buf.key_buf[0]) * buf.header.key_count);

	out.close();

	cout << "< " << filename << " >\n" 
		 << "** export complete **\n"
		 << "v_count - " << buf.header.v_count << '\n'
		 << "i_count - " << buf.header.i_count << '\n'
		 << "b_count - " << buf.header.b_count << '\n'
		 << "ani_count - " << buf.header.ani_count << '\n'
		 << "key_count - " << buf.header.key_count << '\n'
		 << "*********************" << endl;
}

void ExportD2bNoNormalCalc(const string& filename)
{
	DaeParser d;
	d.Parse(filename);
	d.ParseAnifile(filename);

	D2bBuf buf;
	d.GenerateD2bNoNormalCalc(buf);

	ofstream out{ filename + ".d2b", ios_base::binary };

	out.write((char*)&buf.header, sizeof(buf.header));
	if (nullptr != buf.v_buf) out.write((char*)buf.v_buf, buf.GetVertexBytes() * buf.header.v_count);
	if (nullptr != buf.i_buf) out.write((char*)buf.i_buf, sizeof(buf.i_buf[0]) * buf.header.i_count);
	if (nullptr != buf.b_buf) out.write((char*)buf.b_buf, sizeof(buf.b_buf[0]) * buf.header.b_count);
	if (nullptr != buf.ani_buf) out.write((char*)buf.ani_buf, sizeof(buf.ani_buf[0]) * buf.header.ani_count);
	if (nullptr != buf.key_buf) out.write((char*)buf.key_buf, sizeof(buf.key_buf[0]) * buf.header.key_count);

	out.close();

	cout << "< " << filename << " >\n"
		<< "** export complete **\n"
		<< "v_count - " << buf.header.v_count << '\n'
		<< "i_count - " << buf.header.i_count << '\n'
		<< "b_count - " << buf.header.b_count << '\n'
		<< "ani_count - " << buf.header.ani_count << '\n'
		<< "key_count - " << buf.header.key_count << '\n'
		<< "*********************" << endl;
}

int main()
{
	vector<string> filenames;

	// 디렉토리에 있는 모든 파일 이름을 가져온다.
	// for (auto& fn : filesystem::recursive_directory_iterator("./DaeFiles/"))
	// {
	// 	string str_fn{ fn.path().filename().string() };
	// 	
	// 	string ext{ str_fn.end() - 4, str_fn.end() };
	// 	if (ext != ".dae") continue;
	// 	
	// 	string without_ex;
	// 	for (auto ch : str_fn)
	// 	{
	// 		if ('.' == ch) break;
	// 		without_ex.push_back(ch);
	// 	}
	// 
	// 	filenames.push_back(without_ex);
	// }
	// 
	// for (auto& fn : filenames)
	// {
	// 	cout << fn << endl;
	// 	ExportD2b("./DaeFiles/" + fn);
	// }

	// ExportD2bNoNormalCalc("./DaeFiles/ARCH");
	// ExportD2bNoNormalCalc("./DaeFiles/FENC");
	// ExportD2bNoNormalCalc("./DaeFiles/LTRE");
	// ExportD2bNoNormalCalc("./DaeFiles/BENC");
	// ExportD2bNoNormalCalc("./DaeFiles/MPED");


	// ExportD2b("./DaeFiles/STL1");
	// ExportD2b("./DaeFiles/STL2");
	// ExportD2b("./DaeFiles/ROK2");


	// 애니
	ExportD2b("./DaeFiles/NMGD");
	ExportD2b("./DaeFiles/WICH");

	//D2bBuf buf;
	//buf.LoadD2b("./DaeFiles/TREE.d2b");
//
	//cout << buf.header.key_count << endl;
	//for (int i = 0; i < buf.header.key_count; ++i)
	//{
	//	cout << buf.key_buf[i].time << endl;
	//}
}
